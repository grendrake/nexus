#!/bin/bash

# This script takes an array of seeds and produces three maps for each: a
# black & white template map, a greyscale heightmap, and a general-purpose
# coloured map in the equal-rectangular projection suitable for running
# through a program such as G.Projector

WIDTH=1600
HEIGHT=`expr $WIDTH / 2`
SEED=1642957

echo Doing $SEED
./planet -pq -w $WIDTH -h $HEIGHT -s 0.$SEED -C colours/topo.col -o out/${SEED}_topo.bmp
convert out/${i}_base.bmp out/${i}_base.png 2> /dev/null
