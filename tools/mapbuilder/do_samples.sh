#!/bin/bash

# This script is intended to create several map outlines to give a basic
# preview of what each random seed will create. The output is a black and
# white image with the black areas representing land areas and the white areas
# representing water.
#
# This output is then sent through the mapcheck program which will determine
# the proportion of the resulting map that is covered in water by percentage.
#
# This script is intended to get a generally suitable world with a
# hydrographic coverage matching a world with already generated data.

BASESEED=2568

for i in `seq 1 9`;
do
    echo
    echo "Doing 0."$BASESEED$i
    ./planet -pM -w 800 -h 399 -s 0.$BASESEED$i -C colours/landwater.col -o out/sil_$BASESEED$i.bmp
    ./mapcheck out/sil_$BASESEED$i.bmp
done
