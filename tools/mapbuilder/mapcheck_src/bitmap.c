#include <stdio.h>
#include <stdlib.h>

#include "bitmap.h"

bitmap_t* new_bitmap() {
    bitmap_t *bitmap = calloc(sizeof(bitmap), 1);
    bitmap->file = calloc(sizeof(file_header_t), 1);
    bitmap->info = calloc(sizeof(info_header_t), 1);
    return bitmap;
}
void free_bitmap(bitmap_t *bitmap) {
    free(bitmap->file);
    free(bitmap->info);
    free(bitmap->data);
    free(bitmap);
}


bitmap_t *load_bitmap(const char *filename)
{
    FILE *filePtr; //our file pointer
    bitmap_t *bitmap;


    bitmap = new_bitmap();

    //open filename in read binary mode
    filePtr = fopen(filename,"rb");
    if (filePtr == NULL) {
        fprintf(stderr, "Could not open file.\n");
        return NULL;
    }

    //read the bitmap file header
    fread(&bitmap->file->type, 2, 1, filePtr);
    fread(&bitmap->file->size, 4, 1, filePtr);
    fread(&bitmap->file->reserved_1, 2, 1, filePtr);
    fread(&bitmap->file->reserved_2, 2, 1, filePtr);
    fread(&bitmap->file->offset, 4, 1, filePtr);

    //verify that this is a bmp file by check bitmap id
    if (bitmap->file->type !=0x4D42) {
        fprintf(stderr, "Not valid BMP file.\n");
        free_bitmap(bitmap);
        fclose(filePtr);
        return NULL;
    }

    //read the bitmap info header
    fread(&bitmap->info->size, 4, 1, filePtr);
    fread(&bitmap->info->width, 4, 1, filePtr);
    fread(&bitmap->info->height, 4, 1, filePtr);
    fread(&bitmap->info->planes, 2, 1, filePtr);
    fread(&bitmap->info->bits_per_pixel, 2, 1, filePtr);
    fread(&bitmap->info->compression, 4, 1, filePtr);
    fread(&bitmap->info->size_of_bitmap, 4, 1, filePtr);
    fread(&bitmap->info->horz_resolution, 4, 1, filePtr);
    fread(&bitmap->info->vert_resolution, 4, 1, filePtr);
    fread(&bitmap->info->colours_used, 4, 1, filePtr);
    fread(&bitmap->info->colours_important, 4, 1, filePtr);

    if (bitmap->info->size_of_bitmap == 0) {
        bitmap->info->size_of_bitmap =
            bitmap->info->width * bitmap->info->height * (bitmap->info->bits_per_pixel/8);
    }


    //move file point to the begging of bitmap data
    fseek(filePtr, bitmap->file->offset, SEEK_SET);

    //allocate enough memory for the bitmap image data
    bitmap->data = (unsigned char*)malloc(bitmap->info->size_of_bitmap);

    //verify memory allocation
    if (!bitmap->data) {
        fprintf(stderr, "Memory allocation failure.\n");
        free_bitmap(bitmap);
        fclose(filePtr);
        return NULL;
    }

    //read in the bitmap image data
    fread(bitmap->data, bitmap->info->size_of_bitmap, 1, filePtr);

    //make sure bitmap image data was read
    if (bitmap->data == NULL) {
        fprintf(stderr, "Failed to read bitmap data.\n");
        free_bitmap(bitmap);
        fclose(filePtr);
        return NULL;
    }

    //swap the r and b values to get RGB (bitmap is BGR)
    for (unsigned imageIdx = 0; imageIdx < bitmap->info->size_of_bitmap; imageIdx += 3)
    {
        unsigned char tempRGB = bitmap->data[imageIdx];
        bitmap->data[imageIdx] = bitmap->data[imageIdx + 2];
        bitmap->data[imageIdx + 2] = tempRGB;
    }

    //close file and return bitmap iamge data
    fclose(filePtr);
    return bitmap;
}
