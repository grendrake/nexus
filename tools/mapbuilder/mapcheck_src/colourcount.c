#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bitmap.h"

#define BUCKETS 16

#define TO_CODE(r,g,b) ((r << 16) | (g << 8) | b)
#define TO_RED(code) ((code>>16) & 0xFF)
#define TO_GREEN(code) ((code>>8) & 0xFF)
#define TO_BLUE(code) ((code) & 0xFF)

typedef struct COLOR_DEF {
    int code;
    int count;

    struct COLOR_DEF *next;
} color_def_t;

typedef struct color_table {
    color_def_t *table[BUCKETS];
} color_table_t;

int inc_count(color_table_t *table, int code) {
    int hash = code % BUCKETS;

    if (table->table[hash] == 0) {
        color_def_t *def = calloc(sizeof(color_def_t), 1);
        def->code = code;
        def->count = 1;
        table->table[hash] = def;
    } else {
        color_def_t *cur = table->table[hash];
        while (cur && cur->code != code) {
            cur = cur->next;
        }
        if (cur) {
            ++cur->count;
        } else {
            color_def_t *def = calloc(sizeof(color_def_t), 1);
            def->code = code;
            def->count = 1;
            def->next = table->table[hash];
            table->table[hash] = def;
        }
    }
    return 1;
}


int main(int argc, char *argv[]) {
    color_table_t table;
    memset(&table, 0, sizeof(table));
    int pixel_count = 0;


    if (argc != 2) {
        printf("USAGE: %s <filename>\n", argv[0]);
        return 1;
    }

    bitmap_t *file = load_bitmap(argv[1]);
    if (!file) {
        printf("Could not load %s as bitmap\n", argv[1]);
        return 1;
    }

    for (unsigned i = 0; i < file->info->size_of_bitmap; i += 3) {
        if (file->data[i] != 3 && file->data[i+1] != 3 && file->data[i+2] != 3) {
            ++pixel_count;
            inc_count(&table, TO_CODE(file->data[i], file->data[i+1], file->data[i+2]));
        }
    }

    printf("Total Pixels: %d\n", pixel_count);
    for (int i = 0; i < BUCKETS; ++i) {
        color_def_t *cur = table.table[i];
        while (cur) {
            printf("%3d, %3d, %3d: %-10d ", TO_RED(cur->code), TO_GREEN(cur->code), TO_BLUE(cur->code), cur->count);
            double perc = (double)cur->count / (double)pixel_count;
            printf("%0.3lf%%\n", perc*100);
            cur = cur->next;
        }
    }

    free_bitmap(file);
    return 0;
}
