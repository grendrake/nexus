#ifndef BITMAP_H
#define BITMAP_H

#include <stdint.h>

#define WORD uint16_t
#define DWORD uint32_t
#define LONG int32_t

typedef struct BITMAPFILEHEADER {
    WORD type;
    DWORD size;
    WORD reserved_1;
    WORD reserved_2;
    DWORD offset;
} file_header_t;


typedef struct BITMAPINFOHEADER {
    DWORD size;
    LONG width;
    LONG height;
    WORD planes;
    WORD bits_per_pixel;
    DWORD compression;
    DWORD size_of_bitmap;
    LONG horz_resolution;
    LONG vert_resolution;
    DWORD colours_used;
    DWORD colours_important;
} info_header_t;


typedef struct BITMAP {
    file_header_t *file;
    info_header_t *info;
    unsigned char *data;
} bitmap_t;

bitmap_t* new_bitmap();
void free_bitmap(bitmap_t *bitmap);
bitmap_t *load_bitmap(const char *filename);

#endif
