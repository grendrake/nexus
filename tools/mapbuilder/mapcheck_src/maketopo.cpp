#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include "lodepng.h"
extern "C" {
#include "bitmap.h"
}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        std::cerr << "USAGE: ./maketopo <infile> <outfile>\n";
        return 1;
    }
    std::string infile = argv[1];
    std::string outfile = argv[2];


    bitmap_t *bmp = load_bitmap(infile.c_str());
    if (!bmp) {
        std::cerr << "Failed to load source file.\n";
        return 1;
    }

    std::vector<unsigned char> data;
    for (int y = 0; y < bmp->info->height; ++y) {
        for (int x = 0; x < bmp->info->width; ++x) {
            int i = (x + (bmp->info->height - y - 1) * bmp->info->width) * 3;
            int v = bmp->data[i];
            if (v < 32) {
                data.push_back(0);
                data.push_back(0);
                data.push_back(63);
                data.push_back(255);
            } else if (v < 96) {
                data.push_back(0);
                data.push_back(0);
                data.push_back(127);
                data.push_back(255);
            } else if (v < 128) {
                data.push_back(0);
                data.push_back(0);
                data.push_back(196);
                data.push_back(255);
            } else if (v < 160) {
                data.push_back(0);
                data.push_back(127);
                data.push_back(0);
                data.push_back(255);
            } else if (v < 192) {
                data.push_back(0);
                data.push_back(196);
                data.push_back(0);
                data.push_back(255);
            } else if (v < 224) {
                data.push_back(63);
                data.push_back(255);
                data.push_back(63);
                data.push_back(255);
            } else if (v < 240) {
                data.push_back(127);
                data.push_back(255);
                data.push_back(127);
                data.push_back(255);
            } else {
                data.push_back(255);
                data.push_back(255);
                data.push_back(255);
                data.push_back(255);
            }
        }
    }

    lodepng::encode(outfile, data, bmp->info->width, bmp->info->height);
    return 0;
}