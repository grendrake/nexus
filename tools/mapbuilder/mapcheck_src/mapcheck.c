#include <stdio.h>

#include "bitmap.h"


int main(int argc, char *argv[]) {

    if (argc != 2) {
        printf("USAGE: %s <filename>\n", argv[0]);
        return 1;
    }

    bitmap_t *file = load_bitmap(argv[1]);
    if (!file) {
        printf("Could not load %s as bitmap\n", argv[1]);
        return 1;
    }

    int count[4] = {0};
    for (unsigned i = 0; i < file->info->size_of_bitmap; i += 3) {
        if (file->data[i] != file->data[i+1] || file->data[i] != file->data[i+2]) {
            ++count[3];
        } else {
            switch(file->data[i]) {
                case 0:     ++count[0]; break;
                case 64:    ++count[1]; break;
                case 255:   ++count[2]; break;
                default:
                    ++count[3];
            }
        }
    }

    if (count[3] > 0) printf("Found %d invalid pixels.\n", count[3]);

    double total = count[0]+count[2];
    double land_per = 100.0 * ((double)count[0] / total);

    // printf("Total:  %d\n", count[0] + count[2]);
    // printf("Land:   %-7d (%0.1lf%%)\n", count[0], land_per);
    // printf("Water:  %-7d (%0.1lf%%)\n", count[2], 100.0 - land_per);
    // printf("Border: %d\n", count[1]);

    printf("Surface water:  %0.1lf%%\n", 100.0 - land_per);

    // free_bitmap(file);
    return 0;
}
