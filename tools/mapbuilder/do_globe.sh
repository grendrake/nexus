#!/bin/bash

# Generates a basic spinning globe animation for a given seed. Note that the
# final image's width will be HALF of what is specified here.
#
# This requires imagemagick to be installed on the system.

WIDTH=600
HEIGHT=`expr $WIDTH / 2`
SEED=628268
DELAY=10
DEGREES_PER_FRAME=10

rm globe/*

echo Generating frames
for (( i=0; i < 360; i += $DEGREES_PER_FRAME ))
do
    ./planet -po -l $i -w $WIDTH -h $HEIGHT -s 0.$SEED -C colours/Lefebvre2.col -o globe/g_$i.bmp
done

echo Trimming frames
FILES=`ls -v globe/g_*.bmp`
mogrify -crop ${HEIGHT}x${HEIGHT}+`expr $WIDTH / 4`+0 $FILES 2> /dev/null

echo Generating animation
convert -delay $DELAY -loop 0 $FILES out/${SEED}_globe.gif
