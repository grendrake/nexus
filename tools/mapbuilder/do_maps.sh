#!/bin/bash

# This script takes an array of seeds and produces three maps for each: a
# black & white template map, a greyscale heightmap, and a general-purpose
# coloured map in the equal-rectangular projection suitable for running
# through a program such as G.Projector

WIDTH=1600
HEIGHT=`expr $WIDTH / 2`
SEEDS=( 25686  )

for i in "${SEEDS[@]}"
do
    echo Doing $i
    ./planet -pq -w $WIDTH -h $HEIGHT -s 0.$i -C colours/Lefebvre2.col -o out/${i}_base.bmp
    convert out/${i}_base.bmp out/${i}_base.png 2> /dev/null
    rm out/${i}_base.bmp
    ./planet -pq -w $WIDTH -h $HEIGHT -s 0.$i -C colours/greyscale.col -o out/${i}_height.bmp
    convert out/${i}_height.bmp out/${i}_height.png 2> /dev/null
    rm out/${i}_height.bmp
    ./planet -pq -w $WIDTH -h $HEIGHT -s 0.$i -C colours/landwater.col -o out/${i}_template.bmp
    convert out/${i}_template.bmp out/${i}_template.png 2> /dev/null
    rm out/${i}_template.bmp
done
