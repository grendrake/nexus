
echo Base Map
./planet -pq -w 1600 -h 800 -s 0.54405 -C colours/Lefebvre2.col -o out/54405_base.bmp

echo Mollweide
./planet -pM -w 1600 -h 800 -s 0.54405 -C colours/Lefebvre2.col -o out/54405_mollweide.bmp

echo Heightmap
./planet -pq -w 1600 -h 800 -s 0.54405 -C colours/greyscale.col -o out/54405_height.bmp

echo Template / Silhoutte
./planet -pq -w 1600 -h 800 -s 0.54405 -C colours/landwater.col -o out/54405_template.bmp

echo Template / Equal Area
./planet -pM -w 1600 -h 800 -s 0.54405 -C colours/landwater.col -o out/54405_template_equalarea.bmp
./mapcheck out/54405_template_equalarea.bmp
