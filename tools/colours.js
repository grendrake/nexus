var dropZone = document.getElementById('dropZone');
var outputZone = document.getElementById('output');
var fileSelector = document.getElementById('fileSelector');
var statusLine = document.getElementById('statusLine');
var totalArea = document.getElementById('totalArea');

// Optional.   Show the copy icon when dragging over.  Seems to only work for chrome.
dropZone.addEventListener('dragover', function(e) {
    e.stopPropagation();
    e.preventDefault();
    e.dataTransfer.dropEffect = 'copy';
});

// Get file data on drop
dropZone.addEventListener('drop', function(e) {
    e.stopPropagation();
    e.preventDefault();
    fileSelected(e.dataTransfer.files); // Array of all files
});
fileSelector.addEventListener('change', function(e) {
    var files = document.getElementById('fileSelector').files;
    fileSelected(files);
});

function fileSelected(files) {
    fileSelector.innerHTML = "";
    statusLine.textContent = "Loading data... (this may take a while)";
    for (var i=0, file; file=files[i]; i++) {
        if (file.type.match(/image.*/)) {
            var reader = new FileReader();
            reader.onload = midLoad;
            reader.readAsDataURL(file); // start reading the file data.
        } else {
            statusLine.textContent = "Not an image.";
            return;
        }
    }
    statusLine.textContent = "Done.";
}

function midLoad(e2) {
    var img = document.createElement('img');
    img.src = e2.target.result;
    img.onload = finishedLoading;
}

function finishedLoading( e2 ) {
    var img = this;
    var counts = {};
    var total_count = 0;

    var canvas = document.createElement('canvas');
    canvas.width = img.naturalWidth;
    canvas.height = img.naturalHeight;
    const ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0, img.naturalWidth, img.naturalHeight);

    var pixelData = ctx.getImageData(0, 0, canvas.width, canvas.height).data;
    for (let i = 0; i < pixelData.length; i += 4) {
        var key = pixelData[i + 0] + ", " + pixelData[i + 1] + ", " + pixelData[i + 2];
        if (counts.hasOwnProperty(key)) {
            ++counts[key].count;
        } else {
            counts[key] = { colour: pixelData, count: 1 };
        }
        ++total_count;
    }

    var result = "<table><th></th><th>Colour</th><th>Count</th><th>Percent</th><th>Area</th></tr>";

    totalAreaValue = totalArea.value.replace(/[ ,]/g, "");
    totalAreaResult = parseFloat(totalAreaValue);
    for (var key in counts) {
        var percent = (counts[key].count / total_count * 100);
        result += "<tr><td style='background-color:rgb(" + key;
        result += ")'>&nbsp;&nbsp;</td><td>"+ key + "</td><td>";
        result += counts[key].count.toLocaleString() + "</td><td>";
        result += (Math.round(percent * 100) / 100);
        const myArea = (Math.round((totalAreaResult * (percent / 100)) * 100) / 100);
        result += "%</td><td>"
        if (!isNaN(myArea)) result += myArea.toLocaleString();
        result += "</td></tr>";
    }
    result += "</table><br>";
    var resultDiv = document.createElement("div");
    resultDiv.innerHTML = result;
    while (outputZone.firstChild) outputZone.removeChild(outputZone.firstChild);
    img.width = 400;
    outputZone.appendChild(img);
    outputZone.appendChild(document.createElement("br"));
    outputZone.appendChild(document.createElement("br"));
    outputZone.appendChild(resultDiv);
    outputZone.appendChild(document.createElement("br"));
    outputZone.appendChild(document.createElement("br"));

}