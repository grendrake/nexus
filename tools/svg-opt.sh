#!/bin/bash
for filename in ./*.svg; do
    echo "Doing $filename"
    svgo "$filename"
done
