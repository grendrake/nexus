All dates are rendered in Earth's Gregorian calendar.


<li><a href='#erladi_first_gate'>erladi first gate</a></li>
<li><a href='#erladi_forces_zifaelafojun'>erladi forces zifaelafojun</a></li>
<li><a href='#erladi_forces_ingr'>erladi forces ingr</a></li>
<a id='daesra_arrive'>The Daesra Arrive at the Nexus</a>
<a id='vakatr_discovered'>The Va Katr Gate is Forced Open</a>
<a id='kzaknri_opens'>Enter Razaz Nudar</a>



<h2 id='erladi_first_gate'>Erladi Discovers Their Gate</h2>

The first gate to be opened was discovered in Gek Relt on February 15, 1947.
This lead to the discovery of the hub region, populated then only by the zaeren.
Settlers quickly started moving into this area, pushing existing zaeren population back, and research stations were set up to learn more about this new place and to try and discover a way to open the numerous gates that ringed the hub.
For over a decade, the hub was treated as an extension of Gek Relt, and governed by the same government.


<h2 id='erladi_forces_zifaelafojun'>Erladi Forces the Zifaelafojun Gate</h2>

The first successful attempt to open a gate from the hub side occured on September 13, 1948.
This lead Gek Relt scouts to the world of Zifaelafojun, first encountering the nation of Pasha Za Fojun Fae and later Menota itself.
Initial contact was brief and mostly peaceful as the scouts only gathered information for most of their stay.
Just before leaving, they kidnapped a number of low-placed and socially isolated ravon to take with them for study.

Later exbiditions would lay claim to more of the local population, as the unusually shaped, flying sapients proved popular as slaves back in Gek Relt.
In many cases, however, the slave-holders found themselves struggling to find an effective use for their new possessions outside of prestige.
Ravon who were permitted to fly proved prone to escaping and many ravon slaves thus had their wings purposefully damaged, though this significantly reduced the practice use of such slaves.
Those born into slavery or kidnapped at a young age proved much more obediant and these "2nd generation" were soon considered much more valuable.

There were attempts by Gek Relt to settle people on Zifaelafojun, but after the slave raids, the local population had been rendered irrevocably hostile.
Although the local population avoided direct conflict, would-be settlers found themselves bombared with trash and biological matter from the sky.
Attempts to follow the attackers were stymied as the attackers traveled by air, leaving their ground-based pursuit to try and follow across rivers and ravines.
Eventually, only the hardiest and most dedicated would-be settlers remained on Zifaelafojun and even then they focused more on enduring the locals than anything else.


<h2 id='erladi_forces_ingr'>Erladi Forces the Gate to Ingr</h2>

A couple of years after discovering Zifaelafojun, Erladi forced the gate that lead to Ingr open.
Here they found an entirely different problem: the Ingr gate was located underwater, a few kilometres offshore.
Even once they found their way on-shore on a nearby island chain, problems continued.
The locals, the Kthbra Remnants, while friendly, demonstrated little technology beyond the stone age.
Despite this, armed parties of explorers routinely went missing.
Indeed, the better armed the explorers and the larger the group, the faster and more likely they were to disappear.
Eventually, the cause of this was discovered when one group disappeared in view of another: the population might have been very low-tech, but the planet itself had previously hosted an advanced civilization and the area was riddled with automated defenses.
Once this was known, scout teams were able to operate more reliably and regular contact was established.
Between a fear of the automated defenses and the shear technological differences between the two groups, however, contact was mostly limited to researchers from Gek Relt.


<h2 id='daesra_arrive'>The Daesra Arrive at the Nexus</h2>

On March 27, 1955 an event happened that changed things nearly as much as Gek Relt's initial discovery of the gate.
The daesra, from the eponymous "Daesra World", opened their gate on their own.
Although the daesra were very different from the au of Gek Relt, they were equals at least, and some feared they were superior.
Perhaps the most intimidating, the daesra possessed mental powers of then-unknown scope.
As a result, offical policy around the newcomers was very generous and friendly.
By the time the actually fairly limited scope of daesra powers was known, relations had already settled into a comfortable pattern.
Contact was friendly on both sides, but generally limited; the daesra showed little interest in becoming involved with Gek Relt.


<h2 id='vakatr_discovered'>The Va Katr Gate is Forced Open</h2>

In 1956, Gek Relt researches were able to open another gate.
This one opened into a bustling city on the world of Va Katr, a world that surprised everyone by possessing two sapients species (the garor and rakelm) that were (mostly) cooperating with each other.
The more prominent sapient species proved to be intimidating, towering at least half a metre over the tallest au.
The other was capable of natural flight.
Although the locals were technologically inferior, the government of Gek Relt did not feel that conflict was advisable.
Friendly overtures were made and a number of the garor and rakelm relocated to the hub.
Relations remained tense, however; most from Va Katr despised the slave-based economy of Gek Relt, but they keenly felt their technological inferiority and did not feel they were in a position were they could act.


<h2 id='kzaknri_opens'>Enter Razaz Nudar</h2>


September 23, 1960	First Contact Kzaknri	960
 	
The Kzaknri gate is forced open from the Nexus. As the gate was in an unpopulated area, the new arrivals initially thought they had discovered an uninhabited world. It does not take long for reconnaissance to reveal the local population, however.

April 26, 1899	Kzaknri Zifaelafojun Erladi Gek Relt Menota Razaz Nudar	899
 	
Razaz Nudar liberates Menota. This takes the form of an invasion by the forces of Razaz Nudar; all non-ravon in residence are given the choice between death or being sent back to their original homeworld, even if they have no resources or money. Deported individuals are not given recompense for lost property or goods. Menota is set up with an independent government and granted possession of all assets seized from the deported.

Although originally believed to be a PR stunt, records discovered later reveal that Emperor Thiknrae had intended the fledgling government to fail, allow him to return, claim the territory, and look like a hero doing it. Unfortunately for his plan, while the government he'd set up was no more effective than he'd intended, the already existing governing bodies were able to pick up the slack so the intended collapse never occurred.

June 4, 1967	First Contact Isaguyat	967
 	
The gate to Isaguyat is forced open. Not only do the native sekar rebuff the newcomers, the contact squad is caught off-guard and mostly slaughtered. The low technology level leads to some debate about returning with modern weapons, but ultimately it is decided to restrict contact. The death of the contact squad kills popular and political support for the contact program; it would be more than a decade before the next gate is opened.

October 16, 1982	Kzaknri Razaz Nudar Rekuzarae	982
 	
The personal journals of Rekuzarae are discovered in a hidden chamber in the palace.

September 6, 2002	Kzaknri Razaz Nudar	1002
 	
Djurnae hatches.

August 13, 2014	Earth	1014
 	
Earth gate discovered on Earth and opened. First contact with the Interworld Nexus is achieved.

May 27, 2019	Earth	1019
 	
Current year.