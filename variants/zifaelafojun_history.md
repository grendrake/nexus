---
title: Zifaelafojun History
toc: true
group: Zifaelafojun
categories: [history]
world: zifaelafojun
---

The ravon on the world of Zifaelafojun have never been a populous species and are even today limited to the island-continent of Zifaelafojun. No evidence has been found that significant populations have ever inhabited other parts of the world; what traces exist can be easily explained by Menota's decision to exile criminals.


## First Contact (Warrothserkm)

Warrothserkm opened the gate from the Nexus side in the Earth year 1649 and discovered the ravon.

During the time that followed, living conditions and quality of life improved dramatically, but never reached the same point as elsewhere in Warrothserkm. Although many ravon wanted to go elsewhere, Zifaelafojun had little economy and very few were in a position to travel to other worlds. Some went so far as to sell themselves into slavery, either not really understanding what they were doing or believing (not entirely incorrectly) that they'd still have a better life. Prisoners in interclan wars were often sold into slavery as well and this was frequently viewed as one of the best ways for the clan to make money.

While ravon slaves were shipped to all parts of Warrothserkm, they proved most popular in Reka Relt where the native au found their body shape and especially their ability to fly appealing. Many ravon slaves, in Reka Relt and elsewhere, were trained as couriers or messengers. In some cases they were even kept as a kind of intelligent pet, though often with lamed wings and/or devoiced in this case.


## Second Contact

Between their reduced population and having had marvels snatched away, the ravon went into decline after the fall of Warrothserkm. When, in the Earth year 1853, Gek Relt was able to force the gate open from the Nexus side, the ravon were a dying race. On arriving, agents of Gek Relt were shocked to find a small, poverty stricken population who needed training to be able to perform even the simplest "civilized" tasks. It was nothing like their own histories had lead them to believe. Still, Zifaelafojun was annexed by Gek Relt who set up a basic government consisting of the head of each surviving clan (still existing in modern times under the nsme of the Council of Clans and with a slightly expanded membership) and put basic educational services in place, but mostly ignored the place aside from periodically taking slaves.

The local ravon, remembering the coming of Warrothserkm quite well, were far from welcoming. Most of the population avoided and ignored the agents from Gek Relt, but some took advantage of the situation, selling what remaining goods from Warrothserkm that they still had, as well as leasing land rights to au looking to create a homestead. The money from this went to education and the beginnings of what would latter become the Menotan investment hegemony. By virtue of this initial wealth and education, this group became a new nobility. They also demonstrated that while the ravon may have been uneducated in the past, they were by no means unintelligent.

The most successful of the new nobility gathered together to form the Council of Fifty to guide their people (or at least the prosperous ones) while working for the downfall of Gek Relt.

It is popularly believed that when Razaz Nudar liberated Zifaelafojun from the control of Gek Relt that they had been subtly encouraged by the Council of Fifty. Though there is no direct evidence of this, it would fit how the Council of Fifty typically operated.

Regardless of the reasons, in 1899, Razaz Nudar sent an army through the Nexus to liberate Zifaelafojun. This was not a bloodless liberation; those au not willing to Gek Relt were execeuted by the liberating force and the local population were allowed to keep anything left behind. Once this liberation was complete, the then-emperor of Razaz Nudar set up a new government in the form of the Council of Salmae, not realizing the newly liberated nation already had a functioning government. A large sum of money was also gifted to the noble class for the construction of assorted infrastructure;  this money was instead invested and used to produce more wealth for said nobles.

Despite evidence having come to light since its liberation that the emperor of Razaz Nudar had intended for the new government to fail, allowing him to "rescue" the inhabitants from their failed independance, the newly fledged Menota has remained a remarkably free and stable nation. This is no doubt helped by the small mercenary army from Razaz Nudar that remains near the gate. Ironically, what was almost certainly intended to be the core of an invading army has served to foster good relations and to the modern day both Razaz Nudar and the nations of the FASS are the ones with the strongest relations with Menota.
