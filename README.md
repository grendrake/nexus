# The Interworld Nexus

The Interworld Nexus is a worldbuilding project and the current version of a world I've been building for well over a decade. Its a generally realistic, near-future setting with Earth and several alien worlds all connected to the titular *Interworld Nexus*.

This repository stores the project in its raw, unprocessed form. If you're looking to actually browse the site, visiting [https://nexus.grenslair.com/](https://nexus.grenslair.com/) will provide a better experience. That said, the issue tracker here is a good way to report problems and typos.
