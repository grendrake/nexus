---
notoc: true
category: general
---


The Interworld Nexus is the latest incarnation of a long-running worldbuilding project. Originally, it was called *Tarceen* after the planet that it was set on; it was only when I split things up across multiple planets that the setting was renamed to be the *Interworld Nexus*. The project has taken many forms over the years, from its original urban fantasy-ish design, through a brief science fiction offshoot, to its current, mostly realistic version.

The shift from the previous single world design to the new design of having multiple portal-connected worlds occurred because the world was starting to feel crowded; there is only so much that can fit on a single planet. The new design has the additional benefit of making it easier to put up barriers to keep species, events, and cultures separated. In the modern age of cell phones, satellites and aerial drones, having vast, unknown territories has become increasingly difficult.

The setting was originally designed and has mostly continued as, a place for me to set stories. I've written stories that were set in several incarnations of the setting, though many have been lost to time or data corruption. Of those that remain, I rarely make any effort to keep them accurate to the current version of the setting; not only do many reference elements that no longer exist, but I would rather spend the time telling new stories than dwelling on old ones.

I have made a couple of attempts to describe languages used by non-humans. In most cases, these descriptions are old, dating back to when my understanding of language design was *much* weaker and are in desperate need of being updated. Alas, they are some of the least used, and thus lowest priority, parts of the site. Also, these descriptions should be taken as "humanized" versions containing sounds suitable for human pronunciation; the differing mouth and throat shapes of various species mean the sound of a language will depend as much on the species of the speaker as it will on the language itself.


## Tools

This site was created using [MkDocs](https://www.mkdocs.org/) alongside a custom template and plugins. The raw source can be accessed via [the project on GitLab](https://gitlab.com/grendrake/nexus).

The various star systems were generated using the system design sequence presented in [GURPS Space 4e](http://www.sjgames.com/gurps/books/space/).

Planetary maps are generated using "[Planet Generator](http://hjemmesider.diku.dk/~torbenm/Planet/)" and translated between different projections using NASA's [G. Projector](https://www.giss.nasa.gov/tools/gprojector/).

Graphics and art created by me (including maps) is mostly created and modified using [Inkscape](https://www.inkscape.org/) and the [GIMP](https://www.gimp.org/). Art created by other artists is created in their software of choice.


## License and Usage

This site and the text content of it are released under the <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a> and thus may be re-used for personal projects, art, or original characters, but not for commercial projects; if you'd like to use this in a commercial project, please get in touch and we can work out a suitable arrangement. Art created by me (Gren Drake) is also under this license. Art created by other artists remains under their copyright and is not licensed for further distribution or usage without the artist's permission. Some pages use custom JavaScript to enable additional functionality. These scripts are released under the <a href='https://opensource.org/licenses/MIT'>MIT</a> license and may be reused or altered as much as desired.
