---
category: sophant
world: interworld nexus
parent_name: Interworld Nexus
parent_addr: overview
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Syri</th></tr>
<tr><th>Homeworld:</th><td>Unknown</td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>1' (0.3 m)</td></tr>
<tr><th>Avg. Weight:</th><td>13 lbs (6 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>3</td></tr>
<tr><th>Gestation:</th><td>11 months</td></tr>
<tr><th>Weaned:</th><td>2 years</td></tr>
<tr><th>Maturity:</th><td>18 years</td></tr>
<tr><th>Lifespan:</th><td>115 years</td></tr>
</table></div>


The syri are an enigmatic species; little is known of them.
They are present in small numbers nearly everywhere and most people living in the Nexus have seen them on multiple occasions.
Despite this, they are rarely seen interacting with local culture.
While a syri may choose to associate with a single individual, most do not make use of any businesses or tourism spots.

Syri in the Interworld Nexus can be broadly categorized into three groups: affiliated syri, unaffiliated syri, and exiles.
Affiliated syri are those affiliated with the government body they call the Hidden Council.
Exiles are those who were previously affiliated, but who have been banished.
Unaffiliated syri have no connection to the Hidden Council.

## Description

Syri are small, highly agile quadrupeds.
They excel at climbing and can often be found perched in high, inaccessible places despite a lack of wings.
Their long tail is prehensile and assists them in climbing.

<figure class='right' style='width:305px'>
    <img src='/art/species/syri.png' width='305' height='391'>
    <figcaption>
    Male syri.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

A mane runs along their spine and their bodies are covered in a layer of thin fur.
Despite their quadrupedal nature, they have dextrous hands with thumbs.

## Diet

Syri are omnivores.
They have been observed eating a wide range of prepared and processed foods that suggest they have few dietary limitations.
Most prefer their meals to be heavy with fruit or vegetables.
As no syri has been observed outside a modern environment and no syri has spoken of their history, it is unclear what would constitute a "natural" diet.

## Reproduction

Observed syri pairings have involved extended courtships and firm monogamous bonds, but it is unclear whether this is a trait of the species or because most observed relationships involve exiled syri who are cut off from their original culture.
They are fertile year around and give birth to single offspring after an extended gestation period.
While observed syri parenting is often inattentive, this is unusually explained by the memory impairment of exiles rather than being characteristic of the species.

## Culture

### Hidden Council

The Hidden Council is the only known government of the syri species.
Its location and goals are entirely unknown and its activities are shrouded in secrecy.
The syri that served the Hidden Council actively work to hide its activity and conceal or destroy information relating to it.


### Affiliated Syri

[Warrothserkm](warrothserkm.md) records the affiliated syri as having been present in the Nexus when Warrothserkm first achieved access.
Little else is recorded, however, and this powerful nation seems to have been no more successful in penetrating the syri's secrets than modern attempts.

They possess unique, advanced technology.
It appears that they can communicate instantly with one another, even across large distances.
Similarly, massive databanks of information seem to be available to them.
When under sufficiently threatened, injured, or when they die, they can teleport over surprising distances, though they have only been observed to do this in emergency situations.
While normally discreet about using their technology, they have never denied its existence.
Syri do deny the existence of any telepathic or telekinetic abilities in their species.
In the event that technology is forcefully or stealthily removed from its owner, the technology will disappear.
Occasionally, it will explode instead.

The heart of their civilization and their homeworld are unknown.
Attempts to follow or track syri consistently fail.
The syri themselves have stated that their homeworld exists, but that they will not be revealing its location.

Although the syri have a very secretive culture, they are quite open about this secrecy.
Syri rarely lie and have no trouble admitting that they are concealing information.
Those that try to pry into the secrets of the syri too forcefully, however, have a tendency to suffer "accidents".

### Exiles

Exiles are those who served the Hidden Council in the past, but who have since been banished.
While they are unwilling or unable to speak of the reasons, it is generally assumed that they committed some form of crime.
They are typically reclusive, but actively avoid other syri even more than other species.

Those who have been exiled lack the technology associated with affiliated syri and thus the abilities associated with it.
Exiles have never been known to communicate with anything but mundane technology or to teleport.
Faced with injury or death, they have no more options than any other species.
Despite this, they are rarely poor.
Exiles appear to be granted enough money to become settled into a new life.

Notably, all information about the Hidden Council and their homeworld appears to be erased from the mind of an exile.
They are often twitchy and absented-minded, experiencing an unusually high number of memory lapses.
It is suspected this is a side-effect of the memory erasure process.
Their other cognitive functions are not impaired however, and their ability to reason is unimpeded.

Many exiles are depressed and unmotivated.
They typically live alone, declining any romantic involvement.
In the rare case they do find romance, it is with other species.
Sometimes they will make the effort to gather the materials to transform a romantic partner into an unaffiliated syri via [nanite solution](nanite_solution.md).

### Unaffiliated Syri

By far the least common group are the unaffiliated syri.
Most of these are those who have been transformed into syri, such as through nanite solution.
This is uncommon both because syri genetic samples are rare and because relatively few people want to become small quadrupeds.
Otherwise, unaffiliated syri tend to be the offspring of exiles, or of an exile and another unaffiliated syri.
While unaffiliated syri are entirely ignored by affiliated syri, affiliated syri do not seem upset or disturbed by their existence.
