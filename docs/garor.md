---
category: sophant
world: va katr
parent_name: Va Katr
parent_addr: vakatr
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Garor</th></tr>
<tr><th>Homeworld:</th><td><a href='vakatr.html'>Va-Katr</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical (male)</th></tr>
<tr><th>Avg. Height:</th><td>7'3 (2.2 m)</td></tr>
<tr><th>Avg. Weight:</th><td>240 lbs (109 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Physical (female)</th></tr>
<tr><th>Avg. Height:</th><td>6'3 (1.9 m)</td></tr>
<tr><th>Avg. Weight:</th><td>154 lbs (70 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>1</td></tr>
<tr><th>Gestation:</th><td>11 months</td></tr>
<tr><th>Weaned:</th><td>4 years, 2 months</td></tr>
<tr><th>Maturity:</th><td>20 years</td></tr>
<tr><th>Lifespan:</th><td>98 years</td></tr>
</table></div>

The most obvious trait of garor is their size.
Males are over seven feet tall and even the shorter females are taller than members of other species.
This is accompanied by a great deal of physical strength.
While they are not unintelligent, they are prone to trying to solve problems through the use of force rather than logic.


## Description

<figure class='right' style='width:199px'>
    <img src='/art/species/garor.png' width='199' height='434'>
    <figcaption>
    Male garor in common daily wear.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

Garor are large bipeds.
Their bodies are covered in leathery, hairless skin.
Their skin is a light brown or beige colour, with dark horizontal stripes running across their faces, heads, backs, tails, and the back of their thighs.
Starting at puberty, the top of their head grows a thick crop of hair.
This is typically vibrantly coloured and is considered to signal maturity.

Both sexes possess curved horns.
At birth, these are small nubs but will grow in to be a few inches long.
If damaged or removed, horns do not grow back.
During puberty, the horns of males will resume growing to produce substantial curled horns.
Many believe these were once used in mating fights, but in modern garor they are decorative.


## Diet

The diet of the garor is both omnivorous and expansive.
They are capable of digesting a large range of foods, both meat and plant.
They are not scavengers, however, and require their food to be reasonably fresh and unspoiled.


## Reproduction

Garor possess a slow reproductive cycle.
A single child is born after an eleven month gestation period.
Twins are rare, but not unheard of.
In almost all cases, twins (or other multiple births) will result in only female children.
Male children rarely occur as a part of a multiple births.
Children are typically nursed for just over four years, during which time their mother will not ovulate.
Physical maturity occurs around twenty years of age.
