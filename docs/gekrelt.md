---
category: nation
world: erladi
parent_name: Erladi
parent_addr: erladi
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Gek Relt</th></tr>
<!-- <tr><th>Capital:</th><td>Salmae</td></tr> -->
<tr><th>Language:</th><td>Kaegul</td></tr>
<!-- <tr><th>Demonym:</th><td>Menotan</td></tr>
<tr><th>Area:</th><td>8,034,000 km<sup>2</sup></td></tr> -->
<tr><th>Population:</th><td>162 million</td></tr>
<!--<tr><th>Density:</th><td>0.44 pop/km<sup>2</sup></td></tr> -->
</table></div>


Gek Relt is a parliamentary monarchy and has a hereditary nobility. The primary industry of Gek Relt is agriculture and most of the settled population is involved in it. There are travelling traders, but these make up a tiny proportion of the total population; most are newlyweds or childless families working out their wanderlust before settling down somewhere.

Gek Relt has an abnormally high proportion of non-au sophants living and born in the country and this been true since contact with [Warrothserkm](warrothserkm.md). Most non-au are slaves, however, and many have never met one of their own species who was not a slave.

Most non-labour work, both in business and the home, has fallen to women due to the physical limitations of male au, but technology is increasingly making it possible for roles and responsibilities to be shared. Though this is mostly driven by males (who feel increasingly useless as technology makes things like fingers and thumbs more valuable), it is almost universally accepted and welcomed.


## Architecture

Most buildings are large, spacious, and heavily subdivided. Although most buildings are owned by an extended family, different immediate families typically occupy separate parts of the building with little overlap.

Both the interior and exterior of most buildings are heavily decorated to the best of the owner's ability. Interiors often have tapestries, rugs, and artwork, while exteriors have statues, ornate inlays and mosaics, and other decor that is weather-resistant. The edges of roofs are lined with brightly coloured gutters, ending in gargoyles that redirect rainwater into alleys or gardens.


## Government

Gek Relt is a parliamentary monarchy in which the monarch maintains a significant amount of power, but who is not an absolute ruler by any means. The powers and responsibilities of both monarch and parliament are set out in a written constitution largely adopted from the parent nation of Gek Relt.

Each monarch rules for a five local year term (about ten Earth years) and is elected in a process in which every eligible voter is required to vote. Eligible voters are adults who have completed their mandatory military service and who are not and have never been slaves. Once the monarch's term has ended, they are not allowed to run for the position again for at least ten local years (about nineteen Earth years). Members of parliament and their immediate families are not permitted to run for monarch and former monarchs are not permitted to join the parliament.

Traditionally, the monarch of Gek Relt has been a woman. Male monarchs are becoming more common in modern times, but they still have a harder time becoming elected. The previous monarch was a man, but the current monarch is a woman again.


## Daily Life

The core parts of life for most citizens are family, business, and religion. While family and business sometimes such places depending on the person and situation, religion typically remains solidly in third place. The role of religion was historically greater, but the arrival of [Warrothserkm](warrothserkm.md) with other-worldly ideas and the later break-up of Reka Relt weakened the basis of faith; renewed contact with other worlds through the gate has only served to weaken this further. The diminishing role of religion has been replaced by both family and business in mostly equal measure.

The immediate family consists of a man, all his wives, their unmarried children, and any elders no longer living independently, particularly those who no longer have immediate family of their own. The immediate family typically lives near and works close with the first wife's extended family, though modern life sometimes forces families to break up more than they would like. An immediate family that is struggling will be supported by their extended family, but a certain degree of self-sufficiency is expected in the long term. Those who contribute little back to the family may find this support limited; in extreme cases, such as criminal convictions or divorce, they may even be cut off entirely.

### Business

Business is conducted informally. Written contracts are often reserved for larger or more long-term arrangements, or for dealings with foreigners. Reputation and the strength of one's word are vital; someone who loses either will struggle to find success.

Most non-retail business affairs are conducted in the home and often a business is run by an immediate family and are typically named after the first wife or her husband. The husband often serves as the 'face' of the business and is responsible for negotiations and maintaining business relationships. Most of the actual work will be performed by his wives, unmarried children, and any employees they may have. Young families or those without the capital to establish themselves will often be subcontracted by other family businesses, especially within the same extended family. While not as prestigious, this offers a more reliable income. In most cases they are hired or fired as a group, but someone with specialized skills may establish themselves independently.

Long distance trade is typically operated by young family units and will operate out of a ship or as a caravan rather than occupying a traditional home. Most ships and caravans travel as part of a larger caravan as this provides the most protection against robbery and piracy. The exception is those participating in licensed smuggling (that is, who have purchased the right to not have their cargo inspected in exchange and who pay fixed taxes) as many such smugglers try to keep their sources secret.

Although it is often assumed by off-worlders that trade caravans play an important role in the slave trade of Gek Relt, the commercial aspects of the slave trade are typically performed locally where buyers and sellers know each other's reputations. Long distance slave trading has historically been conducted through a process akin to mail-order; most slaves being transported have already been sold.

### Marriage

In Gek Relt, women are always expected to marry. Men are not; because of the regular practice of polygamy, a large proportion of men simply do not get the option. Some will join all male "marriages" (which are officially recognized, but considered distinct from marriages involving women), but others will satisfy themselves with a solitary life or will set out for other worlds.

Those who have gotten married will typically wear a brightly coloured band around their tails about six inches from its base; members of a given marriage will typically have the same pattern on this tail ring and will try to avoid duplicating the patterns of other marriages in the area. The ring serves not only to mark out individuals as unavailable for marriage, but also help indicate who is married to who.

A household is considered to belong to the family of the first wife and individual households within an extended family are distinguished by the identity of the husband. Later wives leave behind their former associations when joining the household. All children are considered to be descended from the household rather than any particular members.

In early history it was common for a group of women to pick out their common husband together; in modern times it has become more common for a single couple to marry then grow their marriage as they see fit. Men also tend to have fewer wives in modern times; historically, it was not uncommon to have as many as five wives, but now three is considered the limit if one wants to look respectable. (An exception to this is made for royalty and the highest ranking nobles.)

Women are expected to wed au men and doing so is considered the final step of becoming an adult. Men do not have this expectation, though many view married men as more respectable.

Those looking to get married engage in extended courtships with multiple potential partners at a time. This courtship is sex-free; premarital sex is considered inappropriate (with exceptions described under _Sexuality_). Those who agree to a marriage are expected to politely excuse themselves from other courtships they are engaged in.

While not common in modern times, a man who has narrowed his options to two or three women who are comfortable with each other may elect to propose to all of them. This occurs mostly with men who are famous or wealthy. The weddings are always held sequentially in an order determined by the prospective wives themselves (a common folk belief is that if they cannot determine this the marriage will not be a happy one) and this order determines their seniority.

Married couples are expected to be on the lookout for a second wife, though not all bother. Second wives are sometimes looked down upon for a variety of reasons. They are often depicted as being insufficiently desirable to get a man of their own, incapable or undesirous of their own children, more interested in marrying an existing wife than the husband, or even not interested in marriage. Despite this, many women are willing, even desirous, of becoming second (or even third) wives, often for these very reasons.

Third wives are often marriages of convenience. In modern times they are often viewed as being "attached" rather than married, or "half-married". A common example is a man marrying female friends with no interest in marriage; becoming a third wife will take care of her "obligation" to be married while having few to no responsibilities or significant involvement in the marriage beyond living under the same roof.

Additional wives are typically courted first by existing wives and often from the ranks of her unmarried siblings and close cousins. It is not unheard of for a man to be encouraged to court a prospective wife only a short time before it is suggested he propose a marriage.

Men with "exotic" tastes will sometimes take non-au as their second wife, even shortly after or even at the same time as taking their first wife. Non-au wives taken after several years of existing marriage are viewed as being non-au more through happenstance rather than exotic tastes on the part of the man.

Although marital are expected to be sexually and emotionally faithful to each other, it is not uncommon for extra-marital affairs by third wives to be ignored as along as they are discreet, do not become pregnant, and the husband and other wives do not object. This is sometimes extended to second wives as well, but second wives are expected to be even more discreet.

Divorce is rare and scandalous with even divorces in otherwise unnotable families becoming newsworthy and both parties will retain a stigma. Those who have a divorce rarely marry again. Before the divorce process can even begin, those involved must seek permission from God (as communicated by a priest). Despite the event being considered newsworthy, the actual process is short and conducted privately. If a man with multiple wives divorces his first wife, none of wives are "promoted" to the empty position and the man remains legally a part of his ex-first wife's family. The fate of children is determined by the priest performing a ceremony; if the man has other wives, children will normally remain with the remaining family, otherwise children still nursing will typically be given to the nursing parent while older children are given to their same-sex parent.

#### Ceremony

All male marriages are conducted without ceremony by registering the union with the local government for a small fee. All participants must be present and consent, but there are few restrictions otherwise. Notably, any marriage involving multiple males must be all male; women are not permitted to marry into an all-male marriage. Although there's a movement to allow individuals of either sex to be a "wife" or "husband", it continues to lack popular support outside of those immediately effected.

Women's wedding ceremonies are long, complex, and considered to be one of the most important events in her life. The more traditionally minded view a woman's wedding as the beginning of her adult wife. It is expected that a woman's family attend her marriage. Conversely, typically only the parents and siblings of the husband and any existing wives attend. After the initial ceremony it is common for the general community to be invited to join the wedding. Weddings for a first wife are more lavish than those for subsequent wives.

The wedding is directed by one or more priests who will trade off with each other during the ceremonies. The first part is when those involved are formally joined and after this they are considered to be married. The rest of the ceremony is a celebration of the marriage; its length is viewed as a sign of wealth and prestige and wealthy families can have celebrations lasting a full day or longer. A few hours into the celebration the bride and groom are expected to leave to find a private place where they will remain together for a few days to thoroughly consummate the marriage and grow closer.

The cost of a first wife's wedding is expected to be paid for by the groom's family. Later marriages are paid for by the existing immediate family and, by extension, the first wife's family.

### Children

Children are nursed by their mother in the few months immediately following birth, but this duty is gradually transferred to the father, taking advantage of the ease with which male au can be induced to lactate. This allows the mother to return to more productive tasks while the traditionally less productive father takes care of the children.

Although no longer common practice, it was historically common for male children beyond the first one or two to be castrated; this was believed to be effective in reducing the demand for wives and making such males more content to be unmarried. Castrated males were typically expected to help with family farms and were often put in the position of overseeing groups of slaves.

On reaching puberty, youth are required to serve for at least one year in the military (excluding slaves who are instead forbidden from performing military service). This is required for both men and women, though there is a growing movement to allow alternative forms of service for men who are increasingly viewed as being less valuable on the battlefield (on account of not having thumbs amongst other reasons). Due to the age of enlistment, most people's first experiences with romance are during their military service.

While the pay rates for this military service are not very good, the military takes care of all living expenses. Most set their pay aside to use in establishing themselves once they return to civilian life.

### Inheritance

The property of married individuals is held communally and is not inherited until all members of the marriage have died. Land is inherited by the eldest daughter, or the eldest son if no daughters were born. If the eldest daughter is deceased, land will be passed to her heir. Only if no heir can be found will land go to the second-eldest daughter.

Other property is divided up all the children, but with males receiving only a half-share of the inheritance. Deceased offspring do not receive a share unless all the offspring have died in which case the property is divided amongst the children's heirs. Once the size of shares is determined, heir choose their inheritance starting from the second eldest daughter to the youngest, then the eldest daughter, then the eldest to the youngest sons. Particularly valuable non-heirloom item may be auctioned off so their value can be divided unless one of the heirs is willing to "buy out" the value beyond what their share of the inheritance would cover. Heirlooms will typically have multiple people contribute some or all of their share of the inheritance to with actual custody being determined later.

Guardianship of underage children typically goes to the (non-underage) heir they know best, or whoever would inherit the family land. Their share of the inheritance is decided by their new guardian, though most will take the child's desires into account. If not of-age guardians can be found, children are taken in by the state with any inheritance held frozen in trust until they are considered adult.

In the event that no heirs at all can be found, land and property are auctioned off and the money granted to the local government.

In all cases, property and children of a slave go to the slave's owner on death.

### Names

Given names are chosen at birth by a child’s mother or the mother's owner in the case of slaves. Names are reused with children often sharing names with their elders or important historic figures. These names often originally described desirable traits, but language drift has rendered them meaningless to most people.

Full names take the form _[Given Name] Kra/Sel [Mother’s Given Name]_.  Kra and Sel are used by men and women respectively and roughly mean _son/daughter of_. In formal situations, it is normal to address people as _[Given Name] Kra/Sel_ and in less formal circumstances, it is normal to address someone, even someone unfamiliar, by their given name, regardless of any difference in age or station. An individual's full name is typically used only in introductions and legal or religious matters.

The exception to this is slaves. Leaves take names in the form of _[Given Name] Kas [Owner's name]_; Kas roughly means _slave of_. They are typically referred to as _[Given Name] Kas_. They are expected to refer to members of their owner's immediately family as master or mistress and others using formal address. In the event that someone ends up owning multiple slaves with the same given name, it is not uncommon to change their names.


## Slavery

The economy of the Gek Relt is based on labour-intensive plantation farming and historically this has used slave-labour. Modern technology is slowly but steadily reducing this need, replacing it with the need for a smaller number of highly trained individuals. As slaves with such training are often no cheaper (and are sometimes more costly) than just hiring trained workers, slavery in modern times is less common than it has been historically. That said, it is still popular and continues to serve as a point of contention between Gek Relt and other worlds.

The tails of slaves were docked to show their status and, later, to reduce the chance of accidents with machinery. This is typically performed twelve to fourteen inches from the base. Although this practice continues today with au slaves, it has never been adopted for use with other species. The tails of freed slaves cannot be restored, but it is considered rude to remark on or stare at the injury. Prosthetic tails are also a common way of hiding this permanent mark of one's former status.

Increasing technology has resulted in an excess of unskilled slave-labour as more and more slaves are replaced with hired workers and long-standing, unrepealed slave breeding incentives. As a result government subsidies have been established to encourage business to make use of slave labour and to provide more advanced training for slaves.

Despite many protests, an increasing number of laws have been created over the history of Gek Relt that offer protections to slaves. In modern times, slaves are required to be given access to clean food, water, and shelter, and it is forbidden to maim or kill a slave without "good cause" (though what counts as a good cause is not specified). It is legal to euthanize slaves with terminal or chronic health conditions with little chance of improvement or meaningful work ability. It is expressly permitted for slaves to be required to engage (or abstain) from sexual activities at any time. Sex between slaves and non-slaves is allowed, but is considered distasteful and desperate.

The same laws that have granted protections to slaves have also granted them the ability to legally hold property. Slave owners are not required to pay their slaves or give them any property, but if the slave does obtain property the slave owner is not permitted to take it. Slave owners are also not permitted to accept payment for anything from their slaves. Relatively few violations of this nature are reported and unfortunately, due to the nature of the slave-owner relationship, it is suspected that it is massively underreported.

Historically, many crimes resulted in enslavement; most have since changed. Such laws are often cited when condemning slavery in Gek Relt. Today, the main crime punished with enslavement is inciting slaves to escape or rebel. The severity is determined by the degree of encouragement as well as whether an escape attempt resulted or was successful.

Children of slaves belong to their mother's owner; this is the most common source of slaves in modern times and most slaves were born into the life. The only other legal sources for slaves are those enslaved for crimes and those willingly selling themselves into slavery. Very few sell themselves into slavery and this is by far the least common source of slaves.

Slaves are divided into three categories: _wild_ slaves are individuals born free and later enslaved. They are considered the least valuable and are rarely trusted or employed in the home; most wild slaves are used for manual labour or breeding stock. _Native_ slaves are those who were born into slavery; this is the standard status and not normally specified. _Nursed_ slaves are a sub-group of native slaves who were taken from their mothers as infants and nursed by existing slaves, a hired wet-nurse, or, for particularly valuable slaves, even nursed directly by their owners. This process was used to avoid the child's biological mother from passing on any culture or history to their child. Today, it is the least common category; most consider it better to stick to existing breeding stocks.

### Emancipation

Able-bodied slaves can be freed through a relatively quick and simply bureaucratic process. Due to historic problems with unemployable ex-slaves resulting in a significant vagrant population, freeing slaves requires their owner to swear that the slave is either capable of finding and holding a job or has sufficient wealth to sustain themselves. If a freed slave is unemployed or unable to sustain themselves for too long within the first five years (about ten Earth years), their former owner is penalized with steep fines.

Because of the difficulties in freeing slaves whose working lives have been ended by disability or disease, such slaves are typically offered the choice between purchasing their freedom at a reduced rate or being euthanized. Most freed slaves are freed as an act of kindness or as a gift and so their former owners will often help arrange or subsidize their initial employment as a free person.

It is also possible for a slave to purchase themselves and obtain freedom that way, though the price is set by their own and there is a surcharge to discourage slaves doing this. Their former owner is not responsible for ensuring that the ex-slave is capable of sustaining themselves in this case.

### Species

Before the arrival of [Warrothserkm](warrothserkm.md) and contact with other species, all slaves were necessarily au. In addition, the excess of men and the value of women as wives (which precludes being a slave) meant most slaves were also male. While female slaves were expected to work, they were also expected to produce children and rewarded for doing so. Most children were transferred to a male nurse-maid as earlier as possible; most of those engaged in breeding slaves would own a small number of male slaves who served only to nurse offspring.

Warrothserkm brought with it numerous species that they were willing to sell as slaves, typically in exchange for agricultural products. Despite modern propaganda that insists these slaves were obtained in raids or through conquest, either by Warrothserkm or Gek Relt itself, most were sold by their own people or were captured by nations on their own world before being sold. The only exception to this was the [ravon](ravon.md); small, mobile slaves were seen as quite valuable and demand, especially early on, far outstripped the supply.

Early slaves were valued for their strength, resilience, or (for ravon) their ability to fly. Most often they were put to work on farms or in mines. Over time, the need for manual labour has decreased while the need for intellectual work has increased resulting in intelligence and creativity being more valued. Ravon, because of their intelligence, smaller size, and flight ability, have remained popular and have become an iconic symbol of prosperity from slavery.

Historically, newly acquired slaves would be put to work as soon as possible regardless of the culture or world they came from. The ravon ability to fly made this problematic as they would readily escape custody and remain out of their would-be owner's reach. This resulting in a new policy of using off-world "wild" slaves primarily as breeding stock with their children being raised as slaves; ravon were particularly subject to this and would be taken from their mother's as young as possible, often to be nursed by their owners. While inconvenient and time-consuming, this was found to be especially effective even if the young ravon experienced some nutritional deficiencies. Such "nursed" ravon slaves were found to have unusually high obedience and loyalty, more-so than other species gained from the same method.


## History

Originally, the au lived mostly in small tribes of hunter-gathers. The males were responsible for hunting and for defending the group while females gathered and did everything that required tools. Typically, on coming of age, a small group of females (two to four, often sisters or otherwise close) would select and bond with a particularly successful male.

This meant many males would never be able to form such a bond. Although some of these would instead form exclusively male bonded groups, many also would serve as a solo hunters or leave the tribe.

The development of agriculture brought changes to the structure of society and not just by becoming more settled. Bonds between women and and their selected man became stronger. While much physical labour was performed with male strength, the husband of the family tended not to be the one doing it. Rather, it was a sign of affluence to be able to have other, unbonded males do the work with female assistance. The main, bonded male remained looking after children and other general caretaking tasks. Due to the quirk of biology that allows male au to readily lactate, they often even ended up nursing their offspring while their wives assisted the unbonded males in the field.

Gradually, a greater mark of affluence was to be able to have numerous male slaves assisted by a smaller number of female slaves to take care of the fields. Men born into slavery rarely escaped it, but it was not unheard of for a male with some inheritance to buy women slaves and free them on the condition they marry him; this gave him the respectability of being married even if he was otherwise unable to attract suitors. Families formed this way were often the lowest class of non-slaves and had little more than subsistence farms; what money that there had been originally having been spent on his wives and land.
