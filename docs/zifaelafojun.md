---
category: world
world: zifaelafojun
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Zifaelafojun</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/zifaelafojun.png'></td></tr>
<tr><th>Star</th>                   <td>F6V</td></tr>
<tr><th>Year Length:</th>           <td>204.79 Local days<br>325.03 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>285 K (11.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>0.73 G</td></tr>
<tr><th>Day Length:</th>            <td>38.092 h</td></tr>
<tr><th>Moons</th>                  <td>None</td></tr>
<tr><td colspan='2' class='wide-data'><a href='systems.html#zifaelafojun'>Zifaelafojun System</a></td></tr>
</table></div>


The world of Zifaelafojun is the sixth planet in orbit around an old G8 star. The other planets in the system are primarily rocky; there is a distinct lack of gas giants. The world itself is the home of the [ravon](ravon.md).

Most of the land area of the planet is uninhabited, with only the small continent also called Zifaelafojun have any known population of sophants. Even the occupied area has a sparse, spread-out population. Between this and the disinclination towards physical aggression of the native ravon, the population has never really been divided into different nations. As a result, the native civilization never developed the national iconography (such as flags) common to other worlds.

The Zifaelafojun gate is located in the territory of [Pasha Za Fojun Fae](pashazafojunfae.md), very close to its border with its parent nation, [Menota](menota.md). Combined, Pasha Za Fojun Fae and Menota occupy the entire area of the island-continent of Zifaelafojun; no other significant settlements are known to exist on the world. Both the world and the island-continent are called Zifaelafojun.

<br clear='all'>
<script>
function swapImage(filename, displayname) {
    const eImg = document.getElementById("mapimg");
    eImg.src = "/art/maps/thumb/" + filename;
    const eLink = document.getElementById("maplink");
    eLink.href = "/art/maps/" + filename;
    const eText = document.getElementById("mapname");
    eText.innerText = displayname;
}
</script>
<figure class='map center' style='width:800px'>
    <a id='maplink' href='/art/maps/zifaelafojun_phys.png'><img id='mapimg' src='/art/maps/thumb/zifaelafojun_phys.png' width='800' height='434'></a>
    <figcaption>
    <span id='mapname'>Physical</span> map of Zifaelafojun.<br>
    View:
        <button onclick='swapImage("zifaelafojun_phys.png","Physical")'>Physical</button>
        <button onclick='swapImage("zifaelafojun_pol.png","Political")'>Political</button>
    <!-- Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Gre</a> -->
    </figcaption>
</figure>
