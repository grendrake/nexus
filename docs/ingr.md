---
category: world
world: ingr
parent_name: Ingr
parent_addr: ingr
notoc: true
---


<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Ingr</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/ingr.png'></td></tr>
<tr><th>Star</th>                   <td>K2V</td></tr>
<tr><th>Year Length:</th>           <td>189.16 local days<br />165.17 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>284 K (10.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>1.032 G</td></tr>
<tr><th>Day Length:</th>            <td>20.956 h</td></tr>
<tr><th>Moons</th>                  <td>None</td></tr>
<tr><td colspan='2' class='wide-data'><a href='systems.html#ingr'>Ingr System</a></td></tr>
</table></div>

The world of Ingr orbits around a K2 star. Much of the world is still uninhabitable with frequent areas of high radiation or blasted, barren wastelands. This had lead to the planet sometimes being nicknamed the "shattered world". The native sophants of Ingr are the [drius](drius.md).

The Ingr gate is in an unusual position; the gate is currently located a few hundred feet under the surface of the ocean a few miles offshore. The entire gate complex is at an angle. Originally, the gate complex was located in a more typical position, but during the war that ended [Warrothserkm](warrothserkm.md) the ground around and below it was destroyed, causing it to sink even as the ocean rushed in to fill the area. The land area nearest the gate is currently inhabited by the [remnants of the Kthbra clan](remnants.md) whose ancestors occupied the area during the time of Warrothserkm.

Currently, an air-filled dome has been set up over the gate complex, but travel to and from land is still challenging enough that goods are typically not shipping between Ingr and [Interworld Nexus](overview.md).
