---
category: god
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
---

<div class='infobox'><table>
    <tr><th class='top-header' colspan='2'>Zakanor</th></tr>
    <tr><th>Religion</th>       <td>State religion of the <a href='tsukina.html'>Tsukina</a></td></tr>
    <tr><th>Title</th>          <td>the Many Eyed</td></tr>
</table></div>

Zakanor is the god of the [Tsukina](tsukina.md) people. Like with most religions born out of what is now [Razaz Nudar](razaznudar.md), He is the only god they worship, though they do not deny the existence of other gods. Zakanor is generally not considered to be omniscient or omnipotent.

As a god, Zakanor is typically credited with an unusually high awareness of the world and is sometimes depicted as possessing many eyes. He is said to be level-headed and possess great wisdom, though He cares only for His chosen people --- the Tsukina.

The military intelligence department of the Tsukina, whose name translates to the Oracular Department, often credits Zakanor for intelligence information of secret origin. Due to the accuracy and extensiveness of this information, especially information the Tsukina should have been unable to obtain, more than a few people have found themselves accepting the supposedly divine origin of this information.
