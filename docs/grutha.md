---
category: god
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
notoc: true
---

<div class='infobox'><table>
    <tr><th class='top-header' colspan='2'>Grutha</th></tr>
    <tr><th>Nraezen</th>        <td><bdo class='nexusFont lang_nr'></bdo></td></tr>
    <tr><th>Religion</th>       <td>State religion of <a href='zanthdrith.html'>Zanth Drith</a></td></tr>
    <tr><th>Title</th>          <td>the Deceiver</td></tr>
</table></div>

Grutha is the god of the state religion of Zanth Drith. He is associated with trickery, guile, deceit, and subtlety. While His followers praise His skill in these things and seek His guidance in things related to them, He is often depicted as being cartoonishly incompetent by other cultures.

His priesthood make up the government of Zanth Drith and they, in turn, prohibit the worship or acknowledgement of any other god.

He is typically depicted as having black scales and with the whites of His eyes rendered in Black.
