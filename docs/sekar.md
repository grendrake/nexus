---
category: sophant
world: isaguyat
parent_name: Isaguyat
parent_addr: isaguyat
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Sekar</th></tr>
<tr><th>Homeworld:</th><td><a href='isaguyat.html'>Isaguyat</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>4'11" (1.5 m)</td></tr>
<!-- <tr><th>Avg. Weight:</th><td>35 lbs (16 kg)</td></tr> -->
<tr><th>Diet:</th><td>Omnivore</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>1</td></tr>
<tr><th>Gestation:</th><td>8 months</td></tr>
<tr><th>Maturity:</th><td>15 years</td></tr>
<tr><th>Lifespan:</th><td>95 years</td></tr>
</table></div>

The sekar are a well-known, but little seen species. They are typically viewed as being very violent and aggressive and as getting along poorly with others. This view is unfortunately correct; unaffiliated sekar will frequently fight. Because they are almost entirely limited to their [homeworld](isaguyat.md), it is unclear how well they will be capable of interacting with other species.

It has been suggested that their aggressive, violent behaviour can be traced to benign origins: sekar are amazingly durable and resistant to harm. An attack that would be brutal for other sophants may well be playful wrestling among sekar.

Unusually, sekar are true hermaphrodites; each individual possess both male and female sexual characteristics, though not all individuals feel the desire to engage in both roles.

Even more unusually, sekar that engage in penetrative sex will form a psychic bond with each other. It is not necessary for said sex to be consensual It is possible, and common, for an individual to have such a bond with multiple other people at once. These bonds are limited to the individuals involved; if **A** and **B** are bonded and **B** and **C** are bonded, **A** and **C** may not be. About three Earth months after a bond is formed or reinforced, it will begin to fade until it disappears entirely after about six Earth months. The bond is reinforced every time a pair engages in penetrative sex.

The psychic bond causes those involved to be aware of where one's bond-mates are, at least in terms of direction and distance. Bond-mates will also be aware of each others general emotion and physical condition, though actual communication is not possible. There is no known limit to the distance these effects can occur over, though nearly all sekar live on a single small continent and no extended testing has been performed. In addition, the bond between a pair is detectable by other sekar when the pair are within about 10 meters of each other.


## Description

<figure class='right' style='width:315px'>
    <img src='/art/species/sekar.png' width='315' height='227'>
    <figcaption>
    Sekar.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

The sekar, while small, are covered in large, heavy armoured plates that make them nearly indestructible and largely bullet-proof. Even their eyes are covered thus, their eyes are covered in transparent armour that replaces the function of eyelids; sekar are incapable of closing their eyes. Sekar have no teeth, but instead their lips are hard and beak like. They have large, prominent horns that come in a variety of designs, a line of spines that runs down their back, as well as large spines growing out of their shoulders. All digits have large, sharp claws.

Although sekar are often brightly coloured, each armour plate is itself only a single colour. Their vibrant patterns are created by the arrangement of the armoured plates.


## Reproduction

All sekar are hermaphrodites though this is not obvious from observation alone as they possess internal genitalia and do not nurse their offspring. Said offspring are born live, but sekar are non-placental. Offspring start eating solid food immediately, albeit in smaller pieces than their parents.

Typically, individuals will find both roles pleasurable, but pregnancy is typically viewed as undesirable and unpleasant. They are sexually active all year due to their bonding, but they are typically only fertile for two-ish weeks in each of spring and autumn.
