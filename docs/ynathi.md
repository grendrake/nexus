---
category: sophant
world: vozruat
parent_name: Vozruat
parent_addr: vozruat
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Ynathi</th></tr>
<tr><th>Homeworld:</th><td><a href='vozruat.html'>Vozruat</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>0.4 m (1’4")</td></tr>
<!-- <tr><th>Avg. Weight:</th><td>96 lbs (43 kg)</td></tr> -->
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>1</td></tr>
<tr><th>Gestation:</th><td>9 months</td></tr>
<tr><th>Weaned:</th><td>5 years</td></tr>
<tr><th>Maturity:</th><td>18 years</td></tr>
<tr><th>Lifespan:</th><td>100 years</td></tr>
</table></div>

Each ynathi is capable of forming a psychic bond with a single other individual (regardless of species).
This bond allows some degree of communication and influence, but is somewhat one-sided in its benefits.

## Description

<!-- {% figure /art/species/zaeren.png | 230 | 390 | right | Male zaeren. | | spelunker %} -->


## Diet


## Reproduction
