---
category: nation
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
---

<figure class='right' style='width:98px'>
    <img src='/art/flags/zanthdrith.svg' width='98' height='198'>
    <figcaption>
    Flag of Zanth Drith.<br>
    Art © <a href='https://www.grenslair.com/'>Gren Drake</a>
    </figcaption>
</figure>

Zanth Drith (Nraezen: <bdo class='nexusFont lang_nr'>&#xe02e;&#xe005;&#xe007;&#xe000;&#x200B;&#xe001;&#xe025;&#xe007;</bdo>) is a large nation founded by the worshipers of Grutha who fled persecution by the worshipers of Tika in Razaz Nudar. Philosophical differences between the two religions have lead to Zanth Drith being a very different country, despite being founded by people from Razaz Nudar.

The nation is almost entirely populated entirely by sakin; although having easy access to the gate by sea, tensions with Razaz Nudar and a wide-spread attitude that off-worlders and non-sakin are suitable only to being slaves virtually eliminate immigration and tourism. Ambassadors and trade missions are sometimes sent to other countries or worlds, but often have difficulty setting aside their self-superiority and thus find little success. Emigration in any form is entirely forbidden.


## Daily Life

### Timekeeping

Zanth Drith uses a timekeeping system derived from that of Razaz Nudar, but with one significant difference: Rather than having a single calendar day with both "day" and "night" portions, Zanth Drith splits this into two calendar days centred around the twilight rising and twilight falling periods.


## Government

Zanth Drith is a theocracy ruled by the priesthood of Grutha. Day-to-day business and the beaucracy of government is handled exclusively by those in the priesthood; non-priests are not permitted to hold any government position. The head of the government is a triad formed of two high priests and a single junior priest who, in many ways, acts as an apprentice to his superiors. Most of the decision making is handled by the high priests, but the junior priest serves as a tie-breaker as neccesary and is permitted to fill in if one of the high priests is unavailable (due to sickness, travel, being judged no longer fit to rule, or whatever).

The nation is divided into provinces, each of which has its own ruling triad (who are, of course, subordinate to the national ruling triad). Each province is divided into districts (with a governing triad each), and each district is divided into territories (with a governing triad each). Every triad in this sequence has an associated bureaucracy. Most issues are dealt with at the lowest level possible, unless a higher ranked triad takes a personal interest.

While their are a number of punishments for those who break the law, one particular punishment is as infamous as it is unusual; if recommended by a judge and approved by a ruling triad, criminals can be turned into an animal, at least to a degree. This is based on the similarity between sakin and the common domesticated zuzara.

Originally, those so convicted were quietly swapped for the animal in question, but modern medical technology, along with more suspicious commoners, has resulted in the procedure becoming more real. In modern times, the convicted are subjected to surgical procedures to reshape their body into a form more like the zuzara and to have an artificial horn grafted onto their snouts. A branding is applied to mark them for what they are, their ability to speak is removed, they are sterilized, and, often, they receive hormonal treatments to promote more bestial behaviour. Despite sakin lifespans being significantly longer than those of zuzara, those transformed rarely live longer than the animals they're made to resemble; the harsh treatment of their new lives combined with complications and side-effects of the procedures do not make for a pleasant life.

### Immigration and Emigration

Emigration from Zanth Drith is forbidden. The penalty for attempting to leave the country without authorization is death.

Very few people seek to move to Zanth Drith; most "immigration" occurs in one of two ways. The first is by people arriving through mischance; typically, only those whose disappearance is notable are permitted to leave. The second is people kidnapped by the priesthood because of skills or abilities they possess.

Mischance arrives are typically those who became shipwrecked or lost, though most would count those deliberately seeking to move to Zanth Drith in this group as well. New arrives are initial quarantined, the delay allowing the government to see how much of an outcry will occur at their disappearance. Of those not returned, any who show special skills or knowledge will be given a chance to swear themselves to Grutha and, if they do, are admitted to the priesthood at the lowest ranks and are carefully watched over for the rest of their life. Those who don't agree and everyone not judged valuable enough to ask, are sold as slaves to both commoners and priests. Such slaves are branded and required to wear a collar; they have no rights whatsoever. Although being sold to a commoner often means a life of hard work, it is often viewed as the superior choice as commoners are rarely deliberately cruel.

Those who are kidnapped are given the chance to swear to Grutha; those who do are treated as above. Those who do not are imprisoned and put through a brainwashing program until they agree.


## Social Structure

There are two major social classes. One is the priesthood that rules the nation and the other is the commoners. The national god, Grutha, is considered to be above both. One's position in this structure is hereditary; there is almost no social mobility.

Officially, it is impossible for a commoner to join the priesthood. In practice, on rare occasion, an especially talented or competent commoner will be taken by the priesthood and will later reappear with the official story that they had always been a priest. Feelings about this are often mixed, both amongst others and the unofficially-promoted; although priests have a higher quality of life and are guaranteed jobs and homes, they are also subject to the attention and whims of higher ranking priests in a way few commoners are.

It is possible, though rare, for a priest to demoted to the status of commoner. This can only be authorized by a ruling triad superior to individual in question. When done, this is typically done in groups of three and the new group is placed in a different territory than they originated in.

### Commoners

Commoners are expected to obey the priests at all times. Many aspects of their lives are organized and run by the priesthood, though children under eight are ignored; their guardians are responsible for handling them. Adults live in groups of three along with any children. Groups that lose members (to death, for example) often merge so they maintain at least three members.

Once per year, all children who are eight years old are scheduled for an interview with their territory's government and are assessed and divided into groups of three. Ideally these groups consist of two females and a male, but other combinations are acceptable as long as no group consists of only one sex. As an exception to this, those with <a href='sakin.html#zikanthaes-syndrome'>Zikanthae's Syndrome</a> are grouped together whenever possible to encourage its spread. Outside of sex, individuals are assigned to groups randomly.

As the number of eight-year-olds in any given year is generally not an even multiple of three, this typically leaves one or two extras. The fate of these extras is not set by law and is at the whim of the territory triad; they may be sold into slavery, kept as personal "assistants" to the priests, tacked onto a group of three, left single, or just added to the next year's collection of eight-year-olds. Although these groups are assigned at eight, they do not begin living together until the first member reaches puberty.

### Priests

The priesthood is divided into a number of levels; those on a lower level are required to obey the commands of their superiors. Commands can be appealed, but are still expected to be carried out until and unless the appeal is successful, though an exception is sometimes made for commands likely to result in one's own death.

Rank is indicated by a band worn on each arm; these bands are identical and can be changed quickly to accommodate changes in rank. Promotions can be granted by anyone of a higher rank than the promotee is being promoted to with only minor paperwork (with promotions to the uppermost ranks by decree of the ruling triad only), but a demotion was be approved by a high priest. In theory, this encourages those of lower rank to focus on pleasing their superiors. Broadly speaking, rank is divided into three groups, the low priests, the common priests, and the high priests, each of which is divided into nine sub-ranks. This results in a total of 27, or 3 cubed, ranks.

Priests are forbidden from forming permanent familial relationships; although females are expected to have children regularly, these children are given over to specialized nurseries or creches; recent mothers are not permitted to raise children and ties between parent and child are specifically eliminated. There is, by design, a high rate of child mortality; officially this is because the priesthood seeks only the best and brightest children to join its ranks.
