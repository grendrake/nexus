---
category: general
---

<script src='../scripts/timeline-data.js' defer></script>
<script src='../scripts/timeline-core.js' defer></script>

This timeline contains all the events for the setting as a whole; to focus on events relating to a single region, use the keyword filtering options available below. Events on the timeline are categorized into four time periods indicated by the colour behind the event date. The periods are:

<table>
<tr><td style='background:#BB77BB'>&nbsp&nbsp&nbsp</td><td>Pre-Contact</td>             <td>Events before contact with other worlds</td></tr>
<tr><td style='background:#8888CC'>&nbsp&nbsp&nbsp</td><td>Time of Warrothserkm</td>    <td>Events occuring while in contact with Warrothserkm</td></tr>
<tr><td style='background:#00BBBB'>&nbsp&nbsp&nbsp</td><td>Interregnum</td>             <td>Events occuring after the fall of Warrothserkm, but before the modern period</td></tr>
<tr><td style='background:#CC8888'>&nbsp&nbsp&nbsp</td><td>Modern</td>                  <td>Events occuring after contact was re-established</td></tr>
</table>

Not all worlds have events in every period. 
In particular, those worlds uncontacted by Warrothserkm jump straight from "Pre-Contact" to "Modern".

You can filter the timeline by keyword. 
Click one a keyword once to require it, again to forbid it, and a third time to return to ignoring it.

"Time Since" causes the timeline to show how many years before or after the inputed year each event occurred. 
The displayed difference in years is interpreted using the currently selected calendar. 
Clicking on the date for an event in the timeline will set "Time Since" to that year.

<div id='timeline-controls'>
    <div id='timeline-controls-title'>Controls</div>
    <div id='controls-controls'>
        <div>
            <b>Show Eras:</b><br>
            <label><input type='checkbox' id='era-precontact' onclick='toggleEra("pre-contact")' checked> Pre-Contact</label><br>
            <label><input type='checkbox' id='era-warrothserkm' onclick='toggleEra("warrothserkm")' checked> Time of Warrothserkm</label>
            <label><input type='checkbox' id='era-interregnum' onclick='toggleEra("interregnum")' checked> Interregnum</label><br>
            <label><input type='checkbox' id='era-modern' onclick='toggleEra("modern")' checked> Modern Period</label><br>
            <br>
            <label><input type='checkbox' id='show-text' onclick='doHistoryList()' checked> Show Full Event Text</label>
        </div><div>
            Order: <select id='calenderOrder' onchange='doHistoryList()'><option value='asc' selected>Ascending</option><option value='dec'>Descending</option></select><br>
            Calendar: <select id='currentCalender' onchange='doHistoryList()'></select><br>
            Date Range:
                <input type='edit' style='width:3em;' id='minDate' onkeyup='doHistoryList()'>
                &mdash;
                <input type='edit' style='width:3em;' id='maxDate' onkeyup='doHistoryList()'><br>
            Time Since:
                <input type='edit' style='width:3em;' id='fromDate' onkeyup='doHistoryList()' value='1000'><br>
                <br>
            <button onclick='resetSettings()'>Reset View Settings</button>
        </div>
    </div>
    <div class='keyword-header' onclick='toggleElement("keyword")'>Keyword Index <span id='arrow-keyword'>⮟</span></div>
    <div id='zone-keyword'>
        <div style='text-align:center;font-size:80%'>Sort by:
            <button class='tag-sort' onclick='sortTagsName()'>name</button>
            <button class='tag-sort' onclick='sortTagsFreq()'>frequency</button><br>
            <label><input type='checkbox' id='hide-empty-tags' onclick='toggleHideEmpty()'> Hide Tags With Zero Entries</div>
        <div class='keyword-list' id='keyword-counts'></div>
    </div>
</div>

<table id='timeline'></table>
<div id='timeline-results'>
    <span id='resultcount'></span> of <span id='totalcount'></span> shown; generated in <span id='runtime'>ERR</span> seconds
</div>

