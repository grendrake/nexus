---
category: nation
world: va katr
parent_name: Va Katr
parent_addr: vakatr
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Fianalys</th></tr>
<tr><th>Capital:</th><td>Shoivor</td></tr>
<tr><th>Language:</th><td>Riale</td></tr>
<!-- <tr><th>State Religion</th><td>Atheist</td></tr> -->
<!-- <tr><th>Demonym:</th><td>Menotan</td></tr> -->
<!-- <tr><th>Area:</th><td>8,034,000 km<sup>2</sup></td></tr> -->
<!-- <tr><th>Population:</th><td>220 million</td></tr> -->
<tr><th colspan="2" style='text-align:center;font-size:120%;font-weight:normal;font-style:italic;padding-top:0.5em'>Demographics</th></tr>
<tr><th>Garor:</th><td>66%</td></tr>
<tr><th>Rakelm:</th><td>33%</td></tr>
<!-- <tr><th>Density:</th><td>0.44 pop/km<sup>2</sup></td></tr> -->
</table></div>


Fianalys is a prominent nation on the world of [Va Katr](vakatr.md). It, like Va-Katr, is unusual in that it is shared by two different species: the [garor](garor.md) and the [rakelm](rakelm.md). Power in the nation is held almost entirely by the garor, however, with the rakelm population consisting almost entirely of slaves and "rogue" groups.

The originally population of the area consisted entirely of garor; all present rakelm, including those in free populations, are descended from those brought as slaves.

Before contact was established, the general technology level in Fianalys was roughly equivalent to the late medieval or early renaissance period on Earth, but those in power have shown an eagerness to adopt outside ideas and technology which has given the population a significant boost, even if its less of one than they'd have liked given that they have little to trade.


## Interspecies Relations

Two thirds of the population of Fianalys consists of the garor with the remaining third being rakelm. Off-world species do not possess a significant presence; the relatively primitive available technology combined with the arbitrary nature of the government make Fianalys an undesirable travel destination.

Garor of Fianalys view themselves as being the pinnacle of creation although they attempt to disguise this when dealing with off-worlders who are typically more wealthy and powerful than they are. They view rakelm as strictly inferior; though often useful, the rakelm are viewed as a disposable resource with no opinions or ideas of value.

Rakelm in their "proper place" (i.e. enslaved) are expected to be meek and subservient, speaking only when they are spoken to. Free, independent rakelm, without garor to control them, are considered to be subversive and deceitful. Those living in the "free communities" are considered to be the the worst, actively using trickery and deception to overcome garor opposition. That the rakelm are half the size of and considerably more frail than the garor is typically ignored.

Rakelm, in turn, view the garor as oppressive and cruel, but undeniably powerful. They are to be avoided were possible and placated when its not. Most rakelm would relish the opportunity to strike back, but fear reprisals that such an act would bring.

Enslaved rakelm are not trusted by their free brethren, but are not blamed for this mistrust. Most free rakelm either are former slaves or are close to some and are aware of the pressures garor masters can bring to bear. Escaped slaves are often expected to remain free on their own for a time before being accepted into a community for fear of spies or sabotage.

Despite garor views of rakelm, it is possible for free rakelm to obtain enough status and blackmail material to maintain a free and independent existence. Such individuals typically make a show of acquiring and ordering around garor slaves. Other than terminal embarrassment that comes from serving a lesser creature, these positions are some of the easiest and safest a slave can have. Even the highest placed rakelm knows there is a limit to what free garor will tolerate.

### Rakelm Free Communities

Escaped rakelm will often join together and form small communities. As they grow, such communities often merge into larger ones that can better provide for their members. Although a certain amount of raiding occurs, these communities are mostly independent. They typically settle into locations unwanted or inaccessible to others (such as those that can only be reached by flight), both so they'll be left alone and because such locations have the most untapped resources.

Free communities are sometimes raided for slaves, but this is more for appearances than practical value; between their smaller size and their ability to fly, rakelm in these communities are typically well able to avoid slavers. Though not something the general population admits to, garor are sometimes captured during these raids (or, more rarely, at other times).

While the idea of rakelm being able to control garor is often described as absurd---the garor are nearly twice the size of rakelm---it undeniably happens and has been getting easier as technology advances. Most communities control their captives through the use of metal restraints, chains, collars, and sedatives. Cooperation is often rewarded with more, better food and drink. Being uncooperative typically results in a restricted diet and being limited to one's cell.

While they are degraded and belittled, actual physical harm or abuse is uncommon for fear the barons will take a more proactive stance against the free communities.  Captives with powerful or wealthy connections are usually ransomed back pretty quickly. The typical ransom consists of favours and equipment; rakelm communities have little use for outside currency. Those who are not ransomed lead a relatively safe, if uncomfortable and degrading, life performing manual labour. Those with valuable skills may be permitted to use them for the rakelm's benefit instead, most choose not to do so.

Persistent rumours exist that sufficiently unfortunate or downtrodden garor may deliberately seek capture as, while degrading, rakelm captors make extra effort to make sure their captives are physically healthy and receive enough food and water to survive. No evidence exists, but such individuals would be among the least like to be ransomed (or even missed) and the rakelm typically limit communication between captives.


## Marriage

Marriage is viewed as a union between two equals. If the partners were not equal before, the lower is typically elevated to the status of the higher, but there are some cases where the reverse is true. Marriage is not required to be between a man and a woman, though that is the most common case. Same-sex marriage is viewed as being limiting due to the difficult of producing an heir, especially as marriage lasts until death. The survivor of a marriage is permitted to remarry.

Most marriages are arranged for political or personal gain rather than love and typically by parents or by the individuals involved. A small number are mandated by higher authorities, sometimes even barons or the king. While these marriages are technically optional, it is ill-advised to decline.

Despite the arranged and largely impersonal nature of marriage, marital partners are still expected to remain faithful and restrict their sexual activities to each other and their respective consorts. Many marry late because of this; discrete premarital sex is not forbidden allowing the unmarried to be more sexually active.

### Consortships

Individuals (married or otherwise) are also permitted to have a consort-inferior, a partner in an unequal relationship. Consortships are typically formed out of personal desire, power, or lust. Unlike a marriage, a consortship can be broken by either party with fairly short notice.

Most consort-inferiors are young adults of low station who benefit from having an established patron while becoming independent. Consort-superiors are typically older individuals looking for a young man or woman to accompany them socially and share their bed.

Despite the terminology, a consortship is not slavery; a consort-inferior still maintains all their rights, property, and independence though a certain degree of subservience and deference is expected to be shown to one's superior. The main distinction is that any assets that, in a marriage, would be jointly held, and any children produced, belong exclusively to the consort-superior.

A consortship is an unequal relationship between two people; when a consortship is formed, one partner is declared "superior" and the other "inferior". Like marriage, consortships are not restricted to partners of the opposite sex, though those in a same-sex marriage are encouraged to find a consort-inferior of the opposite sex.

Though not required, a consort-superior typically holds a higher status or possesses more wealth than a consort-inferior. The reverse situation is viewed as slightly embarrassing and those involved will avoid bringing it up.

A consort-superior is legally permitted to have multiple consort-inferiors, but this is considered to be tasteless and gaudy. A consort-inferior is permitted only one consort-superior and is not permitted to be married or to have a consort-inferior of their own.


## Government

Fianalys is ruled by a hereditary king or queen. In theory this monarch is an absolute ruler, but in practice the barons are inclined to ignore their royalty, often leaving the king or queen as little more than an arbiter. As the land and armies are held by the barons instead of the crown, it is impossible for the ruler to enforce any decree without support from the barons.

The bulk of the nation is composed of states, each of which is ruled by a hereditary ruler called a baron. The capital of each city is surrounded by thick walls and other defences intended to ward off not only armies but aggressive predators, bandits, and the like. Most homes outside the cities (mostly belonging to peasants) are likewise fortified. These fortifications are often subsidized by the barons whose desire for live peasants able to reliably produce food outweighs their desire not to spend money on the peasants.

It is quite common for neighbouring states to raid or steal steal land from each other. Officially, this is permitted by the king under the belief that it culls the weak and makes those who survive stronger. In practice, the king is not in a position to prevent it.


## Inheritance

Individuals are permitted to specify who their inheritance goes to. It is common to leave small gifts for friends or to specifically exclude or favour hated or beloved relatives. That said, one's inheritance is expected to be distributed in a certain way and if most of one's wealth is distributed otherwise the inheritance may be disputed in court.

In a marriage, the surviving spouse receives the entire inheritance. The inheritance from unmarried individuals is spread over their children with the eldest children getting the most and each successively younger child getting less. The inheritance of unmarried, childless individuals goes to surviving immediate family or to the government if none survives.

Note that inheritance never flows along a consortship; neither a consort-superior nor a consort-inferior are expected to inherit from each other.


## Slavery

Slavery is common in Fianalys and individuals of any species or sex can be enslaved. The traditional way of becoming a slave is to be captured during a cross-state raid and not be ransomed, rescued, or to otherwise escape within a certain time period. This applies both to any locals captured as well as to the raiders themselves. While less traditional, slaves who escape and are not recaptured within a certain time are declared free as well. This originates more from a desire to limit how much time and paperwork is spent on an escape and possibly dead slave than anything else.

While not law, capturing off-worlders as slaves is discouraged and, in more populated and progressive areas, unofficially forbidden. Most people are aware that Fianalys is technologically behind and the good-will and assistance of off-worlders is the easiest way to catch up.

Having sex with slaves is considered to be disgusting, barely one step above having sex with animals. The exception to this is that a slave can be declared a consort-inferior. Being a slave, the slave gets no choice in this.  At this point, the act is viewed only as being desperate or perverted.

The child of a female slave is also a slave and belongs to its mother's owner. Children sired on a free woman by a slave are considered free, but the woman will be avoided if she admits to the identity of the father (most in this situation don't). The exception to this is female slave who is also a consort-inferior; such a child belongs to the consort-superior as normal and this is sometimes used as a way of obtaining an heir.

The official process to free a slave is complex, error-prone, and expensive. Since most people don't want to free their slaves, this is rarely an issue and there is little push for reform. Those few who actually want to free a slave will typically arrange for the slave to "escape" and remain at large for the requisite time rather than deal with the bureaucracy.
