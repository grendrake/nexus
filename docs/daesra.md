---
category: sophant
world: daesra world
notoc: true
parent_name: Daesra World
parent_addr: daesraworld
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Daesra</th></tr>
<tr><th>Homeworld:</th><td><a href='daesraworld.html'>Daesra World</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>2'3" 0.6 m)</td></tr>
<tr><th>Avg. Weight:</th><td>80 lbs (36 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>2</td></tr>
<tr><th>Gestation:</th><td>7 months</td></tr>
<tr><th>Weaned:</th><td>1 year, 2 months</td></tr>
<tr><th>Maturity:</th><td>12 years</td></tr>
<tr><th>Lifespan:</th><td>70 years</td></tr>
</table></div>

The daesra are an unusual species as all members possess psychic powers; they can manipulate objects with their minds and they can send their thoughts into other’s heads.

## Description

<figure class='right' style='width:311px'>
    <img src='/art/species/daesra.png' width='311' height='220'
    <figcaption>
    Male daesra.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

They have a strongly animalistic appearance, often deceiving the unfamiliar into believing that they are nothing more than what they seem. They lack both hands and a voice. Despite appearances, they are intelligent and many can learn to understand spoken language.

The length and density of their yellowish-brown fur changes with the seasons in response to temperature shifts. Those in warm climates have only very short fur while those in cold climates will have long fur. As most wild daesra live in the north, long fur is often viewed as being the more natural. Most individuals have spotted fur, although there is a rare mutation that will cause it to be solid instead.

The daesra are primarily carnivores and prefer a diet of meat, but are entirely capable of eating a more vegetative diet if circumstances warrant. They are able hunters, using their telekinetic ability to disable their prey from a safe distance.

## Reproduction

Daesra experience estrus in early fall and give birth to a small litter of two or three cubs the following summer. The psychic abilities of daesra are erratic when they are young and become more controlled with age and experience.

## Telepathy

The daesra possess an innate telepathic ability to send their thoughts to others, regardless of species. Even animals can receive thoughts this way, although their comprehension is limited. In adults, this is a highly controlled ability and they are able to send to either individuals or to groups. Those not being sent to are not even aware of others receiving the thoughts.

Although fears exist, especially among those who don’t interact with the daesra, that they are capable of reading minds, stealing thoughts, or even controlling one’s thoughts, they are capable of none of these. The receiver can always identify the thoughts as not being theirs, although they may be confused for a time if not familiar with this ability.

Their telepathic ability is mostly sight-based: they can send to anyone they can see, regardless of any barriers that may be present. Maximum range varies between individuals, but is normally between 4 and 5 kilometres (2 1/2 to 3 miles). They are also capable of sending to anyone they are touching. They cannot send to someone who is, for example, behind them, but the person receiving the thoughts does not need to be able to see the daesra.

## Telekinesis

Daesra telekinetic abilities are distinct from what most people think of as telekinesis. All individuals are equipped with six invisible, normally insubstantial ‘tentacles’ that originate from small pits on the individual’s head. Although the tentacles themselves cannot be damaged, damage to these pits will disable them. The tentacles are used to manipulate objects much like hands are (though with less dexterity) and can become substantial at will. Although they cannot do so inside of anything, one portion can become solid while another has passed through something. Their strength varies and can be increased through exercise and use. The daesra do not need to see what they are manipulating, although this (of course) makes it more difficulty. Although their tentacles are invisible, the daesra is always aware of their position.
