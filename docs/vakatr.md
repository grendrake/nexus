---
category: world
world: va katr
---


<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Va Katr</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/vakatr.png'></td></tr>
<tr><th>Star</th>                   <td>G6V</td></tr>
<tr><th>Year Length:</th>           <td>359.41 local days<br />335.18 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>286 K (12.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>1.07 G</td></tr>
<tr><th>Day Length:</th>            <td>22.3854 h</td></tr>
<tr><th>Moons</th>                  <td>None</td></tr>
<tr><td colspan='2' class='wide-data'>{% link va katr system %}</td></tr>
</table></div>


Va Katr is the fifth world around an old G6 star. Its star is part of a multi-star system; the closest companion star has a habitable planet in orbit, but, due to the low technology available on Va Katr, no expeditions or extended monitoring have occurred and it is not known if the other world possesses a native sapient species.

Va Katr is unusual in that it possess two intelligent species: the [garor](garor.md) and the [rakelm](rakelm.md). The gate is located in the garor nation of [Fianalys](fianalys.md).