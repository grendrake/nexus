---
category: world
world: ujan
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Ujan</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/ujan.png'></td></tr>
<tr><th>Star</th>                   <td>G8V</td></tr>
<tr><th>Year Length:</th>           <td>164.43 Local days<br>172.32 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>298 K (24.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>1.02 G</td></tr>
<tr><th>Day Length:</th>            <td>25.00 h</td></tr>
<tr><th>Moons</th>                  <td>None</td></tr>
<!-- <tr><td colspan='2' class='wide-data'>{% link zifaelafojun system %}</td></tr> -->
</table></div>


