---
category: sophant
world: interworld nexus
parent_name: Interworld Nexus
parent_addr: overview
---


The vorsyth are the now-absent builders of the Interworld Nexus. As the Nexus is believed to have been built about ten-thousand years ago, this is also the most recent time they're known to have been active. Many also believe that the vorsyth also played a role in shaping and evolutionary history and possibly even the homeworlds of the various sophants present on the nexus, but the only actual evidence of this is the surprising similarities between the sophants. Neither the goals of the vorsyth in this nor the reason they disappeared is known.

Almost nothing is actually known about the vorsyth; even the name "vorsyth" is just what [Warrothserkm](warrothserkm.md) called them. This has lead to a large number of theories about them, most with no evidence for them. A popular theory is that the [syri](syri.md) either are the vorsyth or are their descendants, though the normally reticent syri entirely deny this. The [zaeren](zaeren.md) are sometimes proposed as vorsyth descendants as well due to their long presence on the Nexus and lack of a known homeworld, but those making such a suggestion are typically mocked as the less-then-favourable status of the zaeren is pointed out.

A measure of distance called the [nalwen](serkmhub.md#common-system) is believed to have been used by the vorsyth because of the frequency with which it occurs in Interworld Nexus design. Likewise, the set of symbols used to identify specific gates are often believed to be based on numbers and/or letters used by the vorsyth. These symbols are the only form of writing that has ever been discovered and it remains unclear what, if any, meaning they had.

Cases of people coming forth and claiming to have discovered vorsyth artifacts regularly. The vast majority of the cases are eventually demonstrated to be hoaxes or errors; the more unusual artifacts produced by Warrothserkm are particularly prone to being passed of as vorsyth. The remainder of cases have been inconclusive. No artifacts to date have been determined to be genuine and no evidence of modern vorsyth activity has stood up to scrutiny. Perhaps because of this lack of real information, vorsyth often feature at the centre of conspiracy theories and urban legends.
