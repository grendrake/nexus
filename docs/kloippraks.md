---
category: world
world: kloip praks
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Kloip Praks</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/kloippraks.png'></td></tr>
<tr><th>Star</th>                   <td>K6V</td></tr>
<tr><th>Year Length:</th>           <td>55.28 Local days<br>124.28 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>278.81 K (5.66 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>1.08 G</td></tr>
<tr><th>Day Length:</th>            <td>53.96 h</td></tr>
<tr><th>Moons</th>                  <td>None</td></tr>
<!-- <tr><td colspan='2' class='wide-data'>{% link zifaelafojun system %}</td></tr> -->
</table></div>


