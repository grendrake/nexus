---
category: god
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
---

<div class='infobox'><table>
    <tr><th class='top-header' colspan='2'>Tika</th></tr>
    <tr><th>Nraezen</th>        <td><bdo class='nexusFont lang_nr'></bdo></td></tr>
    <tr><th>Religion</th>       <td>Former state religion of <a href='razaznudar.html'>Razaz Nudar</a></td></tr>
    <tr><th>Title</th>          <td>the Burning Destruction</td></tr>
</table></div>

The core belief in the worship of Tika is that the world is impure and must be purified in flame.
In the eyes of Tika, everyone and everything is impure.
Even the priesthood was expected to self-immolate if a natural death was approaching, though most were able to arrange for other, less unpleasant ends.

Tika was known as the god of fire and destruction; all of His rites involved one or the other.
Living sacrifices were also required for His holy days.
The priesthood preferred to use enemies for this, but Tika Himself was said to be fine with anyone of good health.

Tika was originally the patron god of [Tkinae's](tkinae.md) band.
When Tkinae forged multiple bands into the fledgling empire of [Razaz Nudar](razaznudar.md), the worship of Tika was mandated by the government and the priesthood served as ministers of state.

When Rekuzarae became emperor the worship of Tika was banned entirely and the top priesthood publicly executed.
Because of the increasing enthusiasm of the priesthood for ``purifying'' those they found inconvenient, this ban was widely accepted by the population.
The surviving priesthood was rather less pleased, but with neither public nor government support, there was little they could do and those who tried discovered Rekuzarae's willingness to make his opponents vanish in the night.

In modern times, the worship of Tika remains banned.
Most have heard third or fourth hand stories about Tika revivals, but this is largely regarded as being the domain of conspiracy theorists.
Very few living people want anything to do with Tika worship.
