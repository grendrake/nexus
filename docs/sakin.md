---
world: kzaknri
category: sophant
parent_name: Kzaknri
parent_addr: kzaknri
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Sakin</th></tr>
<tr><th>Homeworld:</th><td><a href='kzaknri.html'>Kzaknri</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>5' (1.5 m)</td></tr>
<tr><th>Avg. Weight:</th><td>127 lbs (58 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Clutch size:</th><td>2</td></tr>
<tr><th>Gestation:</th><td>3 month</td></tr>
<tr><th>Incubation:</th><td>5 months</td></tr>
<tr><th>Maturity:</th><td>13 years</td></tr>
<tr><th>Lifespan:</th><td>120 years</td></tr>
</table></div>

Sakin (Nraezen: <bdo class='nexusFont lang_nr'>&#xe008;&#xe004;&#xe005;</bdo>) are a pseudo-reptilian species originating from the world of [Kzaknri](kzaknri.md). Their bodies are covered in small, yellow-green scales that are shed in large patches. This occurs every two to three years in adults but is more frequent in children, especially young children. Both adults and children are [crepuscular](https://en.wikipedia.org/wiki/Crepuscular) and thus most active during twilight. As a result, they have strong night vision, but a sensitivity to bright light, including direct sunlight.

Most male sakin and a few female sakin have red spots on their tail, their thighs, their shoulders, and the back of their next. Although this is often taken as a sign of an individual's sex, it is not a reliable indicator. There is no reliable way to determine an individual's sex without asking or performing an invasive physical check.

The sakin snout has little in the way of facial muscles resulting in a limited repertoire of facial expressions. This is replaced by rich body language that particularly focuses on the ears and tail. Their dentition is limited as well with only a single kind of tooth. These teeth are small and pointed, intended to hook into and hold food to be torn free rather than chewing. Each tooth is replaced every few years; common dental practice is to simply extract any tooth that develops problems.

<figure class='right' style='width:242px'>
    <img src='/art/species/sakin.png' width='242' height='434'>
    <figcaption>
    Sakin in typical Razaz Nudar dress.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

Contrary to popular belief, sakin are not <a href='https://en.wikipedia.org/wiki/Cold-blooded'>cold-blooded</a>. That said, their ideal temperature range is both smaller and higher than most other species and their bodies often respond slowly to changes in temperature; both of these can result in actions and situations that suggest they're cold-blooded.

Sakin are well known for their sensitive hearing and sense of smell as well as their below-average visual acuity. They're also completely colour-blind, but most individuals in mixed-species cultures are sufficiently practiced at living in a world built on colours that this is often not obvious. Due to their crepuscular nature, sakin have excellent night vision, but suffer from a sensitivity to bright lights; even the midday sun on many worlds is uncomfortable for them.

Although the typical lifespan for sakin in [Razaz Nudar](razaznudar.md) is about sixty years, this is caused by cultural factors; the most common cause of death is violence or accidents. In ideal conditions, they can easily live for over a century during which they experience <a href='https://en.wikipedia.org/wiki/Negligible_senescence'>negligible senescence</a> (that is, they show few signs of ageing). That said, plenty of health problems exist that are unrelated to ageing that can still cause death or incapacity.


## Psychology and Behaviour

Sakin are often believed to be volatile and lecherous. Neither belief is entirely without merit, though they are not entirely accurate, either. They have an impulsive nature that leads them to act on their emotions rather than waiting or considering things logically. This makes their emotions, and the changes in them, more obvious than with other, less demonstrative species.

Their lecherous reputation does not come from being more sexually active than other species, but is a combination of factors. One is that the best known sakin cultures (principally Razaz Nudar) are very open about sexuality which results in more discussion of sexual topics. The other is that while sakin become aroused less often than other species, this arousal is stronger. Thus, a given individual will not typically demonstrate much interest in sex, but will periodically develop a much stronger interest. Subtle, unconscious body language and pheromonal cues will cause sakin in regular contact to gradually synchronize these periods of arousal with each other and to the fertile periods of females in the group.


## Diet

The evolutionary ancestors of sakin were scavenging carnivores and the sakin remain obligate carnivores. They also retain scavenging related traits that protect them from disease, such as highly acidic stomach acids and a powerful immune system.

Although some sakin are known to include small amounts of plant matter in their diet, sakin cannot readily digest this material and will experience gastronomic distress if plant matter makes up more than a tiny fraction of their diet.


## Reproduction

Sakin are promiscuous by nature and do not experience any special bonding with their sexual partners. For most, watching others have sex is not only arousing but makes the current participants more attractive and desirable.

Female sakin lay clutches of three. They are fertile about every six Earth months and both sexes experience increased arousal during this time. Female sakin can recognize the signs associated with this their fertility period and avoid sex with male sakin as a form of birth control. Male sakin remain fertile all year.

Sakin genitalia is internal and not normally visible. When aroused, the penis emerges from the body, but sakin are capable of learning to suppress this and prevent an erection from occurring. In most cultures, this is something that is expected to be learned as a child. When the penis is not erect, male and female genital slits are nearly indistinguishable and both can be penetrated in the same fashion.

Hatchlings have an egg-tooth that is used to break free from their egg. This is normally lost after a couple weeks. They are 12-16 inches when they hatch, but develop quickly for the first few of years, and soon surpass the growth human offspring of a similar age. Hatchlings are not nursed; they eat the same food as their parents, though in smaller pieces.

### Zikanthae's Syndrome

About 1 in 2,500 sakin have Zikanthae's Syndrome. Those affected possess primary sexual characteristics from both sexes. Specifically, they develop both a penis and a uterus as well as both a testicle and a ovary. As a result, those with Zikanthae's Syndrome are fully functional hermaphrodites, though in all known cases they have with reduced fertility. The children of those affected by Zikanthae's Syndrome are more likely to be affected themselves. Due to the sakin genital arrangement, this condition is not always obvious, even when examining the naked body.

Some researchers believe the condition is more common than currently believed; relatively few sakin undergo extensive medical examinations, especially in isolated or poverty-heavy areas.
