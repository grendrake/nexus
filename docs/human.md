---
category: sophant
world: earth
parent_name: Earth
parent_addr: earth
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Human</th></tr>
<tr><th>Homeworld:</th><td><a href='earth.html'>Earth</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>5'6" (1.7 m)</td></tr>
<tr><th>Avg. Weight:</th><td>3136 lbs (59 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>1</td></tr>
<tr><th>Gestation:</th><td>9 months</td></tr>
<tr><th>Weaned:</th><td>3 years, 6 months</td></tr>
<tr><th>Maturity:</th><td>17 years</td></tr>
<tr><th>Lifespan:</th><td>90 years</td></tr>
</table></div>

Humans are a mammalian species notable for lacking any form of snout or tail, features otherwise quite common. Their skin is covered in only sparse hair, save for the tops of their head, their groin, and their armpits.
