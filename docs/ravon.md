---
category: sophant
world: zifaelafojun
parent_name: Zifaelafojun
parent_addr: zifaelafojun
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Ravon</th></tr>
<tr><th>Homeworld:</th><td><a href='zifaelafojun.html'>Zifaelafojun</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>2' (0.6 m)</td></tr>
<tr><th>Avg. Weight:</th><td>48 lbs (22 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>1</td></tr>
<tr><th>Gestation:</th><td>6 months</td></tr>
<tr><th>Weaned:</th><td>6 years</td></tr>
<tr><th>Maturity:</th><td>20 years (male)<br>25 years (female)</td></tr>
<tr><th>Lifespan:</th><td>175 years</td></tr>
</table></div>

Ravon are notable for being one of the few sophants capable of flight.
They are stereotyped as being intelligent, vain, and delusional and not without cause.
They are also averse to physical aggression, rarely engaging in it and finding it unpleasant to experience.
This doesn't mean that they are incapable of it, though, given the opportunity, they typically approach problems from an indirect angle.

Ravon have excellent vision, suitable for watching the ground while in flight, but their other senses are unremarkable.

## Description

<figure class='right' style='width:434px'>
    <img src='/art/species/ravon.png' width='434' height='360'>
    <figcaption>
    Male ravon with female headshot.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

Ravon are bipedal non-humanoids.
Other than their wings, their bodies are theropod-like.
When startled or frightened they spread their wings outwards, making themselves appear larger.
Though somewhat flexible, their tail is usually held stiff behind them when standing or walking.

The best way to visually distinguish the sex of a ravon is by their horns.
Female ravon have curled horns similar to those of a ram while male ravon possess short, straight horns.
Both sexes possess a pair of nipples just forward of their thighs, but only female ravon are known to lactate.
They are completely hidden by fur while not lactating.


## Diet

Their diet consists primarily of fruits and other vegetive foods and is supplemented with small animals and insects.
Modern ravon may found consuming larger portions of meat but this is a modern change to their diet.


## Reproduction

Gestation is about six months after which infants are born in an undeveloped state.
They remain close to their mother for their first year.
During this time, the infant does little more than nurse while undergoing physical and mental development.
The body size of the infant increases slowly during this time, remaining small and light enough that they can be easily carried by their mother, even while in flight.

Offspring are almost always born singly; twins account for less than 1 in 10,000 of all pregnancies.
Individuals nursing multiple children are usually acting as a wet nurse.

After the first year, the infant will take an interest in the world around them, though physical and mental development still proceeds slowly.
Breastfeeding continues for multiple years, supplemented with solid food after about one and a half years.

Physical maturity occurs at about 20 years (for males) or 25 years (for females), but they remain infertile for up to two more decades.

Ovulation occurs in early summer and winter, but only in individuals who are not nursing.
Despite this limited ovulation, ravon have one of the higher libidos.
Unusually, it peaks in spring and autumn rather than when they are fertile; this is often considered to be why they have such a low population.

