---
category: sophant
world: va katr
parent_name: Va Katr
parent_addr: vakatr
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Rakelm</th></tr>
<tr><th>Homeworld:</th><td><a href='vakatr.html'>Va-Katr</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>4' (1.2 m)</td></tr>
<tr><th>Avg. Weight:</th><td>85 lbs (39 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>2</td></tr>
<tr><th>Incubatoin:</th><td>2 months</td></tr>
<tr><th>Gestation:</th><td>6 months</td></tr>
<tr><th>Maturity:</th><td>15 years</td></tr>
<tr><th>Lifespan:</th><td>83 years</td></tr>
</table></div>

Rakelm are often perceived as being meek and subservient.
This is a trained behaviour of the population around the gate area of Va-Katr.
In that area, rakelm are primarily kept as slaves and are specifically bred and trained for this behaviour.
Free populations are more assertive, but as they share their homeworld with the larger and more aggressive [garor](garor.md), they still appear submissive by comparison.

They are capable of flight, though they are clumsy in the air.
This trait made them valuable to the garor&mdash;couriers capable of flight have a distinct advantage&mdash;and resulted in their enslavement en mass.

## Description

<figure class='right' style='width:223px'>
    <img src='/art/species/rakelm.png' width='223' height='434'>
    <figcaption>
    Androgynous rakelm.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

Rakelm are short bipeds whose arms double as wings.
Their bodies are covered in short, light blue fur.
As a result, they are often viewed as being mammalian despite lacking other mammalian traits.
Wing membranes connect between their arms and the sides of their bodies and their long tails end with a spade.

Rakelm are androgynous with little to mark the difference between the sexes.
Female rakelm are slightly bulkier and possess wider hips.

## Diet

Rakelm are omnivorous but traditionally have a very meat-heavy diet.
Historically, most of their food consisted of small animals.

## Reproduction

Rakelm are oviparous, laying eggs not long after conception.
The resultant hatchlings are not nursed at any point but are given food that is pre-chewed or cut into tiny pieces.
It is not uncommon for rakelm eggs to be taken from their parents before hatching; in some areas, this has occurred often enough and for long enough that it has become an accepted part of life.
