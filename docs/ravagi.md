---
category: language
world: raesan vie
parent_name: Raesan Vie
parent_addr: raesanvie
---

 <img src="/art/warning.svg" style="float:left;border:0;padding-right:10px" height="30">
 <p style='text-indent:0;font-weight:bold;font-style:italic'> Note that the contents of this section are up for revision due to my greatly increased understanding of linguistic from when I originally wrote it. Not to mention a number of changes to the setting.</p>

Ravagi is the language native to the [asuni](asuni.md) of [Raesan Vie](raesanvie.md). Although still spoken everywhere, in many of the areas
with more outside contact the language is being supplanted by non-native
languages.

There are two major dialects to Ravagi. One is used exclusively by the shamans
and the other is used by everyone else. Shamans will typically speak both
dialects clearly, along with whatever other languages are in use in the area.
Most people will only speak the one. The shamans claim that using any other for
their practices would not be practical.

The 'common' dialect is focused primarily on the dialect used near where other
species contact has occurred; Raesan Vie is large enough that usage can vary
considerably as one gets further away from this area.


## Phonology

Ravagi has three vowels and three tones: high, low and neutral. Each of the
vowels has a different sound depending on its position in a word.

Keep in mind, this is the most common dialect of the language. Individuals in
other parts of Raesan Vie may speak differently.

| Vowel | Initial      | Medial         | Final         |
|-------|--------------|----------------|---------------|
| A     | ah (*aw*ful) | Aa (apple)     | Ae (apron)    |
| O     | Oe (*o*pen)    | Oo (f*oo*l)    | Uh (*o*ven)   |
| E     | Ee (*e*vil)  | Eh (*e*lavate) | Ih (*i*gnite) |

<!-- Ravagi Vowels (sans tones) -->

There are three tones: high, low and neutral. High and low are fairly
self-explanatory. The neutral tone is also sometimes called a medial or middle
tone.

In addition to the vowels, there are nine basic consonants and five valid
intrasyllable clusters.

<table>
    <tr><th colspan='3'>Consonants</th> <th colspan='2'>Clusters</th></tr>
    <tr><td>G</td><td>N</td><td>R</td>  <td>Gn</td><td>Rs</td></tr>
    <tr><td>L</td><td>J</td><td>P</td>  <td>Rn</td><td>Rv</td></tr>
    <tr><td>T</td><td>S</td><td>V</td>  <td>Sn</td><td></td></tr>
</table>

<!-- Consonants and Clusters -->

|                  | Bilabial   | Dental     | Alveolar | Velar | Uvular | Post-Alveolar |
|------------------|------------|------------|----------|-------|--------|---------------|
| Plosive          | X (p)      | (t/), (/g) |
| Nasal            | (/n)       |            |
| Frictive         | (/v), (s/) | (/r), (/j) |
| Lateral Frictive | (/l)       |            |

<!-- % Updated IPA version for consonants -->


## Transcription

Ravagi has no general written language. There is a simple logographic system
used by the shamans, but this use is limited and not readily usable to others.
Thus, I present here a system for transcribing ravagi. This is by no means the
only way, or even the best, but it is the system I shall be using throughout
this document.

The three vowels are written as a, o or e respectively. Their high and low tones
are indicated by an acute (á) and grave (à) accent respectively. The neutral
tone is unmarked.

I realize that the acute and grave accents can be difficult to type on some
keyboards. If you would prefer, a (preferably) subscript 1 can be used for the
low tone, a 2 for the high tone and either nothing or a 0 for the neutral tone.

Consonants (and clusters) are written as they appear in the chart above.

## Sound Change

The same vowel cannot occur twice in a row. If it does because a word is a
compound or because of an affix, it is changed as per the following.

'''
A -> E -> O -> A
'''

If an affix causes two vowels to be placed next to each other, the vowel of the
affix is to be dropped.

## Morphology

Valid syllable forms are CV CVC VC VCV. The same vowel sound cannot occur twice
in succession. If it does, see the Sound Change (above) section for how to deal
with it.

Consonants may cluster only as seen in the consonant chart or across the break
of a syllable.

Nouns always end with a vowel.

Verbs always end with a consonant.

Stress occurs.

## Nouns

### Plurality

In the standard dialect of Ravagi, nouns come with three numbers: there can be
none, one or many of it.

The shamanistic dialect uses a couple of additional numbers as well: double and
triple.

### Gender

There are five genders. Gender is not marked and is used only for pronouns and
articles. The genders are: male, female, child, animal, and object. The
offspring of animals is typically still considered to be the 'animal' gender.

### Case

| Case     | Suffix |
|----------|--------|
| Genitive | -n     |
| Dative   | -ra    |

<!-- Case Endings -->


## Articles

Both genders have their own articles. The articles also come in definite and
indefinate forms.

|      | Male | Female | Child | Animal | Object |
|------|------|--------|-------|--------|--------|
| Def. | Gna  | Rna    | Su    | Ro     | Gnu    |
| In.  | Ja   | La     | Sa    | Ve     | Ju     |

<!-- Articles -->

## Postpositions

These are brief words that modify the word they follow, but which do not
actually join with it. Typical examples are the English words on, above and
before.

## Adjectives

Adjectives are not a separate word class, rather they are a particular form of
noun. There is, for example, a noun meaning 'largeness' that can be turned into
describing words (adjectives or adverbs).

Adjectives follow the noun and are separated by the conjunction sae.

## Pronouns

Asuni pronouns distinguish between person, number and gender.

| Male | Female  | Child   | Animal | Object |
|1s    | Ora/Oru | Ori/Ovi |        |        |
|2sk   |

## Verbs

### Mood

Most moods are indicated by a prefix to the verb. One, and only one, of these
moods must be present with all verbs. Some moods are indicated by an additional
particle placed before the verb; these moods are used in addition to the basic
moods.

Factual
: (va-) The action is real and certain. *He is running. I am fishing.*

Obligative
: The action should be done. *He should mean his socks. I should eat dinner.*

Repuative
: The action is hearsay or assumed. *Tom is supposed to be a good runner. Jack supposedly fell down the hill.*

Possible
: (o-) The action might occur. *I might hunt. He might dance.*

Neccessative
: (sa-) The action must occur. *You must run. I must live.*

Approximate
: The action appears to occur. *He seems to be running.*



Hortative
: The action is desired; requesting, permission, allowance. *Let me run. Allow him to live.*

Interagative
: Question; did the action occur? *Did you clean behind your ears? Is he sorting trash?*

Negation
: (na) The action did not occur. *I am not running. He is not dying.*


### Tense and Aspect

The tense and spect combine to form a particle that comes after the verb. If the
action is complete (perfect), an additional 'ie' sound is added to the end of
the particle.

<table>
    <tr><td>Future (tomorrow)</td> <td></td>  <td>Future (later today)</td> <td></td></tr>
    <tr><td>Future (general)</td>  <td></td>  <td>Now</td> <td></td></tr>
    <tr><td>Past (yesterday)</td>  <td></td>  <td>Past (general)</td> <td></td></tr>
    <tr><td>Past (today)</td>  <td></td>  <td>Past (general)</td> <td></td></tr>
    <caption>Tense - Succeeding Particle</caption>
</table>

<table>
    <tr><td>Ongoing</td> <td></td> <td>Single Action</td> <td></td></tr>
    <tr><td>Repeated (do again)</td> <td>-al</td> <td>Beginning</td> <td></td></tr>
    <tr><td>Ending</td> <td></td> <td>Complete</td> <td>-i</td></tr>
    <caption>Aspect - Succeeding Particle (suffix)</caption>
</table>


### Forms

Impersonal verbs are verbs which do not require either object or subject
(Spanish '*llover*' [to rain]). Reflexive verbs are verbs that describe
something happening to the subject. Transitive verbs are verbs that require a
subject and an object. Intransitive verbs require only a subject. Ditransitive
verbs require two objects (like for the English 'give'--'She gave me her
books'). Most verbs can be used in only one form. The main restriction is on
whether the action would make sense in that form.

## Participles

### Adjectival

Although there are no adjectives in the asuni language, nouns can become verbs
by the addition of the correct affix (or particle). Adding this affix to the
noun 'large' would produce 'to be large'.

### Nominal

Likewise, verbs can become nouns through the addition of the correct affix (or
particle). Ths would make 'to hunt' into 'the hunting'.

### Conjunctions

And
: (sae, lu) Sentences can be joined together by the particle sae, as can clauses. Words use the particle li instead, but it functions identically. The particle is placed between the two things needing joining. Longer lists have the particle inserted between all the items.

So
: Joins two sentences in a cause and effect relationship. The first is the cause of the second. ('I am tired, so I am going home.')

Or
: Either of the constituents are correct, or both are.

Nor
: ?

Xor
: ?

## Grammer

Ravagi uses the VSO word order. Both verbs and nouns are head-first, meaning
that any modifiers (adverbs, adjectives) come after the verb or noun. THe word
order is absolutely fixed, however, as neither the subject nor the object are
marked; they are indicated only by their position in the sentance.

When preseent, the indirect object of a sentance is tacked onto the end of the
sentnace. Unlike the direct object and subject, the indirect subject is in the
dative case and given the '-ra' suffix.

## Clauses

### Subordinate

Subordinate clauses come after the noun they modify. The following is quoted from https://www.angelfire.com/ego/pdf/lng/draseleq/dg_misc.html (no longer available) under Subordinate Clauses.

> **Subordinate Clauses** in Drasel\'eq come after the noun phrase they modify (only exception to the head-final rule). To understand how a subordinate clause works, first let's look at the English construction, for example: 'The woman who left is my mother'. We can think of this as a sentance embbed into another: 'The woman [she left] is my mother'. 'The woman is my mother' and 'She left' are two perfectly possible sentnaces. The two of them are joined by the presence of a relative pronoun, 'who', which replaces the personal pronoun 'she' in the inner (subordinate) sentance.

Subordinate clauses replace the subject of the clause with the relative form of
the appropriate pronoun (see pronoun chart). The clause is then dropped in after
the noun, although the noun does not receive the genitive case.

## Very Old Vocab List

Sae
: conj. Connects sentences, clauses, and words

Gae
: part. Indicates possessiveness towards adjectives

A'su'ni
: n. The people; the local sapient species

Ar'na'ja
: n. A small borrowing animal. Herbivorous. Mainly eats fruits and nuts. Very quick and nervous.

As'na
: n. Eye, organ of sight

Ava'ru
: post. A giver, sender, or source. (from the store, from Fred, etc.)

Ev'a'ju
: n. Keenness, sharp, functioning well

Ga'jer'al
: vt. To banish, as of spirits. This is not unsummoning; the spirit does not go back to where it came from if it had been summoned.

Ga'le'ra
: n. Tooth

Ga'rel
: vt. To die. Lit. to have one's spirit leave one's body. Not necessarily permanent.

Ga've'tu
: n. Tail. Extension of the spine past the hips.

Ga'ver
: vi. To have one's spirit enter their body, both at conception and after death.
: vt. To have a spirit enter a body, not necessarily its own, and not necessarily one that is alive. This is used to indicate a spirit possessing someone.

Gan
: n. A name.
: part. Affixed to end of surname.

Gap'ar
: v. To pull

Gar'al
: vt. To force a spirit to leave a body, principally as an excorsism. Expelling the only spirit present kills the subject unless the spirit can re-enter the body.

Ge'nav
: vt. To bite. To grip with the teeth. To sink teeth into.

Gur'in
: vt. To wear. To put on, as of clothing.

Ja'pa'la
: n. Spitefulness. Misintent. Ill-willed. Mis-tempered.

Jan'tal'la
: n. An edible tuber.

La'se
: n. The body in total, but without an occupying spirit. A corpse.

La'su
: n. The body in total, including an occupying spirit. A living body.

Lu'sa'ga
: n. A monster. A freak. An abomination.

Ra'sa
: n. Land. Especially a large area of land.

Ra'va'gi
: n. Name of this language.

Rsav
: vt. To fasten with laces or cord, as of a vest or pack.

Ru'sa
: n. Unknown or forgotten.

Ru'sav
: v. To forget. To not remember.

Rva'ju
: n. Largeness. Large.

Rva'ti
: n. A female asuni. A woman.

Rva'tu
: n. A male asuni. A man.

Sa'ge'ja
: n. The spirit of a mortal creature.

Sa'ge'ja'ra
: n. A spirit that is not normally bound to a mortal or living form.

Sa'ge
: n. A spirit in general.

Saj'pa
: n. A sleeveless garment worn over the torso and extending to just before the waist. A vest, jacket, or coat.

Va'jel'la
: n. A shaman-priest, spiritualist, summoner, etc. A person who communicates with the spirits and can effect them and his own.

Jav'ar
: v. To hunt, as of animals or food.

Val'aj
: v. To search for. To look for. To look in.

Vi
: n. Completeness. Entirety.
: n. Without exception or error
