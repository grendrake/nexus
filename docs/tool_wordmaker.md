---
category: tool
---

<p>This tool is designed to generate words suitable for the various conlangs used by the Interworld Nexus setting.
That said, the generation algorithm is relatively primitive and generates large numbers of unsuitable words so the output will need to be checked over to elimate inappropriate or unpronouncable words. Additionally, there are no checks to prevent the duplication of existing words in a language's lexicon.

<p>Actual use is simple: select the desired language, the number of words to create, and hit the "Generate" button.

<hr>


<script>

const wordData = {
    "Low Drius": {
        "word_syl_chance": 25,
        "word_syl": [ "CVCCC", "CCCCV" ],
        "max_syl": 3,
        "syl": [ "VCV", "VCCC", "VCC", "VC", "CVCC", "CVC", "CCV" ],
        "V": [ 
            [ "a", 43],     [ "i", 61 ],    [ "e", 77],     [ "o", 89 ], 
            [ "u", 101 ] 
        ],
        "C": [ 
            [ "r", 19 ],    [ "s", 33 ],    [ "n", 45 ],    [ "g", 54 ],
            [ "k", 63 ],    [ "v", 71 ],    [ "d", 77 ],    [ "l", 83 ],
            [ "t", 88 ],    [ "th", 93 ],   [ "w", 97 ],    [ "b", 98 ],
            [ "m", 99 ]
        ]
    },
    "Nraezen": {
        "word_syl_chance": 0,
        "max_syl": 3,
        "syl": [ "CVC", "CV", "CCV" ],
        "V": [
            [ "a", 34],     [ "u", 59 ],    [ "i", 80],     [ "ae", 100 ],
        ],
        "C": [ 
            [ "th", 11 ],   [ "n", 22 ],    [ "r", 33 ],    [ "t", 43 ],
            [ "k", 53 ],    [ "l", 63 ],    [ "d", 72 ],    [ "z", 80 ],
            [ "g", 86 ],    [ "j", 92 ],    [ "y", 98 ],    [ "nr", 101 ],
        ]
    }
};

function rndArray(arr) {
    return arr[rnd(arr.length)];
}
function rnd(max) {
    return Math.floor(Math.random() * max);
}

function makeSyl(pattern, data) {
    const res = [];
    for (let i = 0; i < pattern.length; ++i) {
        const v = rnd(100);
        const vals = data[pattern[i]];
        for (let j = 0; j < vals.length; ++j) {
            if (v < vals[j][1]) {
                res.push(vals[j][0]);
                break;
            }
        }
    }
    return res.join("");
}
function generate() {
    const output = document.getElementById("output");
    if (!output) { console.error("Output element not found."); return; }
    output.innerText = "";
    const language = document.getElementById("language").value;
    if (!language || !wordData.hasOwnProperty(language)) {
        const errMsg = "Invalid language \"" + language + "\".";
        output.innerText = errMsg;
        console.error(errMsg);
        return;
    }
    let count = parseInt(document.getElementById("wordCount").value);
    if (!count || count < 1) count = 1;

    const data = wordData[language];

    for (let z = 0; z < count; ++z) {
        if (rnd(100) < data.word_syl_chance) {
            const syl = rndArray(data.word_syl);
            const s = makeSyl(syl, data);
            output.innerText += s + " (" + s + ")\n";
        } else {
            const syl_count = 1 + rnd(data.max_syl);
            const syls = [];
            for (let i = 0; i < syl_count; ++i) {
                const syl = rndArray(data.syl);
                syls.push(makeSyl(syl, data));
            }
            output.innerText += syls.join("") + " (" + syls.join("-") + ")\n";
        }
    }
}

</script>



<div style='display:flex;width:100%;justify-content:space-between'>
<div><label>Language: <select id="language" title="Language to generate words for">
    <option>Low Drius</option>
    <option>Nraezen</option>
    </select></label><br></div>
<div><label>To Make: <input type='edit' id='wordCount' value='10' title="Number of words to make"></label><br></div>
<div><button onclick='generate()'>Generate</button></div>
</div>

<div id='output' style='border-top:1px solid black;border-bottom:1px solid black;margin-top:1em;'></div>
