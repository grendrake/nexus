---
category: world
world: rynux
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Rynux</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/rynux.png'></td></tr>
<tr><th>Star</th>                   <td>G6V</td></tr>
<tr><th>Year Length:</th>           <td>348.59 Local days<br>347.59 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>286 K (12.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>0.72 G</td></tr>
<tr><th>Day Length:</th>            <td>23.93 h</td></tr>
<tr><th>Moons</th>                  <td>None</td></tr>
<!-- <tr><td colspan='2' class='wide-data'>{% link zifaelafojun system %}</td></tr> -->
</table></div>


