---
category: nation
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
---

<figure class='right' style='width:99px'>
    <img src='/art/flags/razaznudar.svg' width='99' height='200'>
    <figcaption>
    Flag of Razaz Nudar.<br>
    Art © <a href='https://www.grenslair.com/'>Gren Drake</a>
    </figcaption>
</figure>

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Razaz Nudar</th></tr>
<tr><th>Capital:</th><td>Reaklin</td></tr>
<tr><th>Language:</th><td><a href='nraezen.html'>Nraezen</a></td></tr>
<tr><th>Number Base</th><td>Six</td></tr>
<tr><th>State Religion</th><td>Atheist</td></tr>
<!-- <tr><th>Demonym:</th><td>Menotan</td></tr> -->
<!-- <tr><th>Area:</th><td>8,034,000 km<sup>2</sup></td></tr> -->
<tr><th>Population:</th><td>220 million</td></tr>
<!-- <tr><th>Density:</th><td>0.44 pop/km<sup>2</sup></td></tr> -->
</table></div>

The area of Razaz Nudar (Nraezen: <bdo class='nexusFont lang_nr'>&#xe026;&#xe02e;&#xe008;&#xe000;&#x200B;&#xe023;&#xe012;&#xe006;</bdo>) is known as the birthplace ("Hatching Grounds") of the sakin.

The nation is possibly the nation on Kzaknri most familiar to offworlders, not the least because it is where the gate is located. It is known for many things: having one of the shortest legal codes of any modern nation, low levels of safety, minimal government regulation, and more. Despite a lack of any significant native film industry, many film companies have moved there to take advantage of the low costs.

All is not good for incoming offworlders, though. Although a large nation, much of Razaz Nudar is barely habitable desert for non-sakin; even the sakin populate it sparsely. Even in modern times, most of the population is composed of nomadic herders who constantly move through through the inhospitable terrain.

Most of the population of Razaz Nudar lives in nomadic groups that travel with herds of animals. These animals serve as the major food-source for the nation and as the main income source for this nomadic population.

Razaz Nudar is famously lacking in customs and immigration services and freely accepts all who would want to live there. This attitude is often linked to the near total lack of national social services. Many cities, however, do possess immigration systems and most will possess some form of standardized ID.

The borders of Razaz Nudar have expanded and contracted many times during the nation's life.
Many emperors were good at (or at least focused on) conquest and plunder, but they often failed at other aspects of statecraft.
Rebellions were common and most emperors were short-lived; most were assassinated or died on the battle field.
The populace typically viewed these assassinations as a good thing that helped keep the government alert and attentive to the needs of the people.
The extend to which it actually accomplished this, however, is debatable at best.


## The Cities

The most important locations in Razaz Nudar are, without a doubt, the cities. There are just over a dozen cities, the names of which regularly change depending on the identity of the person ruling it. For example, the city of Zaezeklin is ruled by Zaezeklin. They are often known by two or three different names across the nation depending on how recently one's maps have been updated (though modern communications technology is helping with this).

The cities are connected by an underground train system that runs across the nation. Due to the haphazard safety standards in Razaz Nudar, most people prefer to travel by other means; the trains are used primarily for cargo and by foreigners, especially tourists.

### Reaklin

Reaklin is the capital of Razaz Nudar and home of the emperor. Unlike the other cities, the name of the city has not changed since Rekuzarae took the throne. The city is held by the emperor directly and all taxes and fees go to emperor himself rather than the government. In practice, this makes no real difference; the income is slight as most government organizations do not pay rent and the emperor is fully entitled to use government funds for whatever he pleases.

The city is often visited by tourists, but most go away disappointed. The city is quite small and almost everything within is a part of the federal government. The only things to see are a number of large, rectangular, plain stone buildings. Despite the expectations of many tourists, there is no part of the imperial palace that is accessible to the general public.

### Zaezeklin

Zaezeklin began life as a military fortification nearly a thousand years ago and has been controlled by various parties since; it is only in the past few centuries that the city has developed a significant civilian population.

This city is the premier (and ultimately only) tourist destination in the nation. It is easily the safest city for foreigners, something of a self-fulfilling prophecy as an increased dependence on tourism has created more of an emphasis on protecting and attracting them. As a result, the residents are accustomed to the strange ways of foreigners and often speak languages other than Nraezen.

The reason for its military value is that the city is carved into the side of a cliff with much of the city being buried deep underground. Access to the city is exclusively through a series of long, straight tunnels, though a train station has been added in modern times. This has also contributed to its interest for tourists and help mitigate the harshness of the local climate.

A popular tourist attraction is a massive underground river that runs through an expansive cavern and, just before it enters the area of the city, forms a series of waterfalls. Even aside from its tourism value, these waterfalls have been put to use generating hydroelectricity.

The city's subterranean nature has resulted in it possessing one of the most advanced ventilation systems in the nation. They are currently maintained by a private company aptly named the *Zaezeklin Ventilation Company*. This company was originally formed by a squad of retired military engineers and now has branches across not only Kzaknri, but the Interworld Nexus itself and even other worlds entirely. It is universally considered to be one of the best in the business.


## Daily Life

### Art

The main artistic focuses of the population are in music and dance; arts that require significant physical supplies are less popular, especially among the nomadic population.

Music is heavy on percussion and is typically accompanied by singing. Stringed instruments are rare and wind instruments are largely non-existent.

Most traditional dances are based on rituals and forms devised from the worship of Tika and have thus fallen from favour. Modern dances are typically face-paced, demonstrating the dancer's skill and agility even as they entertain audiences. Lewd dances are not uncommon and are designed to suggest the dancer's virility; some lewd dances will incorporate explicit sexual acts. Perhaps the most popular dances, especially amongst off-world tourists, are those which can pose significant risk to the dancer if mistakes are made, particularly the infamous "knife dances".

### Sports and Entertainment

A popular form of public entertainment is gladiatorial combat. This is rarely taken to the point of fatalities, with participants typically yielding after only minor injuries or blood loss. Although regular fights typically have professional gladiators (who often have their own fanbase cheering them on), personal duels are sometimes inserted between major fights and these are rarely between professionals. Aside from the obvious uses, gladiatorial arenas often host other performances as well; musical or dance groups often rent them for shows, as do teams of less popular sports. Most arenas also offer a variety of medical services to the general public and are one of the better places to get physical injuries treated.

### Calendar

<figure class='right' style='width:400px'>
    <a href='/art/calendars/razaznudar.svg'><img src='/art/calendars/razaznudar.svg' width='400' height='100'></a>
    <figcaption>
    Standard Razaz Nudar Calendar.<br>
    Art © <a href='https://www.grenslair.com/'>Gren Drake</a>
    </figcaption>
</figure>

Thanks to [Kzaknri's](kzaknri.md) nearly perfect 600 day, the calendar used in Razaz Nudar is quite simple.
The days are divided into 20 months, each with 30 days.
These are distributed over five six day weeks.
The calendar uses the offical year of the founding of Razaz Nudar as year 0.

### Timekeeping

<figure class="right">
    <table style='text-align: center'>
        <tr><th>Earth</th><th>Razaz Nudar</th></tr>
        <tr><td>0:00</td><td>3N0</td></tr>
        <tr><td>3:00</td><td>0TR0</td></tr>
        <tr><td>9:00</td><td>0D0</td></tr>
        <tr><td>12:00</td><td>3D0</td></tr>
        <tr><td>15:00</td><td>3TF0</td></tr>
        <tr><td>21:00</td><td>0N0</td></tr>
    </table>
    <figcaption>Common Time Conversations</figcaption>
</figure>

A distinctive form of timekeeping is used in Razaz Nudar; this was originally based on the sakin's biological crepuscular schedule. Each day is divided into four quarters, called *Night* (N), *Twilight Rising* (TR), *Day* (D), and *Twilight Falling* (TF), each with six hours of thirty-six minutes. Each minute is divided into seventy-two seconds. This is written as hours-quarter-minutes, e.g. 3TF25. Significantly, the calendar day is considered to start at the beginning of *night* and midnight occurs midway through this quarter.


## Government

The titular ruler of Razaz Nudar is the emperor, currently Emperor Zdajgae. Although the actual authority held by the emperor has varied of the history of the nation, since the time of Rekuzarae, the power of the emperor has been absolute: the emperor has the final say in all matters and serves as the highest level of court. The emperor also serves as a warleader in times of war, but typically leaves the details of managing the war to military personnel.

Most day-to-day business is not personally attended to by the emperor, but rather by a national bureaucracy composed of appointed ministers and hired staff who carry out the process of law and the emperor's decrees. It is rare that any emperor takes direct involvement in this, typically giving high-level guidance as well as dismissing and appointing individuals to posts.

### Departments

**Imperial Guard.**
The imperial guard was originally formed by emperor Rekuzarae as a force of elite bodyguards.
During his reign, this was their sole function, but in modern times they often serve as a small, personal military force that directly and exclusively serves the emperor.
They have become, in essence, the emperor's "right hand men" and often take care of covert or sensitive matters that the emperor would prefer no one else know about.
The original guard was a small force, most of whom Rekuzarae had picked up in his travels before becoming emperor; many were not sakin.
The modern force is drawn almost entirely from the local population and are chosen for demonstrating exceptional loyalty and skill.

**Secret Police.**
The secret police were formed by Rekuzarae with the explicit goal of finding and exposing his political and personal opponents.
In many cases, they also took care of removing those uncovered, often by making them disappear without a trace.
The original organization was a nebulous department where the staff were not publicly employed.
Under emperor Zdajgae, they are mostly tasked with domestic intelligence and counter-intelligence as well enforcing federal law.

**Public Relations and Propoganda.**
This department handles most communications issued by the government, as well as processing and distributing any incoming mail.
They are also responsible for liaising with private businesses, particularly the news and media, to ensure messages are to government standards.
(That is, they handle any required censorship of the media.)

### Land Ownership

Most land in Razaz Nudar is held by either by a small number of landowners (or *city lords*) or collectively by the nomadic bands that populate much of the country. Landowners, whether collective or individual, have direct and personal control of the land, including the authority to create and police laws. In exchange for this, they pay taxes to the emperor based on the amount of land, the location of the land, and whether they are an individual owner or a collective. While many cities were originally founded either to protect immovable resources (such as mines or water sources) or as markets and gatherings of tradesmen, modern cities provide enough services that living in them is desirable in itself.

Claiming a patch of unclaimed land is a simple process mostly consisting of verifying that the land is unclaimed and then prepaying the appropriate taxes for it. Doing anything productive with this land is often harder; city lords are often reluctant to share services without significant payments and very little available land is suitable for agricultural purposes or possessed with easily-accessible resources. Legal enforcement is often lacking as well as national law enforcement normally prioritizes more populated areas.

Individual landowners will then rent out small parts of the land, along with public services such as electrical power and sewage, to the tenants who then live there. These tenants are required to deal with two sets of laws: those created by the emperor and those created by the city lord. And while the city lord is not permitted to alter or violate the emperor's law, neither are they permitted to directly enforce it; cities are thus obligated to maintain two separate police forces: one that works for the city lord and one that works for the emperor.


## Law

Most law in Razaz Nudar is based around maintaining order or the concept of an *eye for an eye* rather than justice. Many laws are viewed by foreigners as being unnecessarily harsh or more about vengeance than reparations and rehabilitation. At the same time, Razaz Nudar is often accused of being largely lawless; many things banned in other countries are considered to be none of the government's business. Law at the national level embraces minimalism and being easy to understand and enforce rather than being comprehensive.

National and local law are separate to the point that two separate police forces are necessary; neither is permitted to enforce the other's laws (local police forces are expected to pass on relevant information, however). While national law does not vary, local law, and its enforcement, can vary quite a bit between different cities and those travelling between cities are well advised to be aware of the laws of both.

National law deals primarily with fraud and with *theft of choice*, a broad category that covers areas like murder, rape, and theft. Crimes of morality, those without direct victims, are not included such that things like prostitution and pornography are completely unrestricted. Slavery is also permitted (under the guise of selling one's right to make choices) with the caveat that one can be enslaved only by choice (such as selling themselves into slavery) or through due process of law.

Trials tend to be quick with judges directly interrogating those witness and involved. Appeals can be made to the city lord for local law or to the emperor for national law. Both city lords and the emperor are expected to hear all appeals, but unlike judges are not constrained by their own laws when making decisions and unnecessary or unwarranted appeals may result in offenders receiving much harsher sentences (or, in extreme cases, being executed).

Unless an appeal is made, legal judgements are carried out quickly. They tend towards reparations (in the case of crimes like theft or fraud) or corporeal punishment (for everything else). Most judgements are chosen with the idea of discouraging offenders and preventing repeat offences. Prison time is rare and usually a side-effect of a judgement rather than the intent (an offender sentenced to slavery may be publicly imprisoned so that potential buyers can see them, for example).

Interpersonal disputes can be decided by civil trials or, if both parties agree, by duels. These civil duels have a very strict, formal structure and are considered legally binding. Civil judges may also decree that a dispute is to be settled by duel. Civil judges are expected to make non-duel judgements that are in line with the situation presented and that are in-line with the law (e.g. they can't have someone executed or enslaved). Appeals can be made to both the city lord and the emperor, but this is under the same conditions as in criminal cases and few find the risk worthwhile.

It is important to note that animals, slaves, and those not yet of age are not considered legal persons; in the eyes of the law, they are all considered akin to animals and thus lack the rights privileges that adults possess.

### Religion

As of the reign of Rekuzarae, Razaz Nudar has been an officially athiest country. Despite this, not all residents are atheists and there are a few prominent religions (and even more small ones). Most people are tolerant of beliefs different from their own, but most are also suspicious of religions that try prescribe too many behaviours.

Historically, the state religion of Razaz Nudar was the worship of [Tika](tika.md), but this is forbidden since the reign of [Rekuzarae](rekuzarae.md).

#### Nratha

Nratha (Nraezen: <bdo class='nexusFont lang_nr'>&#xe005;&#xe026;&#xe02a;</bdo>) is often known as the Commercial God. The tenets of Nratha are forthrightness, honesty, and self-empowerment. Non-followers often, inaccurately, associate Nratha with law, order, or judgement as well. Even those who are not believers consider Nratha to have a good, prototypical sakin character. In this vein, Nratha is often characterized as being both blunt and pragmatic.

Worship of Nratha is spread over a wide area, but, typically, only a few worshippers are found in any specific area. The temples of Nratha are located in the mountains, away for populated areas, but many cities and nomadic bands have shrines in Nratha's honour.

Larger shrines often operate more like business than non-followers expect and the priesthood is known for being extremely mercantile.


## Names

Those who have not reached the age of majority are not considered to have a legal name and are referred to as "so-and-so's child" in law, though they will still have nicknames and familiar names. On becoming an adult, individuals are expected to choose a personal name for themselves.

Most names are common words reflecting aptitudes, traits, or personal preferences that the individual either has or desires. Most people have only one name, though the name of their nomadic band, home city, or employer may be added to the end if several people share the same name.
