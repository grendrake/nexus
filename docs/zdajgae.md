---
category: person
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
---

<div class='infobox'><table>
    <tr><th class='top-header' colspan='2'>Zdajgae</th></tr>
    <tr><th>Nraezen</th>        <td><bdo class='nexusFont lang_nr'></bdo></td></tr>
    <tr><th>Species</th>        <td><a href='sakin.html'>Sakin</a></td></tr>
    <tr><th>Position</th>       <td>Emperor of <a href='razaznudar.html'>Razaz Nudar</a></td></tr>
    <tr><th>Title</th>          <td>the Undying</td></tr>
    <tr><th>Hatched</th>        <td>1895</td></tr>
    <tr><th>Current Age</th>    <td>124</td></tr>
    <tr><th>Crowned</th>        <td>1903 (aged 8)</td></tr>
</table></div>

The current emperor of Razaz Nudar. Zdajgae ascended at about eight years of age. His predecessor, Thiknrae, was widely considered to be insane and had declared himself immortal and thus appointed a random youth off the street as his heir. Shortly afterwards, Thiknrae was assassinated. When Zdajgae ascended the throne it was assumed he would have a very short reign, but he proved remarkably resistant to both assassination and having others rule through him.

The first impression of anyone on looking at Zdajgae is that he looks good for his age. His eyes are keen and in conversation he quickly proves his mind remains sharp despite his advanced age. He is often seen wearing a thick cloak over his shoulders and walks using a simple but elegant cane. Those meeting him for the first time typically leave with the impression of a kindly and welcoming old man. Anyone who comes to interact with him over an extended period comes to learn this is from an absolute confidence in his control of his surroundings and his empire. Zdajgae is friendly because he can afford to be; he knows that the world will move at his word.

Although often thought of as highly conservative, many of Zdajgae’s early policies were very progressive and benefited the poor and disenfranchised. This tends to be forgotten, however, as most of these policies were enacted before the majority of the population was born. Even those that consider Zdajgae to be conservative will grant that he is open-minded and willing to consider new evidence and science as well as to publicly admit to mistakes and misunderstandings. Despite this, he is very much anti-democracy, being known to have described it as incoherent and inevitably focused on the short-term.

Zdajgae has held the throne for well over a century. Despite this, he continues to be in good health, especially for someone of his age. He had so far outlived two heirs and many believe he will outlast the current heir, [Djurnae](djurnae.md) as well. Attempts to assassinate him or otherwise usurp the throne have almost entirely stopped; most people have become afraid of the turmoil that can accompany a change of emperor, especially such a long-reigning one, than of any benefit that may come from the change.
