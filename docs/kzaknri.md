---
category: world
world: kzaknri
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Kzaknri</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/kzaknri.png'></td></tr>
<tr><th>Primary Star</th>           <td>F6V</td></tr>
<tr><th>Companion Star</th>         <td>F9V</td></tr>
<tr><th>Year Length:</th>           <td>600.10 Local days<br>681.19 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>301 K (27.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>1.20 G</td></tr>
<tr><th>Day Length:</th>            <td>27.243 h</td></tr>
<tr><th>Moons</th>                  <td>Two</td></tr>
<tr><td colspan='2' class='wide-data'><a href='systems.html#kzaknri'>Kzaknri System</a></td></tr>
</table></div>

Kzaknri (Nraezen: <bdo class='nexusFont lang_nr'>&#xe004;&#xe02e;&#xe004;&#xe005;&#xe025;</bdo>) is the fifth world in orbit around a F6 star, the primary of a binary star system. Its system is notable for consisting almost entirely of gas giants. It is the home of the [sakin](sakin.md).

The gate on Kzaknri is located near the ocean in nation of [Razaz Nudar](razaznudar.md). The coastal location also provides easy access for other nations, particularly the [FASS](fass.md), [Zanth Drith](zanthdrith.md), and the [Tsukina](tsukina.md) who make up the closest neighbours by sea. The [Nraezen](nraezen.md) language is the most common on Kzaknri; it is the primary language of two major countries ([Razaz Nudar](razaznudar.md), where it originated, and the [FASS](fass.md)) and no matter where you go on Kzaknri you can probably find somebody who speaks the language.

Several characteristics are common to those from Kzaknri, regardless of national origin. Those from Kzaknri tend to be quicker to violence and better trained with weapons. There is a tendency towards blunt, direct speech.

The world itself is both hotter and drier than [Earth](earth.md). The average temperature worldwide is notably higher; it is considered to have poor habitability for almost all non-sakin species.  Even the native sakin find the equatorial regions inhospitable and humans are most comfortable in the temperate or polar regions.
