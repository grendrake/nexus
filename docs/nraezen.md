---
category: language
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
notoc: true
---

<p>Nraezan (Nraezan: <bdo class='nexusFont lang_nr'>&#xe040;&#xe02e;&#xe005;</bdo>) is the main and offical language of Razaz Nudar as well as being one of the most common languages across Kzaknri. A dialect is also used in Zanth Drith. The phonetic chart below is constructed using the <a class="reference external" href="http://www.omniglot.com/writing/ipa.htm">http://www.omniglot.com/writing/ipa.htm</a> (IPA). ASCII equivelents are provided for each non-ASCII IPA character in square brackets.
<p>Nraezan is written right to left. Vowels are not indicated except in names or in some educational materials or materials intended for foreign consumption.
<p>When added suffixes to a word, if the word ends in a vowel and the suffix begins with one, the vowel of the suffix replaces that of the word (<em>kazkiri</em> + <em>-iz</em> = <em>kazkiriz</em>). In addition, any time adding an afix causes a doubled consonant one of the consonants are dropped (<em>gik-</em> + <em>kiyagt</em> = <em>gikiyagt</em>).
<p>The stress is always on the second syllable of a word. Single syllable words are unstressed.
<p>The &#8220;NR&#8221; character replaces the character sequence &#8220;N&#8221; + &#8220;R&#8221; within a syllable, but not over a syllable boundary. For example, &#8220;<em>nrlae</em>&#8221; is <bdo class='nexusFont lang_nr'>&#xE010;&#xE00A;</bdo>, but &#8220;<em>zan-rud</em>&#8221; is <bdo class='nexusFont lang_nr'>&#xE008;&#xE005;&#xE006;&#xE001;</bdo>.

<table>
    <tr>
        <th></th>
        <th>Dental</th>
        <th>Alveolar</th>
        <th>Post-Alveolar</th>
        <th>Velar</th>
    </tr>
    <tr>
        <td>Plosive</td>
        <td>&nbsp;</td>
        <td>t,d</td>
        <td>&nbsp;</td>
        <td>k, g</td>
    </tr>
    <tr>
        <td>Nasal</td>
        <td>&nbsp;</td>
        <td>n</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Trill</td>
        <td>&nbsp;</td>
        <td>r</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Frictive (voiced)</td>
        <td>ð [T]</td>
        <td>z</td>
        <td>ʒ [z&#8217;]</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Lateral-Approximate</td>
        <td>&nbsp;</td>
        <td>l</td>
        <td>&nbsp;</td>
        <td>ʟ [L]</td>
    </tr>
</table>

<br>

<table>
<tr>
    <td>Front-Close</td>
    <td>i</td>
    <td>Back-Close</td>
    <td>ɯ [M]</td>
</tr>
<tr>
    <td>Front-Open</td>
    <td>a</td>
    <td>Front-Close-mid</td>
    <td>e</td>
</tr>
</table>

<br>

<table>
<caption>Nraezen Lexography</caption>
<tr>
    <th>Index</th>
    <th>Letter</th>
    <th>Trans.</th>
    <th>IPA</th>
    <th>Index</th>
    <th>Letter</th>
    <th>Trans.</th>
    <th>IPA</th>
</tr>
<tr>
    <td>1</td>
    <td class='nexusFont lexography lang_nr'>&#xE001;</td>
    <td>d</td>
    <td>d</td>
    <td>9</td>
    <td class='nexusFont lexography lang_nr'>&#xE009;</td>
    <td>j</td>
    <td>ʒ</td>
</tr>
<tr>
    <td>2</td>
    <td class='nexusFont lexography lang_nr'>&#xE002;</td>
    <td>t</td>
    <td>t</td>
    <td>10</td>
    <td class='nexusFont lexography lang_nr'>&#xE00A;</td>
    <td>l</td>
    <td>l</td>
</tr>
<tr>
    <td>3</td>
    <td class='nexusFont lexography lang_nr'>&#xE003;</td>
    <td>g</td>
    <td>g</td>
    <td>11</td>
    <td class='nexusFont lexography lang_nr'>&#xE00B;</td>
    <td>y</td>
    <td>ʟ</td>
</tr>
<tr>
    <td>4</td>
    <td class='nexusFont lexography lang_nr'>&#xE004;</td>
    <td>k</td>
    <td>k</td>
    <td>12</td>
    <td class='nexusFont lexography lang_nr'>&#xE00C;</td>
    <td>i</td>
    <td>i</td>
</tr>
<tr>
    <td>5</td>
    <td class='nexusFont lexography lang_nr'>&#xE005;</td>
    <td>n</td>
    <td>n</td>
    <td>13</td>
    <td class='nexusFont lexography lang_nr'>&#xE00D;</td>
    <td>a</td>
    <td>a</td>
</tr>
<tr>
    <td>6</td>
    <td class='nexusFont lexography lang_nr'>&#xE006;</td>
    <td>r</td>
    <td>r</td>
    <td>14</td>
    <td class='nexusFont lexography lang_nr'>&#xE00E;</td>
    <td>u</td>
    <td>ɯ</td>
</tr>
<tr>
    <td>7</td>
    <td class='nexusFont lexography lang_nr'>&#xE007;</td>
    <td>th</td>
    <td>th</td>
    <td>15</td>
    <td class='nexusFont lexography lang_nr'>&#xE00F;</td>
    <td>ae</td>
    <td>e</td>
</tr>
<tr>
    <td>8</td>
    <td class='nexusFont lexography lang_nr'>&#xE008;</td>
    <td>z</td>
    <td>z</td>
    <td>16</td>
    <td class='nexusFont lexography lang_nr'>&#xE010;</td>
    <td>nr</td>
    <td>nr</td>
</tr>
</table>

<br>

<table>
<caption>Nraezen Numerals</caption>
<tr>
    <th>Digit</th>
    <th>Value</th>
    <th>Name</th>
    <th>Digit</th>
    <th>Value</th>
    <th>Name</th>
</tr>
<tr>
    <td class='nexusFont lexography lang_nr'>&#xE041;</td>
    <td>0</td>
    <td>zithya</td>
    <td class='nexusFont lexography lang_nr'>&#xE042;</td>
    <td>1</td>
    <td>rthya</td>
</tr>
<tr>
    <td class='nexusFont lexography lang_nr'>&#xE043;</td>
    <td>2</td>
    <td>latna</td>
    <td class='nexusFont lexography lang_nr'>&#xE044;</td>
    <td>3</td>
    <td>digta</td>
</tr>
<tr>
    <td class='nexusFont lexography lang_nr'>&#xE045;</td>
    <td>4</td>
    <td>yuktja</td>
    <td class='nexusFont lexography lang_nr'>&#xE046;</td>
    <td>5</td>
    <td>ga</td>
</tr>
</table>

<br>

<table>
<caption>Other Glyphs</caption>
<tr>
    <th>Glyph</th>
    <th>Meaning</th>
    <th>Name</th>
</tr>
<tr>
    <td class='nexusFont lexography lang_nr'>&#xE047;</td>
    <td>Full Stop</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td class='nexusFont lexography lang_nr'>&#xE048;</td>
    <td>Pause</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td class='nexusFont lexography lang_nr'>&#xE049;</td>
    <td>Open Quote</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td class='nexusFont lexography lang_nr'>&#xE04A;</td>
    <td>Close Quote</td>
    <td>&nbsp;</td>
</tr>
</table>

<h2 id='nouns'>Nouns</h2>
<p>Nouns in Nraezan come in four genders: sapient (ending -ae), animal (-u), inanimate (-i), and abstract (ending with a consonant). They do not use articles. Pronouns are inflected just like the nouns they replace (e.g. <em>krulzae</em> (I/me) ⇒ <em>krulzalae</em> (mine)).
<p>The nominative (subject; <em>Kvikae ran</em>.) and accusative (direct object; <em>Kvikae ate dinner</em>.) case are indicated solely by word order, but the other cases are marked. They are: the dative (used for indirect objects; <em>Kvikae gave the book to Zarekae</em>.), the genitive (used for possession; <em>Kvikae&#8217;s</em> book.), the instrumental (used for instrument or means of an action, <em>Kvikae wrote the memo with the pen</em>., or the time an action occured, <em>Kvikae wrote the memo in the morning</em>.), and the lative (used to indicate the motion or direction of an action; <em>Kvikae walked to the store</em>.).
<p>There are two ways of forming questions. The first is to replace one of the verb arguments with an interogative pronoun (You are <em>who</em>?). The other is to use the intergative mood of a verb.
<p class="todo">Add dative affixes

<table>
<tr>
    <th>Nominative/</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
    <th>&nbsp;</th>
</tr>
<tr>
    <th>Accusitive</th>
    <th>Dative</th>
    <th>Genitive</th>
    <th>Instrumental</th>
    <th>Lative</th>
</tr>
<tr>
    <td>-ae</td>
    <td>&nbsp;</td>
    <td>-alae</td>
    <td>-uzae</td>
    <td>-ithae</td>
</tr>
<tr>
    <td>-u</td>
    <td>&nbsp;</td>
    <td>-lu</td>
    <td>-uzae</td>
    <td>-uthu</td>
</tr>
<tr>
    <td>-i</td>
    <td>&nbsp;</td>
    <td>-uli</td>
    <td>-azlu</td>
    <td>-uthi</td>
</tr>
<tr>
    <td>-C</td>
    <td>&nbsp;</td>
    <td>-Cul</td>
    <td>-Cuj</td>
    <td>-Cuth</td>
</tr>
</table>

<h2 id='adjectives'>Adjectives/Adverbs</h2>
<p>Adjectives and adverbs are part of the same word class and can be used interchangably and will be refered to collectively as adjectives. They preceed their head words, but do not inflect based on them.
<table>
<tr>
    <th>Meaning</th>
    <th>Affix</th>
</tr>
<tr>
    <td>Negation (<em>not</em> bright)</td>
    <td>yu-</td>
</tr>
<tr>
    <td>Comparative (bright <em>er</em>)</td>
    <td>nra-</td>
</tr>
<tr>
    <td>Superlative (bright <em>est</em>)</td>
    <td>kin-</td>
</tr>
<tr>
    <td>Allows nouns as adjectives</td>
    <td>yaz-</td>
</tr>
</table>

<h2 id='verbs'>Verbs</h2>
<table>
<tr>
<th>Verb Form</th>
<th>Indicative<br>[he&nbsp;did]</th>
<th>Negative<br>[he&nbsp;did&nbsp;not]</th>
<th>Neccesative<br>[he&nbsp;must]</th>
<th>Subjunctive<br>[he&nbsp;have]</th>
<th>Interogative<br>[did&nbsp;he?]</th>
</tr>
<tr>
<td>Transitive (2+ args)</td>
<td>-ud</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Intransitive (1 arg)</td>
<td>-iz</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>Impersonal (no args)</td>
<td>&#8211;</td>
<td>-ur</td>
<td>-an</td>
<td>-il</td>
<td>&nbsp;</td>
</tr>
</table>

<br>

<table>
<tr>
    <th>Afix</th>
    <th>Meaning</th>
</tr>
<tr>
    <td>-inul</td>
    <td>Gerund; noun form of verb</td>
</tr>
<tr>
    <td>gik-</td>
    <td>Past Tense</td>
</tr>
<tr>
    <td>dae-</td>
    <td>Future Tense</td>
</tr>
</table>

<h2 id='syntax'>Syntax</h2>
<p class="todo">make note of A-N-R
<p>Nraeven is written in SOV order with head-final noun and verb phrases. Post-positions are used rather than the prepositions English speakers will be familiar with. The word order is fixed and cannot be changed.

<h2 id='examples'>Examples</h2>

<table class='interlex'>
<caption>Example 1</caption>
<tr>
    <td colspan="3" style='text-align:right' class='interlex'>
        <bdo class='nexusFont lang_nr'>&#xE004;&#xE006;&#xE00A;&#xE008;&#xE000;&#xE004;&#xE004;&#xE000;&#xE003;&#xE004;&#xE004;&#xE008;&#xE004;&#xE006;&#xE008;&#xE047;</bdo>
    </td>
</tr>
<tr>
    <td>krulzae</td>
    <td>kaka</td>
    <td>gikkazkiriz.</td>
</tr>
<tr>
    <td>(PRO-1stSap)</td>
    <td>(N-egg)</td>
    <td>(PT-V-find)</td>
</tr>
<tr>
    <td colspan="3" class='interlex-ex'>I egg found</td>
</tr>
<tr>
    <td colspan="3" class='interlex-ex'>I found an egg.</td>
</tr>
</table>

<table class='interlex'>
<caption>Example 2</caption>
<tr>
    <td colspan="3" style='text-align:right' class='interlex'>
        <bdo class='nexusFont lang_nr'>&#xE004;&#xE02D;&#xE020;&#xE000;&#xE004;&#xE008;&#xE000;&#xE004;&#xE00B;&#xE003;&#xE002;&#xE008;&#xE047;</bdo>
    </td>
</tr>
<tr>
    <td>kvikae</td>
    <td>kazi</td>
    <td>kiyaegtiz</td>
</tr>
<tr>
    <td>(PropN)</td>
    <td>(N-coffee)</td>
    <td>(V-want)</td>
</tr>
<tr>
    <td colspan="3" class='interlex-ex'>Kvik coffee wants</td>
</tr>
<tr>
    <td colspan="3" class='interlex-ex'>Kvik wants some coffee.</td>
</tr>
</table>

<table class='interlex'>
<caption>Example 3</caption>
<tr>
    <td colspan="4" style='text-align:right' class='interlex'>
        <bdo class='nexusFont lang_nr'>&#xe008;&#xe005;&#xe006;&#xe00a;&#xe000;&#xe003;&#xe000;&#xe004;&#xe004;&#xe000;&#xe003;&#xe004;&#xe008;&#xe004;&#xe009;&#xe047;</bdo>
    </td>
</tr>
<tr>
    <td>znrula</td>
    <td>ga</td>
    <td>kaka</td>
    <td>gekzakij</td>
</tr>
<tr>
    <td>(PRO-2ndSap)</td>
    <td>(Num-Five)</td>
    <td>(N-Egg)</td>
    <td>(PT-V-eat)</td>
</tr>
<tr>
    <td colspan="4" class='interlex-ex'>You five egg ate</td>
</tr>
<tr>
    <td colspan="4" class='interlex-ex'>You ate five eggs.</td>
</tr>
</table>

<table class='interlex'>
<caption>Example 4</caption>
<tr>
    <td colspan="4" style='text-align:right' class='interlex'>
        <bdo class='nexusFont lang_nr'>&#xe010;&#xe00a;&#xe000;&#xe003;&#xe004;&#xe005;&#xe007;&#xe00a;&#xe009;&#xe000;&#xe005;&#xe009;&#xe004;&#xe00a;&#xe002;&#xe000;&#xe00a;&#xe005;&#xe007;</bdo>
    </td>
</tr>
<tr>
    <td>Nrla</td>
    <td>geknuthalj</td>
    <td>nijklat</td>
    <td>linuthi</td>
</tr>
<tr>
    <td>(PRO-3rdSap)</td>
    <td>(PT-V-walk)</td>
    <td>(ADJ-bright)</td>
    <td>(Lat-N-city)</td>
</tr>
<tr>
    <td colspan="4" class='interlex-ex'>She/he walked bright to-city</td>
</tr>
<tr>
    <td colspan="4" class='interlex-ex'>She/he walked to the bright city.</td>
</tr>
</table>
