---
category: nation
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
---


<figure class='right' style='width:98px'>
    <img src='/art/flags/fass.svg' width='98' height='198'>
    <figcaption>
    Flag of the FASS.<br>
    Art © <a href='https://www.grenslair.com/'>Gren Drake</a>
    </figcaption>
</figure>

The FASS (an acronym for "Free Associated Sakin States", the English translation of the nation's actual name) is a nation comprised mainly of former colonies of Razaz Nudar. It is located in the less desirable northern latitudes. Its cooler, temperate climate, which is less pleasant for the native [sakin](sakin.md) standards, has forced the residents into a more outwardly focused culture built largely on trade. With the opening of the gate, they have been quick to start adapting cultural elements from other worlds and taking advantage of the more moderate climate to encourage tourism.

The timekeeping system used in the FASS is the same as [that used in Razaz Nudar](razaznudar.md#timekeeping).


## Politics

The FASS is a confederation of mostly independent city-states. Each city-state typically controls a single city and the surrounding area. The governors of the city-states range from brutal despots to democratically elected leaders. The central government of the FASS is a council of the city-states' governors; those that are figureheads do little more than report information between the council and the real government of their city-state.

The FASS is not a superpower and has very little influence in world or cross-world politics. Despite this, the FASS often serves as neutral ground for international meetings.

Officially the FASS still claims allegiance to Razaz Nudar. This is commonly acknowledged as being little more than a political fiction; the emperor of Razaz Nudar has no authority in the FASS and serves only as a figurehead.

The central government of the FASS maintains only a small standing army which mostly serves to respond to major disasters. Several member states maintain larger armies, however, and the central government can call on them when necessary.


## Life and Society

Most land in the FASS is unoccupied wilderness. The majority of the population lives within large city-states and the pastoral farmland surrounding them. This wildness is mostly untouched other than the roads and railway that run through it. Some groups protest the usage and exploitation of this land, but this is uncommon as there is little actual demand for using the land.

Local culture is a blend of older traditions from [Razaz Nudar](razaznudar.md) along with customs adopted from tourists and locally developed ones. The ocean between the FASS and Razaz Nudar allowed the FASS to free itself from the dominion of the followers of Teka before Razaz Nudar, though they often retained the superficial forms for the sake of diplomacy. As a result, the FASS actually retains more customs from the religion, though they are now disconnected from the religion that created them.

The FASS retains the loose sexual mores of Razaz Nudar; very little is actually banned or disapproved of, though generally people are expected to keep their sexuality out of the streets and common areas.

Duelling also remains popular, but although the customs are evolved from those of Razaz Nudar, they have developed in a different direction. Duels are typically conducted unarmed or with small melee weapons and will end with first blood or when one party yields. Although it originally served as a way of settling disputes and often still does, duel has also become a common form of entertainment similar to martial arts on Earth.

The most popular forms of art are dance and music. Most residents have at least tried them, though not everyone keeps up with them. Scent-mixing is also common and, unlike dance and music, most people practice them regularly. Visual arts (e.g. painting or sculpture) are less common on account of the lower visual acuity of the native [sakin](sakin.md).

### Architecture

Modern architecture favours tall buildings; high-rise buildings and skyscrapers are common. Underground levels are common as well, often extending for numerous levels into the ground. Buildings on the outskirts of or outside of a city-state are often smaller, sometimes with only one or two levels. While not especially common, completely underground facilities can be found in nearly all city-states. The upper levels of such structures typically serve as park areas.

Building materials are typically steel and glass; the glass is typically tinted to reduce incoming sunlight. The most desirable spaces in buildings are those near ground level.

### Industry

Most people in the FASS in modern times work in the service industries or in tourism. Tourism from off-world is steadily increasing due to the amount of untouched wilderness and the much more mild climate than exists on the more populated parts of the world. Most city-states are surrounded by [pastoral farms](https://en.wikipedia.org/wiki/Pastoral_farming), but the food produced is mostly consumed locally rather than being exported.

Industrial centres are located in or near the city-states; many of these industries take advantage of the high resilience and poison tolerance of the native [sakin](sakin.md) and possess greatly reduced safety standards compared to nations on other worlds. As a result, manufacturing in the ASS is typically cheaper, though cross-world transport prevents other worlds from taking much advantage of this.

### Law

Although the FASS shares Razaz Nudar's expectation that people are able to defend themselves, the increased emphasis on tourism has resulted in far stronger legal protections and a considerably more active police force. Most citizens carry weapons and are familiar with their use, though doing harm outside of a duel is illegal. Self-defence is considered to be a strong mitigating factor.

Although Razaz Nudar fully permits slavery, law in the FASS discourages slavery and no law exists to enslave or emancipate people. Foreigners receive, at most, a slap on the wrist for slavery-related offences; the government is more interested in promoting foreign relations than taking a stand regarding slavery.
