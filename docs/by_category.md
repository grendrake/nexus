---
category: index
template: by_category.html
---

<div class='index-list'>
<div class="index-head"> Indexes</div>
<ul>
<li><a href="/by_alpha.html">Alphabetical Index</a></li>
<li><a href="/by_category.html">Category Index</a></li>
<li><a href="/glossary.html">Glossary</a></li>
<li><a href="/">Welcome</a></li>
<li><a href="/by_world.html">World Index</a></li>
</ul>
<div class="index-head"> General</div>
<ul>
<li><a href="/homeworlds.html">Homeworlds</a></li>
<li><a href="/nanite_solution.html">Nanite Solution</a></li>
<li><a href="/overview.html">Overview</a></li>
<li><a href="/about.html">Project Information</a></li>
<li><a href="/sophants.html">Sophants</a></li>
<li><a href="/systems.html">Star System Raw Data</a></li>
<li><a href="/timeline.html">Timeline</a></li>
</ul>
<div class="index-head"> Animals</div>
<ul>
<li><a href="/aksrith.html">Aksrith</a></li>
<li><a href="/eksar.html">Eksar</a></li>
<li><a href="/ini.html">Ini</a></li>
<li><a href="/jiruan.html">Jiruan</a></li>
<li><a href="/ruksou.html">Ruksou</a></li>
<li><a href="/zuga.html">Zuga</a></li>
<li><a href="/zuzara.html">Zuzara</a></li>
</ul>
<div class="index-head"> Guidelines</div>
<ul>
<li><a href="/clothing_guidelines.html">Clothing Guidelines</a></li>
<li><a href="/nation_guidelines.html">Nation and Culture Guidelines</a></li>
<li><a href="/species_guidelines.html">Species Guidelines</a></li>
</ul>
<div class="index-head"> Gods</div>
<ul>
<li><a href="/grutha.html">Grutha</a></li>
<li><a href="/tika.html">Tika</a></li>
<li><a href="/vaere.html">Vaere</a></li>
<li><a href="/zakanor.html">Zakanor</a></li>
</ul>
<div class="index-head"> History</div>
<ul>
<li><a href="/zifaelafojun_history.html">Zifaelafojun History</a></li>
</ul>
<div class="index-head"> Languages</div>
<ul>
<li><a href="/jowvon.html">Jowvon</a></li>
<li><a href="/jo_vocab.html">Jowvon Lexicon</a></li>
<li><a href="/nraezen.html">Nraezan</a></li>
<li><a href="/nr_vocab.html">Nraezen Lexicon</a></li>
<li><a href="/or_vocab.html">Ortasa Lexicon</a></li>
<li><a href="/ravagi.html">Ravagi</a></li>
</ul>
<div class="index-head"> Nations</div>
<ul>
<li><a href="/fianalys.html">Fianalys</a></li>
<li><a href="/gekrelt.html">Gek Relt</a></li>
<li><a href="/kerrelt.html">Ker Relt</a></li>
<li><a href="/remnants.html">Kthbra Remnants</a></li>
<li><a href="/menota.html">Menota</a></li>
<li><a href="/pashazafojunfae.html">Pasha Za Fojun Fae</a></li>
<li><a href="/raesanvie_culture.html">Raesan Vie Culture</a></li>
<li><a href="/razaznudar.html">Razaz Nudar</a></li>
<li><a href="/rekarelt.html">Reka Relt</a></li>
<li><a href="/serkmhub.html">Serkm Hub</a></li>
<li><a href="/fass.html">The FASS</a></li>
<li><a href="/tsukina.html">The Tsukina</a></li>
<li><a href="/warrothserkm.html">Warrothserkm</a></li>
<li><a href="/zanthdrith.html">Zanth Drith</a></li>
</ul>
<div class="index-head"> People</div>
<ul>
<li><a href="/djurnae.html">Djurnae</a></li>
<li><a href="/rekuzarae.html">Rekuzarae</a></li>
<li><a href="/tkinae.html">Tkinae</a></li>
<li><a href="/zdajgae.html">Zdajgae</a></li>
</ul>
<div class="index-head"> Sophants</div>
<ul>
<li><a href="/adranel.html">Adranel</a></li>
<li><a href="/asuni.html">Asuni</a></li>
<li><a href="/au.html">Au</a></li>
<li><a href="/daesra.html">Daesra</a></li>
<li><a href="/drius.html">Drius</a></li>
<li><a href="/garor.html">Garor</a></li>
<li><a href="/guas.html">Guas</a></li>
<li><a href="/human.html">Human</a></li>
<li><a href="/klugrut.html">Klugrut</a></li>
<li><a href="/naesir.html">Naesir</a></li>
<li><a href="/rakelm.html">Rakelm</a></li>
<li><a href="/ravon.html">Ravon</a></li>
<li><a href="/sakin.html">Sakin</a></li>
<li><a href="/sekar.html">Sekar</a></li>
<li><a href="/syri.html">Syri</a></li>
<li><a href="/teshi.html">Teshi</a></li>
<li><a href="/vorsyth.html">Vorsyth</a></li>
<li><a href="/ynathi.html">Ynathi</a></li>
<li><a href="/zaeren.html">Zaeren</a></li>
</ul>
<div class="index-head"> Tools</div>
<ul>
<li><a href="/tool_planet.html">Star System Tool</a></li>
<li><a href="/tool_text.html">Text Input Tool</a></li>
<li><a href='/tool_wordmaker.html'>Word Maker</a></li>
</ul>
<div class="index-head"> Worlds</div>
<ul>
<li><a href="/daesraworld.html">Daesra World</a></li>
<li><a href="/earth.html">Earth</a></li>
<li><a href="/erladi.html">Erladi</a></li>
<li><a href="/ingr.html">Ingr</a></li>
<li><a href="/isaguyat.html">Isaguyat</a></li>
<li><a href="/kloippraks.html">Kloip Praks</a></li>
<li><a href="/kzaknri.html">Kzaknri</a></li>
<li><a href="/raesanvie.html">Raesan Vie</a></li>
<li><a href="/rojizruzz.html">Rojizruzz</a></li>
<li><a href="/rynux.html">Rynux</a></li>
<li><a href="/ujan.html">Ujan</a></li>
<li><a href="/vakatr.html">Va-Katr</a></li>
<li><a href="/vozruat.html">Vozruat</a></li>
<li><a href="/zifaelafojun.html">Zifaelafojun</a></li>
</ul>


</div>
