---
category: tool
layout: custom_css
custom_css: tool_planet.css
---

<div id='planettool'>

<p>This calculation sheet is based on and intended to be used with <a href='http://www.sjgames.com/gurps/books/space/'>Gurps Space 4e</a>. The book should be referenced for instructions and detailed explainations.
<p id='error-message'></p>

<table><tr><td>
    <h2>Star Calculator</h2>

    <table>
        <tr><th>System Age:</th>        <td><input type='edit' value='1.0' id='systemAge' oninput='doStarUpdate()'> Gy</td></tr>
        <tr><th>Stellar Mass:</th>      <td><input type='edit' value='1.0' id='stellarMass' oninput='doStarUpdate()'> × Sun</td></tr>
        <tr><th>Type:                   <td><span id='starType'></span></td></tr>
        <tr><th>Subtype:                <td><span id='starSubtype'></span></td></tr>
        <tr><th>Stellar Luminosity:     <td><span id='stellarLum'></span> × Sun</td></tr>
        <tr><th>Stellar Temperature:    <td><span id='stellarTemp'></span> K</td></tr>
        <tr><th>Stellar Radius:         <td><span id='stellarRadius'></span> AU</td></tr>
        <tr><td colspan='2'>&nbsp;</td></tr>
        <tr><th>Inner Limit:            <td><span id='innerLimit'></span> AU</td></tr>
        <tr><th>Outer Limit:            <td><span id='outerLimit'></span> AU</td></tr>
        <tr><th>Snow Line:              <td><span id='snowLine'></span> AU</td></tr>
    </table>


    <h2 class='section-title'>Orbital Distances</h2>
    <p class='quick-tip'>Orbit ratios should be between 1.4 and 2.0 inclusive.
    <table id='orbitTable'>
        <tr><th>Orbit</th> <th>Distance</th>                                                     <th>Ratio/Prev</th>         <th>Blackbody</th></tr>
        <tr><td>I</td>     <td><input type='edit' id='orbit1'  oninput='doOrbitUpdate()'></td>  <td id='orbit1ratio'></td>  <td id='orbit1blackbody'></td></tr>
        <tr><td>II</td>    <td><input type='edit' id='orbit2'  oninput='doOrbitUpdate()'></td>  <td id='orbit2ratio'></td>  <td id='orbit2blackbody'></td></tr>
        <tr><td>IIII</td>  <td><input type='edit' id='orbit3'  oninput='doOrbitUpdate()'></td>  <td id='orbit3ratio'></td>  <td id='orbit3blackbody'></td></tr>
        <tr><td>IV</td>    <td><input type='edit' id='orbit4'  oninput='doOrbitUpdate()'></td>  <td id='orbit4ratio'></td>  <td id='orbit4blackbody'></td></tr>
        <tr><td>V</td>     <td><input type='edit' id='orbit5'  oninput='doOrbitUpdate()'></td>  <td id='orbit5ratio'></td>  <td id='orbit5blackbody'></td></tr>
        <tr><td>VI</td>    <td><input type='edit' id='orbit6'  oninput='doOrbitUpdate()'></td>  <td id='orbit6ratio'></td>  <td id='orbit6blackbody'></td></tr>
        <tr><td>VII</td>   <td><input type='edit' id='orbit7'  oninput='doOrbitUpdate()'></td>  <td id='orbit7ratio'></td>  <td id='orbit7blackbody'></td></tr>
        <tr><td>VIII</td>  <td><input type='edit' id='orbit8'  oninput='doOrbitUpdate()'></td>  <td id='orbit8ratio'></td>  <td id='orbit8blackbody'></td></tr>
        <tr><td>IX</td>    <td><input type='edit' id='orbit9'  oninput='doOrbitUpdate()'></td>  <td id='orbit9ratio'></td>  <td id='orbit9blackbody'></td></tr>
        <tr><td>X</td>     <td><input type='edit' id='orbit10' oninput='doOrbitUpdate()'></td> <td id='orbit10ratio'></td>  <td id='orbit10blackbody'></td></tr>
        <tr><td>XI</td>    <td><input type='edit' id='orbit11' oninput='doOrbitUpdate()'></td> <td id='orbit11ratio'></td>  <td id='orbit11blackbody'></td></tr>
        <tr><td>XII</td>   <td><input type='edit' id='orbit12' oninput='doOrbitUpdate()'></td> <td id='orbit12ratio'></td>  <td id='orbit12blackbody'></td></tr>
        <tr><td>XIII</td>  <td><input type='edit' id='orbit13' oninput='doOrbitUpdate()'></td> <td id='orbit13ratio'></td>  <td id='orbit13blackbody'></td></tr>
        <tr><td>XIV</td>   <td><input type='edit' id='orbit14' oninput='doOrbitUpdate()'></td> <td id='orbit14ratio'></td>  <td id='orbit14blackbody'></td></tr>
        <tr><td>XV</td>    <td><input type='edit' id='orbit15' oninput='doOrbitUpdate()'></td> <td id='orbit15ratio'></td>  <td id='orbit15blackbody'></td></tr>
    </table>
    <button onclick='clearOrbits()'>Clear Orbits</button>


</td><td>

    <h2 class='section-title'>Planet Calculator</h2>
    <table>
    <tr><th>Orbital Radius:</th>            <td><span id='orbitalRadius'></span> AU</td></tr>
    <tr><th>Orbital Period:</th>            <td><span id='orbitalPeriodXEarth'></span> × Earth<br>
                                                <span id='orbitalPeriodEarthDays'></span> days (Earth)<br>
                                                <span id='orbitalPeriodLocalDays'></span> days (local)</td></tr>
    <tr><td colspan='2'>&nbsp;</td></tr>
    <tr><th>Planet Type:</th>               <td><select id='planetSize' oninput='doPlanetUpdate()'>
                                                    <option value='tiny'>Tiny</option>
                                                    <option value='small'>Small</option>
                                                    <option value='standard' selected>Standard</option>
                                                    <option value='large'>Large</option>
                                                </select>
                                                <select id='planetType' oninput='doPlanetUpdate()'>
                                                    <option value='asteroid belt'>Asteroid Belt</option>
                                                    <option value='ice'>Ice</option>
                                                    <option value='rock'>Rock</option>
                                                    <option value='sulfur'>Sulfur</option>
                                                    <option value='hadean'>Hadean</option>
                                                    <option value='ammonia'>Ammonia</option>
                                                    <option value='ocean'>Ocean</option>
                                                    <option value='garden' selected>Garden</option>
                                                    <option value='greenhouse'>Greenhouse</option>
                                                    <option value='chthonian'>Chthonian</option>
                                                </select></td></tr>
    <tr><th>Atmosphere:</th>                <td><span id='atmosphere'></span></td></tr>
    <tr><th>Atmospheric Mass:</th>          <td><input type='edit' value='1.0' id='atmoMass' oninput='doPlanetUpdate()'> × Earth</td></tr>
    <tr><th>Hydrographic Coverage:</th>     <td><input type='edit' value='75' id='hydro' oninput='doPlanetUpdate()'> %</td></tr>
    <tr><th>Surface Temperature:</th>       <td><input type='edit' value='300' id='surfTemp' oninput='doPlanetUpdateSurf(); doPlanetUpdate()'> K<br>
                                                <span id='surfTempC'></span> &deg;C</td></tr>
    <tr><th>Blackbody Temperature:</th>     <td><input type='edit' value='300' id='blackbodyTemp' oninput='doPlanetUpdateBlackbody(); doPlanetUpdate()'> K</td></tr>
    <tr><th>Density:</th>                   <td><input type='edit' value='1.0' id='density' oninput='doPlanetUpdate()'> × Earth<br>
                                                <span id='densityGCC'></span> g/cc</td></tr>
    <tr><th>Gravity:</th>                   <td><input type='edit' value='1.0' id='gravity' oninput='doPlanetUpdate()'> Gs <span id='gravRange'></span></td></tr>
    <tr><th>Escape Velocity:</th>           <td><span id='escapeVelocity'></span> km/s</td></tr>
    <tr><th>Diameter:</th>                  <td><span id='diameter'></span> × Earth<br>
                                                <span id='diameterKM'></span> km<br>
                                                <span id='diameterMI'></span> mi</td></tr>
    <tr><th>Radius:</th>                    <td><span id='radiusKM'></span> km<br>
                                                <span id='radiusMI'></span> mi</td></tr>
    <tr><th>Mass:</th>                      <td><span id='mass'></span> × Earth</td></tr>
    <tr><th>Atmospheric Pressure:</th>      <td><span id='atmoPressure'></span> <span id='atmoPressureName'></span></td></tr>
    <tr><th>Rotation Period:</th>           <td><input type='edit' value='1.0' id='rotationPeriod' oninput='doPlanetUpdate()'> hours</td></tr>
    <tr><th>Local Day:</th>                 <td><span id='localDay'></span> hours</td></tr>
    <tr><td colspan='2'>&nbsp;</td></tr>
    <tr><th>Circumference:</th>             <td><span id='circumferenceKM'></span> km<br>
                                                <span id='circumferenceMI'></span> mi</td></tr>
    <tr><th>Total Surface Area:</th>        <td><span id='areaKM'></span> km<sup>2</sup><br>
                                                <span id='areaMI'></span> mi<sup>2</sup></td></tr>
    <tr><th>Land Surface Area:</th>         <td><span id='landAreaKM'></span> km<sup>2</sup><br>
                                                <span id='landAreaMI'></span> mi<sup>2</sup></td></tr>
    <tr><th>Water Surface Area:</th>        <td><span id='waterAreaKM'></span> km<sup>2</sup><br>
                                                <span id='waterAreaMI'></span> mi<sup>2</sup></td></tr>
    <tr><th>Volume:</th>                    <td><span id='volumeKM'></span> km<sup>3</sup><br>
                                                <span id='volumeMI'></span> mi<sup>3</sup></td></tr>
    </table>
    <div id='relativePressureTable'></div>

</td><td>

    <h2 class='section-title'>Moon Calculator</h2>
    <p class='quick-tip'>Lunar orbits should be between 5 and 40 planetary diameters (inclusive).

    <table>
    <tr><th>Orbital Radius:</th>            <td><span id='moonOrbit'></span> km<br>
                                                <input type='edit' value='10' id='moonOrbitDiameters' oninput='doMoonUpdate()'> × Planetary Diameters<br>
                                                <span id='moonOrbitEarthDiameters'></span> × Earth Diameters</td></tr>
    <tr><th>Orbital Period:</th>            <td><span id='moonOrbitalPeriodEarthDays'></span> days (Earth)<br>
                                                <span id='moonOrbitalPeriodLocalDays'></span> days (local)</td>
    <tr><td colspan='2'>&nbsp;</td></tr>
    <tr><th>Lunar Mass:</th>                <td><input type='edit' value='0.01' id='moonMass' oninput='doMoonUpdate()'> × Earth</td></tr>
    <tr><th>Lunar Diameter:</th>            <td><input type='edit' value='0.2' id='moonDiameter' oninput='doMoonUpdate()'> × Earth</td></tr>
    <tr><th>Tidal Force on Planet:</th>     <td><span id='tidalForceOnPlanet'></span></td></tr>
    <tr><th>Tidal Force From Planet:</th>   <td><span id='tidalForceFromPlanet'></span></td></tr>
    </table>

    <h2 class='section-title'>Tidal Force Calculator</h2>
    <p class='quick-tip'>If Total Tidal Effect is greater than or equal to 50, the planet or moon is tidaly locked to its parent.
    <p class='quick-tip'>A Tidal Force of 1 is an ocean tide of about 2 feet at high tide in deep ocean (will vary due to local geography).
    <p class='quick-tip'>If a planet does not have a moon, enter zero for its tidal effect.

    <table>
    <tr><th>Solar Tidal Effect:</th>        <td><span id='solarTide'></span></td></tr>
    <tr><th>Moon 1 Tidal Effect:</th>       <td><input type='edit' value='0' id='moon1tides' oninput='doTidesUpdate()'> × Earth</td></tr>
    <tr><th>Moon 2 Tidal Effect:</th>       <td><input type='edit' value='0' id='moon2tides' oninput='doTidesUpdate()'> × Earth</td></tr>
    <tr><th>Total Tidal Force:</th>         <td><span id='totalTidal'></span></td></tr>
    </table>

</td></tr></table>

<script src='scripts/big.min.js'></script>
<script src='scripts/tool_planet.js'></script>
</div>