---
category: animal
world: raesan vie
parent_name: Raesan Vie
parent_addr: raesanvie
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Ruksou</th></tr>
    <tr><th>Homeworld</th><td><a href='raesanvie.html'>Raesan Vie</a></td></tr>
    <tr><th>Biome</th><td>Tropical Rain Forest</td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
    <tr><th>Height</th><td>2' (0.6 m)</td></tr>
    <tr><th>Weight</th><td>70 lbs (32 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
    <tr><th>Litter size:</th><td>1</td></tr>
    <tr><th>Maturity:</th><td>6½ months</td></tr>
    <tr><th>Lifespan:</th><td>10 years</td></tr>
</table></div>


Ruksou are animals native to [Raesan Vie](raesanvie.md). They stand two feet to their shoulders and have a short tail ending with a white tip. Most of their body is covered in dark red-brown fur, but their belly and throat are bare, revealing their lighter coloured skin.

Ruksou are herbivores and eat primarily fruit.

## Reproduction

It is important for off-worlders to be able to recognize ruksou and identify when the animals are near as ruksou reproduction is parasitical. Ruksou do not distinguish between sophants and animals, though sophants are better at avoiding ruksou. Become a ruksou host is especially problematic for off-worlders; not only is the experience embarrassing, but many governments consider ruksou to be dangerous animals and those hosting an embryo are often required to remain on Raesan Vie until after the birth.

Ruksou are not capable of reproducing with hosts that are significantly larger or smaller than themselves and non-mammalian species are unaffected and ignored. Off-world species are sometimes effected. In effected off-world species, implantation has a high failure rate.

Ruksou have only one sex, one that resembles a male mammal. They produce a viable embryo without any form of coupling or contact with other ruksou. Once ready, this embryo will remain viable for several months. During this time, the ruksou will produce pheromones that greatly increase any sexual arousal being experienced by females. Males will initially react to the pheromones with lassitude, followed by strong sexual arousal.

Implantation occurs through a sex-like act. The female will typically be sufficiently aroused that she will not resist; if they do, the ruksou will back off and find another victim. After this, the ruksou will flee the area. As the pheromones wear off, nearby males will become highly aroused and will try to mount the still aroused female, though sophants may show more self-control. Impregnation can occur as normal, only an extra fetus will develop alongside the host's real offspring. If a regular pregnancy does not occur, the ruksou embryo will still attempt to implant.

The ruksou embryo will absorb a small amount of DNA from its host and it will mimic the appearance of its host's natural offspring until it reaches maturity. The gestation period will be identical to its host's normal span. In animals, the host typically cannot tell the difference between a newborn ruksou and its natural offspring.

Six to seven months after being born, the young ruksou will shed the appearance of its host and begin seeking hosts. Because of the timing and proximity of a ruksou to its host, it is not uncommon for a ruksou's host to also be its victim.
