---
category: world
world: earth
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Earth</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/earth.png'></td></tr>
<tr><th>Star</th>                   <td>G2V</td></tr>
<tr><th>Year Length:</th>           <td>365.26 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>288 K (14.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>1.00 G</td></tr>
<tr><th>Day Length:</th>            <td>24 h</td></tr>
<tr><th>Moons</th>                  <td>One</td></tr>
<tr><td colspan='2' class='wide-data'><a href='https://en.wikipedia.org/wiki/Earth'>Earth System</a></td></tr>
</table></div>

Earth is the third planet around a G2 star. The native sophants of Earth are the [humans](human.md).

The Earth gate is located in northern Canada, in the northeastern Nunavut Territories. The location is generally inaccessible, though the desirability of off-planet contact is resulting in the growth of a surrounding settlement and the construction of an airport rather larger than would otherwise suit the area.

