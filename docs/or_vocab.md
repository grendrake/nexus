---
category: language
world: interworld nexus
parent_name: Interworld Nexus
parent_addr: overview
notoc: true
---

<div class='vocab-frequencies-button'>
    <button onclick='doFrequencies()'>Calculate Letter Frequencies</button>
</div>
<div id='freqOutput' class='vocab-frequencies'></div>

<table clas='vocabtable'>
    <thead>
        <tr>
            <th onclick='resort("lexical")'>Lexical</th>
            <th onclick='resort("gloss")'>Gloss</th>
            <th>Definition</th>
        </tr>
    </thead>
    <tbody id='vocablist'>
    </tbody>
</table>

<script src='/scripts/vocab-or.js'></script>
<script src='/scripts/vocab.js'></script>
