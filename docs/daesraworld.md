---
category: world
notoc: true
world: daesra world
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>The Daesra World</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/daesraworld.png'></td></tr>
<tr><th>Star</th>                   <td>G1V</td></tr>
<tr><th>Year Length:</th>           <td>208.18 Local days<br>363.19 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>303 K (29.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>0.90 G</td></tr>
<tr><th>Day Length:</th>            <td>41.870 h</td></tr>
<tr><th>Moons</th>                  <td>None</td></tr>
<tr><td colspan='2' class='wide-data'><a href='systems.html#the-daesra-world'>Daesra World System</a></td></tr>
</table></div>

The Daesra World is an unremarkable planet orbiting around a G1 star. Unlike the planet itself, however, the name of the world is unusual. The native sophants, the [daesra](daesra.md), communicate through telepathy and do not have a spoken language. As their telepathy results in this being translated into the listener's native language, the world has come to be known as the "Daesra World" (or whatever the appropriate translation of that phrase would be).
