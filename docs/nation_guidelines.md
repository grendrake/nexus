---
category: guidelines
notoc: true
---

While I'm not looking for anyone else to create nations or cultures for the Nexus, I thought I'd jot down some notes on the guidelines I consider when adding new ones to this setting. I hope it will be of some interest to you. Many of the guidelines here are similar to those in the species guidelines, but there are some notable differences.

While cultures and nations are not the same things, I normally create them at the same time as linked entities.

- Both should be loosely plausible. Like with species, I'd like to avoid things where people's first reaction is disbelief.
- They should be unique. I try to avoid anything that immediately makes readers think of any real-world culture or nation or any from pop culture. To an extent this is unavoidable (there are a lot of real and fictional countries and cultures), but I at least try to avoid deliberate similarity.
- Nations and cultures should be meaningfully distinct. This is a much looser guideline here than with species. Although the goal is for readers to be able to tell different nations or cultures apart, some really are quite similar, especially at the level of detail I'm trying to document.
- When documenting information about a nation or culture, I try to keep the description at a fairly high level and not focus on specific details except those I actually come up with and use in the course of writing a story. In the past I have gone into great detail when creating these, but I find it often results in a large amount of material that I never end up using, but that makes it harder to find the detail that I actually need.
