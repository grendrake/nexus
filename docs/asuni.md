---
category: sophant
world: raesan vie
parent_name: Raesan Vie
parent_addr: raesanvie
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Asuni</th></tr>
<tr><th>Homeworld:</th><td><a href='raesanvie.html'>Raesan Vie</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>2'6" (0.8 m)</td></tr>
<tr><th>Avg. Weight:</th><td>215 lbs (114 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>1</td></tr>
<tr><th>Gestation:</th><td>9 months</td></tr>
<tr><th>Weaned:</th><td>1 year, 1 month</td></tr>
<tr><th>Maturity:</th><td>17 years</td></tr>
<tr><th>Lifespan:</th><td>85 years</td></tr>
</table></div>

The asuni are the species that have been on Tarceen the longest and are the only only true native sophant to the planet. They have retained the same range for longer than any other species and had occupied it even before the original arrival of the drius.

Although they’re quadrupeds, asuni forepaws are surprisingly dexterous and can function as hands. They perform this task worse than the hands of any bipeds even before taking into account their dual purpose as feet. This dual purpose also makes it difficult to carry anything while walking; unless they’ve got clothing with pockets on they are limited to stumbling along on three legs or carrying things in their mouth.

Far more asuni body language and behaviour is instinctual than in other sophant species. Some researchers believe that even portions of their language and spirituality operate on an instinctive level.

<figure class='center' style='width:704px'>
    <img src='/art/species/asuni-both.png' width='704' height='393'
    <figcaption>
    Female asuni with skull mask (left) and male asuni (right).<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

Their base fur colours is red-brown or tan and ranges from light to dark. Most have spots on their thights, shoulders, neck, and the base of their tail. These spots will contrast with their base fur color. Their belly fur is a lighter shade than their base color.

Male asuni have a small fur tuft on the end of their tail. Female asuni have a thick lion-like mane. In either case, the fur is the same color as their base color.

They have saber-teeth that are about six centimetres (2¼ inches). As a result they are often thought of as carnivores, but they are actually omnivores and meat is no more than half their diet. The remainder is composed of fruits, berries. and tubers.

## Reproduction

For asuni living in temperate regions, estrus occurs in late summer. Those living in tropical regions will often experience estrus every few months while not pregnant or nursing. Estrus lasts for about twenty days during which female asuni release pheromones that stimulate sexual desire in nearby males. These pheromones work on an unconscious level; their presence is noticeably only by the way male asuni become aroused.

Despite its strongly instinctual nature, sexuality among the asuni is not mechanistic. Most individuals enjoy sex. It is not uncommon for asuni to take advantage of herbs that can stimulate or suppress estrus or that can duplicate the effects of female pheromones in males. This later is especially common amongst male asuni outside of Raesan Vie as they may only rarely encounter other asuni, never mind female asuni in estrus.

On the other hand, this instinctual nature has been blamed for the typical near total lack of creativity in asuni sexuality even more than the limitations that come of being a quadruped.

(**Todo:** unseasonal sexuality; should probably go in the Raesan Vie article anyway)

Offspring are born in early spring. Sometimes female asuni who recently gave birth with experience pseudoestrus for up to four days; they will not ovulate during this.

For reasons that have yet to be identified, only about a third of female asuni are fertile. Sterile females will still experience estrus. This sterilty can be easily diagnosed through modern technology (largely unavailable in Raesan Vie) or through trial and error. Most asuni are phlegmatic about this widespread sterility, despite the threat it could potentially pose should the death rate rise.

### Courting

Like many areas of asuni behaviour, asuni courtship has a strong instinctual basis to it. That isn’t to say the asuni don’t have any control, but it does result in courting rituals that other species tend to find highly formalized and that vary little between groups.

Courtship begins with a physical gesture, such as nuzzling or a brushing of sides. The other individual will either move away or return the attention. If it is returned than such gestures may be exchanged for some time. None of the actions taken at this point are overtly sexual, although it is not uncommon for the male to be partially erect from close proximity to the pheromones of the female asuni. The courtship is typically initiated by a male, but in as many as a quarter of instances, may be initiated by a female asuni.

After a while the male asuni will roll onto his back, displaying his belly and genitals to the female. In rare cases the female may change her mind and leave at this point, but in most cases she will look over and play with the male’s sheath and penis. This can take an extended period for the first male a female takes interest in during a given estrus period, but is often quite short for others.

Once satisfied, the female will step away and crouch, exposing her genitals to the male. In some cases the male will engage in a brief examination of the female’s genitals similar to the one she had just performed, but in most cases the male will mount the male at this point. Coitus is relatively brief, but may be repeated several times; as many as a dozen repitions have been reported.

After the final round of coitus, the couple will resume the physical gestures they started with; in many cases one or both individuals will go to sleep. Those who don’t go to sleep will depart after this.
