---
category: animal
world: zifaelafojun
parent_name: Zifaelafojun
parent_addr: zifaelafojun
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Ini</th></tr>
    <tr><th>Homeworld</th><td><a href='zifaelafojun.html'>Zifaelafojun</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
    <tr><th>Height</th><td>6'6" (1.1 m)</td></tr>
<!--    <tr><th>Weight</th><td>390 lbs (177 kg)</td></tr> -->
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
    <tr><th>Litter size:</th><td>5</td></tr>
    <tr><th>Gestation:</th><td>11 months</td></tr>
    <tr><th>Weaned:</th><td>1 year</td></tr>
    <tr><th>Maturity:</th><td>5 years</td></tr>
    <tr><th>Lifespan:</th><td>15 years</td></tr>
</table></div>


The ini is an animal commonly kept as livestock in [Menota](menota.md) and [Pasha Za Fojun Fae](pashazafojunfae.md). Even more than the [jiruan](jiruan.md), ini are the prduct of extensive selective breeding. They are used for a range of purposes; they are the primary source of meat in the [ravon](ravon.md) diet as well as providing milk. They are also a prime source of physical labour and are often put to plowing fields, pulling wagons, and the like.

Ini have a very thick-bodied appearance and are significantly larger than ravon. Even a single animal is able to provide more meat and milk than a single household is capable of using.  As they naturally live in large groups, a single herd of ini is typically held in common between multiple households of a family.

<figure class='center' style='width:500px'>
    <img src='/art/species/ini.png' width='500' height='231'>
    <figcaption>
    Female ini specimen.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

Perhaps the most memerable attribute of the ini is their intelligence. Or rather, their lack thereof. Millenia of selective breading have resulting in a domesticated animal that is very unintelligent. It is popularly believed that they are kept in round pens because they'd be unable to find their way out of a corner and that they'd wander off cliffs if unattended. They're also very placid; not only do ini barely react to loud, sudden events, they also tend not to react to the pain or panic of their herdmates.
