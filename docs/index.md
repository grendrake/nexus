
Welcome to the [Interworld Nexus](overview.md) information centre! The Interworld Nexus is an extradimensional space that connects different planets (and their native [sophant](https://en.wiktionary.org/wiki/sophont) species) together through portals. These portals link a single location on a world to a specific hub in the Nexus itself.

Information is available about several worlds and their inhabitants as well as the combined culture that has grown in [shared areas](serkmhub.md) of the Nexus. The [overview](overview.md) page is a good place to get an overview of how the Nexus functions. There are also indexes broken down [alphabetically](by_alpha.md) by <a href='/by_category.html'>category</a> and <a href='/by_world.html'>world</a> as well as a <a href='/glossary.html'>glossary</a> that provides quick explanations of many subjects.

This site is structured as a <a href='https://en.wiktionary.org/wiki/wiki#English'>wiki</a>, though it lacks the collaborative and online editing functions a wiki would normally possess. The <a href='about.html'>project background</a> page provides more information about the history and goals of this project.
