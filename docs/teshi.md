---
category: sophant
world: rynux
parent_name: Rynux
parent_addr: rynux
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Teshi</th></tr>
<tr><th>Homeworld:</th><td><a href='rynux.html'>Rynux</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>1.6 m (5’3")</td></tr>
<!-- <tr><th>Avg. Weight:</th><td>96 lbs (43 kg)</td></tr> -->
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>1</td></tr>
<tr><th>Gestation:</th><td>10 months</td></tr>
<tr><th>Weaned:</th><td>4 years</td></tr>
<tr><th>Maturity:</th><td>22 years</td></tr>
<tr><th>Lifespan:</th><td>87 years</td></tr>
</table></div>

Best known as the creators of the [klugrut](klugrut.md) species.

## Description

<!-- {% figure /art/species/zaeren.png | 230 | 390 | right | Male zaeren. | | spelunker %} -->


## Diet


## Reproduction
