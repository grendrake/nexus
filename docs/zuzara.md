---
category: animal
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Zuzara</th></tr>
<tr><th>Homeworld:</th><td><a href='kzaknri.html'>Kzaknri</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>4.5' (1.4 m)</td></tr>
<!-- <tr><th>Avg. Weight:</th><td>127 lbs (58 kg)</td></tr> -->
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Clutch size:</th><td>2</td></tr>
<tr><th>Gestation:</th><td>3 weeks</td></tr>
<tr><th>Incubation:</th><td>6 months</td></tr>
<tr><th>Maturity:</th><td>3 years</td></tr>
<tr><th>Lifespan:</th><td>25 years</td></tr>
</table></div>

Zuzara (Nraezen: <bdo class='nexusFont lang_nr'>&#xe008;&#xe008;&#xe006;</bdo>) are large domesticated animals used for riding, pulling wagons, and other general tasks in [Razaz Nudar](razaznudar.md), particularly among the nomadic population. Due to their size, they are not normally kept as personal pets.

Wild zuzara are known for being aggressive, especially around their hatchlings. Domesticated breeds are more passive, which is to say that they rarely try to eat or maul people. Zuzara who interact with specific people for an extended time may become more companionable to those people, though this is often a mixed blessing as they also become protective of those people and will becoming uncooperative if they see their companion interacting with other people or smell other, unknown people on them.

As the animal with the closest genetic relationship to the [sakin](sakin.md), zuzara share many physical traits with the sakin, such as their scale colouration, the shape of their ears and their general lack of sexual dimorphism, though male zuzara tend to be smaller than females. Unlike the sakin, they are theropodoid with a single, prominent horn on their snout that develops starting at physical maturity.

<figure class='center' style='width:500px'>
    <img src='/art/species/zuzara.png' width='500' height='359'>
    <figcaption>
    Sakin riding zuzara.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

Maturity occurs at about three years. Zuzara experience annual estrus, though domesticated breeds have been bred to be sexually active all year. They will only enter estrus when well fed and not for one or two years after producing a clutch.

Wild zuzara are seasonally monogamous with the male remaining with the female until a few months after the eggs hatch. It is only after this that the female will begin teaching her offspring to hunt and they will remaining with her until they near physical maturity. Domesticated zuzara do not exhibit pair bonding and display considerably less parental care.

Domesticated zuzara have a typical lifespan of 25 years. The lifespan of their wild relatives tends to be shorter due to natural hazards and starvation.
