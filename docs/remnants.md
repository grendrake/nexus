---
category: nation
world: ingr
parent_name: Ingr
parent_addr: ingr
---

Drius surviving into the modern age are almost entirely descendants of members of the Kthbra clan who managed to survive the civil war that ended Warrothserkm. Although it is suspected that other surviving enclaves of drius exist on Ingr, planetary conditions (such as surviving automated defence installations and inhospitable areas created by weapons of mass destruction) make exploring difficult. These known surviving drius are often referred to as the Kthbra Remnants to distinguish them from the drius of Warrothserkm. Aside from the surviving drius population, a small community of researchers and traders exists on the shore closest to the gate.

The inhabited territory is a couple of large, temperate islands. Like all of Ingr, these islands experience strong seasonal variations and most foreign residents will choose to return home rather than stay for the winter.

The culture among the Kthbra Remnants is drastically changed from that of the empire they originated from; much of what was considered normal in the empire is now considered inappropriate. Whereas automated farms and production centres produced almost all food and products in the empire, the Kthbra Remnants are primarily a hunter-gatherer, though they do make use of some horticultural techniques as well. Although daily activities and life vary by season, it is otherwise uncommon for anyone to keep track of the date.


## Daily Life

Most people live in small, semi-nomadic groups called bands consisting of 20 to 30 people, plus children. Membership in this groups is fluid; although children typically remain with one parent, adults move between different groups freely. Residence in a specific group is based as much on personal convenience as anything.

Although a few permanent structures exist, especially in those areas where metalworking is practised, most of the Kthbra Remnants live in large, hide tents. Most adults have their own tents that they share with their children, there are often additional tents for guests, storage, or other purposes.

Most locals are quite happy to welcome foreigners into their lives, at least as any individual foreigner does not stay more than 3 or 4 months. Foreign guests are welcomed into the band's daily lives, typically excluded only from pilgrimages and from the classes for youth held by shamans. Drius not raised on Ingr are considered to be just as foreign as non-drius.

### Family Life

Most people live alone or with a single partner; an exception to this is that children live with one parent (normally their mother) until they become adults. While couples living together have close social ties, they do not always (or even usually) have a sexual relationship.

The Kthbra Remnants practice serial monogamy. Each year, they take a single partner and, while they remain faithful for the entire year, there is no expectation that they will take the same partner in future years. Two people consistently partnering with each other over multiple years, while permitted, would not be viewed positively. Conversely, despite having distinct gender roles, the sex of one's chosen partner is considered unimportant. Likewise, choosing to have no partner (or being unable to get one) is also considered entirely reasonable, even over multiple years.

There is a strong expectation of fidelity to one's chosen partner for the year. The general attitude is that, even in the worst case, it's only one year of an adult life that will span multiple decades and the pair can just avoid each other. This extends to those choosing not to have a partner; they are expected to hold to this decision, though sex with foreigners does not count as long as it's discreet.

A young adult will often form a stronger bond with the first person they partner with after becoming an adult. As new adults are encouraged to partner with other new adults, they bonds are typically mutual and sometimes result in the pair becoming a more permanent couple. Not only do these couples not normally have sexual relations after the first year, they will commonly assist each other in finding new partners every year.

Children will live with their mother until they become independent. Despite this, they are raised communally. Having a child is considered a major responsibility; despite the community's love for children, the decision to become pregnant is made only with consultation with others and the shaman. Various means are used to prevent pregnancies, the most popular of which is an herbal mixture that hinders ovulation and implantation while having minimal side-effects. Such contraceptives are not perfect, nor are they always used correctly, and unexpected children occur from time to time. Such children are warmly welcomed, even if their parents face questioning and scorn.

Most new parents are young adults. Older adults will sometimes have children, but it is generally felt that childbearing is best left to the young and energetic.

### Festivals

The Kthbra Remnants are a cheerful people and take even the slightest reason to celebrate. The coming of spring and summer and celebrated with festivals, as are good hunts, meetings between bands, and the birth and coming of age of children. Festivals are filled with light, song, dance, and plentiful food.

#### Coming of Age

A youth is considered to be ready to come of age when they become physically mature. Once this is known, they are expected to take part in a coming-of-age ritual during the following spring. Failing the rite is rare, but possible and is considered embarrassing. Those who fail are permitted to try again the following year.


### Social Roles

Neither sex is held to be superior, but men and women do have different roles in society. Physical differences are often used to justify these roles, but incidences of individuals taking the opposite sex's role suggest they are enforced mostly by tradition.

Women are considered to be hunters and their duller fur colours are believed to make it easier for them to stalk prey. Women with young children often remain in camp to nurse and raise them, but particularly effective hunters may be encouraged to have other women take on nursing their children so they can continue to hunt during this time.

Men typically mind the camp and take care of older children. They often also practice horticulture and forage for fruits and edible plants as their bright coloration won't startle or scare off plants. Many shamans are men.

Children remain in camp with the men and will assist with daily tasks while adolescents and young adults will be tasked to train and learn the skills that will be required of them as an adult. Many will also sneak off to fish or pick shellfish from the beach; adults pretend not to notice this as long as it doesn't happen too often.


## Technology

Visible technology typically varies between appearing to be stone age and early iron age. Despite this, considerable knowledge is in evidence and it is not unusual to discover someone constructing flint tools who is also able to discuss cutting-edge science. *Shamans* are responsible for keeping this knowledge and acting as teachers to the youth in their band. The name 'shaman' was originally given as the techniques they used (especially in dealing with the sick and injured) appeared supernatural in the low-tech environment, but it is now known that they were merely using modern methods without the equipment and surroundings typically associated with modern practices.

Perhaps the best demonstration of the difference between the visible technology and the actual technological level is the general good health of the locals. Sickness, injuries, and even scars are virtual unknown. Even individuals missing limbs have been known to reacquire them after not being around foreigners for a while; the mechanics of this are unknown and explanations are not forthcoming, but it is known that drius do not naturally regrow missing limbs.

Shamans are known to make regular pilgrimages to "sacred" sites where they "speak with their ancestors" or to an entity named Argus. Given the near total lack of mysticism and spirituality most shamans exhibit, these are presumed to be euphemisms and that the shamans are actually visiting surviving facilities and accessing the knowledge still stored there. Only shamans and those training to become shamans are welcome on the pilgrimages. Attempts to follow shamans are discouraged. Those who try anyway find themselves losing their way and the most persistent have a tendency to vanish entirely.
