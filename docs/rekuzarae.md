---
category: person
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
---

<div class='infobox'><table>
    <tr><th class='top-header' colspan='2'>Rekuzarae</th></tr>
    <tr><th>Nraezen</th>  <td><bdo class='nexusFont lang_nr'></bdo></td></tr>
    <tr><th>Species</th>  <td><a href='sakin.html'>Sakin</a></td></tr>
    <tr><th>Position</th> <td>Emperor of <a href='razaznudar.html'>Razaz Nudar</a></td></tr>
    <tr><th>Title</th>    <td>the Tyrant-Emperor</td></tr>
    <tr><th>Hatched</th>  <td>1853</td></tr>
    <tr><th>Crowned</th>  <td>1873 (aged 20)</td></tr>
    <tr><th>Died</th>     <td>1877 (aged 24)</td></tr>
</table></div>

Rekuzarae is known both for his callous, heavy-handed reign, and for the progressive and beneficial policies he put in place. He is often credited with being responsible for making sure Razaz Nudar stayed relevant in the modern world. Such was the influence of his reign that many forgotten that he was not in power for long; roughly the equivalent of four Earth years.

The general picture of Rekuzarae is of a young sakin, but not a healthy one. Even the most flattering portraits show sunken eyes and pale scales. Scars from his failed execution covered his wrists and ankles. He is typically depicted wearing a loose vest and bandages wrapped around his lower torso made necessary by his poor health. Most depictions have him lounging on a divan or throne. The descriptions of those who met him suggest that this was the position he was usually found in. He was nearly always found with a beverage, but rather than those popular in his time, he primarily drank dark teas laced with opioid-equivalents for pain control.

One of the first things he did in his reign was to "banish" everyone he felt affection for to the colonies that would become the [FASS](fass.md); historians have proposed that his intention was to protect them from the fallout of his ascension and reign. These colonies were also granted independence; this was what caused the creation of the FASS as the colonies banded together in the more hostile-to-sakin climate, but was largely unremarked upon at the time.

At no point during his reign was his position secure; he was constantly besieged by assassins and would-be usurpers. Both as a result of this and as a cause, his rule was harsh; conspirators and enemies would vanish in their sleep, taken by the secret police and never seen again. He demanded that all submit to him and his decrees, publicly executing those who did not. This reached its peak when he had the entire population of a small city executed.

Despite the harshness of his rule, Rekuzarae was also responsible for many societal improvements, particularly in the field of public education (which was nonexistent before). Said public education included both topics such as literacy and basic mathematics, but also subjects such as detailed sexual education and armed and unarmed combat. Most mandatory material focused on practical, day-to-day subjects with little focus on literature, the arts, or science. Both his public education program and others he implemented were highly unpopular at first, but his remorseless reign pushed them through anyway; by the time his reign had come to an end they had become generally accepted.

Personal journals kept by Rekuzarae during his life were found in a hidden chamber in the palace in 1982.
[Zdajgae](zdajgae.md) has made access to these openly available to scholars and copies have been made available on the open market.
Knowledge of the events of Rekuzarae's life has been greatly expanded by this discovery.

There exists a vocal minority that claims Rekuzarae did not die (or, at least, did not die when historians claim he did).
This minority instead suggests that Rekuzarae faked his death to avoid assassins and has since exercised power through his descendants.
This belief has become increasingly discredited as more information about his health becomes known.
His personal journals particularly highlight just how badly his health was afflicting them.
Even among those that believe the theory, however, most believe he has since died.
If Rekuzarae still lived today, he would be nearly 170 years old.