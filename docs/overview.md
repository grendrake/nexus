---
category: general
world: interworld nexus
---

The Interworld Nexus is a place that connects a huge number of worlds with sapient life.
According to current estimates, the Nexus is at least ten thousand Earth-years old.
Its builders are entirely unknown but are referred to as the [vorsyth](vorsyth.md), a name that originated with [Warrothserkm](warrothserkm.md).
Every world is associated with an extra-dimensional region called a *hub*.

The species that make up the inhabitants of the Nexus have remarkably similar biology, more akin to what one might expect to find on a new continent rather than a different planet. All known species breathe compatible atmospheres and can eat the same food, at least to the extent different species ever can. Most diseases cannot cross species lines, though species that have been in extended contact are more likely to have compatible diseases.

This similarity extends to the worlds connected to the Interworld Nexus. The gravity, atmosphere, and atmospheric pressure always lies within a limited range. No known world is different enough that its inhabitants cannot interact with other worlds. It is popularly assumed that, because of this, the [vorsyth](vorsyth.md) seeded worlds with life as well as building the Nexus, though there is no actual evidence of this.


## Planetary Gates

<figure class='right' style='width:310px'>
    <img src='/art/gate.svg' width='310' height='163'>
    <figcaption>
    Diagram of gate.<br>
    Art © <a href='https://www.grenslair.com/'>Gren Drake</a>
    </figcaption>
</figure>

Gates are half-hexagons in shape with dimensions visible in the diagram shown.
Both the gates themselves and a narrow surface at their bases are made from the [engl](engl.md).
In the centre of the top edge of the gate are two symbols.
The first identifies the world and is the same for every gate on the planet.
The second uniquely identifies the gate from all others on the same world.
The set of symbols is the same across all worlds, however.

In their natural state, these gates are inactive, and look like nothing so much as huge, blocky stone arches.
They are unusually conductive, however, far more so than any natural or most artificial materials.
When sufficient electrical power is run through the gate, it will open a connection to its paired gate located in the world's associated hub.


## Hubs

Only one hub is currently known: [Serkm Hub](serkmhub.md), but gates to connect to other hubs exists (even if they haven't been opened) and other hubs do exist, waiting to be discovered.
All hubs are the same size and structurally identical.

A hub is an extra-dimensional space with a hard, fixed border.
This border is made from engl and extends most of a kilometre off of the ground.
Above it is a dome apparently made of the same material, but with a naturalistic image of a sky displayed on it.
This image slowly changes in the same fashion as a natural sky would and, unless one is close enough to touch it, is very difficult to tell apart from a natural sky.

<figure class='center' style='width:600px'>
    <img src='/art/plans/generichub.svg' width='600' height='421'>
    <figcaption>
    Common structure of a hub.<br>
    Art © <a href='https://www.grenslair.com/'>Gren Drake</a>
    </figcaption>
</figure>

Hub have a radius of 100,000 nalwen (88.17 km), giving it a circumference of 314,159 nalwen (554 km) and an area of 7,853,981,634 square nalwen (24,423 km<sup>2</sup>).
This area is, initially, wild but contains scattered connections that supply seemingly limitless amounts of electricity, water, and sewage disposal.
Planetary attributes, such as gravity and atmospheric pressure, are the average of every connected world, whether or not that world is in regular contact with the hub.

### Gates

The border of the hub is spaced with gates nearly identical to those on the connected world.
There are three sets of gates, each containing a gate connected to one of the hub-connected worlds.
This sets are refered to in English as Alpha, Beta, and Gamma gates.
Which set of gates gets which label is entirely random; there are no distinguishing traits between the different groups.

Each gate is linked to its planetary counterpart; both gates in a given pair will be identified by the same pair of symbols at the top of the arch.
If a sophant native to the linked world touches an inactive gate, the gate will open a connection to its planetary counterpart.
As a result, in most cases, it is easier to open a gate from the hub than from a gate on the planet.
If a traveller who is native to a hub or to a different world than the one the gate connects to touches the gate, there is no effect; only those the system consists native to the world can open a gate from the hub.

Despite this, it is possible to open a gate from the hub without being native.
The mechanism was developed by Warrothserkm.
While the understanding of the mechanism was lost in the Days of Fire a few dozen of the actual devices have been recovered.
Each, when attached to a gate, will cause it to be activated as if someone native to the associated world had touched.
Any given device will only work once, however, during which the complex circuitry will be melted.

In addition, there are three larger, gates as well.
In-universe, their purpose is not known and it has proven impossable to open them.
Out-of-universe, they provide access to other hubs, each linking to a specific gate in another hub the same way as the hub-to-planet gates.
The activation mechanism of these gates is undecided, but can be activated by anyone in either hub touching the gate.
Opening devices have no effect on these gates.


### Wilderness

"Wilderness" consists of light forest with small animals.
These are not based on any associated world but are the same across all known hubs.
Both flora fauna are clearly artificial and can provide all required nutrition for sophants from any associated world.
Fauna have never been observed to reproduce or eat.
Their behaviour is almost entirely deterministic; when one dies or is removed from the area an identical replacement will appear from out of sight within a few minutes.
Flora are fast growing until they reach a certain size then they stop growing.
The plants and animals are both consistently reported to taste bland.

### List

**[Serkm Hub](serkmhub.md).**
This is the hub Earth is associated with and the only one currently known.
It is primarily associated with commerce and cooperation.
Although formed by descendants of Warrothserkm, it is focused on the needs of its population in the modern age.
Slavery is permitted, but not encouraged and slaveholders are encouraged to deal with slave-related business outside the hub.


## Technology

Many of the worlds connected to the Interworld Nexus have a similar level of technology. This is roughly similar to that of Earth in the twenty and twenty-first centuries. Most exceptions are worlds with very low tech levels that lack both infrastructure and trade goods valuable enough to warrant the difficulties of interworld trade.

One of the reasons for this is the spread of Warrothserkm over multiple worlds; where they went, they brought ideals of innovation and development with them. Other worlds lacked the ability to reproduce Warrothserkm technology after their fall, but the knowledge and ideals gained served as a catalyst for technological development.


## Identity

The first time a sapient uses a gate, that person is assigned a unique, universal ID number by the Nexus.
Although the fashion in which this is imprinted on a person is not understood, devices were developed by [Warrothserkm](warrothserkm.md) to read this number and these devices have been copied by modern nations.
As the nature of this number is not understood, it is currently impossible to forge, fake, or otherwise change this number.

This number is neither linked to, nor contained by any physical, mental, social, or genetic traits.
Sapients can change sex or species without impacting the number.
Even being subject to the [stone of change](raesanvie.md#stone-of-change) does not cause the number to change.
Conversely, however, a perfectly clone will not share the number of their source sapient and will be assigned their own, unique number the first time they use a gate.

As the ID number is assigned the first time a gate is used, those born in a hub do not have one.
Most hubs expect parents to take children through a gate at a young age, even if all the parents do is immediately return.

### Native Worlds and Hubs

The Interworld Nexus demonstrates an unerring ability to determine where one is native to.
It distinguishes not only between sophants from their traditional homeworld and those raised elsewhere, but also those raised in one of the various hub areas.
This information is neither determined nor stored genetically; even those who have had their DNA completely rewritten do not change where they are native to.
This system applies only to sapients, however.

The rules determining this are below and are considered in sequence; only the first matching rule is used.
"Raised" in this context, means to have spent the majority of one's pre-biologically-adult life somewhere.
"Born" refers to a specific moment during the birthing or hatching process, though it is unclear which moment or how it is determined.

1. A child born on a planet and raised there or the connected hub is native to the planet.
2. A child born to parents native to the same planet will be native to planet if raised in that planet's hub.
3. If one parent was born on a planet, and the other is native to the planet's hub, and the child is raised in the hub, they count as native to the planet.
4. If a child is raised in single location, they are native to that location.
5. Otherwise, a child is native to place where born.
