---
category: god
world: zifaelafojun
parent_name: Zifaelafojun
parent_addr: zifaelafojun
---

<div class='infobox'><table>
    <tr><th class='top-header' colspan='2'>Vaere</th></tr>
    <tr><th>Religion</th>       <td>State religion of <a href='menota.html'>Menota</a> and <a href='pashazafojunfae.html'>Pasha Za Fojun Fae</a></td></tr>
</table></div>

Long ago, [Menota](menota.md) practiced a polytheistic religion, worshipping a large pantheon of gods who symbolized various parts of the world. One of these was Vaere, a god of fertility with a touch of hedonism. She was typically either depicted as being pregnant or as nursing a child. A number of ancient artifacts and artworks depict her engaged in coitus as well, and with a variety of partners not limited to male [ravon](ravon.md). These later depictions are, in modern times, censored or hidden in private collections.

As the population of Menota, and of the ravon species, decreased, the worship of other gods fell off as more and more people focused on Vaere in an attempt to reverse the downward trend. About 1500 Earth years ago, while other gods were still acknowledged, Vaere became the only god receiving meaningful worship.

Over time since then, Menotan culture slowly became more prudish with sex and sexuality becoming something that was hidden away. Likewise, the sexual aspects of the former fertility goddess were hidden and deemphasized. Older rituals that drew on sexuality or sensuality were sanitized for modern worshippers. While traces of their origins are still evident, the original purpose of most rituals has become obscured and unrecognizable.

The Rise of the Association of Vaere and the call to bring back the old rituals has resulted in Vaere regaining many of her former, more sexual and sensual attributes. This, in turn, has caused the church bearing her name to change the focus of worship away from the goddess and to the church itself.
