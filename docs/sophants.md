---
category: general
notoc: true
---

<script>
function swapTo( baseName ) {
    const inl = document.getElementById("inline-lineup");
    inl.src = "/art/lineup_" + baseName + "_sm.png";
    const lnk = document.getElementById("lineup-link");
    lnk.href = "/art/lineup_" + baseName + ".png";
}
</script>
<figure style='margin-right:auto;margin-left:auto'>
    <a href='/art/lineup_male.png' id='lineup-link'><img src="/art/lineup_male_sm.png" width="950" height="231" id='inline-lineup'></a>
    <figcaption>Lineup the the various major sophants; art by <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>. Click on the lineup to view a larger version.<br>
    Version: [<button style='border:none;background:none' onclick='swapTo("male")'>Male</button>][<button style='border:none;background:none' onclick='swapTo("female")'>Female</button>]<br>
From left to right: syri, sekar, daesra, asuni, asuni, ravon, zaeren,
rakelm, au, sakin, drius, female drius, garor.</figcaption>
</figure>

Several sophants can be found in the nexus. Most people will only interact with a few over the course of their lives. Some sophants, such as the asuni or the sekar, are simply rarely seen outside their homelands while others, such as the ravon, are actively reclusive.

Quick reference tables are provided for various different sophants.
All values should be taken as averages rather than absolutes; some individuals will be larger or smaller and possibly greatly so.

Species are divided into a small number of groups, determined mostly by how common they are in [Serkm Hub](serkmhub.md).

**Group A**
:   These species are the most common and collectively make up about 80% of Serkm Hub's population.
    Most people will not only recognize them, but also be reasonably familiar with them.
    They can expect businesses to already have suitable arrangements to accomodate them.

**Group B**
:   About 15% of the population are composed of these species.
    They will typically be recognized, but only those regularly interacting with them will reliably know anything that's not obvious about them.
    Most major businesses will have suitable accomodations, but small businesses will be more hit-and-miss.

**Group C**
:   These species combined make up the remaining population.
    They will seem exotic to most other residents and most businesses will only have suitable arragenements by chance.

**Group E**
:   The only known species in this group is apparently extinct and have no known living members, either in or out of Serkm Hub.

**Group R**
:   These species are restricted and not permitted free entry to Serkm Hub, though individual members may be granted travel permits.
    Most group R species pose a safety or security risk to the general population that cannot be easily mitigated.

**Group Z**
:   This final group is made up of the [Zaeren](zaeren.md) and [Syri](syri.md), two species whose main known populations are found in Serkm Hub.

<script>
function sorttable(tableId, columnNum, sortType) {
    if (sortType == "trait") sortType = 1;
    else if (sortType == "number") sortType = 2;
    else sortType = 0;

    const eTable = document.getElementById(tableId);
    if (!eTable) {
        console.error("No such table:", tableId);
        return;
    }
    const eBody = eTable.getElementsByTagName("tbody")[0];
    if (!eBody) {
        console.error("Table", tableId, "has no body.");
        return;
    }

    const rows = [];

    for (let i = 0; i < eBody.children.length; ++i) {
        rows.push(eBody.children[i]);
    }

    rows.sort(function(l, r) {
        const e1 = l.children[columnNum];
        const e2 = r.children[columnNum];
        if (sortType == 2) {
            v1 = parseFloat(e1.innerText);
            v2 = parseFloat(e2.innerText);
            if (isNaN(v1) && isNaN(v2)) return 0;
            if (isNaN(v1) && !isNaN(v2)) return 1;
            if (!isNaN(v1) && isNaN(v2)) return -1;
            if (v1 > v2) return 1;
            if (v1 < v2) return -1;
        } else if (sortType == 1) {
            if (e1.innerText < e2.innerText) return 1;
            if (e1.innerText > e2.innerText) return -1;
        } else {
            if (e1.innerText > e2.innerText) return 1;
            if (e1.innerText < e2.innerText) return -1;
        }
        return 0;
    });

    const eNewBody = document.createElement("tbody");
    rows.forEach(function(row) {
        eNewBody.appendChild(row);
    });
    eBody.replaceWith(eNewBody);
}
</script>

<table class='table-centered-text' id='species-general'><caption>General Traits Table</caption>
<thead>
<tr><th onclick='sorttable("species-general", 0, false)' class='td-left'>Species</th>
    <th onclick='sorttable("species-general", 1, false)' title='Species Group'>Grp</th>
    <th onclick='sorttable("species-general", 2, false)' class='td-left'>Height</th>
    <th onclick='sorttable("species-general", 3, false)' class='td-left'>Homeworld</th>
    <th onclick='sorttable("species-general", 4, "trait")' class='line-left' title="Biped">Bi</th>
    <th onclick='sorttable("species-general", 5, "trait")' title="Quadruped">Qu</th>
    <th onclick='sorttable("species-general", 6, "trait")' title="Theropodoid">Th</th>
    <th onclick='sorttable("species-general", 7, "trait")' class='line-left' title="Hide">Hi</th>
    <th onclick='sorttable("species-general", 8, "trait")' title="Fur">Fu</th>
    <th onclick='sorttable("species-general", 9, "trait")' title="Scales">Sc</th>
    <th onclick='sorttable("species-general", 10, "trait")' class='line-left' title="Can Fly">Fl</th>
    <th onclick='sorttable("species-general", 11, "trait")' title="Has Horns">Ho</th>
    <th onclick='sorttable("species-general", 12, "trait")' title="Carnivore">Ca</th>
</tr>
</thead><tbody>
<tr><td class='td-left'><a href='/adranel.html'>Adranel</a></td>     <td>R</td> <td class='td-left'>0.1&nbsp;m&nbsp;(0‘4”)</td>   <td class='td-left'><a href='/kloippraks.html'>Kloip Praks</a></td>         <td class='line-left'></td><td></td><td></td>           <td class='line-left'></td><td></td><td></td>           <td class='line-left'></td>         <td></td>           <td></td>    </tr>
<tr><td class='td-left'><a href='/asuni.html'>Asuni</a></td>         <td>C</td> <td class='td-left'>0.8&nbsp;m&nbsp;(2‘6”)</td>   <td class='td-left'><a href='/raesanvie.html'>Raesan Vie</a></td>           <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td>         <td></td>           <td></td>    </tr>
<tr><td class='td-left'><a href='/au.html'>Au &#9794;</a></td>       <td>A</td> <td class='td-left'>0.9&nbsp;m&nbsp;(3’)  </td>   <td class='td-left'><a href='/erladi.html'>Erladi</a></td>                  <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td>         <td></td>           <td></td>    </tr>
<tr><td class='td-left'><a href='/au.html'>Au &#9792;</a></td>       <td>A</td> <td class='td-left'>1.6&nbsp;m&nbsp;(5‘3”)</td>   <td class='td-left'><a href='/erladi.html'>Erladi</a></td>                  <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td>         <td></td>           <td></td>    </tr>
<tr><td class='td-left'><a href='/daesra.html'>Daesra</a></td>       <td>B</td> <td class='td-left'>0.6&nbsp;m&nbsp;(2’)  </td>   <td class='td-left'><a href='/daesraworld.html'>The Daesra World</a></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td>         <td></td>           <td>&#10003;</td>    </tr>
<tr><td class='td-left'><a href='/drius.html'>Drius</a></td>         <td>C</td> <td class='td-left'>1.9&nbsp;m&nbsp;(6‘3”)</td>   <td class='td-left'><a href='/ingr.html'>Ingr</a></td>                      <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td>         <td></td>           <td></td>    </tr>
<tr><td class='td-left'><a href='/garor.html'>Garor &#9794;</a></td> <td>A</td> <td class='td-left'>2.2&nbsp;m&nbsp;(7’3")</td>   <td class='td-left'><a href='/vakatr.html'>Va-Katr</a></td>                 <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td>         <td>&#10003;</td>   <td></td>    </tr>
<tr><td class='td-left'><a href='/garor.html'>Garor &#9792;</a></td> <td>A</td> <td class='td-left'>1.9&nbsp;m&nbsp;(6’3")</td>   <td class='td-left'><a href='/vakatr.html'>Va-Katr</a></td>                 <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td>         <td>&#10003;</td>   <td></td>    </tr>
<tr><td class='td-left'><a href='/guas.html'>Guas</a></td>           <td>C</td> <td class='td-left'>1.3&nbsp;m&nbsp;(4’3")</td>   <td class='td-left'><a href='/ujan.html'>Ujan</a></td>                      <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td><td></td><td>&#10003;</td>   <td class='line-left'></td>         <td>&#10003;</td>   <td></td>    </tr>
<tr><td class='td-left'><a href='/human.html'>Human</a></td>         <td>B</td> <td class='td-left'>1.8&nbsp;m&nbsp;(5‘11”)</td>  <td class='td-left'><a href='/earth.html'>Earth</a></td>                    <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td>         <td></td>           <td></td>    </tr>
<tr><td class='td-left'><a href='/klugrut.html'>Klugrut</a></td>     <td>B</td> <td class='td-left'>1.2&nbsp;m&nbsp;(3’11")</td>  <td class='td-left'><a href='/rynux.html'>Rynux</a></td>                    <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td>         <td>&#10003;</td>   <td></td>    </tr>
<tr><td class='td-left'><a href='/naesir.html'>Naesir</a></td>       <td>C</td> <td class='td-left'>1.9&nbsp;m&nbsp;(6’2")</td>   <td class='td-left'><a href='/rojizruzz.html'>Rojizruzz</a></td>            <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td>         <td></td>           <td></td>    </tr>
<tr><td class='td-left'><a href='/rakelm.html'>Rakelm</a></td>       <td>A</td> <td class='td-left'>1.2&nbsp;m&nbsp;(4’)  </td>   <td class='td-left'><a href='/vakatr.html'>Va-Katr</a></td>                 <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'>&#10003;</td> <td></td>           <td></td>    </tr>
<tr><td class='td-left'><a href='/ravon.html'>Ravon</a></td>         <td>B</td> <td class='td-left'>0.6&nbsp;m&nbsp;(2’)  </td>   <td class='td-left'><a href='/zifaelafojun.html'>Zifaelafojun</a></td>      <td class='line-left'></td><td></td><td>&#10003;</td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'>&#10003;</td> <td>&#10003;</td>   <td></td>    </tr>
<tr><td class='td-left'><a href='/sakin.html'>Sakin</a></td>         <td>A</td> <td class='td-left'>1.5&nbsp;m&nbsp;(5’)  </td>   <td class='td-left'><a href='/kzaknri.html'>Kzaknri</a></td>                <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td><td></td><td>&#10003;</td>   <td class='line-left'></td>         <td></td>           <td>&#10003;</td>    </tr>
<tr><td class='td-left'><a href='/sekar.html'>Sekar</a></td>         <td>R</td> <td class='td-left'>1.5&nbsp;m&nbsp;(4’11")</td>  <td class='td-left'><a href='/isaguyat.html'>Isaguyat</a></td>              <td class='line-left'></td><td></td><td>&#10003;</td>   <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td>         <td>&#10003;</td>   <td></td>    </tr>
<tr><td class='td-left'><a href='/syri.html'>Syri</a></td>           <td>Z</td> <td class='td-left'>0.3&nbsp;m&nbsp;(1’)  </td>   <td class='td-left'>Unknown</td>                                            <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td>         <td></td>           <td></td>    </tr>
<tr><td class='td-left'><a href='/teshi.html'>Teshi</a></td>         <td>C</td> <td class='td-left'>1.6&nbsp;m&nbsp;(5’3")</td>   <td class='td-left'><a href='/rynux.html'>Rynux</a></td>                    <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td>         <td></td>           <td></td>    </tr>
<tr><td class='td-left'><a href='/ynathi.html'>Ynathi</a></td>       <td>R</td> <td class='td-left'>0.4&nbsp;m&nbsp;(1’4")</td>   <td class='td-left'><a href='/vozruat.html'>Vozruat</a></td>              <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td>         <td></td>           <td></td>    </tr>
<tr><td class='td-left'><a href='/zaeren.html'>Zaeren</a></td>       <td>Z</td> <td class='td-left'>1.0&nbsp;m&nbsp;(3’6")</td>   <td class='td-left'>Unknown</td>                                            <td class='line-left'>&#10003;</td><td></td><td></td>   <td class='line-left'></td><td>&#10003;</td><td></td>   <td class='line-left'></td>         <td></td>           <td></td>    </tr>
</tbody><tfoot>
<tr class='totalrow' class='td-left'><td class='td-left'>Average&nbsp;(biped)</td>       <td></td> <td class='td-left'>1.6&nbsp;m&nbsp;(5'3")</td>   <td class='td-left'>Percent</td>                            <td class='line-left'>61%</td><td>33%</td><td>11%</td>  <td class='line-left'>33%</td><td>56%</td><td>11%</td>  <td class='line-left'>11%</td>      <td>28%</td>        <td>11%</td> </tr>
<tr class='totalrow2' class='td-left'><td class='td-left'>Average&nbsp;(non-biped)</td>  <td></td> <td class='td-left'>0.6&nbsp;m&nbsp;(2')  </td>   <td></td>                                                   <td class='line-left'></td>     <td></td>     <td></td> <td class='line-left'></td><td></td><td></td>           <td class='line-left'></td>         <td></td>           <td></td>    </tr>
</tfoot></table>

<p>Adranel not included in stats. Height for bipeds is head to ground. Height for quadrupeds is top of shoulder to ground. Height for ravon and sekar is top of hips to ground.


<table class='table-centered-text' id='species-reproductive'><caption>Reproductive Traits Table</caption><thead>
<tr class='preheader'>
    <th onclick='sorttable("species-reproductive", 0, false)'   >Species</th>
    <th onclick='sorttable("species-reproductive", 1, false)'    title='Litter Size'>LS</th>
    <th onclick='sorttable("species-reproductive", 2, "trait")'  title='Oviparous'>Ovi</th>
    <th onclick='sorttable("species-reproductive", 3, "number")'>Gestation</th>
    <th onclick='sorttable("species-reproductive", 4, "number")'>Incubation</th>
    <th onclick='sorttable("species-reproductive", 5, "number")'>Weaned</th>
    <th onclick='sorttable("species-reproductive", 6, "number")'>Maturity</th>
    <th onclick='sorttable("species-reproductive", 7, "number")'>Lifespan</th>
</tr>
<tr class='unitheader'>
    <th colspan='3'></th>
    <th onclick='sorttable("species-reproductive", 3, "number")'>(months)</th>
    <th onclick='sorttable("species-reproductive", 4, "number")'>(months)</th>
    <th onclick='sorttable("species-reproductive", 5, "number")'>(years)</th>
    <th onclick='sorttable("species-reproductive", 6, "number")'>(years)</th>
    <th onclick='sorttable("species-reproductive", 7, "number")'>(years)</th>
</tr>
</thead><tbody>
<tr><td class='td-left'>Adranel</td>  <td>6</td>    <td>&#10003;</td>   <td>0.25</td>   <td>4</td>          <td>&mdash;</td> <td>14</td>         <td>64</td></tr>
<tr><td class='td-left'>Asuni</td>    <td>1</td>    <td></td>           <td>9</td>      <td>&mdash;</td>    <td>1.083...</td> <td>17</td>        <td>85</td></tr>
<tr><td class='td-left'>Au</td>       <td>2</td>    <td></td>           <td>10</td>     <td>&mdash;</td>    <td>5</td>     <td>16</td>           <td>75</td></tr>
<tr><td class='td-left'>Daesra</td>   <td>2</td>    <td></td>           <td>7</td>      <td>&mdash;</td>    <td>1.166...</td> <td>12</td>        <td>70</td></tr>
<tr><td class='td-left'>Drius</td>    <td>1</td>    <td></td>           <td>10</td>     <td>&mdash;</td>    <td>3</td>     <td>23</td>           <td>110</td></tr>
<tr><td class='td-left'>Garor</td>    <td>1</td>    <td></td>           <td>11</td>     <td>&mdash;</td>    <td>4.166...</td> <td>20</td>        <td>98</td></tr>
<tr><td class='td-left'>Guas</td>     <td>1</td>    <td>&#10003;</td>   <td>4</td>      <td>4</td>          <td>&mdash;</td> <td>17</td>         <td>94</td></tr>
<tr><td class='td-left'>Human</td>    <td>1</td>    <td></td>           <td>9</td>      <td>&mdash;</td>    <td>3.5</td> <td>17</td>             <td>90</td></tr>
<tr><td class='td-left'>Klugrut</td>  <td>2</td>    <td></td>           <td>8</td>      <td>&mdash;</td>    <td>3</td>       <td>22</td>         <td>79</td></tr>
<tr><td class='td-left'>Naesir</td>   <td>1</td>    <td></td>           <td>11</td>     <td>&mdash;</td>    <td>5</td>       <td>25</td>         <td>81</td></tr>
<tr><td class='td-left'>Rakelm</td>   <td>2</td>    <td>&#10003;</td>   <td>2</td>       <td>6</td>         <td>&mdash;</td> <td>15</td>         <td>83</td></tr>
<tr><td class='td-left'>Ravon ♂</td>  <td>1</td>    <td></td>           <td>6</td>      <td>&mdash;</td>    <td>10</td>      <td>20</td>         <td>175</td></tr>
<tr><td class='td-left'>Ravon ♀</td>  <td>1</td>    <td></td>           <td>6</td>      <td>&mdash;</td>    <td>10</td>      <td>25</td>         <td>175</td></tr>
<tr><td class='td-left'>Sakin</td>    <td>2</td>    <td>&#10003;</td>   <td>3</td>       <td>5</td>         <td>&mdash;</td> <td>13</td>         <td>120</td></tr>
<tr><td class='td-left'>Sekar</td>    <td>1</td>    <td></td>           <td>8</td>      <td>&mdash;</td>    <td>&mdash;</td> <td>15</td>         <td>95</td></tr>
<tr><td class='td-left'>Syri</td>     <td>1</td>    <td></td>           <td>11</td>     <td>&mdash;</td>    <td>2</td>       <td>18</td>         <td>115</td></tr>
<tr><td class='td-left'>Teshi</td>    <td>1</td>    <td></td>           <td>10</td>     <td>&mdash;</td>    <td>4</td>       <td>22</td>         <td>87</td></tr>
<tr><td class='td-left'>Ynathi</td>   <td>1</td>    <td></td>           <td>9</td>      <td>&mdash;</td>    <td>5</td>       <td>18</td>         <td>100</td></tr>
<tr><td class='td-left'>Zaeren</td>   <td>3</td>    <td></td>           <td>7</td>      <td>&mdash;</td>    <td>1.5</td>     <td>12</td>         <td>65</td></tr>
</tbody></table>

<p>For ravon, the physical maturity for both sexes occurs as listed, but individuals are not fertile for another twenty years.


