---
category: animal
world: ingr
parent_name: Ingr
parent_addr: ingr
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Aksrith</th></tr>
    <tr><th>Homeworld</th><td><a href='ingr.html'>Ingr</a></td></tr>
    <tr><th>Biome</th><td>Temperate Forest</td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
    <tr><th>Height</th><td>1'6" (0.5 m)</td></tr>
    <tr><th>Weight</th><td>30.54 lbs (14 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
    <tr><th>Litter size:</th><td>5</td></tr>
    <tr><th>Gestation:</th><td>5 months</td></tr>
    <tr><th>Weaned:</th><td>7 months</td></tr>
    <tr><th>Maturity:</th><td>2 years</td></tr>
    <tr><th>Lifespan:</th><td>14 years</td></tr>
</table></div>

Aksrith are herbivores found on [Ingr](ingr.md) in the region occupied by the [Kthbra Remnants](remnants.md). Their most visible distinctive feature is their large eyes with large irises and tiny, dot-like pupils. They also have claws specialized for digging and burrowing, though this are less eye-catching than their eyes. Their bodies are awkwardly proportioned and appear to move awkwardly.

Although alert to their surroundings, aksrith are slow-moving creatures with an awkward-looking gait. They never move at more than a brisk walk. Despite this, they are capable of moving their head very quickly allowing them to use their unique defence more easily.

Many universities and governments offer funding for experimenting with and learning about aksrith, but obtaining specimens and convincing the local [drius](drius.md) to permit the animal's removal is often difficult. It is commonly believed that aksrith owe at least part of their lineage to bioengineering by [Warrothserkm](warrothserkm.md).

## Diet

The aksrith diet consists mostly of low-lying berries and roots that it digs up using its specialized claws on its forepaws

## Psychic Defence

Despite being slow and awkward, aksrith are very successful and this is almost entirely due to a unique form of defence; they are capable of a form of minor, temporary mental manipulation. Although this ability is very limited in what it can do, it is very powerful within that scope. Repeated exposure to this ability will reduce its effectiveness.

The aksrith defence activates when the aksrith makes eye-contact with another, non-aksrith creature. This does not require any effort on the part of the aksrith and will always occur on eye-contact. Initially, this defence causes the victim to experience a strong sense of relaxation and lassitude along with visual abnormalities such as large spots drifting across the victim's vision. The combination of these things is enough to discourage most predators.

Although the first stage of their defence is effective on its own, the second stage of the defence protects the aksrith from even the most determined predators. One of the victim's primal emotions (e.g. fear, lust, etc.) is stimulated and associated with a nearby creature or object. Creatures are the preferred target; this stage is less effective if applied to inanimate objects.

Both stages are temporary. The duration depends on how stressed the aksrith is, the animacy of the target, and how often the victim has been exposed to the aksrith's defence. It is not uncommon for people exposed to this defence for the first time or two to experience weaker lingering effects; this is considered harmless, if occasionally embarrassing, at least by the local [drius](drius.md).
