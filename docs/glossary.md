---
title: Glossary
categories: [indexes]
---

This index will gradually be expanded to include every significant term and proper noun in the setting. General words will not be included unless they're especially prominent or common. For general vocabulary of in-setting languages, check the vocabulary page for the language in question.

<script>
function playAudio(forItem) {
    var e = document.getElementById("audio-" + forItem);
    if (e) e.play();
}
</script>

<dl class='glossary'>

<h2 id='A'>A</h2>
<dl>

<dt><a id='aksrith' href='aksrith.html'>aksrith</a>
    <!-- <audio preload='none' id='audio-aksrith'><source type="audio/mpeg" src="audio/aksrith.mp3"></audio>
    <img style='height:1em' src='art/play.svg' onclick="playAudio('aksrith')"> -->
        &#x20;<span class='glossary-tag'>Animal</span>
        &#x20;<span class='glossary-tag'>Ingr</span></dt>
<dd>Animal native to <a href='ingr.html'>Ingr</a>. Possess a mind-effect self-defence mechanism.</dd>

<dt><a id='asuni' href='asuni.html'>Asuni</a>
    <audio preload='none' id='audio-asuni'><source type="audio/mpeg" src="audio/asuni.mp3"></audio>
    <img style='height:1em' src='art/play.svg' onclick="playAudio('asuni')">
        &#x20;<span class='glossary-tag'>Sophant</span>
        &#x20;<span class='glossary-tag'>Raesan Vie</span></dt>
<dd>Intelligent quadrupeds from the world of Raesan Vie capable of seeing and hearing the spirits of the dead.</dd>

<dt><a id="au" href="/au.html">Au</a>
    <audio preload="none" id="audio-au"><source type="audio/mpeg" src="audio/au.mp3" /></audio>
    <img style="height:1em" src="art/play.svg" onclick="playAudio('au')" />
        &#x20;<span class="glossary-tag">Sophant</span>
        &#x20;<span class="glossary-tag">Erladi</span></dt>
<dd>Intelligent species from the world of Erladi. Known for their significant sexual dimorphism.</dd>
</dl>

<h2 id="D">D</h2>
<dl>

<dt><a id="daesra" href="/daesra.html">Daesra</a><audio preload="none" id="audio-daesra"><source type="audio/mpeg" src="audio/daesra.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('daesra')" />&#x20;<span class="glossary-tag">Daesra World</span>&#x20;<span class="glossary-tag">Sophant</span></dt>
<dd>Intelligent quadrupeds with telekinetic and telepathic abilities.</dd>

<dt><a id="daesra world" href="/daesraworld.html">Daesra World</a>&#x20;<span class="glossary-tag">World</span></dt>
<dd>The homeworld of the <a href="/daesra.html">Daesra</a>; so named because of the lack of a language amongst the native daesra.</dd>

<dt><a id="djurnae" href="/djurnae.html">Djurnae</a>&#x20;<span class="glossary-tag">Kzaknri</span>&#x20;<span class="glossary-tag">Person</span></dt>
<dd>Heir apparent in <a href="/razaznudar.html">Razaz Nudar</a>.</dd>

<dt><a id="drius" href="/drius.html">Daesra</a><audio preload="none" id="audio-drius"><source type="audio/mpeg" src="audio/drius.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('drius')" />&#x20;<span class="glossary-tag">Ingr</span>&#x20;<span class="glossary-tag">Sophant</span></dt>
<dd>Founders of <a href="/warrothserkm.html">Warrothserkm</a>, their civilization collapsed and the survivors, the <a href="/remnants.html">Kthbra Remnants</a>, mostly live as hunter-gatherers.</dd>
</dl>

<h2 id="E">E</h2>
<dl>
<dt><a id="earth" href="/earth.html">Earth</a>&#x20;<span class="glossary-tag">World</span></dt>
<dd>Homeworld of the <a href="/human.html">humans</a>.</dd>

<dt><a id="eksar" href="/eksar.html">Eksar</a><audio preload="none" id="audio-eksar"><source type="audio/mpeg" src="audio/eksar.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('eksar')" />&#x20;<span class="glossary-tag">Animal</span>&#x20;<span class="glossary-tag">Raesan Vie</span></dt>
<dd>Large, predatory animal native to <a href="/raesanvie.html">Raesan Vie</a>.</dd>

<dt><a id="engl" href="/engl.html">Engl</a>&#x20;<span class="glossary-tag">Term</span></dt>
<dd>Durable building material used to construct Interworld Nexus zones.</dd>

<dt><a id="erladi" href="/erladi.html">Erladi</a><audio preload="none" id="audio-erladi"><source type="audio/mpeg" src="audio/erladi.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('erladi')" />&#x20;<span class="glossary-tag">World</span></dt>
<dd>Homeworld of the <a href="/au.html">au</a>.</dd>
</dl>

<h2 id="F">F</h2>
<dl>
<dt><a id="fass" href="/fass.html">The FASS</a>&#x20;<span class="glossary-tag">Kzaknri</span>&#x20;<span class="glossary-tag">Nation</span></dt>
<dd>The "Free Associated Sakin States", a confederation of former colonies of <a href="/razaznudar.html">Razaz Nudar</a>.</dd>
</dl>

<h2 id="G">G</h2>
<dl>
<dt><a id="garor" href="/garor.html">Garor</a><audio preload="none" id="audio-garor"><source type="audio/mpeg" src="audio/garor.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('garor')" />&#x20;<span class="glossary-tag">Sophant</span>&#x20;<span class="glossary-tag">Va-Katr</span></dt>
<dd>Large bipedal sophant from <a href="/vakatr.html">Va-Katr</a>.</dd>

<dt><a id="gek relt" href="/gekrelt.html">Gek Relt</a><audio preload="none" id="audio-gek relt"><source type="audio/mpeg" src="audio/gek relt.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('gek relt')" />&#x20;<span class="glossary-tag">Erladi</span>&#x20;<span class="glossary-tag">Nation</span></dt>
<dd>A prominent nation on <a href="/erladi.html">Erladi</a>. It is also where the nexus gate is located.</dd>
</dl>

<h2 id="H">H</h2>
<dl>
<dt><a id="human" href="/human.html">Human</a>&#x20;<span class="glossary-tag">Earth</span>&#x20;<span class="glossary-tag">Sophant</span></dt>
<dd>Intelligent mammals from the world of <a href="/earth.html">Earth</a>.</dd>
</dl>

<h2 id="I">I</h2>
<dl>
<dt><a id="ingr" href="/ingr.html">Ingr</a><audio preload="none" id="audio-ingr"><source type="audio/mpeg" src="audio/ingr.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('ingr')" />&#x20;<span class="glossary-tag">World</span></dt>
<dd>Homeworld of the <a href="/drius.html">drius</a>. Mostly inhospitable in modern times leading to it being called the "shattered world".</dd>

<dt><a id="isaguyat" href="/isaguyat.html">Isaguyat</a><audio preload="none" id="audio-isaguyat"><source type="audio/mpeg" src="audio/isaguyat.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('isaguyat')" />&#x20;<span class="glossary-tag">World</span></dt>
<dd>Homeworld of the <a href="/sekar.html">sekar</a>. The planet orbits a close binary pair and is tidally locked to its moon resulting in very long days.</dd>
</dl>

<h2 id="J">J</h2>
<dl>
<dt><a id="jiruan" href="/jiruan.html">Jiruan</a>&#x20;<span class="glossary-tag">Animal</span>&#x20;<span class="glossary-tag">Zifaelafojun</span></dt>
<dd>Domesticated animal native to <a href="/zifaelafojun.html">Zifaelafojun</a>. Typically kept as a guard animal.</dd>

<dt><a id="jowvon" href="/jowvon.html">Jowvon</a><audio preload="none" id="audio-jowvon"><source type="audio/mpeg" src="audio/jowvon.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('jowvon')" />&#x20;<span class="glossary-tag">Language</span>&#x20;<span class="glossary-tag">Zifaelafojun</span></dt>
<dd>Language spoken in the nations of <a href="/menota.html">Menota</a> and <a href="/pashazafojunfae.html">Pasha Za Fojun Fae</a> on the world of <a href="/zifaelafojun.html">Zifaelafojun</a>.</dd>
</dl>

<h2 id="K">K</h2>
<dl>
<dt><a id="ker relt" href="/kerrelt.html">Ker Relt</a><audio preload="none" id="audio-ker relt"><source type="audio/mpeg" src="audio/ker relt.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('ker relt')" />&#x20;<span class="glossary-tag">Erladi</span>&#x20;<span class="glossary-tag">Nation</span></dt>
<dd>A prominent nation on <a href="/erladi.html">Erladi</a>.</dd>

<dt><a id="kthbra remnants" href="/remnants.html">Kthbra Remnants</a><audio preload="none" id="audio-kthbra remnants"><source type="audio/mpeg" src="audio/kthbra remnants.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('kthbra remnants')" />&#x20;<span class="glossary-tag">Ingr</span>&#x20;<span class="glossary-tag">Nation</span></dt>
<dd>The term used to refer to the descendants of the <a href="/drius.html">drius</a> clan Kthbra who survived the civil war that ended <a href="/warrothserkm.html">Warrothserkm</a>.</dd>

<dt><a id="kzaknri" href="/kzaknri.html">Kzaknri</a><audio preload="none" id="audio-kzaknri"><source type="audio/mpeg" src="audio/kzaknri.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('kzaknri')" />&#x20;<span class="glossary-tag">World</span></dt>
<dd>Homeworld of the <a href="/sakin.html">sakin</a>. Known for its hot and dry climate.</dd>
</dl>

<h2 id="M">M</h2>
<dl>
<dt><a id="menota" href="/menota.html">Menota</a><audio preload="none" id="audio-menota"><source type="audio/mpeg" src="audio/menota.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('menota')" />&#x20;<span class="glossary-tag">Nation</span>&#x20;<span class="glossary-tag">Zifaelafojun</span></dt>
<dd>Country on the world of <a href="/zifaelafojun.html">Zifaelafojun</a>. Populated almost entirely by <a href="/ravon.html">ravon</a>.</dd>
</dl>

<h2 id="N">N</h2>
<dl>
<dt><a id="nalwen" href="/serkmhub.html#common-system">Nalwen</a>&#x20;<span class="glossary-tag">Term</span></dt>
<dd>Unit of measure based on the sizes of pre-existing structures in the Interworld Nexus. Approximately 0.8817 meters.</dd>

<dt><a id="nraezen" href="/nraezen.html">Nraezen</a><audio preload="none" id="audio-nraezen"><source type="audio/mpeg" src="audio/nraezen.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('nraezen')" />&#x20;<span class="glossary-tag">Language</span>&#x20;<span class="glossary-tag">Kzaknri</span></dt>
<dd>A language that originates from the nation of <a href="/razaznudar.html">Razaz Nudar</a> on <a href="/kzaknri.html">Kzaknri</a>, but has spread to be commonly spoken in numerous other nations as well.</dd>
</dl>

<h2 id="P">P</h2>
<dl>
<dt><a id="pasha za fojun fae" href="/pashazafojunfae.html">Pasha Za Fojun Fae</a><audio preload="none" id="audio-pasha za fojun fae"><source type="audio/mpeg" src="audio/pasha za fojun fae.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('pasha za fojun fae')" />&#x20;<span class="glossary-tag">Nation</span>&#x20;<span class="glossary-tag">Zifaelafojun</span></dt>
<dd>A sub-nation of <a href="/menota.html">Menota</a> originally formed of exiles and dissidents. The <a href="/zifaelafojun.html">Zifaelafojun</a> gate is located here.</dd>
</dl>

<h2 id="R">R</h2>
<dl>
<dt><a id="rakelm" href="/rakelm.html">Rakelm</a><audio preload="none" id="audio-rakelm"><source type="audio/mpeg" src="audio/rakelm.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('rakelm')" />&#x20;<span class="glossary-tag">Sophant</span>&#x20;<span class="glossary-tag">Va-Katr</span></dt>
<dd>Small, flying sophant from <a href="/vakatr.html">Va-Katr</a>.</dd>

<dt><a id="ravon" href="/ravon.html">Ravon</a><audio preload="none" id="audio-ravon"><source type="audio/mpeg" src="audio/ravon.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('ravon')" />&#x20;<span class="glossary-tag">Sophant</span>&#x20;<span class="glossary-tag">Zifaelafojun</span></dt>
<dd>Flying theropod-like intelligent species from <a href="/zifaelafojun.html">Zifaelafojun</a>.</dd>

<dt><a id="raesan vie" href="/raesanvie.html">Raesan Vie</a><audio preload="none" id="audio-raesan vie"><source type="audio/mpeg" src="audio/raesan vie.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('raesan vie')" />&#x20;<span class="glossary-tag">World</span></dt>
<dd>Homeworld of the <a href="/asuni.html">asuni</a>. Mostly unexplored.</dd>

<dt><a id="razaz nudar" href="/razaznudar.html">Razaz Nudar</a><audio preload="none" id="audio-razaz nudar"><source type="audio/mpeg" src="audio/razaz nudar.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('razaz nudar')" />&#x20;<span class="glossary-tag">Kzaknri</span>&#x20;<span class="glossary-tag">Nation</span></dt>
<dd>Country on the world of <a href="/kzaknri.html">Kzaknri</a>. Primarily populated by <a href="/sakin.html">sakin</a>.</dd>

<dt><a id="reka relt" href="/rekarelt.html">Reka Relt</a><audio preload="none" id="audio-reka relt"><source type="audio/mpeg" src="audio/reka relt.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('reka relt')" />&#x20;<span class="glossary-tag">Erladi</span>&#x20;<span class="glossary-tag">Nation</span></dt>
<dd>Once an empire consisting of modern <a href="/gekrelt.html">Gek Relt</a> and <a href="/kerrelt.html">Ker Relt</a>, Reka Relt is now a nation consisting of little more than the former capital and surrounding farmlands.</dd>

<dt><a id="rekuzarae" href="/rekuzarae.html">Rekuzarae</a>&#x20;<span class="glossary-tag">Kzaknri</span>&#x20;<span class="glossary-tag">Person</span></dt>
<dd>Former emperor of <a href="/razaznudar.html">Razaz Nudar</a>. Known as "the Tyrant-Emperor".</dd>

<dt><a id="ruksou" href="/ruksou.html">Ruksou</a>&#x20;<span class="glossary-tag">Animal</span>&#x20;<span class="glossary-tag">Raesan Vie</span></dt>
<dd>Animal native to <a href="/raesanvie.html">Raesan Vie</a>. Has a parasitical method of reproduction.</dd>
</dl>

<h2 id="S">S</h2>
<dl>
<dt><a id="sakin" href="/sakin.html">Sakin</a><audio preload="none" id="audio-sakin"><source type="audio/mpeg" src="audio/sakin.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('sakin')" />&#x20;<span class="glossary-tag">Kzaknri</span>&#x20;<span class="glossary-tag">Sophant</span></dt>
<dd>Intelligent species of reptile-like bipeds from <a href="/kzaknri.html">Kzaknri</a>. They superficially resemble lizards.</dd>

<dt><a id="sekar" href="/sekar.html">Sekar</a><audio preload="none" id="audio-sekar"><source type="audio/mpeg" src="audio/sekar.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('sekar')" />&#x20;<span class="glossary-tag">Isaguyat</span>&#x20;<span class="glossary-tag">Sophant</span></dt>
<dd>Theropod shaped sophants from the largely unexplored world of <a href="/isaguyat.html">Isaguyat</a>. The sekar are best known for their violent temperaments and durable natural armour.</dd>

<dt><a id="sophant" href="https://en.wiktionary.org/wiki/sophont">Sophant</a>&#x20;<span class="glossary-tag">Term</span></dt>
<dd>A person with intelligence roughly equivalent to that of a human or better.</dd>

<dt><a id="syri" href="/syri.html">Syri</a><audio preload="none" id="audio-syri"><source type="audio/mpeg" src="audio/syri.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('syri')" />&#x20;<span class="glossary-tag">Sophant</span></dt>
<dd>The syri are a small, non-humanoid species with no known origin. Best known for their love of keeping secrets and apparent access to very advanced technology.</dd>
</dl>

<h2 id="T">T</h2>
<dl>
<dt><a id="tkinae" href="/tkinae.html">Tkinae</a>&#x20;<span class="glossary-tag">Kzaknri</span>&#x20;<span class="glossary-tag">Person</span></dt>
<dd>Founder of <a href="/razaznudar.html">Razaz Nudar</a>.</dd>

<dt><a id="tsukina" href="/tsukina.html">Tsukina</a><audio preload="none" id="audio-tsukina"><source type="audio/mpeg" src="audio/tsukina.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('tsukina')" />&#x20;<span class="glossary-tag">Kzaknri</span>&#x20;<span class="glossary-tag">Nation</span></dt>
<dd>The Tsukina are a nation formed from the followers of the god <a href="/zakanor.html">Zakanor</a> who fled <a href="/razaznudar.html">Razaz Nudar</a> while it was being formed by <a href="/tkinae.html">Tkinae</a>.</dd>
</dl>

<h2 id="V">V</h2>
<dl>
<dt><a id="va-katr" href="/vakatr.html">Va-Katr</a><audio preload="none" id="audio-va-katr"><source type="audio/mpeg" src="audio/va-katr.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('va-katr')" />&#x20;<span class="glossary-tag">World</span></dt>
<dd>Homeworld of the <a href="/rakelm.html">rakelm</a> and the <a href="/garor.html">garor</a>.</dd>
</dl>

<h2 id="W">W</h2>
<dl>
<dt><a id="warrothserkm" href="/warrothserkm.html">Warrothserkm</a><audio preload="none" id="audio-warrothserkm"><source type="audio/mpeg" src="audio/warrothserkm.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('warrothserkm')" />&#x20;<span class="glossary-tag">Ingr</span>&#x20;<span class="glossary-tag">Nation</span></dt>
<dd>A large, multi-world empire created by the <a href="/drius.html">drius</a> that originated on the world of <a href="/ingr.html">Ingr</a>. It had access to advanced technology and was populated by multiple species, but collapsed in civil war. Relics of Warrothserkm can be found scattered across multiple worlds.</dd>
</dl>

<h2 id="Z">Z</h2>
<dl>
<dt><a id="zanth drith" href="/zanthdrith.html">Zanth Drith</a><audio preload="none" id="audio-zanth drith"><source type="audio/mpeg" src="audio/zanth drith.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('zanth drith')" />&#x20;<span class="glossary-tag">Kzaknri</span>&#x20;<span class="glossary-tag">Nation</span></dt>
<dd>A nation formed by the followers of the god <a href="/grutha.html">Grutha</a> after being driven out from <a href="/razaznudar.html">Razaz Nudar</a>.</dd>

<dt><a id="zaeren" href="/zaeren.html">Zaeren</a><audio preload="none" id="audio-zaeren"><source type="audio/mpeg" src="audio/zaeren.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('zaeren')" />&#x20;<span class="glossary-tag">Sophant</span></dt>
<dd>The zaeren are short, rat-like humanoids. Their homeworld is believed to have been destroyed as they now originate from a dead zone. Best known for being consummate scavengers and true experts at re-use.</dd>

<dt>Zaeren Zone&#x20;<span class="glossary-tag">Zone</span></dt>
<dd>The Zaeren Zone is a region in <a href="/serkmhub.html">Serkm Hub</a> that serves as the centre of <a href="/zaeren.html">Zaeren</a> civilization in place of a homeworld.</dd>

<dt><a id="zaezeklin" href="/razaznudar.html#zaezeklin">Zaezeklin</a><audio preload="none" id="audio-zaezeklin"><source type="audio/mpeg" src="audio/zaezeklin.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('zaezeklin')" />&#x20;<span class="glossary-tag">City</span>&#x20;<span class="glossary-tag">Kzaknri</span></dt>
<dd>The premier (and nearly only) tourist destination in <a href="/razaznudar.html">Razaz Nudar</a>. The city is built entirely underground and is famous for its underground rivers.</dd>

<dt><a id="zaezeklin ventilation company" href="/razaznudar.html#zaezeklin">Zaezeklin Ventilation Company</a>&#x20;<span class="glossary-tag">Business</span>&#x20;<span class="glossary-tag">Kzaknri</span></dt>
<dd>A privately held company, the <em>Zaezeklin Ventilation Company</em> was originally formed to maintain the ventilation systems in the city of <a href="/razaznudar.html#zaezeklin">Zaezeklin</a> by retired military engineers. It is now found across multiple worlds and is considered one of the best in the industry.</dd>

<dt><a id="zdajgae" href="/zdajgae.html">Zdajgae</a>&#x20;<span class="glossary-tag">Kzaknri</span>&#x20;<span class="glossary-tag">Person</span></dt>
<dd>Current emperor of <a href="/razaznudar.html">Razaz Nudar</a>. Known as "the Undying".</dd>

<dt><a id="zifaelafojun" href="/zifaelafojun.html">Zifaelafojun</a><audio preload="none" id="audio-zifaelafojun"><source type="audio/mpeg" src="audio/zifaelafojun.mp3" /></audio> <img style="height:1em" src="art/play.svg" onclick="playAudio('zifaelafojun')" />&#x20;<span class="glossary-tag">world</span></dt>
<dd>Homeworld of the <a href="/ravon.html">ravon</a>. Mostly unpopulated.</dd>

<dt><a id="zuga" href="/zuga.html">Zuga</a>&#x20;<span class="glossary-tag">Animal</span>&#x20;<span class="glossary-tag">Kzaknri</span></dt>
<dd>Domesticated animal native to <a href="/kzaknri.html">Kzaknri</a>. Zuga meat is a staple of the diet in Razaz Nudar and surrounding areas.</dd>

<dt><a id="zuzara" href="/zuzara.html">Zuzara</a>&#x20;<span class="glossary-tag">Animal</span>&#x20;<span class="glossary-tag">Kzaknri</span></dt>
<dd>Domesticated animal native to <a href="/kzaknri.html">Kzaknri</a>. Serves as a mount and a beast of burden.</dd>
</dl>


</dl>

