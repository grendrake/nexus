---
category: general
world: interworld nexus
parent_name: Interworld Nexus
parent_addr: overview
notoc: true
---

Engl is the name for the material used to construct the hubs and gates of the Interworld Nexus. It has no known source and current technology is unable to manipulate it, making it usable only in the forms it was found in.

Very little is known about what engl is, but several properties have been observed. The most obvious is that it is effectively indestructible; explosive testing conducted by [Razaz Nudar](razaznudar.md) resulted in no observable damage.

Engl has a rough surface and holds many kinds of paint quite well. Its durability also makes it easy to remove paint as harsh cleaning process will leave the engl unharmed.

All currently known engl is grey, though the shade of grey can be anywhere between nearly white and nearly black.
