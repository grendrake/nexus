---
category: nation
world: zifaelafojun
parent_name: Zifaelafojun
parent_addr: zifaelafojun
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Menota</th></tr>
<tr><th>Capital:</th><td>Salmae</td></tr>
<tr><th>Language:</th><td><a href='jowvon.html'>Jowvon</a></td></tr>
<tr><th>Demonym:</th><td>Menotan</td></tr>
<tr><th>Area:</th><td>8.5 million km<sup>2</sup></td></tr>
<tr><th>Population:</th><td>3.5 million</td></tr>
<tr><th>Density:</th><td>0.41 pop/km<sup>2</sup></td></tr>
</table></div>

Menota is the larger of the two nations on Zifaelafojun as well as the older. It is a nation of contrasts and has one of the largest gaps between the wealthy nobility and the poverty-stricken peasantry. Most of the population consists of the peasantry who are considered well off if they have a spare set of clothes and who often lived in shared or communal homes. The nobles make up about a hundredth of the population; they are officially responsible for the care of the peasantry, but in practice are more focused on interpersonal and inter-clan politics and with expanding their wealth and holdings.

The nation has sometimes been called a "nation of smoke and mirrors" because of how severely its public image clashes with the secret reality behind it. What people say and do in public and what they do in private is not only different but often neither bears any resemblance to each other.

Modern technology and medicine has yet to reach much of the population; one of the leading causes of death among peasents remains predation.
All but the most disadvantaged nobility are able to afford these things.
While this is enough to have stopped the prior population decline, growth rates still have not become positive.

## Architecture and Infrastructure

Menotan homes are typically placed with significant distance between them. If one was to walk between them, taking twenty minutes to reach the neighbours would not be uncommon. Of course, given that the native ravon can fly, most inhabitants are not walking between homes. Menotan "villages" are often the size of a large town or even small city, despite having only a fraction of the size.

When considering Menotan homes it is important to consider both the relatively flat population growth curve and how recently modern technology was introduced to the country. The first means that relatively few new homes are constructed and, instead, old homes are refitted to suit current needs. Combined with the second, most Menotan homes were not designed with electricity in mind, and many without even considering indoor plumbing. Those with the money may have extensive work down to lay pipe and wire down out of site, but having exposed piping and wire conduits is not uncommon in the homes of those peasant families that can afford them.

Because of the lack of infrastructure in Menota, electricity and water services must be provided for by the home-owner. Electricity, for example, is often provided by solar panels on the roof or even a small, personal hydroelectric generator. While this has plenty of disadvantages, not the least being that the cost must be handled entirely by the home-owner, it also makes each home independent of those around it.

## Art

The peasant population practices a wide variety of arts, with only those requiring expensive or heavy supplies (like creating statues) being uncommon. In addition to what is often found on other worlds, the ravon of Menota also practice aerial forms of dance and competitions. Most Menotan art is created by peasants and this is often encouraged by the nobility.

Conversely, the nobility view art more as something for showing off what one can afford to buy then as something someone should create for themselves. The worth of art is almost always considered to be that of the price-tag attached to it and for how long it will last; for example, paintings are preferred to live music because paintings can be viewed repeatedly.

Despite the public disdain of the nobility for creating art, many nobles do, in fact, practice a variety of art forms, but they consistently do so in private or with close friends; being publicly known as an artist would be considered to be scandalous.

### Homes

<figure class='right' style='width:300px'>
    <a href='/art/plans/menotan-noble-home.svg'><img src='/art/plans/menotan-noble-home.svg' width='300' height='216'></a>
    <figcaption>
    Floor plan of typical Menotan noble's home.<br>
    Art © <a href='https://www.grenslair.com/'>Gren Drake</a>
    </figcaption>
</figure>

The typical Menotan home is built into the side of a hill. Most of the interior area while consist of a single, central, common room. The outwards facing part of the common room is lined with windows, either in the form of shutterable openings or large glass panes, that serve as the primary form of interior lighting. Ceilings have multiple grooves running along them to vent any smoke from the inside. Outside of the home is typically a balcony that serves both as an outdoor social area as well as a landing and take-off zone. Because of the slope of the hill, Menotan homes rarely have direct ground-level access, though stairs or ramps are sometimes installed for the disabled or for bringing in large items. Access inside is typically through a door between the balcony and the common room.

Interior rooms are built back into the hill. This largely prevents windows from being included, so most interior rooms are very dark, especially in homes without electric lights. These rooms are divided from the common room by thick doors that slide into the wall and serve both privacy and to reduce drafts. Kitchens are typically placed as one of the outermost rooms, simplifying the venting of excess heat and kitchen waste.

In many homes it is still normal for toilets to be located outside the home; only in homes that have been retrofitted with indoor plumbing are they inside. When they are, toilets are located in small, dedicated rooms.

Like indoor toilets, indoor bathing is mainly found in homes with indoor plumbing. In these cases, it, too, gets a dedicated room, often with a large, shallow basin built into the floor to hold the water. Menotan baths are relatively large and spacious, often able to fit two or more ravon occupants comfortably. Those without indoor baths will make use of nearby streams and lakes instead.

### Home Safety

While the exterior doors of Menotan homes often have some form of locking mechanism, these are often of questionable quality and pose as only a slight deterrent at best. The main form of home security takes the form of an animal called a jiruan. Most homes will have one that either lives in an entry chamber or that is allowed to roam the common area. These creatures are aggressive, but protective of the ones they live with. The approach of unfamiliar people or those who haven't been properly introduced will initially produce a series of warning barks. This will often scare off intruders or encroaching wildlife, or at the least wake up the occupants. Failing this, jiruan will attack intruders.

Jiruan are the most aggressive domesticated animal in Menota and despite extensive training, accidents between jiruan and their owners are not unheard of. Most jiruan used in home defence are castrated males in an attempt to mitigate their aggression; Jiruan aggression makes many uncomfortable, especially given the relative size of the animal compared to the native ravon.


## Economy and Technology

Menota has no high-tech manufacturing capabilities; the most advanced form of manufacturing available is the blacksmiths among the present population. While capitals of producing a wide range of goods, including quite a few that are used inside noble homes, they entirely lack the ability to make modern goods.

This is not to say they possess no high-tech goods. Laptops are frequently sought among nobles, particularly in the smaller, more portable styles. Given the lack of infrastructure, their is no phone or internet-equivalent, but printers allow digital communications to be sent through the existing postal system. Power is typically provided by solar panels attached to individual dwellings.

There has been talk among the younger nobles of seeing you a cellular network for faster communications, but cost and security concerns have so far prevented it.

There is also a small, but significant, number of contractors. These mostly come from the peasant class and cover trades like plumbers and electricians. They are comparatively unskilled and underpaid compared to their equivalents from other worlds and nobles tend to look down on those in these professions even add they hire them to do work.

As a nation with minimal exports (mostly food products for those forced to live abroad or foreigners looking for exotic tastes) and significant imports despite the expense and difficulty of inter-world shipping, sometimes presents a confusing economic picture. In practice, most of the money coming into Menota is from foreign investments; when the nation was liberated from Gek Felt, much of value was left behind. Razaz Nudar permitted the locals to keep this in the expectation that the capital from the sale of what was left would be used to build infrastructure Razaz Nudar then wouldn't have to pay for when they annexed the nation. Instead, the material was sold out and the money used to place foreign investments. And to everyone's surprise, the Menorah nobility turned out to be shrewd, if amoral ("it's only foreigners," nobles have been hard to say) investors and have managed to parlay that meagre starting capable into a financial empire.


## Clans

The population of Menota is divided into eleven clans; officially there are thirteen, but two of the clans have no living members. Every clan consists of a mix of peasants and nobles; on average, nine percent of a clan's population is noble. Clan affiliation is taken very seriously; even friendships between members of different clans are discouraged. Marriage is forbidden and having children with mixed clan heritage is considered disgraceful. Although the children are not held responsible, their status of not being entirely in either clan often leads to them being socially isolated.

Clan membership does not change; every individual is born into their clan and that is their clan for the rest of their life. One cannot be kicked out of a clan, nor can they switch clans under any circumstances. Anything between a very casual friendship between members of different clans is considered inappropriate; marriage and sexual encounters are outright forbidden.

As well as the unfortunate mixed-clan individuals, there are some people for whom their clan association is simply not known. This occurs mainly with freed slaves and foreign-born ravon who do not know their heritage well enough to say. Although such circumstances are seen as more unfortunate and not anyone's fault, this has much the same effect of creating social isolation. Despite the circumstances, mixed-clan and unknown-clan individuals have been known to develop friendships; this is typically overlooked in recognition of the difficulties such individuals face. Any form of romance is still forbidden, however.

To the despair of the nobles, many peasants actually treat clan affiliation as a kind of extended-family and tolerate romances between individuals of different clans, though it is still discouraged.

Non-ravon never belong to a clan as their species makes it self-evidence that they could not have been born to a clan member.

## Families

Every clan is composed of a large number of families. Each family typically consists of a few hundred individuals, though married couples typically live alone with their children and, at least for nobles, sometimes servants. Despite this separation, the members of most families are very close and provide a great deal of support for one another.

Unlike a clan, the members of most families are of about the same social and wealth level. This is a result of the belief that family needs to support family; those who are falling behind will be supported by those who are more successful.

In the not unheard-of situation where a member of a family is known to be problematic or not presentable in public, but where they have not run afoul of the law, the family will also keep them out of sight while making sure they have everything they need in life. In especially problematic cases, or where individuals show an excessive interest in outsiders, they may be "encouraged" to go into self-exile outside of Menota and given a basic stipend to live on.

## Names

Names consist of a clan name, a family name, and a personal name. Family and clan names each tend to be short phrases stuck together as a single word and, as a result, tend to look long and complex to those not familiar with them. As an example, an individual might have the full name of:

Kujarozazifaela Ju Wijaezakozafethaejalu Va Mane

Kujarozazifaela is their clan name and is the phrase "kujaro za zifaela" meaning green-eyed. Ju is a particle that always precedes one's family name. Wijaezakozafethaejalu is thus the family name from the phrase "wijaezako za fethaejalu" or long-flight. "Va" is the person's personal particle; unlike Ju, this varies by sex: Va is for women, Re is for men, and Tae is for sexless people or respected foreigners. Finally, "Mana" (meaning, roughly, wisdom) is the person's personal name.

Note that since most sexless ravon choose to either use Re or Va, the Tae particle is mostly used either as an insult or for flattering foreigners. Since neither insults nor foreigners are common in public, it is rarely used. What foreigners do appear are typically referred to without a particle. Referring to a Menotan without a particle is considered a worse insult than calling them Tae, however.

Individuals of mixed breeding do not have a clan but may still have a family name if they have not been disowned. Conversely, those of an unknown clan are specifically said to belong to the "unknown" clan but typically lack a family name. That said, mixed-breeds are sometimes raised as "orphans" by more prominent mothers as either unknown clan or their mother's clan.

## Peasants and Nobles

Every peasant is associated with a particular noble family. Normally a peasant family will all be associated with the family, but this is not always the case. In principle, the nobles are expected to protect and aid their associated peasants, but this is not something that one tends to see. It is a popular belief of both groups that they would be better off without the other. Most nobles pretend the peasants don't exist and have occasionally even been able to convince foreigners that this is true. In return, peasants often don't bother paying the taxes they're required too; this is often overlooked by the nobility because pursuing back taxes would require admitting the peasants exist and most nobles have sufficient foreign wealth that the taxes are not necessary.

In situations where the nobility cannot pretend the peasants don't exist, the will instead try to conceal the poor living conditions of the peasants even going as far as to pretend the peasants prefer their lives the way they are. Most nobles believe that foreigners discovering the truth of the matter would lead to a wide-spread public outrage they couldn't ignore or buy their way out of. The immense wealth at the command of most noble families has enabled them to be quite successful in this, especially as most foreigners don't want to know about it anyway. Very few people off of Zifaelafojun (and a surprising number of nobles) have no real understanding about the lives of the peasantry.

Despite their physical proximity, there are significant cultural differences between the peasantry and the nobility. Peasant culture tends to vary more by distance and is not widely known outside of Zifaelafojun; most peasants cannot afford to travel to the Nexus. A superficial examination of peasant and noble culture will note many similarities, but the low-tech nature of peasant life and focus on subsistence farming has lead to their being significant differences under the surface. Conversely, some peasants will deliberately act differently to distance themselves from the nobility.

The servants of nobles are nearly always hired peasants who are regularly paid in cash. As one of the only ways of making money and improving their lives, these positions are in demand and have many applicants. Those not hired rarely have hard feelings for those who are; most peasants send much of their earnings back to their families and improve the lot of their entire family. That said, nobles who treat their servants badly, abusing, beating, or excessively berating them, may discover their positions go unfilled as servants continue to communicate with their families during their employment.

Social mobility is nearly non-existent. A peasant could theoretically claim a noble title if they were to come into possession of enough money, but it would be nearly impossible for one to accomplish this. A different possibility would be to leave the country, but the lack of education amongst peasants means that few have the skills required to find employment in Pasha Za Fojun Fae or on other worlds even if they can afford to get there.

Conversely, nobles will, on rare occasion, be stripped of their money and title, but they are rarely welcomed by the peasants and rarely have the skills to be successful as a peasant. Most are simply exiled instead.

## Government

The biggest mistake foreigners, even those who regularly deal with politics or with Menota, make is to consider the Menotan government as a single entity. In practice, "Menota" is as much a confederation of nations that all happen to occupy the same space as it is a single nation. The population is broken into several clans, each with their own customs, laws, and punishments. In addition, there are other organizations that oversee the nation as a whole, though these more often provide oversight, handle inter-clan affairs, and deal with foreign politics.

The official, formal government of Menota is the **Council of Salmae**. This was put into place by Razaz Nudar when they liberated Menota from Gek Relt; the emperor of the time was not aware that Menota already possessed an unofficial government. Documents that have come to light in since in Razaz Nudar have revealed that the government was intended to fail so that the emperor at the time could take over (aka "save") the nation while maintaining good appearances. Although contested by the Menotan government, these documents are accepted by almost everyone else.

The Council of Salmae consists of no less than five-hundred people, chosen at random from the entire able-bodied population, both of peasants and nobles. At least a quarter of the members present at any time are required to be nobles. Service on the council always takes the form of a six-year term. For peasants, this is wonderful as it includes lush living quarters and a regular paycheck. For nobles, it's considered an annoying duty that doesn't pay nearly enough to make up for the time involved.

Because of its design, the Council of Salmae is largely ineffective at decision making, though modern technology is alleviating this somewhat. It is not uncommon for motions to expire or become irrelevant before everyone is made fully aware of them, never mind having any discussion occur. This situation is not improved by the poor, even nonexistent, education of the peasants on the council, many of whom are not even literate. When the Council of Salmae does come to a decision, however, that decision is considered binding across all of the governing bodies. There are many, especially among those outside the nation, that the Council of Salmae may become far more effective as technology and improving education reduce the council's logistical problems.

Most actual day-to-day business is handled by the **Council of Fifty**. Despite openly existing, this is effectively a shadow government and its existence has never been officially sanctioned by anyone. That said, the Council of Fifty is considered by many to be necessary to the country's functioning.

The **Church of Vaere** also serves as a governing organization. It claims authority over all the spiritual and personal aspects of the population's lives. This authority is described as being divine in origin and thus not subject to mere secular decrees. Historically, membership in the Church of Vaere was mandatory, but with the publicity of Va Zvae's denouncement of the church, it is becoming more acceptable to take membership with the more liberal Association of Vaere instead. Between this and the difficulty in policing behaviour that goes unseen, the church is rather less powerful than it claims to be, though when it does exercise power it tends to do so more visibly than any other part of the government.

The last of the major governmental organizations is the **Council of Clans**. This is a council composed of the heads of each of the thirteen clans (officially including the two deceased clans, though obviously these are not able to make it to the meetings). They are considered by many, inside Menota and out, to be the "real" government and are certainly the oldest governing organization still in operation. In those times that Menota has been conquered and local government prohibited, the Council of Clans continued to exist in an underground state. Mostly they deal in affairs involving multiple clans, but they are also known to meddle in pretty much anything. The Council of Clans is accepted by tradition and is acknowledge, even welcomed (as much as any government is), by everyone down to the peasants.

### The City of Salmae

Salmae is the only place in Menota that a foreigner would be tempted to call a city; unlike typical Menotan construction, the buildings are Salmae are densely packed, sometimes even to the point of directly touched each other. The city itself is built in and around the crater of an extinct volcano.

It is in this city that the Council of Salmae finds its home. Indeed, the city owes its existence to the need for a centralized, populated area to put the council. Foreign visitors, such as diplomats, are often brought directly to Salmae, giving them a distorted perception of what life in Menota is like.


## Law and Order

As befits a nation with multiple governing bodies, law in Menota is complex, restrictive, and inconsistent. As well as the four major governing bodies, each clan can create their own laws, but these apply only to members of that single clan. As a result, the laws that apply to one person may be different than those applying to their neighbour.

In the broadest sense, it is safer to presume that there is a law against anything not explicitly legal. As a result, most people keep everything as private as possible to avoid legal trouble. On the surface, Menota appears to consist solely of highly formal, law-abiding, god-fearing citizens. Most situations are handled discretely, with arrests, judgments, and even punishments being handled out of sight. The rare cases that come to public attention (such as that of Va Zvae) are typically dealt with very harshly, but also suggest that the situation out of the public eye is much different than what can be readily seen.

Law enforcement is typically handled by the clergy of the Church of Vaere, even for laws passed by other governing bodies. The church is vocally against inhumane treatment; criminals are never executed or enslaved. Corporeal punishment of any sort is reserved for the worst crimes. Instead, serious crimes result in the perpetrated being exiled to one of the many islands that surround Zifaelafojun at a distance just far enough that flying back would be difficult, if not impossible. In the interests of being humane, such exiles are provided with a basic weapon and some supplies. Exiles are not normally sent to already occupied islands, but if multiple people of the same sex are being exiled at once, they may be exiled together, especially if they committed a crime together. Those who have escaped or otherwise returned from exile without permission will have their wings broken and be re-exiled; this is considered to be humane as it will heal with time, though without medical attention it is likely to do so badly and thus limit one's ability to fly. Peasants are rarely exiled; there is an unfortunate tendency for their family to "rescue" them and hide them from the authorities.

As the clergy is mostly made up of nobles, they tend to be disinterested in investigating minor crimes committed by peasants and are unreliable when it comes to investigating major ones. As a direct consequence, justice among peasants typically takes more direct forms, such as shunning (for lesser crimes) or execution by lynch mob (for major ones). Particularly heinous crimes may result in the perpetrator's limbs being broken and their being left for predators; often those performing this "justice" will watch from a safe, elevated position as the perpetrator is torn apart.

In the rare case that there are foreigners in Menota long enough to commit a crime, they are always exiled back to where-ever they came from and barred from re-entering the country.

## Social Events

Social events among the nobility can be divided into a few different groups. The *formal entertainment* is a publicly announced and widely attended event. Formal entertainments are extremely formally events typically held in rented facilities with plenty of staff and a large, if plain, banquet. Despite the name, though, these events are rarely entertaining for the guests and serve primarily as matchmaking and social networking events. It is not unheard of for formal entertainments to have a couple of hundred guests.

An upstanding noble is expected to make regular appearances at formal entertainments. Due to their long and boring nature, however, it is common for a sudden, short-lived pandemic of minor injuries, diseases, and "urgent" business when one is announced. It is considered highly prestigious to host a well-attended formal entertainment but is also expensive. As a result, formal entertainments are normally put on by an entire family, rather than any particular person.

*Private entertainments* (often just called entertainments) are held by specific individuals and have short, highly exclusive guest lists of maybe two or three dozen people at most. Word and invitations are spread informally and mentioning one to someone who was not invited is considered a faux pas. As privately held, discreet events, these events are much less formal, often featuring behaviour that would be called "wild" or "inappropriate" if anyone was willing to admit to having been there. They are often considered to be the most important part of a nobles personal social life, even if no one will admit to actually attending one.

The third major group of noble social events is the *personal entertainment* (normally just called "personals"). These are even more restricted with few having more than half a dozen attendees. Invitations are typically issued personally by the host and even mentioning their existence is considered a major impropriety. None of the normal social limits are required (or expected) at a personal, though of course, no word of anything improper is ever spoken of outside the event itself. On those occasion that a personal becomes public knowledge, it is the indiscretion that is despised, though most people will claim that they would "of course not attend such a horrible thing".

All three kinds of events have servants present and performing their normal job functions, though, in the case of private and personal entertainments, these are just the host's regular servants (who are often given a bonus for the extra hours, work, and as a preemptive bonus for their discretion). Asking a servant what happened at an entertainment is considered even more rude than asking a guest.

A fourth kind of social event exists as well: the *peasant gathering*. These are publicly looked down on by nearly all nobles as they are low, bawdry events with plenty of music and noise and will have as much of a feast as the peasants attending can put together. In theory, these events are held to celebrate specific religious or cultural events, the peasantry have come up with a dizzying array of events that "need" celebrating. This list of events varies wildly by region, clan, or even individual and efforts are often made to be inclusive by celebrating all of them.

When given the choice, servants of nobles almost always choose to attend peasant gatherings rather than any of the noble events on the rare occasion that have a chance to be a guest rather than an employee. It is not unheard of to see nobles who are on unusually good terms with the local peasants at such gatherings as well, though of course it is not spoken of and they would never admit to it.

## Childhood

Children are raised at home. For the first few years of their life they are breastfed by their mother; even in the wealthiest families, it is considered important for a mother to breastfeed her own child if it's at all possible. That said, breastfeeding is considered to be inappropriate in public, though taking children of an age to breastfeed out in public would be considered rather questionable as well.

Peasants are taught and raised by their parents while noble child will often have tutors and caretakers. This distinction underlies a large part of the education gap as peasants are not in a position to give their children more knowledge than they themselves know.

It is not expected that children will understand proper etiquette or discretion, but it is expected that their parents or caretakers will correct them when they get something wrong. As a result, children are usually sent to spend time with a caretaker when their parents are hosting any kind of entertainment.

Children are considered to become young adults at about twenty years of age, the point in a ravon's life that they reach physical maturity. Although they begin to have the responsibilities and expectations of an adult at this point, one's twenties and thirties are when ravon are expected to become accustomed to their bodies and work out any excessive "urges" in their system.

"True" adulthood begins in one's early forties. At this point, the new adults are expected to get married and start a family. Amongst the nobility, marriages are almost always arranged, though popular individuals may be given a choice between several candidates. Marriage is much more free-form amongst peasants. In both cases, the ideal candidate will be someone of a different family but the same clan.

## Clothing and Modesty

Clothing is typically very simple, often little more than whitewashed linens providing for basic modesty and coverage. Peasant clothes are often done with more colours, but nobles instead adopt a staggering array of jewellery. This jewellery is often used as a demonstration of wealth with both its quantity and quality reflecting on its wearer.

Jewellery typically takes the form of necklaces, rings, earrings, and tail-rings. While the nobility is often known for their extravagant and gaudy jewellery, the jewellery of peasants is often quite plain. Tail-rings are metal bands that clamp around the tail and tightened to keep them from slipping out of place. In especially formal situations, it is not unheard of for tail-rings to be tightened enough to hamper blood circulation.

Collars are not worn as decoration but are sometimes seen on those undergoing penance from the Church of Vaere or on close family who want to show support. They are also used in many ceremonies in the church.

Makeup and dyes are used almost exclusively to cover up blemishes or unattractive features. A small number of people will dye all their fur, but this is both rare and time-consuming. Being known to use dyes considered embarrassing.

Each particular style of dress is referred to as a "kit". While peasants will often only have a single kit of clothing, nobles will have many. Noble kits can be divided into three general categories: casual, semi-formal, and formal. The poorest nobles may share a formal kit between multiple family members.

Every traditional Menota outfit consists of some combination of three garments: the breast-cloth, the groin-cloth, and the backcloth. The breast-cloth covers the chest and stomach and is considered to be the core of any outfit; someone not wearing a breast-cloth is considered naked regardless of what else they might be wearing. Conversely, someone wearing a breast-cloth is not considered naked even if they wear nothing else. The groin-cloth attaches to the breast-cloth and covers the groin; while not necessary to be considered "not naked" it is expected anyone going out in public wear one. The backcloth is a formal garment that attaches around the wearer's neck and lays flat on their back between their wings; it serves no practical purpose but is considered a part of formal dress.

The outfits change slightly in winter with the addition of an extra layer simply referred to as one's *outdoor garments*. These consist of heavier materials for the already covered areas as well as gloves, boots, scarves, and tail-sleeves. Wing-covers are sometimes worn as well but are universally disliked due to the way they hamper flight.

The *casual kit* is worn for comfort when home alone, or is worn by servants, labourers, soldiers, or peasants. It consists of little more than a breast-cloth and sometimes a groin-cloth. Little or no jewellery is worn and the manufacture and materials are the cheapest available.

Conversely, the *formal kit* is worn only for formal ceremonial and official functions, but even then is only worn by the actual participants (such as the bride, groom, and priest at a wedding). It is heavily laden with jewellery and the fabric is as expensive and sheer as possible. Although all three garment pieces are required, the fabric is often so sheer that nothing is actually concealed. Because of the sheer weight of the jewellery involved, someone who will be wearing their formal kit will often change into it while at the event in question. It is not unheard of for the formal kit of especially wealthy individuals to be heavy enough to make flight impossible and to even make walking difficult.

The *semi-formal kit* is similar to the formal kit, but has a much more modest level of jewellery, often having more in common with a fancier version of the casual kit than the formal kit. That said, exactly where the semi-formal kit falls between the casual and formal kits depends on the circumstances and how formal one wishes to appear.


## Sexuality

The Church of Vaere publishes guidelines for allowed and proper sexual behaviour under the (translated) name "A Moral Approach to Sex".
This describes the appropriate position, partner, and times for sexual activity.
Given the Menotan penchant for privacy, it is difficult to judge how closely these are followed, but the authority of the Church requires at least lip service to its tenets.

According to the guidelines, married couples should have sex at least weekly during early spring and winter.
More frequent sex or sex at other times of the year is permitted, but not encouraged.
Regardless of the time, there is only a single approved position for sex and, with one exception, sex must always be conducted in privacy.
Extra-marital sex and same-sex activities are both strictly forbidden.

The guidelines advise that those in adolescence will push the boundaries of what is acceptable.
It is recommended that they be discouraged from forbidden activities, but that a blind eye should be turned to minor youthful indiscretions to allow adolescents to work out their improprieties before adulthood.
For many young ravon, this serves both to reinforce the expected behaviours while also teaching the benefits of discretion and a relatively safe environment to practice in.
This approach rarely results in youth "working out" their non-mainstream preferences so much as it teaches them to keep things hidden.
Given the Menotan focus on outward appearances, this is often considered sufficient.

The one exception to the prohibition against public sex is that a couple is expected to engage in a single coupling during their wedding in view of the priest and attendees.
This is believed to demonstrate that the couple is capable of the act and have a sufficiently strong bond for the marriage to stand the test of time.
A child whose conception can be dated to this coupling is considered especially blessed.
As few participants are comfortable having sex before an audience, it is common for the couple to be provided with aphrodisiac tanathi candy to overcome any reluctance.

### Tanathi Candy

Named after the chef credited with its development, tanathi candy is a small, very sweet candy typically dosed with an aphrodisiac.
According to popular belief, it was developed by the chef Va Tanathi to assist her relatives in the bedroom, but the facts of its development are lost.
In its original form, tanathi candy used local ingredients and provided only a mild boost to the libido.
With cross-world access and shipping, these are often replaced with extracts of [ruksou](ruksou.md) hormones.
Modern tanathi candy comes in a variety of strengths from "barely noticeable" to "desperately aroused".

Early formations of tanathi candy were correlated with reduced fertility and, when used for extended periods, reduced libido.
Modern versions, especially those incorporating ruksou hormone, have few side effects and no major complications, beyond the risk of addiction when used regularly.
While the effects of tanathi candy rarely last more than a couple of hours, it can be detected in the bloodstream for several days afterward.

The incorporation of ruksou hormones has changed the candy from something that affects only ravon to a powerful cross-species aphrodisiac.
Tanathi candy sold on the interworld market is rarely produced on Zifaelafojun and is often poorly produced or has inaccurate levels of ruksou hormones.
Due to its mind-altering effects, many areas consider tanathi candy to be a controlled substance.


## Religion

Long ago, the clans shared a complex pantheon of gods and goddesses.
When the population of the ravon started dropping off, focus increasingly shifted to the worship and rituals of Vaere, the goddess of fertility.
By the time the population stabilized, worship was focused almost entirely on just Vaere.
This focus caused her to take on aspects of the other deities, with many of Her original rituals fading away.
The shift has been so complete that the rituals originally associated with her would shock, even disturb modern Menotan sensibilities.

Most people in Menota are members of the Church of Vaere.
Modern religious practice in the Church of Vaere has become focused almost entirely on the rituals and forms of worship.
Neither actual belief and an understanding of the purpose of the rituals is considered important.
Almost no nobles and less than half of the peasantry believe in the existence of the goddess Vaere.
Despite the Church's claims to be only capable interpreters of divine will and morality, Vaere Herself has become increasingly deemphasized and neglected.

Although everyone is expected to maintain membership in the Church of Vaere, the church's focus is mainly on the nobility.
Its rites and rituals are flamboyant, showy, and expensive enough to be impractical for most peasants to participate in and, while priests can be of any sex, they are mostly from the noble class and are expected to remain celibate.
In addition, noble priests are no more welcoming of peasants than any other noble and many will refuse to deal with peasants, leaving the peasants to handle their spiritual affairs on their own or with an unsanctioned peasant priest.

The Association of Vaere is a movement that began among the disenfranchised of Pasha Za Fojun Fae before making inroads into the peasantry of Menota as well.
It emphasizes a return to the original worship of Vaere in the ways Her holy word prescribes.
Adherents to the Association have even been able to put together a mostly accurate modern translation of Her holy word.
In practice, many of the restored rituals have been sanitized to be less offensive (though often still shocking) to modern sensibilities.
Some small groups instead enact the older rituals (including large orgies) accurately, but this is opposed by both the Church of Vaere (who consider it immoral) and the leaders of the Association of Vaere (who say it makes general acceptance of the Association harder).

On an official level, the Association of Vaere is a banned organization and thus associated with it are subject to strict penalties. The traditional blind eye of the nobility, however, has allowed it to flourish amongst the peasants, and the recent concession of permitting Va Zavae's public membership in the Association has resulted in more nobles considering it as a lower-cost alternative to the Church of Vaere.

The Association of Vaere has claimed that increasing modern population growth rates are a sign of Vaere's favour resulting from their return to more traditional practices.
While this growth does roughly correspond to the Association's beginnings, the more cynical observers note that this is also about the time contact with the Interworld Nexus was reestablished, resulting in an increase in modern medicine.

A third, much smaller, practice exists as well.
This group, calling themselves the "Keepers of the Ways" are attempting to bring back the old polytheistic practices.
They claim to be researching the old ways and filling in the gaps in their knowledge, but given the timescale involved there are more gaps in their knowledge than there is knowledge.
To the extent that they are acknowledged by the other religious groups, they are looked down on, but mostly they're just ignored.
