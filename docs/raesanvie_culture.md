---
category: nation
world: raesan vie
parent_name: Raesan Vie
parent_addr: raesanvie
---

## The Raesan Vie Tourism Company

Despite the name, the *Raesan Vie Tourism Company* is not located on Raesan Vie, but on Serkm Hub.
A small group of asuni live in there and maintain the business.
This population is mostly composed of those who prefer off-world civilization and modern technology to the traditional lifestyle of Raesan Vie.

The *Raesan Vie Tourism Company* handles opening the gate as required and will make arrangements with native bands for off-worlders interested in low-tech vacations as well as arranging for native guides for any groups that wish to travel independently. The company's mission is to generally facility travel and what shipping is necessary. It is a non-profit organization and charges little for its services.


## Lifestyle

Because of the way expeditions vanish, the only asuni people from the Nexus are in contact with are those that live in the area around the gates. The local asuni have a hunter-gatherer culture; before contact from the Nexus, they were just starting to develop metallurgy. Since then, they have been entirely open to trade, but highly resistant to any form of permanent settlement by non-locals.

The asuni live in numerous small bands. Each typically has 30 to 40 members and always less than 100. The population moves regularly, living highly nomadic lives. Different bands interact on a regular basis and it is not unusually for a few individuals to move to another band when two bands meet. Because of how often different bands meet, information is shared and spread far more rapidly than those from more technological worlds would expect.

The band typically sleeps in a group, typically in a large, tent-like structure that protects from the weather and much of the wildlife. Shamans and others with specialized skills may maintain what appears to be a personal residence, this serves more as a work area than an actual residence.

There is no government as would be recognized in more modernized worlds; although individual bands are largely independent, law, to the extent that it exists as a concept, is largely through consensus rather than formal rules and changes gradually with distance. Disputes are often settled by decisions made by the oldest and widest band members and, if a decision absolutely cannot be reached, a shaman may be asked for a final resolution.

Locals pay very little attention to the time of day except in very broad terms, such as "morning" and "afternoon". In areas with thick tree cover, even this can get simplified to "day" or "night". Dawn and dusk are the only times that are consistently identified. The date is treated similarly, with years being identified only through the cycle of seasons. Not all asuni pay attention to the count of years and it is not uncommon for individuals to be unsure of their age or how many years have passed since an event.

No permanent construction exists beyond the facilities built in the immediate area of the gate (an those where created entirely for off-worlders for the benefit of off-worlders). Traditionally, most shelter is provided by temporary structures of hide and fallen wood. Each band typically has a few more substantial structures they erect when not travelling, either for group use or for the use of individuals with specialized skills.

### Clothing

The asuni make use of relatively little clothing. Most don't see the issue with public nudity and consider attempts to avoid it to be more trouble than its worth, especially given the climate, their fur, and their quadrupedal nature. Vests and harnesses are common due to the practically of pockets and pouches.

Shamans wear the hollowed skull of an [eksar](eksar.md) as a sign of their position. Most will only wear it during ceremonial functions or when otherwise engaging in spiritual tasks, but some wear it on a more regular basis.

### Technology

The native level of technology is neolithic, but enterprising locals regularly find ways to be import and incorporate modern technology from other worlds into their lives. Most such imports are things that do not require electricity or skilled operation; cookware and other metal tools are common imports.

Off-world charities often try to provide modern electronics for the local population, but are hindered by many factors, not the least being the need to provide some form of power generation. Other issues encountered include widespread illiteracy, exposure to the elements, and the need for everything to be portable. Most locals prefer more immediately practical goods, such as pots, pans, knives, etc..

No motor vehicles are currently used on Raesan Vie; the dense jungle is impassable to most vehicles and even helicopters have difficulty finding safe landing zones.

### Language

The language used by the known asuni population is called Ravagi. The constant movement of bands and the movement of individuals between bands has resulted in a surprisingly consistent language and even when individuals know different dialects, they adapt to the local version quickly. The local language has no written form, but there is some evidence that a few shamans have started adapting the writing systems of other languages and applying them to Ravagi.

### Names

Names are simple; the mother gives her children a name shortly after they're born. Most names are only one or two syllables. Those will the same name are distinguished with a descriptive adjective (e.g. Skon the Brave); these adjectives change regularly depending on what a given individual has done recently.

The lack of a system of writing or transliteration scheme for Ravagi makes it difficult for off-worlders to reliably transcribe local names. The same name is often spelt differently by different people or even the same person on different days (e.g. Skon may be spelt as Scon, Scahn, Skahn, Skone, etc.) and the constantly changing descriptive adjectives do not help, nor does the fact that most locals do not notice these issues.


### Sexuality

Asuni have an annual estrus cycle (locally called the rnuga). Neither sex has much desire for sex outside of this period, though males have more than females. This is considered to be an unfortunate medical condition in either sex, but is especially problematic in females as it results in the release of pheromones that effect the rest of the band. Females are often given herbal compounds to suppress their hormones, or at least the pheromones.

As a result of this, most out-of-season sexual encounters are between males. Although homosexual contact is normally ignored, out-of-season sex is considered to be inappropriate and to set a bad example. Asuni near the gate will often make contact with off-worlders to sate their lust rather than face the disapproval of their band. The idea of being sexually active all year is viewed as strange and many express the opinion that it would be overly distracting.

All asuni are highly promiscuous and while individuals often have a preference for their partner's sex, no stigma is attached to either homosexual or heterosexual encounters, or even to encounters with off-worlders, at least as long as the encounters occur during the appropriate season.

Courtship and sex have a strong instinctual component; although not completely fixed, most encounters take on a similar structure. Both courtship and sex are conducted openly and are considered no more private than eating. Asuni are comfortable talking about sex, but it is not a common topic when not experiencing estrus; they do not actively avoid it, but it does rarely comes up naturally, either.

One's "first mate" with the first individual one has sex with in a given season. A woman's first mate is considered the offical father of her children, even in situations where this is obviously not true (such as homosexual encounters). It is not uncommon for a particular pair of individuals to be each other's first mate.


## Daily Life

In known Raesan Vie culture, the day begins with a meal made from the remnants of the previous evenings meal. On days that the band will be travelling, the rest of the morning is spent preparing and packing. Food will be collected as the band travels and eaten on the go; no hunting parties set out, but animals that happen to cross the band's pass may still be killed.

On days the band is not travelling, hunting and gathering parties set out to find food and other supplies for the day; they will return gradually over the course of the afternoon. A small group will remain with the band's camp to attend to domestic tasks, such as mending or creating tools and clothing, preserving extra food for lean times, or looking after young children. In particularly cold or miserable weather, a fire may be started in the centre of camp.

As the hunting and gathering parties return, preparations begin for the evenings meal. If one wasn't already started, a fire may be started to cook the evening meal. Once the meal is eaten and the remnants cleaned up or stored for the morning, the members of the band relax until they go to sleep. Sports and storytelling are both popular during this time.

### Entertainment

Physical games (tag, hide-and-seek) are popular, especially among younger individuals. Additional games involving balls (either hide balls made locally or imported ones) or other basic equipment are common.

Mock combat also occurs, and is one of the only activities among youth that adults will keep a constant eye on. Aside from keeping both participants and spectators entertained, this is considered to be go practice for survival in the wild.

In both games and combat, rules vary wildly between different bands and often mutate quickly.

Storytelling and poetry is common, especially later in the evening, and forms the main method of passing on oral history. Although most band members will tell the occasional story, it's the shamans that do the bulk of the storytelling. The focus of both stories and poetry tends to be current or past band members rather than distant figures of legend. Although often humourous and dramatic, most stories are also instructive and hold lessons for the listeners.


