---
category: person
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
---

<div class='infobox'><table>
    <tr><th class='top-header' colspan='2'>Tkinae</th></tr>
    <tr><th>Species</th>  <td><a href='sakin.html'>Sakin</a></td></tr>
    <tr><th>Position</th> <td>Emperor of <a href='razaznudar.html'>Razaz Nudar</a></td></tr>
    <tr><th>Title</th>    <td>the Founder</td></tr>
    <tr><th>Hatched</th>  <td>Unknown</td></tr>
    <tr><th>Crowned</th>  <td>1344</td></tr>
    <tr><th>Died</th>     <td>1358</td></tr>
</table></div>

Tkinae was the original emperor of Razaz Nudar and was intimately involved in its founding about 1300 years ago. He also served as the head of the followers of Teka and was responsible for imposing the worship of Teka on all residents of Razaz Nudar.

Very little is known of Tkinae, nor are there any surviving depictions or descriptions.