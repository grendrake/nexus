---
category: animal
world: zifaelafojun
parent_name: Zifaelafojun
parent_addr: zifaelafojun
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Jiruan</th></tr>
    <tr><th>Homeworld</th><td><a href='zifaelafojun.html'>Zifaelafojun</td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
    <tr><th>Height</th><td>2'3" (0.7 m)</td></tr>
<!--    <tr><th>Weight</th><td>149.37 lbs (68 kg)</td></tr> -->
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
    <tr><th>Litter size:</th><td>4</td></tr>
    <tr><th>Gestation:</th><td>6 months</td></tr>
    <tr><th>Weaned:</th><td>9 months</td></tr>
    <tr><th>Maturity:</th><td>2 years</td></tr>
    <tr><th>Lifespan:</th><td>30 years</td></tr>
</table></div>

Jiruan are domesticated animals common to [Menota](menota.md) and [Pasha Za Fojun Fae](pashazafojunfae.md). Almost all jiruan are kept for purposes of home security. Beyond this, a small number are employed in hunting. They are not normally kept for companionship as jiruan have aggressive natures; although they sometimes form bonds to particular individuals, this is considered undesirable as it increases their aggression towards everyone else and is accompanied by sexual behaviours directed towards the bonded individual, even in castrated males.

Jiruan have a loosely feline build. They have been bred to have vividly coloured fur as well as large, sharp claws. Centuries of controlled breeding have aimed to create a guardian that is as intimidating to the eye as possible to act as a deterent.

<figure class='center' style='width:500px'>
    <img src='/art/species/jiruan.png' width='500' height='314'>
    <figcaption>
    Jiruan specimen.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

Most jiruan are bred are from specialized breeders who are equipped to deal with the aggression and sexual behaviours of multiple individuals. Animals for sale are typically castrated males, who display far less in the way of undesirable sexual behaviours and who have been extensively trained so that their new owners can control them. Despite this, many owners find it desirable or necessary to chain their jiruan in place, especially when they have guests the animal is not familiar with.
