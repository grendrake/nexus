---
category: animal
world: raesan vie
parent_name: Raesan Vie
parent_addr: raesanvie
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Eksar</th></tr>
    <tr><th>Homeworld</th><td><a href='raesanvie.html'>Raesan Vie</a></td></tr>
    <tr><th>Biome</th><td>Tropical Rain Forest</td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
    <tr><th>Height</th><td>3'6" (1.1 m)</td></tr>
    <tr><th>Weight</th><td>390 lbs (177 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
    <tr><th>Litter size:</th><td>1</td></tr>
    <tr><th>Gestation:</th><td>4 months</td></tr>
    <tr><th>Weaned:</th><td>2 months</td></tr>
    <tr><th>Maturity:</th><td>2 years</td></tr>
    <tr><th>Lifespan:</th><td>15 years</td></tr>
</table></div>


Eksar play a significant role in local spiritual beliefs and practices, but are not considered to be sacred. Prospective shamans are required to kill an eksar unarmed as part of their spiritual initiation. This is a dangerous undertaking and failure is typically fatal. Outside of spirutal practicses, the local [asuni](asuni.md) will often send out groups of hunters to eliminate eksar near asuni populations.

They are large carnivores originating from [Raesan Vie](raesanvie.md). Both sexes stand three and a half feet to their shoulders. Their teeth are unusually large and prominent, They have short fur with surprisingly vivid green stripes. Their fur is valued both on and off Raesan Vie for its unique coloration and its softness. Despite this, there is no commercial export of the fur. Eksar fur found off-world was almost certainly purchased by individuals from the locals; hunting parties from offworld are not exactly discouraged, but face the same problems as any group of offworlds in navigating Raesan Vie.

Eksar are always aggressive and are prone to trying to eat anything smaller than themselves, or at least not too much larger, They are even more dangerous near their offspring or when starving and have been known to kill animals twice their size in such circumstances. Because of this, it is always recommended not to go anywhere alone and to report sightings immediately.

Anything smaller than themselves, or at least not too much larger, is considered to be edible and they are aggressive about sampling anything they can catch. This includes asuni and off-worlders. They will not normally approach a large group.

Their aggression is not limited to other species; eksar are extremely solitary and are found with other individuals of their species only for copulation or mothers raising their offspring. Eksar courting is slow and cautious; it is not unheard of for eksar to decide they are more interested in eating their prospective partner than mating. Even once coitus has finished the risk remains and the individuals split up quickly unless they decide to try and kill their partner instead.