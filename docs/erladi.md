---
title: Erladi
category: world
world: erladi
---


<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Erladi</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/erladi.png'></td></tr>
<tr><th>Star</th>                   <td>F6V</td></tr>
<tr><th>Year Length:</th>           <td>1,187.26 local days<br />838.71 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>296 K (22.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>0.90 G</td></tr>
<tr><th>Day Length:</th>            <td>16.954 h</td></tr>
<tr><th>Moons</th>                  <td>Two</td></tr>
<tr><td colspan='2' class='wide-data'><a href='systems.html#erladi'>Erladi System</a></td></tr>
</table></div>

The world of Erladi is the sixth planet in orbit around a young F6 star. It is home to the [au](au.md), sophants with an extreme degree of sexual dimorphism.

The Erladi gate is located in the nation of [Gek Relt](gekrelt.md), near the borders of Ker Relt and Reka relt.
