---
category: sophant
world: erladi
parent_name: Erladi
parent_addr: erladi
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Au</th></tr>
<tr><th>Homeworld:</th><td><a href='erladi.html'>Erladi</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical (Male)</th></tr>
<tr><th>Avg. Height:</th><td>3'3" 0.9 m)</td></tr>
<tr><th>Avg. Weight:</th><td>250 lbs (114 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Physical (Female)</th></tr>
<tr><th>Avg. Height:</th><td>5'3" 1.3 m)</td></tr>
<tr><th>Avg. Weight:</th><td>131 lbs (60 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>2</td></tr>
<tr><th>Gestation:</th><td>10 months</td></tr>
<tr><th>Weaned:</th><td>5 years</td></tr>
<tr><th>Maturity:</th><td>16 years</td></tr>
<tr><th>Lifespan:</th><td>75 years</td></tr>
</table></div>

The au are best known for their extreme level of sexual dimorphism; those encountering one of the au for the first time often have trouble believing that they aren’t two separate species. These differences have led to nearly every au society having strong gender roles and expectations.

## Description (Male)

<figure class='right' style='width:380px'>
    <img src='/art/species/au-male.png' width='380' height='275'
    <figcaption>
    Male au.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

Male au are quadrupedal; although they lack thumbs, the toes on their forepaws are more dextrous than most animals. Their entire body is covered in a coat of red-brown or tan fur with a thicker mane running from the top of their head and ending between their shoulder blades. The flexible, tapered ears on their head automatically shift to better hear sounds. Their tail is a couple of inches thick over most of its length, but is thicker at the base and narrows at the tip where the fur flares up into a large tuft.

## Description (Female)

<figure class='right' style='width:163px'>
    <img src='/art/species/au-female.png' width='163' height='400'
    <figcaption>
    Female au.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

Unlike male au, female au are bipeds with a striking similarity to humans, especially as reflected in their face. Unlike humans their ears are longer and taper in a way similar to male au and the hair on their head also grows from the back of their neck down to their shoulder blades, much as it does in males. Above the waist their bodies are covered in a layer of very thin, light coloured fur. Below the waist this fur becomes much thicker and darker, though still short. The hair on their head and their lower bodies is the same colour. Like male au, they have a tail, though theirs is much shorter in comparison and the end of the tuft typically doesn’t quite reach the ground.

## Diet

Although omnivores, the majority of the au diet consists of vegetable matter, especially grains. Some male au have instincts that encourage them to engage in hunting behaviours, such as chasing things, but in modern times these are rarely put to use.

## Reproduction

Au children are typically born as twins, one of each sex. The mother carries them for about eight months after which they are born in a slightly less developed form than human offspring. While the mother carries the principle duty of nursing her offspring, the au are unusual in that it is not uncommon for the father to assist in this. Male lactation can be induced either through appropriate stimulation or the usage of some drugs or herbs. In either case, it occurs far more readily than in most mammals.

The male sex drive peaks during puberty, then slowly builds up reaching nearly the same level as they approach fifty before falling off again. Conversely, after puberty females have a very stable sex drive; although stronger than the male sex drive for the first few years after puberty, it quickly falls behind. Many have claimed this as evidence that the au are “intended” to be polygynous.
