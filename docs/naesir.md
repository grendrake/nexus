---
category: sophant
world: rojizruzz
parent_name: Rojizruzz
parent_addr: rojizruzz
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Naesir</th></tr>
<tr><th>Homeworld:</th><td>Rojizruzz</td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>1.9 m (6’2")</td></tr>
<!-- <tr><th>Avg. Weight:</th><td>96 lbs (43 kg)</td></tr> -->
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>1</td></tr>
<tr><th>Gestation:</th><td>11 months</td></tr>
<tr><th>Weaned:</th><td>5 years</td></tr>
<tr><th>Maturity:</th><td>25 years</td></tr>
<tr><th>Lifespan:</th><td>81 years</td></tr>
</table></div>

The naesir have an aquatic heritage reflected in their smooth, streamlined bodies.

## Description

<!-- {% figure /art/species/zaeren.png | 230 | 390 | right | Male zaeren. | | spelunker %} -->


## Diet


## Reproduction
