---
category: world
world: vozruat
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Vozruat</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/vozruat.png'></td></tr>
<tr><th>Star</th>                   <td>G4V</td></tr>
<tr><th>Year Length:</th>           <td>232.35 Local days<br>282.94 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>304 K (30.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>0.83 G</td></tr>
<tr><th>Day Length:</th>            <td>29.23 h</td></tr>
<tr><th>Moons</th>                  <td>None</td></tr>
<!-- <tr><td colspan='2' class='wide-data'>{% link zifaelafojun system %}</td></tr> -->
</table></div>


