---
category: sophant
world: rynux
parent_name: Rynux
parent_addr: rynux
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Klugrut</th></tr>
<tr><th>Homeworld:</th><td><a href='rynux.html'>Rynux</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>1.2 m (3’11")</td></tr>
<!-- <tr><th>Avg. Weight:</th><td>96 lbs (43 kg)</td></tr> -->
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>2</td></tr>
<tr><th>Gestation:</th><td>8 months</td></tr>
<tr><th>Weaned:</th><td>3 years</td></tr>
<tr><th>Maturity:</th><td>22 years</td></tr>
<tr><th>Lifespan:</th><td>79 years</td></tr>
</table></div>

The klugrut are an artifical species.
They were created by the [teshi](teshi.md) as a slave race, but later rebelled and established their own freedom.

## Description

<!-- {% figure /art/species/zaeren.png | 230 | 390 | right | Male zaeren. | | spelunker %} -->


## Diet


## Reproduction
