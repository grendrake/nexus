---
category: language
world: zifaelafojun
parent_name: Zifaelafojun
parent_addr: zifaelafojun
notoc: true
---

Jowvon is the offical and the only language used in Menota and Pasha Za Fojun Fae. It is rarely seen or heard outside of those countries. The phonetic chart below is constructed using the <a class="reference external" href="http://www.omniglot.com/writing/ipa.htm">http://www.omniglot.com/writing/ipa.htm</a> (IPA). ASCII transcriptions are provided in square brackets.

Words in Jowvon are constructed in a CV[alv] pattern where "alv" is an optional alveolar consonant or n. Writing is performed using a single syllabry of 88 characters plus assorted numerals, punctuation, etc.

<table>
    <caption>Consonent Sounds</caption>
    <tr>
        <th></th>
        <th>Labial</th>
        <th>Labial-Dental</th>
        <th>Dental</th>
        <th>Alveolar</th>
        <th>Post-Alveolar</th>
    </tr>
    <tr>
        <td>Stop</td>
        <td>p</td>
        <td>&nbsp;</td>
        <td>t</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Nasal</td>
        <td>m</td>
        <td>&nbsp;</td>
        <td>n̪  [n]</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Frictive</td>
        <td>&nbsp;</td>
        <td>f, v</td>
        <td>θ [th]</td>
        <td>s, z</td>
        <td>ʃ [sh], ʒ [j]</td>
    </tr>
    <tr>
        <td>Approximate</td>
        <td>&nbsp;</td>
        <td>ʋ [w]</td>
        <td>&nbsp;</td>
        <td>ɹ [r]</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Lateral-Approximate</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>l</td>
        <td>&nbsp;</td>
    </tr>
</table>

<br>

<table>
    <caption>Vowel Sounds.<br>Back vowels are rounded, others are not.</caption>
    <tr>
        <th></th>
        <th>Front</th>
        <th>Back</th>
    </tr>
    <tr>
        <td>Close</td>
        <td>i</td>
        <td>u</td>
    </tr>
    <tr>
        <td>Close-mid</td>
        <td>e [ae]</td>
        <td>o</td>
    </tr>
    <tr>
        <td>Open-mid</td>
        <td>ɛ [e]</td>
        <td></td>
    </tr>
    <tr>
        <td>Open</td>
        <td>a</td>
        <td></td>
    </tr>
</table>


## Writing

Jowvon is written using paired glyphs written one above the other. The top glyph is for the consonant of a syllable and the lower glyph represents the vowel. Four consonants (S, Z, R, and L) may also be used without an accompanying vowel. In this case, the glyph is written centred on the line.

I have also created a <a href='/glyphs/jowvon.svg'>full table of all combinations</a> as well.


<table style='text-align:center'>
    <caption>Jowvon Glyphs</caption>
    <tr>
        <td>i<br><img alt="i" src="/glyphs/jo/001.svg" /></td>
        <td>u<br><img alt="u" src="/glyphs/jo/002.svg" /></td>
        <td>ae<br><img alt="ae" src="/glyphs/jo/003.svg" /></td>
        <td>o<br><img alt="o" src="/glyphs/jo/004.svg" /></td>
        <td>e<br><img alt="e" src="/glyphs/jo/005.svg" /></td>
        <td>a<br><img alt="a" src="/glyphs/jo/006.svg" /></td>
        <td></td>
    </tr>
    <tr>
        <td>p<br><img alt="p" src="/glyphs/jo/010.svg" /></td>
        <td>t<br><img alt="t" src="/glyphs/jo/020.svg" /></td>
        <td>m<br><img alt="m" src="/glyphs/jo/030.svg" /></td>
        <td>n<br><img alt="n" src="/glyphs/jo/040.svg" /></td>
        <td>f<br><img alt="f" src="/glyphs/jo/050.svg" /></td>
        <td>v<br><img alt="v" src="/glyphs/jo/060.svg" /></td>
        <td>th<br><img alt="th" src="/glyphs/jo/070.svg" /></td>
    </tr>
    <tr>
        <td>s<br><img alt="s" src="/glyphs/jo/080.svg" /></td>
        <td>z<br><img alt="z" src="/glyphs/jo/090.svg" /></td>
        <td>sh<br><img alt="sh" src="/glyphs/jo/100.svg" /></td>
        <td>j<br><img alt="j" src="/glyphs/jo/110.svg" /></td>
        <td>w<br><img alt="w" src="/glyphs/jo/120.svg" /></td>
        <td>r<br><img alt="r" src="/glyphs/jo/130.svg" /></td>
        <td>l<br><img alt="l" src="/glyphs/jo/140.svg" /></td>
    </tr>
</table>

<!--<br>

<table>
    <caption>Jowvon Numerals</caption>
    <tr>
        <th>Digit</th>
        <th>Value</th>
        <th>Name</th>
        <th>Digit</th>
        <th>Value</th>
        <th>Name</th>
    </tr>
    <tr>
        <td><img alt="0" src="glyphs/jo/.svg" /></td>
        <td>0</td>
        <td>zithya</td>
        <td><img alt="1" src="glyphs/jo/.svg" /></td>
        <td>1</td>
        <td>rthya</td>
    </tr>
    <tr>
        <td><img alt="2" src="glyphs/jo/.svg" /></td>
        <td>2</td>
        <td>latna</td>
        <td><img alt="3" src="glyphs/jo/.svg" /></td>
        <td>3</td>
        <td>digta</td>
    </tr>
    <tr>
        <td><img alt="4" src="glyphs/jo/.svg" /></td>
        <td>4</td>
        <td>yuktja</td>
        <td><img alt="5" src="glyphs/jo/.svg" /></td>
        <td>5</td>
        <td>ga</td>
    </tr>
</table>

<br>

<table>
    <caption>Other Glyphs</caption>
    <tr>
        <th>Glyph</th>
        <th>Meaning</th>
        <th>Name</th>
    </tr>
    <tr>
        <td><img alt="fs" src="glyphs/nr/029.svg" /></td>
        <td>Full Stop</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><img alt="pa" src="glyphs/nr/030.svg" /></td>
        <td>Pause</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><img alt="oq" src="glyphs/nr/031.svg" /></td>
        <td>Open Quote</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td><img alt="cq" src="glyphs/nr/032.svg" /></td>
        <td>Close Quote</td>
        <td>&nbsp;</td>
    </tr>
</table>-->

## Nouns

Nouns come in three genders: male, female, and neuter. The gender of a noun is determined by the vowel of the first character:

<table>
    <caption>Noun Genders</caption>
    <tr><th>Gender</th><th>Vowel</th></tr>
    <tr><td>Male</td><td>a, u</td></tr>
    <tr><td>Female</td><td>ae, e, i</td></tr>
    <tr><td>Neuter</td><td>o</td></tr>
</table>

<p>Nouns are marked for number (none, one, two, several, many) and case.

<table>
    <caption>Noun Number Prefixes</caption>
    <tr><th>Number</th>       <th>Prefix</th></tr>
    <tr><td>None</td>         <td>rae-</td></tr>
    <tr><td>One</td>          <td>ja-</td></tr>
    <tr><td>Two</td>          <td>ma-</td></tr>
    <tr><td>Several (3-8)</td><td>ra-</td></tr>
    <tr><td>Many</td>         <td>pa-</td></tr>
</table>

<br>

<table>
    <caption>Noun Case Particles</caption>
    <tr><th>Number</th>       <th>Prefix</th></tr>
    <tr><td>Subjective</td>   <td>&mdash;</td></tr>
    <tr><td>Causitive</td>    <td>guta</td></tr>
    <tr><td>Genitive</td>     <td>za</td></tr>
</table>

<p>Articles come in definate and indefinate forms and must agree with the nouns gender.

<table>
<caption>Articles</caption>
<tr><th>Gender</th><th>Definate</th><th>Indefinate</th></tr>
<tr><td>Male</td>  <td>fa</td>       <td>wu</td></tr>
<tr><td>Female</td><td>sen</td>      <td>thi</td></tr>
<tr><td>Neuter</td><td>lumae</td>    <td>po</td></tr>
</table>

<p>Particles follow the nouns or noun-phrases they modify

<h2 id='verbs'>Verbs</h2>

<p>Verbs must agree with the gender of the subject (if present) and are marked for tense, and if they are perfect or negative. All verbs are intransitive (they take a single argument). What would normally be the object of a transitive verb is instead a seperate, optional clause.

<table>
    <caption>Verb Affixes</caption>
    <tr><th>Ending</th> <th>Tense</th></tr>
    <tr><td>-wa</td>    <td>Past</td></tr>
    <tr><td>&mdash;</td><td>Present</td></tr>
    <tr><td>-ji</td>    <td>Future</td></tr>
    <tr><td>-pae</td>   <td>Negative</td></tr>
    <tr><td>-sha</td>   <td>Perfect</td></tr>
<table>

<h2 id='syntax'>Syntax</h2>

<p>Jowvon is often described as being an Object-Verb language; compared to languages like English, everything in Jowvon is cast in the passive voice. Sentances are constructed as [subject] [verb] with modifiers (adjectives, adverbs, etc.) coming before the words they modify and post-positions being used.


<h2 id='examples'>Examples</h2>

<table class='interlex'>
    <caption>Example 1</caption>
    <tr><td colspan='3'></td></tr>
    <tr>
        <td>fa</td>
        <td>jalozaputa</td>
        <td>javo</td>
    </tr>
    <tr>
        <td>(DefArt-M)</td>
        <td>(N-glossyness)</td>
        <td>(V-is)</td>
    </tr>
    <tr>
        <td colspan="3" class='interlex-ex'>the glossiness is</td>
    </tr>
    <tr>
        <td colspan="3" class='interlex-ex'>The glossyness is.</td>
    </tr>
</table>

<table class='interlex'>
    <caption>Example 2</caption>
    <tr><td colspan='6'></td></tr>
    <tr>
        <td>fa</td>
        <td>jalozaputa</td>
        <td>javo</td>
        <td>sen</td>
        <td>jasitaevu</td>
        <td>guta</td>
    </tr>
    <tr>
        <td>(DefArt-M)</td>
        <td>(N-glossyness)</td>
        <td>(V-is)</td>
        <td>(DefArt-F)</td>
        <td>(one N-sun)</td>
        <td>(PART-cause)</td>
    </tr>
    <tr>
        <td colspan="6" class='interlex-ex'>the shining is because of the sun</td>
    </tr>
    <tr>
        <td colspan="6" class='interlex-ex'>The sun is shining.</td>
    </tr>
</table>

<table class='interlex'>
    <caption>Example 3</caption>
    <tr><td colspan='3'></td></tr>
    <tr>
        <td>lumae</td>
        <td>jajotospa</td>
        <td>soljaesha</td>
    </tr>
    <tr>
        <td>(DefArt-N)</td>
        <td>(one N-window)</td>
        <td>(PERF-V-breaking)</td>
    </tr>
    <tr>
        <td colspan="3" class='interlex-ex'>the window was broken</td>
    </tr>
    <tr>
        <td colspan="3" class='interlex-ex'>The window was broken.</td>
    </tr>
</table>

<table class='interlex'>
    <caption>Example 4</caption>
    <tr>
        <td>lumae</td>
        <td>jajotospa</td>
        <td>soljae</td>
        <td>fa</td>
        <td>rapusaluvo</td>
        <td>guta</td>
    </tr>
    <tr>
        <td>(DefArt-N)</td>
        <td>(one N-window)</td>
        <td>(PRES-V-breaking)</td>
        <td>(DefArt-M)</td>
        <td>(SEV N-man)</td>
        <td>(PART-cause)</td>
    </tr>
    <tr>
        <td colspan="6" class='interlex-ex'>the window is breaking by several men</td>
    </tr>
    <tr>
        <td colspan="6" class='interlex-ex'>The window is being broken by several men.</td>
    </tr>
</table>


<table class='interlex'>
    <caption>Example 5</caption>
    <tr>
        <td>wu</td>
        <td>jamane</td>
        <td>forae</td>
        <td>fa</td>
        <td>vujaro</td>
        <td>za</td>
        <td>zifaela</td>
        <td>za</td>
        <td>japutha</td>
        <td>guta</td>
    </tr>
    <tr>
        <td>(IndefArt-M)</td>
        <td>(one N-wisdom)</td>
        <td>(PRES-V-spoken)</td>
        <td>(DefArt-M)</td>
        <td>(N-green)</td>
        <td>(PART-gen)</td>
        <td>(N-eye)</td>
        <td>(PART-gen)</td>
        <td>(SEV N-man)</td>
        <td>(PART-cause)</td>
    </tr>
    <tr>
        <td colspan="10" class='interlex-ex'>a wisdom is spoken by the green-eyed male ravon</td>
    </tr>
    <tr>
        <td colspan="10" class='interlex-ex'>The green-eyed man speaks wisdom.</td>
    </tr>
</table>

