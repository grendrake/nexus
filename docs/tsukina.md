---
category: nation
world: kzaknri
parent_name: kzaknri
parent_addr: kzaknri
---


<figure class='right' style='width:98px'>
    <img src='/art/flags/tsukina.svg' width='98' height='198'>
    <figcaption>
    Flag of Tsukina.<br>
    Art © <a href='https://www.grenslair.com/'>Gren Drake</a>
    </figcaption>
</figure>

The population of Tsukina split off very early in the history of Razaz Nudar and travelled at great personal loss to an area that was, at the time, very inaccessible. Throughout history they have been isolated and often forgotten about entirely. As a result, although their culture has the same origins as modern-day Razaz Nudar, it is gone in a very different direction.

The language used by the tsukina is very different than that of Razaz Nudar and was even before their migration. They use an logographic writing system.

The Tsukina are best known for being isolationists; visitors to their lands are not welcome and will be politely escorted out. Their government is a theocracy centred around their deity Zakanor and the worship of same is integrated into every facet of their daily lives.

Although the population of the Tsukina has grown since they migrated from Razaz Nudar, their population still remains small with little genetic diversity. Despite this, immigration is discouraged and relatively few individuals have parents from beyond the nation's borders.


## Clothing and Body Markings

In part because of the unusually hot and humid climate, the people of the Tsukina largely forgo anything that would be considered clothing. Most will carry a bag on a shoulder strap used for carrying any goods they need with them.

In place of clothing, the Tsukina make extensive use of body paint. Being unpainted in public is typically viewed in the same fashion that being unclothed would be in other cultures. Every person has their own, individual, pattern, though snout designs vary little between individuals. These cover everything except for their feet, hands, back, their head other than their snout, and last half of their tail. Female patterns consist of a lot of long, connected, curved lines, while male designs consist of short curves, dots, and circles with very few touching each other.
