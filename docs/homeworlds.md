---
category: general
notoc: true
---

<table style='font-size: 85%'>
<tr><th>Name</th>
    <th>Native<br>Sophants</th>
    <th>Star</th>
    <th>Year Length</th>
    <th>Gravity</th>
    <th>Atmo.<br>Pressure</th>
    <th>Surf.<br>Temp.</th>
    <th>Day Length</th>
    <th>Axial Tilt</th>
    <th>Diameter</th></tr>
<tr><td><a href='/daesraworld.html'>The Daesra World</a></td>
    <td><a href='/daesra.html'>Daesra</a></td>
    <td>G1V</td>
    <td>363.19 Earth days<br>208.18 local days</td>
    <td>0.90 G</td>
    <td>0.812 atm</td>
    <td>29.85&deg;C</td>
    <td>41.87 h</td>
    <td>23&deg;</td>
    <td>11,280 km</td></tr>
<tr><td><a href='/earth.html'>Earth</a></td>
    <td><a href='/human.html'>Human</a></td>
    <td>G2V</td>
    <td>365.26 days</td>
    <td>1 G</td>
    <td>1 atm</td>
    <td>14.9&deg;C</td>
    <td>24 h</td>
    <td>23&deg;</td>
    <td>12,742 km</td></tr>
<tr><td><a href='/erladi.html'>Erladi</a></td>
    <td><a href='/au.html'>Au</a></td>
    <td>F6V</td>
    <td>838.71 Earth days<br>1,187.26&nbsp;local&nbsp;days</td>
    <td>0.90 G</td>
    <td>1.17 atm</td>
    <td>22.85&deg;C</td>
    <td>16.95</td>
    <td>17&deg;</td>
    <td>15,123&nbsp;km</td></tr>
<tr><td><a href='/ingr.html'>Ingr</a></td>
    <td><a href='/drius.html'>Drius</a></td>
    <td>K2V</td>
    <td>165.17&nbsp;Earth&nbsp;days<br>189.16 local days</td>
    <td>1.03 G</td>
    <td>1.16 atm</td>
    <td>10.85&deg;C</td>
    <td>20.96 h</td>
    <td>33&deg;</td>
    <td>14,466 km</td></tr>
<tr><td><a href='/isaguyat.html'>Isaguyat</a></td>
    <td><a href='/sekar.html'>Sekar</a></td>
    <td>F3V&nbsp;&amp; K6V</td>
    <td>703.70&nbsp;Earth&nbsp;days<br>129.92 local days</td>
    <td>0.836 G</td>
    <td>0.836 atm</td>
    <td>14.85&deg;C</td>
    <td>129.99 h</td>
    <td>24&deg;</td>
    <td>11,345 km</td></tr>
<tr><td><a href='/kloippraks.html'>Kloip Praks</a></td>
    <td><a href='/adranel.html'>Adranel</a></td>
    <td>K6V</td>
    <td>124.28&nbsp;Earth&nbsp;days<br>55.28 local days</td>
    <td>1.08 G</td>
    <td>1.24 atm</td>
    <td>5.66&deg;C</td>
    <td>53.96 h</td>
    <td>32&deg;</td>
    <td>12,639 km</td></tr>
<tr><td><a href='/kzaknri.html'>Kzaknri</a></td>
    <td><a href='/sakin.html'>Sakin</a></td>
    <td>F6V</td>
    <td>681.19&nbsp;Earth&nbsp;Days<br>600.10 local days</td>
    <td>1.20 G</td>
    <td>1.08 atm</td>
    <td>27.85&deg;C</td>
    <td>27.24 h</td>
    <td>10&deg;</td>
    <td>11,766 km</td></tr>
<tr><td><a href='/raesanvie.html'>Raesan Vie</a></td>
    <td><a href='/asuni.html'>Asuni</a></td>
    <td>G8V</td>
    <td>269.85 Earth days<br>212.46 local days</td>
    <td>0.64 G</td>
    <td>1.04 atm</td>
    <td>25.85&deg;C</td>
    <td>30.48 h</td>
    <td>31&deg;</td>
    <td>7,582 km</td></tr>
<tr><td><a href='/rojizruzz.html'>Rojizruzz</a></td>
    <td><a href='/naesir.html'>Naesir</a></td>
    <td>F7V</td>
    <td>782.10 Earth days<br>749.81 local days</td>
    <td>0.85 G</td>
    <td>1.11 atm</td>
    <td>9.85&deg;C</td>
    <td>25.15 h</td>
    <td>10&deg;</td>
    <td>10,551 km</td></tr>
<tr><td><a href='/rynux.html'>Rynux</a></td>
    <td><a href='/klugrut.html'>Klugrut</a><br><a href='/teshi.html'>Teshi</a></td>
    <td>G6V</td>
    <td>347.59 Earth days<br>348.59 local days</td>
    <td>0.72 G</td>
    <td>1.15 atm</td>
    <td>12.85&deg;C</td>
    <td>23.93 h</td>
    <td>47&deg;</td>
    <td>10,337 km</td></tr>
<tr><td><a href='/ujan.html'>Ujan</a></td>
    <td><a href='/guas.html'>Guas</a></td>
    <td>G8V</td>
    <td>172.32 Earth days<br>164.43 local days</td>
    <td>1.02 G</td>
    <td>0.82 atm</td>
    <td>24.85&deg;C</td>
    <td>25.00 h</td>
    <td>16&deg;</td>
    <td>12,559 km</td></tr>
<tr><td><a href='/vakatr.html'>Va-Katr</a></td>
    <td><a href='/garor.html'>Garor</a><br><a href='/rakelm.html'>Rakelm</a></td>
    <td>G6V</td>
    <td>335.18 Earth days<br>359.409 local days</td>
    <td>1.07 G</td>
    <td>0.931 atm</td>
    <td>12.85&deg;</td>
    <td>22.382 h</td>
    <td>24&deg;</td>
    <td>11,973 km</td></tr>
<tr><td><a href='/vozruat.html'>Vozruat</a></td>
    <td><a href='/ynathi.html'>Ynathi</a></td>
    <td>G4V</td>
    <td>282.94 Earth days<br>232.35 local days</td>
    <td>0.83 G</td>
    <td>0.83 atm</td>
    <td>30.85&deg;C</td>
    <td>29.23 h</td>
    <td>27&deg;</td>
    <td>9,287 km</td></tr>
<tr><td><a href='/zifaelafojun.html'>Zifaelafojun</a></td>
    <td><a href='/ravon.html'>Ravon</a></td>
    <td>G8V</td>
    <td>325.03 Earth days<br>204.79 local days</td>
    <td>0.73 G</td>
    <td>1.02 atm</td>
    <td>11.85&deg;C</td>
    <td>38.09 h</td>
    <td>18&deg;</td>
    <td>10,337 km</td></tr>
</table>
