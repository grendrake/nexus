---
category: world
world: rojizruzz
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Rojizruzz</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/rojizruzz.png'></td></tr>
<tr><th>Star</th>                   <td>F7V</td></tr>
<tr><th>Year Length:</th>           <td>749.81 Local days<br>782.10 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>283 K (9.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>0.85 G</td></tr>
<tr><th>Day Length:</th>            <td>25.00 h</td></tr>
<tr><th>Moons</th>                  <td>1 captured asteroid</td></tr>
<!-- <tr><td colspan='2' class='wide-data'>{% link zifaelafojun system %}</td></tr> -->
</table></div>


