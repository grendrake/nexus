---
category: general
---


The full system specifications for the homeworld systems of the various sophants.
This focuses on data that is either (1) immediately useful or (2) required to regenerate the full system data.
No attempt has been made to be exhaustive; missing information can typically be calculated using the information provided.

I have endeavoured to keep the systems close enough that the inhabitants will have little trouble interacting or visiting other systems.
Improbable elements that do not directly effect habitability (such as Kzaknri's inner system gas giant) have been left in place; in building these systems, I have valued "interesting" over "strictly realistic".

This information was generated using the system design sequence presented in <a href='http://www.sjgames.com/gurps/books/space/'>GURPS Space 4e</a>.
Any errors are most likely mine.



## The Daesra World

<div class='infobox infobox-centre'><table>
    <tr><th class='top-header' colspan='5'>The Daesra World</th></tr>
    <tr><th class='wide-header' colspan='3'>System Data</th>                <th class='wide-header' colspan='2'>Homeworld Data</th></tr>
    <tr><th colspan='2'>System Age</th>         <td>4.6 Gy</td>             <th>Orbital Radius</th>         <td>0.905 AU</td></tr>
    <tr><th colspan='2'>Number of Stars</th>    <td>1</td>                  <th>Orbital Period</th>         <td>363.186 Earth Days</td></tr>
    <tr><th class='wide-header' colspan='3'>Star Data</th>                  <th></th>                       <td>208.178 local days</td></tr>
    <tr><th colspan='2'>Mass</th>               <td>1.05 (G1V)</td>         <th></th>                       <td>0.994 &times; Earth</td></tr>
    <tr><th colspan='2'>Luminosity</th>         <td>1.408</td>              <th>Hydrographic Coverage</th>  <td>75%</td></tr>
    <tr><th colspan='2'>Temperature</th>        <td>5900 K</td>             <th>Surface Temperature</th>    <td>29.85&deg;C</td></tr>
    <tr><th colspan='2'>Star Radius</th>        <td>0.0053 AU</td>          <th>Density</th>                <td>5.63 g/cc (1.02 &times; Earth)</td></tr>
    <tr><th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th>
                                                                            <th>Gravity</th>                <td>0.902 G</td></tr>
    <tr><th>I</th>      <td>0.111</td>  <td>Tiny Rock</td>                  <th>Escape Velocity</th>        <td>9.990 km/s</td></tr>
    <tr><th>II</th>     <td>0.166</td>  <td>Tiny Rock</td>                  <th>Diameter</th>               <td>11,280.306 km (0.884 &times; Earth)</td></tr>
    <tr><th>III</th>    <td>0.316</td>  <td>Std. Chthonian</td>             <th></th>                       <td>7,009.071 mi</td></tr>
    <tr><th>IV</th>     <td>0.538</td>  <td>Std. Greenhouse</td>            <th>Atmospheric Mass</th>       <td>0.9</td></tr>
    <tr><th>V</th>      <td>1.013</td>  <td>Std. Garden</td>                <th>Atmospheric Pressure</th>   <td>0.812 atm (standard)</td></tr>
    <tr><th></th>       <th>moon:</th>  <td>1 moonlet</td>                  <th>Axial Tilt</th>             <td>23&deg;</td></tr>
    <tr><th>VI</th>     <td>1.976</td>  <td>Small Rock</td>                 <th>Rotation Period</th>        <td>41.67 h</td></tr>
    <tr><th></th>       <th>moon:</th>  <td>1 moonlet</td>                  <th>Local Day</th>              <td>41.870 h</td></tr>
    <tr><th>VII</th>    <td>2.339</td>  <td>Std. Ice</td>                   <th>Surface Area (Total)</th>   <td>399,752,902.195 km<sup>2</sup></td></tr>
    <tr><th></th>       <th>moons:</th> <td>2 tiny rock</td>                <th></th>                       <td>154,337,243.809 mi<sup>2</sup></td></tr>
    <tr><th>VIII</th>   <td>3.976</td>  <td>Std. Ice</td>                   <th>Surface Area (Land)</th>    <td> 99,938,225.549 km<sup>2</sup></td></tr>
    <tr><th></th>       <th>moons:</th> <td>tiny rock</td>                  <th></th>                       <td> 35,584,310.952 mi<sup>2</sup></td></tr>
    <tr><th>IX</th>     <td>5.964</td>  <td>Med. Gas Giant</td>             <th>Surface Area (Water)</th>   <td>299,814,676.647 km<sup>2</sup></td></tr>
    <tr><th></th>       <th>moons:</th> <td>5 inner, 3 major, 4 outer</td>  <th></th>                       <td>115,752,932.857 mi<sup>2</sup></td></tr>
    <tr><th>X</th>      <td>9.542</td>  <td>Med. Gas Giant</td>             <th></th>                       <td></td></tr>
    <tr><th></th>       <th>moons:</th> <td>9 inner, 6 major, 3 outer, visible ring</td>  <th></th>         <td></td></tr>
    <tr><th>XI</th>     <td>18.130</td> <td>Small Gas Giant</td>            <th></th>                       <td></td></tr>
    <tr><th></th>       <th>moons:</th> <td>5 inner, 6 major, 2 outer</td>  <th></th>                       <td></td></tr>
    <tr><th>XII</th>    <td>30.210</td> <td>Med. Gas Giant</td>             <th></th>                       <td></td></tr>
    <tr><th></th>       <th>moons:</th> <td>3 inner, 2 major, 2 outer</td>  <th></th>                       <td></td></tr>
</table></div>
<br clear='all' />



## Erladi

<div class='infobox infobox-centre'><table>
    <tr><th class='top-header' colspan='5'>Erladi</th></tr>
    <tr><th class='wide-header' colspan='3'>System Data</th>                <th class='wide-header' colspan='2'>Homeworld Data</th></tr>
    <tr><th colspan='2'>System Age</th>         <td>0.3 Gy</td>             <th>Orbital Radius</th>         <td>1.947 AU</td></tr>
    <tr><th colspan='2'>Number of Stars</th>    <td>1</td>                  <th>Orbital Period</th>         <td>838.717 Earth Days</td></tr>
    <tr><th class='wide-header' colspan='3'>Star Data</th>                  <th></th>                       <td>1,187.264 local days</td></tr>
    <tr><th colspan='2'>Mass</th>               <td>1.4 (F4V)</td>          <th></th>                       <td>2.296 &times; Earth</td></tr>
    <tr><th colspan='2'>Luminosity</th>         <td>3.814</td>              <th>Hydrographic Coverage</th>  <td>67%</td></tr>
    <tr><th colspan='2'>Temperature</th>        <td>6700 K</td>             <th>Surface Temperature</th>    <td>22.85&deg;C</td></tr>
    <tr><th colspan='2'>Star Radius</th>        <td>0.0067 AU</td>          <th>Density</th>                <td>4.195 g/cc (0.76 &times; Earth)</td></tr>
    <tr><th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th>
                                                                            <th>Gravity</th>                <td>0.901 G</td></tr>
    <tr><th>I   </th>   <td>0.167 </td> <td>Asteroid Belt       </td>       <th>Escape Velocity</th>        <td>11,561 km/s</td></tr>
    <tr><th>II  </th>   <td>0.284 </td> <td>Std. Chthonian      </td>       <th>Diameter</th>               <td>15,122.574 km (1.186 &times; Earth)</td></tr>
    <tr><th>III </th>   <td>0.454 </td> <td>Std. Chthonian      </td>       <th></th>                       <td>9,396.482 mi</td></tr>
    <tr><th>IV  </th>   <td>0.818 </td> <td>Large Greenhouse    </td>       <th>Atmospheric Mass</th>       <td>1.3</td></tr>
    <tr><th>V   </th>   <td>1.368 </td> <td>Std. Greenhouse     </td>       <th>Atmospheric Pressure</th>   <td>1.171 atm (standard)</td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>tiny rock           </td>       <th>Axial Tilt</th>             <td>17&deg;</td></tr>
    <tr><th>VI  </th>   <td>1.947 </td> <td>Std. Garden         </td>       <th>Rotation Period</th>        <td>16.94 h</td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>2 tiny rock         </td>       <th>Local Day</th>              <td>16.954 h</td></tr>
    <tr><th>VII </th>   <td>2.703 </td> <td>Small Rock          </td>       <th>Surface Area (Total)</th>   <td>718,457,844.889 km<sup>2</sup></td></tr>
    <tr><th>VIII</th>   <td>4.255 </td> <td>Std. Ice            </td>       <th></th>                       <td>277,383,360.982 mi<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>2 tiny rock         </td>       <th>Surface Area (Land)</th>    <td>237,091,088.813 km<sup>2</sup></td></tr>
    <tr><th>IX  </th>   <td>7.659 </td> <td>Small Ice           </td>       <th></th>                       <td> 91,536,509.124 mi<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>tiny ice            </td>       <th>Surface Area (Water)</th>   <td>481,366,756.075 km<sup>2</sup></td></tr>
    <tr><th>X   </th>   <td>13.021</td> <td>Std. Ice            </td>       <th></th>                       <td>185,846,851.858 mi<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>1 moonlet           </td>       <th></th>                       <td></td></tr>
    <tr><th>XI  </th>   <td>20.833</td> <td>Tiny Ice            </td>       <th></th>                       <td></td></tr>
    <tr><th>XII </th>   <td>33.333</td> <td>Std. Hadean         </td>       <th></th>                       <td></td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>tiny ice            </td>       <th></th>                       <td></td></tr>
    <tr><th>XIII</th>   <td>53.333</td> <td>Asteroid Belt       </td>       <th></th>                       <td></td></tr>
</table></div>
<br clear='all' />



## Ingr

<div class='infobox infobox-centre'><table>
    <tr><th class='top-header' colspan='5'>Ingr</th></tr>
    <tr><th class='wide-header' colspan='3'>System Data</th>                <th class='wide-header' colspan='2'>Homeworld Data</th></tr>
    <tr><th colspan='2'>System Age</th>         <td>3.6 Gy</td>             <th>Orbital Radius</th>         <td>0.534 AU</td></tr>
    <tr><th colspan='2'>Number of Stars</th>    <td>1</td>                  <th>Orbital Period</th>         <td>164.708 Earth Days</td></tr>
    <tr><th class='wide-header' colspan='3'>Star Data</th>                  <th></th>                       <td>188.628 local days</td></tr>
    <tr><th colspan='2'>Mass</th>               <td>0.75 (K2V)</td>         <th></th>                       <td>0.451 &times; Earth</td></tr>
    <tr><th colspan='2'>Luminosity</th>         <td>0.267</td>              <th>Hydrographic Coverage</th>  <td>82%</td></tr>
    <tr><th colspan='2'>Temperature</th>        <td>4900 K</td>             <th>Surface Temperature</th>    <td>10.85&deg;C</td></tr>
    <tr><th colspan='2'>Star Radius</th>        <td>0.0033 AU</td>          <th>Density</th>                <td>5.023 g/cc (0.91 &times; Earth)</td></tr>
    <tr><th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th>
                                                                            <th>Gravity</th>                <td>1.032 G</td></tr>
    <tr><th>I</th>      <td>0.077 </td> <td>Tiny Rock</td>                  <th>Escape Velocity</th>        <td>12.101 km/s</td></tr>
    <tr><th>II</th>     <td>0.123 </td> <td>Asteroid Belt</td>              <th>Diameter</th>               <td>14,466.145 km (1.134 &times; Earth)</td></tr>
    <tr><th>III</th>    <td>0.209 </td> <td>Small Rock</td>                 <th></th>                       <td>8,988.607 mi</td></tr>
    <tr><th>IV</th>     <td>0.335 </td> <td>Std. Greenhouse</td>            <th>Atmospheric Mass</th>       <td>1.127</td></tr>
    <tr><th>V</th>      <td>0.536 </td> <td>Std. Garden</td>                <th>Atmospheric Pressure</th>   <td>1.163 atm (standard)</td></tr>
    <tr><th>VI</th>     <td>0.967 </td> <td>Small Rock</td>                 <th>Axial Tilt</th>             <td>33&deg;</td></tr>
    <tr><th></th>       <th>moons:</th> <td>2 moonlets</td>                 <th>Rotation Period</th>        <td>20.846 h</td></tr>
    <tr><th>VII</th>    <td>1.644 </td> <td>Std. Ice</td>                   <th>Local Day</th>              <td>20.957 h</td></tr>
    <tr><th></th>       <th>moon: </th> <td>tiny rock</td>                  <th>Surface Area (Total)</th>   <td>657,439,061.223 km<sup>2</sup></td></tr>
    <tr><th>VIII</th>   <td>2.466 </td> <td>Tiny Ice</td>                   <th></th>                       <td>253,825,130.786 mi<sup>2</sup></td></tr>
    <tr><th>IX</th>     <td>3.946 </td> <td>Small Ice</td>                  <th>Surface Area (Land)</th>    <td>118,339,031.020 km<sup>2</sup></td></tr>
    <tr><th>X</th>      <td>5.919 </td> <td>Small Ice</td>                  <th></th>                       <td> 45,688,523.541 mi<sup>2</sup></td></tr>
    <tr><th>XI</th>     <td>10.654</td> <td>Small Hadean</td>               <th>Surface Area (Water)</th>   <td>539,100,030.203 km<sup>2</sup></td></tr>
    <tr><th>XII</th>    <td>17.046</td> <td>Std. Hadean</td>                <th></th>                       <td>208,136,607.244 mi<sup>2</sup></td></tr>
    <tr><th></th>       <th>moons:</th> <td>2 moonlets</td>                 <th></th>                       <td></td></tr>


</table></div>
<br clear='all' />



## Isaguyat

<div class='infobox infobox-centre'><table>
    <tr><th class='top-header' colspan='5'>Isaguyat</th></tr>
    <tr><th class='wide-header' colspan='3'>System Data</th>                            <th class='wide-header' colspan='2'>Homeworld Data</th></tr>
    <tr><th colspan='2'>System Age</th>         <td>1.2 Gy</td>                         <th>Orbital Radius</th>         <td>1.967 AU</td></tr>
    <tr><th colspan='2'>Number of Stars</th>    <td>2</td>                              <th>Orbital Period</th>         <td>822.657 Earth Days</td></tr>
    <tr><th class='wide-header' colspan='3'>Star A/B Data</th>                          <th></th>                       <td>152.053 local days</td></tr>
    <tr><th colspan='2'>Mass</th>               <td>1.45 (F3V) / 0.6 (K6V)</td>         <th></th>                       <td>2.252 &times; Earth</td></tr>
    <tr><th colspan='2'>Luminosity</th>         <td>4.809 / 0.132</td>                  <th>Hydrographic Coverage</th>  <td>96%</td></tr>
    <tr><th colspan='2'>Temperature</th>        <td>6900 K / 4200 K</td>                <th>Surface Temperature</th>    <td>14.85&deg;C</td></tr>
    <tr><th colspan='2'>Star Radius</th>        <td>0.0071 AU / 0.0032 AU</td>          <th>Density</th>                <td>5.189 g/cc (0.94 &times; Earth)</td></tr>
    <tr><th colspan='2'>Seperation</th>         <td>Avg: 0.36 AU (0.25 - 0.47 AU)</td>  <th>Gravity</th>                <td>0.836 G</td></tr>
    <tr><th colspan='2'>Eccentricity</th>       <td>0.3</td>                            <th>Escape Velocity</th>        <td>9,645 km/s</td></tr>
    <tr><th colspan='2'>Orbital Period</th>     <td>55.15 d</td>                        <th>Diameter</th>               <td>11,344.698 km (0.889 &times; Earth)</td></tr>
    <tr><th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th>
                                                                                        <th></th>                       <td>7,049.081 mi</td></tr>
    <tr><th>I   </th>   <td>1.967 </td> <td>Std. Garden         </td>                   <th>Atmospheric Mass</th>       <td>1.0</td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>small rock          </td>                   <th>Atmospheric Pressure</th>   <td>0.836 atm (standard)</td></tr>
    <tr><th>II  </th>   <td>3.519 </td> <td>Small Rock          </td>                   <th>Axial Tilt</th>             <td>24&deg;</td></tr>
    <tr><th>III </th>   <td>6.335 </td> <td>Tiny Rock           </td>                   <th>Rotation Period</th>        <td>129 h (5.375 Earth days)</td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>1 moonlet           </td>                   <th>Local Day</th>              <td>129.848 h</td></tr>
    <tr><th>IV  </th>   <td>11.403</td> <td>Small Ice           </td>                   <th>Surface Area (Total)</th>   <td>404,329,791.191 km<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>2 moonlets          </td>                   <th></th>                       <td>156,104,296.478 mi<sup>2</sup></td></tr>
    <tr><th>V   </th>   <td>19.385</td> <td>Asteroid Belt       </td>                   <th>Surface Area (Land)</th>    <td> 16,173,191.648 km<sup>2</sup></td></tr>
    <tr><th>VI  </th>   <td>31.016</td> <td>Asteroid Belt       </td>                   <th></th>                       <td> 6,244,171.859 mi<sup>2</sup></td></tr>
    <tr><th>VII </th>   <td>52.727</td> <td>Tiny Ice            </td>                   <th>Surface Area (Water)</th>   <td>388,156,599.543 km<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>      </th> <td>                    </td>                   <th></th>                       <td>149,860,124.619 mi<sup>2</sup></td></tr>
</table></div>
<br clear='all' />



## Kzaknri

<div class='infobox infobox-centre'><table>
    <tr><th class='top-header' colspan='6'>Kzaknri</th></tr>
    <tr><th class='wide-header' colspan='3'>System Data</th>                <th class='wide-header' colspan='3'>Homeworld Data</th></tr>
    <tr><th colspan='2'>System Age</th>         <td>3.8 Gy</td>             <th colspan='2'>Orbital Radius</th>         <td>1.654 AU</td></tr>
    <tr><th colspan='2'>Number of Stars</th>    <td>2</td>                  <th colspan='2'>Orbital Period</th>         <td>681.191 Earth Days</td></tr>
    <tr><th class='wide-header' colspan='3'>Star A Data</th>                <th colspan='2'></th>                       <td>600.095 local days</td></tr>
    <tr><th colspan='2'>Mass</th>               <td>1.3 (F6V)</td>          <th colspan='2'></th>                       <td>1.865 &times; Earth</td></tr>
    <tr><th colspan='2'>Luminosity</th>         <td>3.66</td>              <th colspan='2'>Hydrographic Coverage</th>   <td>53%</td></tr>
    <tr><th colspan='2'>Temperature</th>        <td>6500 K</td>             <th colspan='2'>Surface Temperature</th>    <td>27.85&deg;C</td></tr>
    <tr><th colspan='2'>Star Radius</th>        <td>0.007 AU</td>          <th colspan='2'>Density</th>                 <td>7.182 g/cc (1.07 &times; Earth)</td></tr>
    <tr><th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th>
                                                                            <th colspan='2'>Gravity</th>                <td>1.20 G</td></tr>
    <tr><th>I   </th>   <td>0.14  </td> <td>Tiny Rock           </td>       <th colspan='2'>Escape Velocity</th>        <td>11.768 km/s</td></tr>
    <tr><th>II  </th>   <td>0.29  </td> <td>Asteroid Belt       </td>       <th colspan='2'>Diameter</th>               <td>11,765.719 km (0.922 &times; Earth)</td></tr>
    <tr><th>III </th>   <td>0.50  </td> <td>Asteroid Belt       </td>       <th colspan='2'></th>                       <td> 7,310.684 mi</td></tr>
    <tr><th>IV  </th>   <td>0.455 </td> <td>Med. Gas Giant      </td>       <th colspan='2'>Atmospheric Mass</th>       <td>0.9</td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>8 inner, 3 major, 5 outer, visible ring</td>
                                                                            <th colspan='2'>Atmospheric Pressure</th>   <td>1.080 atm (standard)</td></tr>
    <tr><th>V   </th>   <td>1.57  </td> <td>Std Garden          </td>       <th colspan='2'>Axial Tilt</th>             <td>10&deg;</td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>2 moonlets          </td>       <th colspan='2'>Rotation Period</th>        <td>27.198 h</td></tr>
    <tr><th>VI  </th>   <td>2.27  </td> <td>Med. Gas Giant      </td>       <th colspan='2'>Local Day</th>              <td>27.243 h</td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>5 inner, 3 major, 4 outer</td>  <th colspan='2'>Surface Area (Total)</th>   <td>434,897,381.505 km<sup>2</sup></td></tr>
    <tr><th>VII </th>   <td>3.63  </td> <td>Asteroid Belt       </td>       <th colspan='2'></th>                       <td>167,905,880.940 mi<sup>2</sup></td></tr>
    <tr><th>VIII</th>   <td>5.80  </td> <td>Med. Gas Giant      </td>       <th colspan='2'>Surface Area (Land)</th>    <td>204,401,769.307 km<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>9 inner, 3 major, 6 outer, visble ring</td>
                                                                            <th colspan='2'></th>                       <td>78,915,764.042 mi<sup>2</sup></td></tr>
    <tr><th>IX  </th>   <td>10.44 </td> <td>Med. Gas Giant      </td>       <th colspan='2'>Surface Area (Water)</th>   <td>230,495,612.198 km<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>7 inner, 6 major, 5 outer, visible ring</td>    <th colspan='2'></th>                       <td>88,990,116.898 mi<sup>2</sup></td></tr>
    <tr><th>X   </th>   <td>10.44 </td> <td>Small Gas Giant     </td>                       <th colspan='2'></th>                       <td></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>9 inner, 4 major, 4 outer, visible ring</td>    <th colspan='2'></th>                       <td></td></tr>
    <tr><th>XI  </th>   <td>10.44 </td> <td>Small Gas Giant     </td>                       <th colspan='2'></th>                       <td></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>8 inner, 4 major, 5 outer, visible ring</td>    <th colspan='2'></th>                       <td></td></tr>
    <tr><th>XII </th>   <td>10.44 </td> <td>Med. Gas Giant      </td>                       <th colspan='2'></th>                       <td></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>8 inner, 5 major, 6 outer, visible ring</td>    <th colspan='2'></th>                       <td></td></tr>
    <tr><th class='wide-header' colspan='3'>Star B Data</th>                        <th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th></tr>
    <tr><th colspan='2'>Mass</th>               <td>1.15 (G8V)</td>                 <th>I   </th>   <td>0.17  </td> <td>Tiny Rock           </td></tr>
    <tr><th colspan='2'>Luminosity</th>         <td>0.529</td>                      <th>II  </th>   <td>0.32  </td> <td>Small Rock          </td></tr>
    <tr><th colspan='2'>Temperature</th>        <td>5400 K</td>                     <th>III </th>   <td>0.54  </td> <td>Std. Greenhouse     </td></tr>
    <tr><th colspan='2'>Star Radius</th>        <td>0.0039 AU</td>                  <th>IV  </th>   <td>0.92  </td> <td>Std. Greenhouse     </td></tr>
    <tr><th colspan='2'>Seperation</th>         <td>Avg: 40 AU (16 - 68 AU)</td>    <th>    </th>   <th>moon: </th> <td>tiny rock           </td></tr>
    <tr><th colspan='2'>Eccentricity</th>       <td>0.7</td>                        <th>V   </th>   <td>1.56  </td> <td>Asteroid Belt       </td></tr>
    <tr><th colspan='2'>Orbital Period</th>     <td>194.03 y</td>                   <th>VI  </th>   <td>2.66  </td> <td>Small Rock          </td></tr>
    <tr><th colspan='2'></th>                   <td></td>                           <th>VII </th>   <td>4.54  </td> <td>Small Rock          </td></tr>
    <tr><th colspan='2'></th>                   <td></td>                           <th>    </th>   <th>moons:</th> <td>2 moonlets          </td></tr>
    <tr><th colspan='2'></th>                   <td></td>                           <th>VIII</th>   <td>7.24  </td> <td>Small Ice           </td></tr>
    <tr><th colspan='2'></th>                   <td></td>                           <th>IX  </th>   <td>12.30 </td> <td>Small Ice           </td></tr>
    <tr><th colspan='2'></th>                   <td></td>                           <th>X   </th>   <td>24.60 </td> <td>Small Hadean        </td></tr>
    <tr><th colspan='2'></th>                   <td></td>                           <th>    </th>   <th>moon: </th> <td>tiny ice           </td></tr>
</table></div>
<br clear='all' />



## Raesan Vie

<div class='infobox infobox-centre'><table>
    <tr><th class='top-header' colspan='6'>Raesan Vie</th></tr>
    <tr><th class='wide-header' colspan='3'>System Data</th>                <th class='wide-header' colspan='3'>Homeworld Data</th></tr>
    <tr><th colspan='2'>System Age</th>         <td>6.0 Gy</td>             <th colspan='2'>Orbital Radius</th>         <td>0.774 AU</td></tr>
    <tr><th colspan='2'>Number of Stars</th>    <td>3</td>                  <th colspan='2'>Orbital Period</th>         <td>269.852 Earth Days</td></tr>
    <tr><th class='wide-header' colspan='3'>Star A Data</th>                <th colspan='2'></th>                       <td>212.462 local days</td></tr>
    <tr><th colspan='2'>Mass</th>               <td>0.85 (G8V)</td>         <th colspan='2'></th>                       <td>0.739 &times; Earth</td></tr>
    <tr><th colspan='2'>Luminosity</th>         <td>0.529</td>              <th colspan='2'>Hydrographic Coverage</th>  <td>73%</td></tr>
    <tr><th colspan='2'>Temperature</th>        <td>5400 K</td>             <th colspan='2'>Surface Temperature</th>    <td>25.85&deg;C</td></tr>
    <tr><th colspan='2'>Star Radius</th>        <td>0.0039 AU</td>          <th colspan='2'>Density</th>                <td>5.906 g/cc (1.07 &times; Earth)</td></tr>
    <tr><th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th>
                                                                            <th colspan='2'>Gravity</th>                <td>0.636 G</td></tr>
    <tr><th>I   </th>   <td>0.099 </td> <td>Std. Chthonian      </td>       <th colspan='2'>Escape Velocity</th>        <td>6.878 km/s</td></tr>
    <tr><th>II  </th>   <td>0.158 </td> <td>Large Chthonian     </td>       <th colspan='2'>Diameter</th>               <td> 7,582.071 km (1.186 &times; Earth)</td></tr>
    <tr><th>III </th>   <td>0.284 </td> <td>Std. Greenhouse     </td>       <th colspan='2'></th>                       <td> 4,711.155 mi</td></tr>
    <tr><th>IV  </th>   <td>0.455 </td> <td>Tiny Rock           </td>       <th colspan='2'>Atmospheric Mass</th>       <td>1.63</td></tr>
    <tr><th>V   </th>   <td>0.774 </td> <td>Std. Garden         </td>       <th colspan='2'>Atmospheric Pressure</th>   <td>1.037 atm (standard)</td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>2 moonlets          </td>       <th colspan='2'>Axial Tilt</th>             <td>31&deg;</td></tr>
    <tr><th>VI  </th>   <td>1.307 </td> <td>Small Rock          </td>       <th colspan='2'>Rotation Period</th>        <td>30.34 h</td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>2 moonlets          </td>       <th colspan='2'>Local Day</th>              <td>30.483 h</td></tr>
    <tr><th>VII </th>   <td>1.961 </td> <td>Small Rock          </td>       <th colspan='2'>Surface Area (Total)</th>   <td>180,603,253.526 km<sup>2</sup></td></tr>
    <tr><th>VIII</th>   <td>3.333 </td> <td>Asteroid Belt       </td>       <th colspan='2'></th>                       <td> 69,727,594.769 mi<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>      </th> <td>2 tiny rock         </td>       <th colspan='2'>Surface Area (Land)</th>    <td> 48,762,878.452 km<sup>2</sup></td></tr>
    <tr><th>    </th>   <td>      </td> <td>                    </td>       <th colspan='2'></th>                       <td> 18,826,450.588 mi<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>      </th> <td>                    </td>       <th colspan='2'>Surface Area (Water)</th>   <td>131,840,375.074 km<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>      </th> <td>                    </td>       <th colspan='2'></th>                       <td> 50,901,144.182 mi<sup>2</sup></td></tr>
    <tr><th class='wide-header' colspan='3'>Star B Data</th>                <th class='wide-header' colspan='3'>Star C Data</th></tr>
    <tr><th colspan='2'>Mass</th>               <td>0.85 (G8V)</td>         <th colspan='2'>Mass</th>               <td>0.20 (M5V)</td></tr>
    <tr><th colspan='2'>Luminosity</th>         <td>0.529</td>              <th colspan='2'>Luminosity</th>         <td>0.0079</td></tr>
    <tr><th colspan='2'>Temperature</th>        <td>5400 K</td>             <th colspan='2'>Temperature</th>        <td>3200 K</td></tr>
    <tr><th colspan='2'>Star Radius</th>        <td>0.0039 AU</td>          <th colspan='2'>Star Radius</th>        <td>0.013 AU</td></tr>
    <tr><th colspan='2'>Seperation</th>         <td>Avg: 40 AU (16 - 68 AU)</td>    <th colspan='2'>Seperation</th>         <td>Avg: 469 AU (234.5 - 703.5 AU)</td></tr>
    <tr><th colspan='2'>Eccentricity</th>       <td>0.7</td>                        <th colspan='2'>Eccentricity</th>       <td>0.4</td></tr>
    <tr><th colspan='2'>Orbital Period</th>     <td>194.03 y</td>                   <th colspan='2'>Orbital Period</th>     <td>9,912.08 y</td></tr>
    <tr><th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th>
                <th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th></tr>
    <tr><th>I   </th>   <td>0.092 </td> <td>Std. Chthonian      </td>       <th colspan='3' style='text-align:center'>(contents undecided)</th></tr>
    <tr><th>II  </th>   <td>0.147 </td> <td>Std. Chthonian      </td>       <th></th><td></td><td></td></tr>
    <tr><th>III </th>   <td>0.235 </td> <td>Std. Greenhouse     </td>       <th></th><td></td><td></td></tr>
    <tr><th>IV  </th>   <td>0.399 </td> <td>Asteroid Belt       </td>       <th></th><td></td><td></td></tr>
    <tr><th>V   </th>   <td>0.678 </td> <td>Asteroid Belt       </td>       <th></th><td></td><td></td></tr>
    <tr><th>VI  </th>   <td>1.288 </td> <td>Small Rock          </td>       <th></th><td></td><td></td></tr>
    <tr><th>VII </th>   <td>2.319 </td> <td>Std. Ice            </td>       <th></th><td></td><td></td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>tiny rock           </td>       <th></th><td></td><td></td></tr>
    <tr><th>VIII</th>   <td>3.478 </td> <td>Small Ice           </td>       <th></th><td></td><td></td></tr>
</table></div>
<br clear='all' />



## Va-Katr

<div class='infobox infobox-centre'><table>
    <tr><th class='top-header' colspan='6'>Va Katr</th></tr>
    <tr><th class='wide-header' colspan='3'>System Data</th>                <th class='wide-header' colspan='3'>Homeworld Data</th></tr>
    <tr><th colspan='2'>System Age</th>         <td>7.0 Gy</td>             <th colspan='2'>Orbital Radius</th>         <td>0.809 AU</td></tr>
    <tr><th colspan='2'>Number of Stars</th>    <td>3</td>                  <th colspan='2'>Orbital Period</th>         <td>279.997 Earth Days</td></tr>
    <tr><th class='wide-header' colspan='3'>Star A Data</th>                <th colspan='2'></th>                       <td>300.072 local days</td></tr>
    <tr><th colspan='2'>Mass</th>               <td>0.9 (G6V)</td>          <th colspan='2'></th>                       <td>0.767 &times; Earth</td></tr>
    <tr><th colspan='2'>Luminosity</th>         <td>0.725</td>              <th colspan='2'>Hydrographic Coverage</th>  <td>68%</td></tr>
    <tr><th colspan='2'>Temperature</th>        <td>5500 K</td>             <th colspan='2'>Surface Temperature</th>    <td>12.85&deg;C</td></tr>
    <tr><th colspan='2'>Star Radius</th>        <td>0.0044 AU</td>          <th colspan='2'>Density</th>                <td>6.293 g/cc (1.14 &times; Earth)</td></tr>
    <tr><th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th>
                                                                            <th colspan='2'>Gravity</th>                <td>1.07 G</td></tr>
    <tr><th>I   </th>   <td>0.114 </td> <td>Asteroid Belt       </td>       <th colspan='2'>Escape Velocity</th>        <td>11,210 km/s</td></tr>
    <tr><th>II  </th>   <td>0.206 </td> <td>Small Rock          </td>       <th colspan='2'>Diameter</th>               <td>11,972.737 km (1.186 &times; Earth)</td></tr>
    <tr><th>III </th>   <td>0.393 </td> <td>Std. Greenhouse     </td>       <th colspan='2'></th>                       <td> 7,439.316 mi</td></tr>
    <tr><th>IV  </th>   <td>0.619 </td> <td>Small Rock          </td>       <th colspan='2'>Atmospheric Mass</th>       <td>0.87</td></tr>
    <tr><th>V   </th>   <td>0.912 </td> <td>Std. Garden         </td>       <th colspan='2'>Atmospheric Pressure</th>   <td>0.931 atm (standard)</td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>2 moonlets          </td>       <th colspan='2'>Axial Tilt</th>             <td>24&deg;</td></tr>
    <tr><th>VI  </th>   <td>1.821 </td> <td>Small Rock          </td>       <th colspan='2'>Rotation Period</th>        <td>22.34 h</td></tr>
    <tr><th>VII </th>   <td>3.097 </td> <td>Std. Rock           </td>       <th colspan='2'>Local Day</th>              <td>22.394 h</td></tr>
    <tr><th>    </th>   <td>moons:</td> <td>4 moonlets          </td>       <th colspan='2'>Surface Area (Total)</th>   <td>450,336,083.522 km<sup>2</sup></td></tr>
    <tr><th>VIII</th>   <td>4.956 </td> <td>Med. Gas Giant      </td>       <th colspan='2'></th>                       <td>173,866,479.860 mi<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>11 inner, 6 major, 4 outer, bright rings</td>
                                                                            <th colspan='2'>Surface Area (Land)</th>    <td>144,107,546.727 km<sup>2</sup></td></tr>
    <tr><th>IX  </th>   <td>8.425 </td> <td>Med. Gas Giant      </td>       <th colspan='2'></th>                       <td>55,637,273.555 mi<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>3 inner, 4 major, 4 outer</td>  <th colspan='2'>Surface Area (Water)</th>   <td>306,228,536.795 km<sup>2</sup></td></tr>
    <tr><th>X   </th>   <td>15.165</td> <td>Med. Gas Giant      </td>       <th colspan='2'></th>                       <td>118,229,206.305 mi<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>10 inner, 1 major, 2 outer, bright rings</td>
                                                                            <th colspan='2'></th><td></td></tr>
    <tr><th class='wide-header' colspan='3'>Star B Data</th>                <th class='wide-header' colspan='3'>Star C Data</th></tr>
    <tr><th colspan='2'>Mass</th>               <td>0.85 (G8V)</td>         <th colspan='2'>Mass</th>               <td>0.60 (K6V)</td></tr>
    <tr><th colspan='2'>Luminosity</th>         <td>0.558</td>              <th colspan='2'>Luminosity</th>         <td>0.142</td></tr>
    <tr><th colspan='2'>Temperature</th>        <td>5400 K</td>             <th colspan='2'>Temperature</th>        <td>4200 K</td></tr>
    <tr><th colspan='2'>Star Radius</th>        <td>0.0040 AU</td>          <th colspan='2'>Star Radius</th>        <td>0.0033 AU</td></tr>
    <tr><th colspan='2'>Seperation</th>         <td>Avg: 80 AU (72 - 88 AU)</td>    <th colspan='2'>Seperation</th>         <td>Avg: 300 AU (180 - 420 AU)</td></tr>
    <tr><th colspan='2'>Eccentricity</th>       <td>0.1</td>                        <th colspan='2'>Eccentricity</th>       <td>0.4</td></tr>
    <tr><th colspan='2'>Orbital Period</th>     <td>540.899 y</td>                  <th colspan='2'>Orbital Period</th>     <td>4,242.641 y</td></tr>
    <tr><th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th>
                <th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th></tr>
    <tr><th>I   </th>   <td>0.131 </td> <td>Small Rock          </td>       <th colspan='3' style='text-align:center'>(contents undecided)</th></tr>
    <tr><th>II  </th>   <td>0.250 </td> <td>Small Rock          </td>       <th></th><td></td><td></td></tr>
    <tr><th>III </th>   <td>0.400 </td> <td>Tiny Rock           </td>       <th></th><td></td><td></td></tr>
    <tr><th>IV  </th>   <td>0.641 </td> <td>Std. Garden         </td>       <th></th><td></td><td></td></tr>
    <tr><th>V   </th>   <td>1.091 </td> <td>Std. Ice            </td>       <th></th><td></td><td></td></tr>
    <tr><th>VI  </th>   <td>1.747 </td> <td>Std. Ice            </td>       <th></th><td></td><td></td></tr>
    <tr><th>VII </th>   <td>3.145 </td> <td>Std. Ice            </td>       <th></th><td></td><td></td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>tiny ice            </td>       <th></th><td></td><td></td></tr>
    <tr><th>VIII</th>   <td>5.032 </td> <td>Tiny Ice            </td>       <th></th><td></td><td></td></tr>
    <tr><th>IX  </th>   <td>8.556 </td> <td>Tiny Ice            </td>       <th></th><td></td><td></td></tr>
    <tr><th>X   </th>   <td>12.834</td> <td>Small Hadean        </td>       <th></th><td></td><td></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>3 moonlet           </td>       <th></th><td></td><td></td></tr>
    <tr><th>XI  </th>   <td>21.818</td> <td>Tiny Ice            </td>       <th></th><td></td><td></td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>1 moonlet           </td>       <th></th><td></td><td></td></tr>
</table></div>
<br clear='all' />



## Zifaelafojun

<div class='infobox infobox-centre'><table>
    <tr><th class='top-header' colspan='5'>Zifaelafojun</th></tr>
    <tr><th class='wide-header' colspan='3'>System Data</th>                <th class='wide-header' colspan='2'>Homeworld Data</th></tr>
    <tr><th colspan='2'>System Age</th>         <td>9.6 Gy</td>             <th>Orbital Radius</th>         <td>0.877 AU</td></tr>
    <tr><th colspan='2'>Number of Stars</th>    <td>1</td>                  <th>Orbital Period</th>         <td>325.420 Earth Days</td></tr>
    <tr><th class='wide-header' colspan='3'>Star Data</th>                  <th></th>                       <td>205.033 local days</td></tr>
    <tr><th colspan='2'>Mass</th>               <td>0.85 (G8V)</td>         <th></th>                       <td>0.891 &times; Earth</td></tr>
    <tr><th colspan='2'>Luminosity</th>         <td>0.631</td>              <th>Hydrographic Coverage</th>  <td>86%</td></tr>
    <tr><th colspan='2'>Temperature</th>        <td>5400 K</td>             <th>Surface Temperature</th>    <td>11.85&deg;C</td></tr>
    <tr><th colspan='2'>Star Radius</th>        <td>0.0042 AU</td>          <th>Density</th>                <td>4.968 g/cc (0.9 &times; Earth)</td></tr>
    <tr><th class='wide-header'>Orbit</th><th class='wide-header'> Radius</th><th class='wide-header'>Contents</th>
                                                                            <th>Gravity</th>                <td>0.78 G</td></tr>
    <tr><th>I   </th>   <td>0.10  </td> <td>Std. Chthonian      </td>       <th>Escape Velocity</th>        <td>9.197 km/s</td></tr>
    <tr><th>II  </th>   <td>0.16  </td> <td>Std. Greenhouse     </td>       <th>Diameter</th>               <td>11,055.200 km (0.867 &times; Earth)</td></tr>
    <tr><th>III </th>   <td>0.27  </td> <td>Small Rock          </td>       <th></th>                       <td>6,869.200 mi</td></tr>
    <tr><th>IV  </th>   <td>0.39  </td> <td>Small Rock          </td>       <th>Atmospheric Mass</th>       <td>1.4</td></tr>
    <tr><th>V   </th>   <td>0.62  </td> <td>Small Rock          </td>       <th>Atmospheric Pressure</th>   <td>1.092 atm (standard)</td></tr>
    <tr><th>VI  </th>   <td>0.88  </td> <td>Std. Garden         </td>       <th>Axial Tilt</th>             <td>18&deg;</td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>2 moonlets          </td>       <th>Rotation Period</th>        <td>37.907 h</td></tr>
    <tr><th>VII </th>   <td>1.31  </td> <td>Small Rock          </td>       <th>Local Day</th>              <td>38.092 h</td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>1 moonlet           </td>       <th>Surface Area (Total)</th>   <td>383,957,433.761 km<sup>2</sup></td></tr>
    <tr><th>VIII</th>   <td>1.86  </td> <td>Std. Ice            </td>       <th></th>                       <td>148,238,903.936 mi<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>1 moonlet           </td>       <th>Surface Area (Land)</th>    <td> 53,754,040.727 km<sup>2</sup></td></tr>
    <tr><th>IX  </th>   <td>2.98  </td> <td>Std. Ice            </td>       <th></th>                       <td> 20,753,446.551 mi<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>2 tiny rock         </td>       <th>Surface Area (Water)</th>   <td>330,203,393.035 km<sup>2</sup></td></tr>
    <tr><th>X   </th>   <td>5.06  </td> <td>Std. Ice            </td>       <th></th>                       <td>127,485,457.385 mi<sup>2</sup></td></tr>
    <tr><th>    </th>   <th>moons:</th> <td>3 moonlets          </td>       <th></th>                       <td></td></tr>
    <tr><th>XI  </th>   <td>8.60  </td> <td>Std. Ice            </td>       <th></th>                       <td></td></tr>
    <tr><th>    </th>   <th>moon: </th> <td>tiny ice            </td>       <th></th>                       <td></td></tr>
    <tr><th>XII </th>   <td>16.35 </td> <td>Tiny Ice            </td>       <th></th>                       <td></td></tr>
    <tr><th>XIII</th>   <td>26.15 </td> <td>Small Hadean        </td>       <th></th>                       <td></td></tr>
</table></div>
<br clear='all' />


## System 1

<div class='infobox infobox-right'><table>
    <tr><th class='top-header' colspan='5'>System 1</th></tr>
    <tr><th class='wide-header' colspan='3'>System Data</th>        <th class='wide-header' colspan='2'>Homeworld Data</th></tr>
    <tr><th colspan='2'>System Age</th>         <td>2.5 Gy</td>     <th>Orbital Radius</th>         <td>0.934 AU</td></tr>
    <tr><th colspan='2'>Number of Stars</th>    <td>1</td>          <th>Orbital Period</th>         <td>347.590 Earth Days</td></tr>
    <tr><th class='wide-header' colspan='3'>Star Data</th>          <th></th>                       <td>348.590 local days</td></tr>
    <tr><th colspan='2'>Type</th>               <td>G6V</td>        <th>Hydrographic Coverage</th>  <td>64%</td></tr>
    <tr><th colspan='2'>Luminosity</th>         <td>0.548</td>      <th>Surface Temperature</th>    <td>12.85&deg;C</td></tr>
    <tr><th colspan='2'>Temperature</th>        <td>5500 K</td>     <th>Density</th>                <td>4.913 g/cc (0.89 &times; Earth)</td></tr>
    <tr><th colspan='2'>Star Radius</th>        <td>0.0038 AU</td>  <th>Gravity</th>                <td>0.721 G</td></tr>
    <tr><th class='wide-header' >Orbit</th><th class='wide-header' >Radius</th><th class='wide-header' >Contents</th>
                                                                    <th>Escape Velocity</th>        <td>8.549 km/s</td></tr>
    <tr><th>I</th>      <td>0.237</td>  <td></td>                   <th>Diameter</th>               <td>10,333.793 km (0.810 &times; Earth)</td></tr>
    <tr><th>II</th>     <td>0.450</td>  <td></td>                   <th></th>                       <td>6,420.951 mi</td></tr>
    <tr><th>III</th>    <td>0.655</td>  <td></td>                   <th>Atmospheric Mass</th>       <td>1.6</td></tr>
    <tr><th>IV</th>     <td>0.934</td>  <td>Std. Garden</td>        <th>Atmospheric Pressure</th>   <td>1.154 atm (standard)</td></tr>
    <tr><th>V</th>      <td>1.710</td>  <td></td>                   <th>Rotation Period</th>        <td>-24 h</td></tr>
    <tr><th>VI</th>     <td>2.736</td>  <td></td>                   <th>Local Day</th>              <td>23.931 h</td></tr>
    <tr><th>VII</th>    <td>4.651</td>  <td></td>                   <th>Surface Area (Total)</th>   <td>335,482,144.109 km<sup>2</sup></td></tr>
    <tr><th>VIII</th>   <td>6.977</td>  <td></td>                   <th></th>                       <td>129,523,486.095 mi<sup>2</sup></td></tr>
    <tr><th>IX</th>     <td>10.466</td> <td></td>                   <th>Surface Area (Land)</th>    <td>120,773,571.879 km<sup>2</sup></td></tr>
    <tr><th>X</th>      <td>17.792</td> <td></td>                   <th></th>                       <td>46,628,454.994 mi<sup>2</sup></td></tr>
    <tr><th>XI</th>     <td>30.246</td> <td></td>                   <th>Surface Area (Water)</th>   <td>214,708,570.230 km<sup>2</sup></td></tr>
    <tr><th></th>       <td></td>       <td></td>                   <th></th>                       <td>82,895,031.101 mi<sup>2</sup></td></tr>
</table></div>
