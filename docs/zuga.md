---
category: animal
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Zuga</th></tr>
<tr><th>Homeworld:</th><td><a href='kzaknri.html'>Kzaknri</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>1' (0.3 m)</td></tr>
<!-- <tr><th>Avg. Weight:</th><td>127 lbs (58 kg)</td></tr> -->
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Clutch size:</th><td>6</td></tr>
<tr><th>Gestation:</th><td>2 weeks</td></tr>
<tr><th>Incubation:</th><td>3 months</td></tr>
<tr><th>Maturity:</th><td>1 year</td></tr>
<tr><th>Lifespan:</th><td>6 years</td></tr>
</table></div>

Zuga (Nraezen: <bdo class='nexusFont lang_nr'>&#xe008;&#xe003;</bdo>) are small pseudo-reptilian omnivores. Both they and their eggs serve as a staple of the [sakin](sakin.md) diet. Zuga are known for their tendancy to try eating almost anything, though given a choice, most prefer a herbivorous diet.

Although originally kept by the nomadic population of [Razaz Nudar](razaznudar.md), they have since been brought to nearly every part of [Kzaknri](kzaknri.md). In many areas they have become an invasive species and most larger cities have developed feral populations. Attempts to combat this tend to focus on encouraging poor populations to hunt the animals for food.

Zuga typically produce eggs in clutches of six and can produce multiple clutches each year. No parental care is provided and most zuga hatchlings do not reach adulthood.

<figure class='center' style='width:500px'>
    <img src='/art/species/zuga.png' width='500' height='454'>
    <figcaption>
    Zuga.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>
