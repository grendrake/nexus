---
category: guidelines
notoc: true
---

While the requirements of every species are different and every culture has its own approach to clothing, I do have a set of general guidelines I follow when designing clothes for my species. That said, these guidelines are mainly aimed at common, day-to-day clothing and not highly formal or specialized garments. The guidelines are also aimed at species which have developed their own clothing designs rather than those adapting the clothing of other species. In all cases, these are general guidelines and special circumstances may apply to specific species.

- Individuals should be able to put on and remove their clothing without assistance.
- Individuals should be able to move their clothing sufficiently that they can use the bathroom unassisted and reasonably quickly.
- Clothing should not significantly restrict the movement of the individual wearing it. That is, people should still be able to walk, flying species should still be able to fly, etc.
- It is not required that all clothing designs cover any specific region of the body, but for practical reasons, clothing should ideally cover anything that would otherwise require pictures to be adults-only.