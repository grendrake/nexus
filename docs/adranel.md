---
category: sophant
world: kloip praks
parent_name: Kloip Praks
parent_addr: kloippraks
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Adranel</th></tr>
<tr><th>Homeworld:</th><td>Kloip Praks</td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>0.1 m (0‘4”)</td></tr>
<!-- <tr><th>Avg. Weight:</th><td>96 lbs (43 kg)</td></tr> -->
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>6</td></tr>
<tr><th>Gestation:</th><td>1 week</td></tr>
<tr><th>Incubation:</th><td>4 months</td></tr>
<tr><th>Maturity:</th><td>14 years</td></tr>
<tr><th>Lifespan:</th><td>64 years</td></tr>
</table></div>


## Description

<!-- {% figure /art/species/zaeren.png | 230 | 390 | right | Male zaeren. | | spelunker %} -->


## Diet


## Reproduction
