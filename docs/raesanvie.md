---
category: world
world: raesan vie
notoc: true
---


<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Raesan Vie</th></tr>
<tr><td colspan='2' class='globe'><img width='302' height='302' src='/art/globe/unknown.svg'></td></tr>
<tr><th>Star</th>                   <td>G8V</td></tr>
<tr><th>Year Length:</th>           <td>212.46 local days<br />269.85 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>299 K (25.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>0.64 G</td></tr>
<tr><th>Day Length:</th>            <td>30.48 h</td></tr>
<tr><th>Moons</th>                  <td>None</td></tr>
<tr><td colspan='2' class='wide-data'><a href='systems.html#raesan-vie'>Raesan Vie System</a></td></tr>
</table></div>

Raesan Vie is the homeworld of the [asuni](asuni.md); it orbits around a G8 star. Like the [homeworld of the sekar](isaguyat.md), most of the world is unexplored and unknown, at least by non-asuni (whether the asuni near the gate know what the rest of the world is like is unknown). Unlike the sekarian homeworld, Raesan Vie is hot, but not an otherwise unpleasant place.

All of the known area of Raesan Vie consists of dense jungle and tropical conditions. Heavy rain is frequent, often punching its way through multiple layers of jungle canopy to find its way to the surface. This jungle grows extremely fast and constant effort is required in order to keep an area clear. The asuni themselves rarely bother clearing areas and simply deal with the thick trees growing through the middle of their encampments.

This isn't the most unusual trait of the jungle, however. That would be the way things have a tendency to disappear. Entire expeditions, including one from [Razaz Nudar](razaznudar.md) that made use of fire to clear a path, have disappeared with a trace. And not just some, either: all expeditions without an asuni have have disappeared. Unmanned drones also disappear, their data-streams disappearing with them. No wreckage or litter from any of these has ever been recovered. Bringing these events up with the asuni results in apathy and claims of ignorance.

While it is likely that many cultures exist across Raesan Vie, the nature of travel has resulted in only one being contacted.
As a result, this [culture](raesanvie_culture.md) is often referred to as if it were the only one.


## Landmarks

Despite it's low and non-technological population, there are a few places of note that asuni guides have led people to.

### Stone of Change

The Stone of Change is located in the jungle at a distance of about 20 km (12 miles) from the gate area.
Between the rapid growth of the jungle and the low frequency of travel to the stone, the trip is typically made through dense jungle.
Despite knowledge of the distance and bearing of the stone from the gate, at least one local guide is always brought along for the trip.

Trips to the stone are arranged through the *Stone Department* of the *Raesan Vie Tourism Company*.
The Stone Department operates out of a small office in [Serkm Hub](serkmhub.md) whose only employees are a small family of au.
Between one and four trips are arranged every Earth year, each consisting of eight to twelve individuals wanting to experience the stone's effects.
Potential tourists are screened for health issues to ensure that they can make the journey successfully.
When forming a tourist group, the Stone Department strives to make the group as diverse as possible, though they are limited to the pool of people interested in a random species and sex change.

The Stone of Change itself is a large, but unassuming boulder located in the middle of a clearing in the jungle.
It is partially buried with its highest point extending about two metres above the ground.
The average width of the stone is two and a half metres.
The stone has the appearance of grey granite, but researchers have been unable to obtain samples for study.

What attracts attention to the stone is not its appearance, however, but the strange effect that occurs around it every night roughly halfway between sunset and sunrise.
This effect affects every sophant within about fifty metres of the stone itself.
If there are not at least two sophants of different species in the area of effect, the stone will not activate.
At this point, the stone will cause all individuals within its range to fall into a deep sleep and then performs a form of body swap.

Each person in the area of effect is matched to a 'donor' of a different species.
These matches are not symmetric; if person A is matched to person B, person B is not necessarily matched to person A.
While the resulting distribution of species is typically a rough match for the initial distribution, the requirement for matches to be of a different species can skew this.
As an example, if ten humans and one ravon were effected, the result would be ten ravon and one human.
The individual's body is then painlessly and rapidly rebuilt using the DNA of the person they were matched to, resulting in a new body of a new species of young adult age.
This rebuilding process takes a few seconds to occur and has been recorded on multiple occasions.
Those who have viewed the recordings typically describe the process as horrific looking and often become ill from the sight.
After this, the individual falls into a natural sleep and will awaken naturally some time later.

The effect of the Stone of Change is not a 'true' body swap; the new body is a fresh creation and will not have scars or non-genetic health conditions.
The individual will wake up in the same location that they were when they fell asleep, often even the same position if their anatomy has not changed too much.
Although the individual will now be a genetic match for their generic donor, they will often have a visibly distinct appearance as their new body will not have been affected by any hardships, injury, or medical issues.
When multiple individuals of the donor's species are present, the stone will sometimes incorporate small amounts of other individual's DNA so that the new body is not a true clone.
As the individual's new body is made with the donor's DNA, it will always be the physical sex of the donor, regardless of the original sex of the individual.
Likewise, they will always be of a healthy weight for their new species, even if their donor was significantly over or underweight.

The way the Stone of Change creates new bodies for those affected means that it can also function as an effective cure for almost any health condition.
It is typically only used this way for major conditions, however, due to its primary effect of changing one's species and because those who would make use of it need to be able to make the trip through the jungle.


### Heart of the World

The "Heart of the World" as the asuni call it is an interesting place scientifically, though not so much to the general public. The Heart of the World is a large, brick-lined hole that leads deep into the world. It is circular and about ten feet across, but it has no other notable characteristics. It has been said to resemble a large well whose top was built level to the surface.

Learning more about it has proved to be extraordinarily difficult as anything that enters the Heart or the space above it instantly dies. Even electronic equipment tends to wear out at a vastly increased pace. In cases where bodies are recovered, there is no nothing about the bodies that would suggest why they died.

The asuni believe that the Heart is where the spirits of the dead go to move on to their next life. The reason it kills everything is because the spirit animating the body has left to go on to the next world thus leaving nothing to keep the body going. In the absence of other information and given the asuni's unique relationship with the dead, many people choose to believe this.


