---
category: nation
world: zifaelafojun
parent_name: Zifaelafojun
parent_addr: zifaelafojun
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Pasha Za<br>Fojun Fae</th></tr>
<tr><th>Capital:</th><td>Breaker Halls</td></tr>
<tr><th>Language:</th><td><a href='jowvon.html'>Jowvon</a></td></tr>
<tr><th>Demonym:</th><td>Breaker</td></tr>
<tr><th>Area:</th><td>1.6 million km<sup>2</sup></td></tr>
<tr><th>Population:</th><td>0.7 million</td></tr>
<tr><th>Density:</th><td>0.44 pop/km<sup>2</sup></td></tr>
</table></div>

The semi-independent nation of Pasha Za Fojun Fae (meaning "the land of the broken") is properly a self-governing territory of [Menota](menota.md). The land in Pasha Za Fojun Fae is ill-suited to agriculture, though the inhabitants have made the most of what's there and supplemented their diet with whatever was available; seafood became common and most of the current population lives near the coastal areas.

Most of the population consists of "undesirables" from Menota and being exiled (and often ejected from the Church of Vaere) to the undesirable area helped drive a wedge between the population and their former culture. Between this and the often already rebellious or liberal attitudes of the exiles, Pasha Za Fojun Fae developed a distinctively different culture than Menota.

Not only was the gate complex located in Pasha Za Fojun Fae, but most of the technology that had been forgotten at the fall of Warrothserkm remained as well. Although it was believed that the gate would never open again, and early attempts to reopen it supported this, a fascination with what was left behind developed. Very little of what was left still worked at all, but there was enough to engender the exiles with a sense of inquiry and investigation that founded the now prominent scientific organizations. What time the inhabitants did not spend on the necessities of survival, they spend studying this technology.

When the gate finally did open, Menota was happy to allow Pasha Za Fojun Fae to act as the public face of the world; it was as contact was being reestablished that the name of the nation was misunderstood as being "the Land of the Breakers", a name the inhabitants encouraged.


## Differences and Similarities from Menota

Although the population of Pasha Za Fojun Fae is almost entirely made up people who left Menota, there is a strong cultural divide between the two nations. Not only was the original population made up of people who held strong views different from the popular view in Menota, but they were not treated well by their former nation.

Religion is far less formalized, with publicly declared agnostics and atheists being accepted. Those seeking the touch of the divine in their lives often turn to the Association of Vaere, not banned in Pasha Za Fojun Fae like it is in Menota, or to any number of small home-grown efforts to return to the worship of other, half-forgotten gods.

Homes are built closer together and many more buildings dedicated exclusively to the operation of some kind of business than in Menota; there are quite a few areas of Pasha Za Fojun Fae that resemble would foreigners would describe as a city, though these remain loose, sprawling places.

### Clans

Perhaps the biggest difference between Menota and Pasha Za Fojun Fae is how the latter removed distinctions between social classes and clans. The original population, in their struggle to survive, had to cooperate, whether noble or peasant and regardless of clan. Even now that survival is less of a concern, the population of Pasha Za Fojun Fae is far more egalitarian than Menota.

Clan affiliation still exists, but is much less dramatic. To the consternation of those in Menota, intermarriage between clans is permitted; each member retains the clan affiliation they were born with even after marriage and children inherit the clan membership of their same-sex parent. This even extends to those of mixed or unknown clan, though in those cases children will inherit the clan of the parent with a known, single clan regardless of sex. If no such parent exists, they'll be known as unknown clan as well, though this is far less limiting than it would be in Menota.

### Technology

Pasha Za Fojun Fae has embraced technology to a much greater degree than Menota has. There are a few reasons for this, including the smaller population, that population living closer together, and being closer to the gate (making importing goods slightly easier).

A thriving IT industry exists; software developers from Pasha Za Fojun Fae are stereotyped as being significantly more competent and software produced there has a string focus on maintaining its users privacy. That said, most of the software is written to run on custom-designed systems; the parts for these systems are ordered from a wide range of suppliers and assembled inside of Pasha Za Fojun Fae.
The operating system designed for these systems was intended for domestic use only and can only be operated using the Jowvon language. That said, the reputation for privacy has resulted in some such systems being exported.

Unlike in Menota, a cross-national computer network exists, supported by a array of wire laid between the different sites, similar to the Internet on Earth. The supporting infrastructure is significantly different, however; the national network's connections are more fragile and the protocols used are designed to maintain privacy first and foremost.


## Tourism

The homes and businesses of Pasha Za Fojun Fae are primarily constructed with the native ravon in mind; ceilings are typically low and many areas are inaccessible to those who cannot fly. Most of the population is leery of foreigners as well; Pasha Za Fojun Fae and Menota have often suffered at the hands of foreigners. Despite all this, there is a thriving tourism industry in Pasha Za Fojun Fae.

The tourism industry is almost entirely accountable to one fact: most of [Zifaelafojun](zifaelafojun.md) is uninhibited, making it one of the few places across multiple worlds to have large expanses of genuinely untouched wilderness; even the main alternative, [Raesan Vie](raesanvie.md), is only untouched by modern life. Combine this with a generally not unpleasant climate and widely tolerable gravity and atmospheric pressure and the result is a surprisingly popular tourist destination, even if the tourists do have to hunch over to get there.

There have been some proposals to open actual resorts, but initial efforts in this direction have been far less profitable; the resort industry has a lot more competition than the untouched-wilderness industry.


## Government

Day-to-day governance and dealing with foreign affairs is almost always handled by the Breaker Council. Much of the business is handled by only a few members and often treated in a very curt fashion as the Breaker Council also serves as the primary body of scientific research and it is this second function that most of the members are actually passionate about.

The Breaker Council is typically in residence at the Breaker Halls. The Halls serve as much as a centre of research and university as they do a government and have most of the foibles of both kinds of organizations.

Law is often based around convenience to rich and powerful scientists and researchers rather than the general population; Pasha Za Fojun Fae is one of the few places were general experimentation on sophants is publicly allowed without strict restrictions. Interpersonal interactions are far less regulated than Menota, though laws do exist to ensure the safety and stability of the general population.

In theory, Pasha Za Fojun Fae is ultimately governed by the same bodies as Menota itself, excepting only the Church of Vaere which withdrew its presence because of the number of non-members. In practice, little government is provided. The Council of Salmae is generally disinterested, making them even less effective at governing Pasha Za Fojun Fae than they are Menota, and the Council of Fifty is more concerned with building their own wealth and keeping Menota functional.

The Council of Clans does actually make an effort to provide governance, but rather than doing so directly, they treat the inhabitants of Pasha Za Fojun Fae as a fourteenth clan, the "broken" clan.
