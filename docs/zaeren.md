---
category: sophant
world: interworld nexus
parent_name: Interworld Nexus
parent_addr: overview
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Zaeren</th></tr>
<tr><th>Homeworld:</th><td>None</td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>3'5 (1 m)</td></tr>
<tr><th>Avg. Weight:</th><td>96 lbs (43 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>3</td></tr>
<tr><th>Gestation:</th><td>7 months</td></tr>
<tr><th>Weaned:</th><td>1 years, 6 months</td></tr>
<tr><th>Maturity:</th><td>12 years</td></tr>
<tr><th>Lifespan:</th><td>65 years</td></tr>
</table></div>

The zaeren are a species that has some visual similarity to the rats of Earth as well as having a lifestyle and role that is often associated with the vermin of many worlds. They are scavengers, eking out a living on whatever resources they can find. While the Nexus itself provides enough food for survival, most individuals would prefer a more varied diet and find it useful to make use of other bits of technology that they are able to obtain.

They are extremely social, living and acting in large groups. Solitary zaeren exhibit signs of nervousness and distraction; if one encounters solitary zaeren that do not, it is far more likely that there are more zaeren out of sight than that the individual has a solitary nature. Zaeren cultures strongly typify the sentiment that the needs of the many outweigh the needs of the one; individual zaeren will not hesitate to sacrifice their lives for the good of their social group.

## Description

<figure class='right' style='width:230px'>
    <img src='/art/species/zaeren.png' width='230' height='390'>
    <figcaption>
    Male zaeren.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

Zaeren are a short, ratlike species. Their bodies are covered in a later of fur, save for their hands, feet, and their tail. The colour of their fur is typically a shade of gray with a few individuals with a pure white or black colouration.

## Diet

The zaeren are omnivores with a very broad diet. Although many zaeren eat a variety of low-quality and partially spoiled foods, this is a matter of availability rather than preference.

## Reproduction

## Native Lands

The zaeren do not have a known homeworld and were already present when [Warrothserkm](warrothserkm.md) first arrived.
While their oral histories suggest that had one once, it has been estimated as having been thousands of years in the past at a minimum.
In the present, zaeren have had a section of the wilderness set aside for their use and can also be found throughout [Serkm Hub](serkmhub.md).
They have even formed colonies on other species homeworlds, often without the native sophants consent or even knowledge.
All worlds that were a part of Warrothserkm are known to have colonies of zaeren.

Although the Nexus supplies everything the zaeren need to survive, it is also entirely lacking in raw materials.
As a result, zaeren culture prizes the ability to scavenge supplies and materials as well as reuse and jury rig equipment that is not entirely functional.

