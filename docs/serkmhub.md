---
category: nation
world: interworld nexus
parent_name: Interworld Nexus
parent_addr: overview
---

Serkm Hub serves as a common area for several worlds to congregate and interact.
It is governed by the *Interworld Governmental Council* and serves as a cross-world marketplace.
The hub holds itself independant from any specific world and attempts to control it are not viewed favourably.
While many worlds would love to have control of Serkm Hub, none are willing to relinquish that control to another world.
Because of its position between worlds, it has very mixed demographics and a wide range of sapients living there.

A common language, currency, and units of measure have been settled on for interworld trade; these are known as the [common system](#common-system).
Disputes between different worlds are typically handled by the [Interworld Governmental Council](#governmental-council).


## History

- originally formed by those stranded when gates fell at end of Warrothserkm
- population was not sustainable and many believed that gradual extinction was inevitable
- culture shifted to be more cross-species integrated and technology slowly regressed as no manufacturing or resources were available
    - wilderness meant food was not an issue
- when gates reopened, the diminished population invited newly discovered outsiders to join the population
- uneasy integration of new immigrants with existing culture


## Population

After the fall of Warrothserkm, the population of Serkm Hub was primarily made up of au, sakin, and drius, with a scattering of ravon.
These the most common species involved in interplanetary travel; many modern species, even well-known ones, would not be contacted until after the gate network was restored.
Once contact was restored or initially established they remained the most populous species thanks in part to their initial lead in numbers as well as having had plenty of time to build the hub to serve their needs.
When Earth made contact with Serkm Hub, humanity was fairly successful due to having a similar level of wealth and technology to that which was already present, though they remain one of the least common "common" sophants.

The modern population of Serkm Hub is maintained primarily by immigration.
Due to the high cost of living, most immigrants are either wealthy or sponsored by a person or company with significant financial resources.
As a result, the population mostly consists of adults.
Due to the wide range of sophants living in Serkm Hub, many relationships that form there are cross-species.
As many as 50% of long-term relationships in the hub are cross-species, though same-species relationships often formed before moving to the hub.

Between the low number of children and the large variety of sophants, most children's playmates are of different species.
While there are a few businesses involved in childcare (daycares and the like), childhood education is considered the responsibility of the parent.
The differing standards between worlds and cultures, as well as the different needs of different species, resulted in the Interworld Governmental Council deciding they did not want to be involved.
Most education is provided by either private instructors or by the child's parents.

### Border Controls and Customs

- borders are pretty open
- allow those from nearly anywhere entry
- only known criminals and exiles are refused entry

**Arriving**

- immigrants and first time visitors must register with customs services
    - registers [identity](overview.md#identity) to central database
        - portable ID document provided for easy reference
    - all in hub are expected to have valid ID at all times
    - can take multiple forms:
        - bracelet (doubles as watch)
        - necklace/collar
        - implanted RFID chip
            - implantation location is standardized
    - RFID
        - moderate cost
        - implantation is permanent
    - necklace/collar/bracelet may be rented or purchased
        - rented
            - small fee plus large deposit
            - get deposit back if returned and functional
        - purchase
            - expensive
            - generally discouraged as can be lost/stolen


## Land Usage

<figure style='text-align:right;margin-left:auto;margin-right:auto'>
<table>
<tr><th style='text-align:left'>Region</th>     <th colspan='2' style='text-align:center'>Width</th> <th>Area</th>       <th>Pop. Dens.</th>     <th>Population</th></tr>
<tr style='font-size:80%'><th></th>             <th>Nalwen</th> <th>km</th>                         <th>(sq. km.)</th>  <th>(pop/sq. km.)</th>  <th></th></tr>
<tr><td style='text-align:left'>Urban</td>      <td>70,000</td> <td>61.72</td>                      <td>3,000</td>      <td>2,388.1</td>        <td>7,140,000</td></tr>
<tr class='subrow'><td style='text-align:left' colspan='3'>Section (12)</td>                        <td>200</td>        <td>2,388.1</td>        <td>600,000</td></tr>
<tr><td style='text-align:left'>Wilderness</td> <td>29,000</td> <td>25.57</td>                      <td>3,000</td>      <td>170.6</td>          <td>430,000</td></tr>
<tr class='subrow'><td style='text-align:left' colspan='3'>Section (5)</td>                         <td>500</td>        <td>170.6</td>          <td>90,000</td></tr>
<tr class='subrow'><td style='text-align:left' colspan='3'>Zaeren Home</td>                         <td>500</td>        <td>4,267.6</td>        <td>2,130,000</td></tr>
<tr><td style='text-align:left'>Entry</td>      <td>1,000</td>  <td>0.88</td>                       <td>100</td>        <td>300</td>            <td>40,000</td></tr>
<tr class='subrow'><td style='text-align:left' colspan='3'>Gate Zone (84)</td>                      <td>1.5</td>        <td>3.57</td>           <td>5</td></tr>
<tr class='totalrow'>
    <td style='text-align:left'>Total</td>      <td>100,000</td><td>88.17</td>                      <td>6,100</td>      <td></td>               <td>8,200,000</td></tr>
</table>
<figcaption style='text-align:center'>Total Area of Serkm Hub Regions.<br><b>Width</b> measures the distance from the inner edge to the outer edge of the region; in the case of the urban region, it measures from the centre to the outer edge.</figcaption>
</figure>


Serkm zone is divided into three sections.
Seperating each section is a major road going in a circle around the zone.

The outermost section the *Entry Area* and contains all the gates as well as roads leading between the gates and the major road.
This section contains the 1,000 [nalwen](#common-system) (1.13 km) of land closest to the hub border and is mostly reserved for customs stations and the like.

The innermost section is the *Urban Area*.
This contains the center of the hub and extends out to a radius of 70,000 nalwen (61.72 km).
This is where the majority of the population lives and nearly all business and personal interactions occurs.
Twelve roads lead from the center of the hub out to the major road that borders the area.

Between these is the wilderness area, left untouched by construction or civilization.
This area is 29,000 nalwen (25.57 km) thick.
The main purpose of this area is to provide a naturalistic border to the urban area as well as to provide park space.
While Serkm Hub law applies to those in this area, it is not actively patrolled and thus often popular with those conducting unlawful business.
Six roads evenly divide the wilderness area and connect the outer Entry Area to the inner Urban Area.
Although the population of this area is given in the table above, it is not known in-universe.

One of the divisions of the wilderness area has been granted to the zaeren for their exclusive use; this section is not included in the demographics of the wilderness area as a whole.
Like the wilderness area, the population of this area is not known in-universe (except to the zaeren themselves); it is often severely underestimated by those who have not been there.

<figure class='center' style='width:600px'>
    <a href='/art/plans/serkmhub.svg'><img src='/art/plans/serkmhub.svg' width='600' height='421'></a>
    <figcaption>
    Map of Serkm Hub.<br>
    Art © <a href='https://www.grenslair.com/'>Gren Drake</a>
    </figcaption>
</figure>

### Real Estate

There is a limited amount of space in Serkm Hub and no possibility of expanding it aside from co-opting other hubs.
As a result, rent for urban spaces is quite high with the money going to support the activities of the local government.
All real estate in Serkm Hub is held by the Interworld Governmental Council.
While a small portion of this is reserved for government buildings, such as police or fire departments or medical facilities, most is leased out to developers.
The developers then construct and maintain buildings and lease interior space to individuals and businesses.

### Enclaves

In exchange for a sufficent annual fee (varying based on area, but always expensive), it is possible to have a rented property or area declared an enclave.
Enclaves are required to be walled off and access controlled.
The reason for this is that they act as a legal "free zone" where (most) of Serkm Hub's laws do not apply.
The intention behind this system is to allow governments to build embassies without worrying about local customs, but it is sometimes also used by the very wealthy.
Enclaves are permitted to transport unspecified and uninspected "goods" between gates and their enclaves provided the transport is done in closed containers and is direct between gate and enclave.
The use of this rule to transport people is discourged, but not forbidden.

Most governments possessing a gate hold an enclave and other major governments on those worlds will often have one as well.

As an exception to the usual rules, an region in the wilderness area has been declared the zaeren enclave and no payment is extracted, despite the size of this area.
This is more an acknowledgement of existing reality than a delibrate decision, however; the zaeren occupied that region prior to the founding of serkm hub and show no interest in this changing.

## Laws

Law in Serkm Hub is based on enabling cooperation, trade, and interpersonal relations.
It is not intended to enforce morality or any cultural viewpoint.
That said, "legally allowed" is not the same as "widely accepted".

As an example, Serkm Hub has no laws regarding public nudity.
There are enough different physiologies and cultural views that the government decided they didn't want to get into enforcing any, especially given the rather minimal harm caused by the absence of such laws.
But while most people in Serkm Hub are accepting of strange ways from other species and those from other worlds, they will typically still expect people from their own culture and/or world to follow their own cultural norms.

While nudity is legally permitted, public sexual activities are not.
Enforcement is not perfect---law enforcement is not monitoring every ally and dark corner---and not all sexual acts are obvious as such, especially from an other-species perspective.
Likewise, public washrooms are not uncommon so it is expected that people will make use of them and not relieve themselves in public.

The most controversial laws are those regarding slavery.
It is permitted as long as the owner has appropriate documentation from a recognized authority.
The buying and selling of slaves is not allowed, however, nor is it permitted for one to be enslaved or emancipated.
Unlike many slave-friend nations, Serkm Hub does enforce extensive "slave rights" and forbids unnecessary harm or suffering to slaves.
Slave-related business is encouraged to be dealt with outside the hub, something its slave-rights laws and the prohibitions against buying and selling slaves encourages.


## Law Enforcement

**Classes of Crime**

- three classes of crime:
    - minor
        - forgetting ID
            - will be taken to station to be IDed by DNA
            - may be escorted to transit station, current residence, or buy new ID depending on circumstances
        - sentance is fine
    - moderate
        - persistant missing ID
        - sentance is corporeal punishment
    - major
        - sentance is exile/deportation

**Enforcement**

- common organization


## Prominent Organizations

### Brothel

- as well as obvious services, also has extensive bar with cross-species facilities

**Advantages**

- no rejection
- safe, hygenic
    - required regular health tests
- variety of species, inc. rare/uncommon
- availability
- focus on cross-species sex
    - removes risk of pregnancy
    - minimizes disease transmission

**Staff**

- staff are never expected to accept same-species customers, but may elect to do so
    - brothel disclaims responsibility for pregnancies and diseases
    - offers maternity leave, sick days
- not required to accept every customer, but are expected to accept a certain minimum
    - those below minimum may be let go, especially if regardly turning down customers
    - brothel will attempt to maintain at least one of each sex of each species, even if not very popular
- living space reserved for staff
    - seperate from "bedrooms" used for servicing customers
    - staff not required to live onsite, but encouraged
    - food and drink made available as well
        - suitable for most species
        - healthy but unremarkable


### Governmental Council

The *Interworld Governmental Council* oversees Serkm Hub as well as resolving disputes between the associated worlds.
It also maintains a small military force to help enforce decisions and maintain public order.

#### Composition

Council members come from two main groups.
The first is planetary members; each world gets one representative per gate in active use.
Typically these are representatives from the governments controlling the planetary gates, but other means of selection are used for gates in areas lacking significant government.
The second group consists of elected residents of the hub.
To qualify, the resident must have primarily resided in Serkm Hub for several common years.
Residents who have been present for at least one common year are permitted to vote.
The term of service is twenty common years, or about 9 Earth years.

In the event that a council member fails to perform their duties or is otherwise found unsatisfactory by the council, they may be impeached.


## Common System

The *Common System* is a standardized set of systems for use in Serkm Hub and in interworld trade in general. By using the Common System, merchants and traders from any world need learn only to covert between their own systems and this one rather than needing to deal with the myriad systems in use across all worlds.

The principle parts of the Common System are a language (based on the *Low Drius* language from [Warrothserkm](warrothserkm.md)), a currency, a system of timekeeping, and standardized units of length.

**Language:** Although every world has at least one language of its own (and usually far more), the [Ortasa](or_vocab.md) language, derived from the language spoken in [Warrothserkm](warrothserkm.md), has become a lingua franca and speakers can be found in nearly every hub. Unlike most homeworld languages, Low Drius was specifically designed to be usable by a wide range of species and is extremely regular.

**Currency:** The Common Currency is used for most trade in Serkm Hub and between worlds, though bartering is a not-infrequent alternative. Others worlds' local currencies are rarely excepted outside of that world or it's associated banks. Each homeworld is responsible for managing the exchanging of Common Currency for their local currencies at whatever rate they choose to set, though it is often possible to barter or to sell goods or services to obtain it while in Serkm Hub.

**Timekeeping:**
The common timesystem has standardized on a 20.96 Earth hour "day" based on the length of the day on [Ingr](ingr.md), the home of Warrothserkm.
This is split into 16 even "hours" of about 1.6 Earth hours each, each of which is split again into 16 "minutes".
Longer time periods are based on the Ingr year and consist of 189 "days" or 165 Earth days.
This is then divided into four "quarters".
The first quarter consists of 48 days and the remainder have 47 days.
No attempt to synchronize the calendar to Ingr is made and the common year contains no leap years.

**Length Units:** The base unit of measure is called the *nalwen*, a word derived from the *low drius* language. A nalwen is approximately 0.8817 metres. Derived units consist of multiples and divisions of ten. The nalwen itself is based on the size of things left by the [vorsyth](vorsyth.md), many of which have measurements that work out to an even number of nalwen.
