---
category: guidelines
---

While I'm not looking for anyone else to create species for the Nexus, I thought I'd jot down some notes on the guidelines I consider when adding new species to this setting. I hope it will be of some interest.

- Species should be loosely plausible. I'm not writing hard science fiction by any means, but I'd like to avoid things where people's first reaction is disbelief.
- Species should be unique. This is not as high a bar as it sounds and is mainly intended as "not standard animal-people" as well as not being easily recognizable duplicates of anything in pop culture.
- Species should be able to interact with and share environments with other species. This limits the options for aquatic or non-oxygen breathing species, but also makes it easier to have plotlines with a variety of species. In addition, it helps make sure a certain level of compatibility is present for more adult-oriented works.
- Species should be able to communicate. In principle this is covered by "interact", but it is still worth specifically mentioning.
- New species should be meaningfully distinct. This is really several guidelines in one. The subguidelines below are listed in approximate order of importance. (As an example/note, in an older version of this setting, I had a species called the Areenaecee that were "sakin with wings" and that even got described that way.)
    1. Species should be visibly distinct. Someone looking at individuals of familiar species should be able to clearly tell what species they are.
    1. Species should be physiologically distinct. I'd like to avoid having any one feature shared by too many different species, especially for features that are more obvious or that'll have more impact on the character. That said, some kinds of features don't lend themselves to much variation; there are only so many practical alternatives to being bipedal, for example.
    1. Species should be culturally and psychological distinct. These are combined because I'm not always very good at separating the two, especially with newer species designs. The goal here is to help reduce the "human in a rubber suit" effect as well as allowing for more fundamental differences in how individuals view the world. These differences enhance my ability to add conflict to stories and make supposedly more exotic species feel more exotic.

When documenting information about a species, I try and focus on aspects that are universal to the species as a whole and avoiding including information that is actually cultural or otherwise specific to a single group. When unsure of whether to include something, I consider "Would an individual of this species raised in a different culture away from their own species still have this trait?"

The above not withstanding, I will sometimes include a homeworld and/or cultural section in a species description if there is not enough information to warrant a separate page.
