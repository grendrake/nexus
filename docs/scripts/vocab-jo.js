var language = {
    name: "Jowvon",
    genders: {
        m: "male",
        f: "female",
        n: "neuter"
    },
    partsOfSpeech: {
        adj: "adjective",
        adv: "adverb",
        conj: "conjunction",
        n:   "noun",
        num: "numeral",
        pro: "pronoun",
        v:   "verb",
        art: "article",
        part:"particle",
    },
};

const letters = [ "ae", "sh", "th", "i", "u", "o", "e", "a", "p", "t", "m", "n", "f", "v", "s", "z", "j", "w", "r", "l" ];

var vocab = [
// articles
    {   raw:"fa",             pos:"art",  gender:"m",
        gloss:"the",          defn:"Male definate article" },
    {   raw:"sen",            pos:"art",  gender:"f",
        gloss:"the",          defn:"Female definate article" },
    {   raw:"lomae",          pos:"art",  gender:"n",
        gloss:"the",          defn:"Neuter definate article" },
    {   raw:"wu",             pos:"art",  gender:"m",
        gloss:"a",            defn:"Male definate article" },
    {   raw:"thi",            pos:"art",  gender:"f",
        gloss:"a",            defn:"Female definate article" },
    {   raw:"po",             pos:"art",  gender:"n",
        gloss:"a",            defn:"Neuter definate article" },
// pronouns
// particles
    {   raw:"ju-ta",          pos:"part", gender:"",
        gloss:"causitive case marker", defn:"" },
    {   raw:"na-ta",          pos:"part", gender:"",
        gloss:"question",     defn:"Particle used to indicate statement is a question." },
    {   raw:"na-ta-va-la",    pos:"part", gender:"",
        gloss:"is it true?",  defn:"Particle used to inquire about the truth of a statement." },
    {   raw:"za",             pos:"part", gender:"",
        gloss:"genitive case marker", defn:"" },
// name particles
    {   raw:"re",             pos:"part", gender:"",
        gloss:"male name",    defn:"Male name marker." },
    {   raw:"tae",            pos:"part", gender:"",
        gloss:"neuter name",  defn:"Neuter or non-ravon name marker." },
    {   raw:"va",             pos:"part", gender:"",
        gloss:"female name",  defn:"Female name marker" },
// other
    {   raw:"fo-rae",         pos:"v",    gender:"",
        gloss:"spoken",       defn:"to be spoken, to be said; as in \"the words were spoken\"" },
    {   raw:"ja-vo",          pos:"v",    gender:"",
        gloss:"is",           defn:"To be. Is." },
    {   raw:"jo-tos-pa",      pos:"n",    gender:"n",
        gloss:"window",       defn:"" },
    {   raw:"lo-za-pu-ta",    pos:"n",    gender:"",
        gloss:"glossyness",   defn:"Glossyness, shininess, the state of being mildly reflective, as of highly polished metal or glass." },
    {   raw:"ma-ne",          pos:"n",    gender:"m",
        gloss:"wisdom",       defn:"The state of knowing the correct, moral choice; the ability to discern such. Also, a grasp of morality and its nuances." },
    {   raw:"pu-sa-lu-vo",    pos:"n",    gender:"m",
        gloss:"man",          defn:"A male, non-ravon sophant." },
    {   raw:"pu-tha",         pos: "n",   gender:"m",
        gloss:"man",          defn:"A male ravon." },
    {   raw:"ra-vo-n",        pos: "n",   gender: "m",
        gloss: "ravon",       defn:"Ravon (Species)" },
    {   raw:"shae-zu",        pos: "n",   gender: "f",
        gloss: "sky",         defn:"" },
    {   raw:"si-tae-vu",      pos:"n",    gender:"f",
        gloss:"sun",          defn:"The sun, the star that a planet orbits." },
    {   raw:"sol-jae",        pos:"v",    gender:"",
        gloss:"breaking",     defn:"To become broken. To break." },
    {   raw:"tha-vi",         pos:"n",    gender:"m",
        gloss:"home",         defn:"" },
    {   raw:"va-la",          pos:"n",    gender:"m",
        gloss:"truth",        defn:"" },
    {   raw:"vu-ja-ro",       pos:"n",    gender:"m",
        gloss:"green",        defn:"" },
    {   raw:"wi-jae-za-to",   pos:"n",    gender:"f",
        gloss:"largeness",    defn:"Largeness, longness. The state of being large, long, or expansive." },
    {   raw:"zi-fae-la",      pos:"n",    gender:"f",
        gloss:"eye",          defn:"" },
];


/*
\word{lumae} (no) \wordtype{art.} Article indicating none of the associated noun.
*/
