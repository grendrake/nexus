function clearChildren(ofElement) {
    while(ofElement.firstChild) {
        ofElement.removeChild(ofElement.firstChild);
    }
}

function getProperty(obj, propertyName, defaultValue) {
    if (obj.hasOwnProperty(propertyName)) return obj[propertyName];
    return defaultValue;
}

function toggleElement(keyName) {
    const eArrow = document.getElementById("arrow-" + keyName);
    const eZone = document.getElementById("zone-" + keyName);
    if (!eArrow || !eZone) {
        console.error("toggleElement: elements configured incorrectly.");
        return;
    }

    if (eArrow.innerText === "⮟") {
        eArrow.innerText = "⮞";
        eZone.style.display = "none";
    } else {
        eArrow.innerText = "⮟";
        eZone.style.display = "block";
    }
}
