for (var i = 0; i < vocab.length; ++i) {
    vocab[i].lexical = vocab[i].raw.replace(/-/g, "");
}

var sortby = "lexical";
var dir = "asc";
function resort( newsort ) {
    if (newsort === sortby) {
        if (dir === "asc") dir = "desc";
        else               dir = "asc";
    } else {
        sortby = newsort;
    }
    updateList();
}

function updateList() {
    var sortmod = (dir === "asc") ? 1 : -1;
    vocab.sort(function(lhs, rhs) {
        if (lhs[sortby] < rhs[sortby]) return -sortmod;
        if (lhs[sortby] > rhs[sortby]) return  sortmod;
        return 0;
    });

    var text = "";
    for (var i = 0; i < vocab.length; ++i) {
        text += "<tr>";
        text += "<td>"+vocab[i].lexical+"</td>";
        text += "<td>"+vocab[i].gloss+"</td>";
        text += "<td><abbr title=\""+language.partsOfSpeech[vocab[i].pos]+"\">"+vocab[i].pos+"</abbr>.";
        if (vocab[i].gender !== "") {
            text += "<abbr title=\""+language.genders[vocab[i].gender]+"\">"+vocab[i].gender+"</abbr>.";
        }
        if (vocab[i].defn != "") {
            text += " "+vocab[i].defn+"</td>";
        } else {
            text += " "+vocab[i].gloss+"</td>";
        }
        text += "</tr>";
    }
    document.getElementById("vocablist").innerHTML = text;
}

document.addEventListener("DOMContentLoaded", function() {
    updateList();
});

function doFrequencies() {
    const outputText = [];
    const counts = [];
    letters.forEach(function(letter) { counts[letter] = 0; });

    vocab.forEach(function(entry) {
        let word = entry.raw;
        letters.forEach(function(letter) {
            const regex = new RegExp(letter, "g");
            word = word.replace(regex, function() {
                ++counts[letter];
                return "";
            });
        });

        word = word.replace(/-/g, "");
        const invalidLetters = word.split("")
        invalidLetters.forEach(function(letter) {
            outputText.push("Word " + entry.raw + " has bad letter " + letter + ".\n");
        });
    });
    
    letters.forEach(function(l) {
        outputText.push(l, ": ", counts[l], "\n");
    });
    
    document.getElementById("freqOutput").innerText = outputText.join("");
    document.getElementById("freqOutput").style.display = "block";
    
}