const requiredTags = [];
const forbidTags = [];
const eraList = [];
let tagSort = "name";


//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
// UI Functions
function setFromDate(newDate) {
    document.getElementById("fromDate").value = newDate;
    doHistoryList();
}

function sortTagsFreq() {
    tagSort = "freq";
    buildTagList();
    saveSettings();
}

function sortTagsName() {
    tagSort = "name";
    buildTagList();
    saveSettings();
}

function toggleTag(tagName) {
    const idxRequired = requiredTags.indexOf(tagName);
    const idxForbid = forbidTags.indexOf(tagName);
    if (idxRequired !== -1) {
        requiredTags.splice(idxRequired, 1);
        forbidTags.push(tagName);
    } else if (idxForbid !== -1) {
        forbidTags.splice(idxForbid, 1);
    } else {
        requiredTags.push(tagName);
    }
    doHistoryList();
}

function toggleEra(eraName) {
    const idx = eraList.indexOf(eraName);
    if (idx === -1) eraList.push(eraName);
    else            eraList.splice(idx, 1);
    doHistoryList();
}

function toggleHideEmpty() {
    saveSettings();
    buildTagList();
}

//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
// reset, save, and restore settings
function resetSettings() {
    "strict mode";
    document.getElementById("era-precontact").checked = true;
    document.getElementById("era-warrothserkm").checked = true;
    document.getElementById("era-interregnum").checked = true;
    document.getElementById("era-modern").checked = true;
    eraList.length = 0;
    eraList.push("pre-contact");    eraList.push("warrothserkm");
    eraList.push("interregnum");    eraList.push("modern");
    document.getElementById("minDate").value = "";
    document.getElementById("maxDate").value = "";
    document.getElementById("fromDate").value = "";
    document.getElementById("currentCalender").value = 0;
    requiredTags.length = 0;
    forbidTags.length = 0;
    doHistoryList();
}

function restoreSettings() {
    "strict mode";
    const rawSaveData = localStorage.getItem("nexus_timeline_settings");
    if (rawSaveData) {
        const data = JSON.parse(rawSaveData);
        const datEraList = getProperty(data, "eraList", undefined);
        datEraList.forEach(function (era) { eraList.push(era); });
        document.getElementById("era-precontact").checked = !datEraList || datEraList.indexOf("pre-contact") >= 0;
        document.getElementById("era-warrothserkm").checked = !datEraList || datEraList.indexOf("warrothserkm") >= 0;
        document.getElementById("era-interregnum").checked = !datEraList || datEraList.indexOf("interregnum") >= 0;
        document.getElementById("era-modern").checked = !datEraList || datEraList.indexOf("modern") >= 0;
        document.getElementById("show-text").checked = getProperty(data, "showText", true);
        document.getElementById("minDate").value = getProperty(data, "minDate", "");
        document.getElementById("maxDate").value = getProperty(data, "maxDate", "");
        document.getElementById("fromDate").value = getProperty(data, "fromDate", "");
        document.getElementById("hide-empty-tags").checked = getProperty(data, "hideEmptyTags", true);
        tagSort = getProperty(data, "tagSort", "name");
        const calOrder = getProperty(data, "order", "asc");
        document.getElementById("calenderOrder").value = calOrder;
        if (data.hasOwnProperty("currentCalendar")) {
            const calendarName = data.currentCalendar;
            for (let i = 0; i < calendars.length; ++i) {
                if (calendars[i].name === calendarName) {
                    document.getElementById("currentCalender").value = i;
                    break;
                }
            }}
        requiredTags.length = 0;
        forbidTags.length = 0;
        getProperty(data, "tags", []).forEach(function(tag) {
            requiredTags.push(tag);
        });
        getProperty(data, "forbid", []).forEach(function(tag) {
            forbidTags.push(tag);
        });
    }
    doHistoryList();
}

function saveSettings() {
    "strict mode";
    const saveData = {};
    saveData.eraList = eraList;
    saveData.showText = document.getElementById("show-text").checked;
    saveData.minDate = parseInt(document.getElementById("minDate").value);
    saveData.maxDate = parseInt(document.getElementById("maxDate").value);
    saveData.fromDate = parseInt(document.getElementById("fromDate").value);
    saveData.order = document.getElementById("calenderOrder").value;
    saveData.hideEmptyTags = document.getElementById("hide-empty-tags").checked;
    saveData.tagSort = tagSort;
    saveData.currentCalendar = calendars[document.getElementById("currentCalender").value].name;
    saveData.tags = requiredTags;
    saveData.forbid = forbidTags;
    localStorage.setItem("nexus_timeline_settings", JSON.stringify(saveData));
}

//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
// update timeline
function historySortAsc(a, b) {
    if (a.date < b.date) return -1;
    if (a.date > b.date) return 1;
    return 0;
}
function historySortDec(a, b) {
    if (a.date < b.date) return 1;
    if (a.date > b.date) return -1;
    return 0;
}
function includeEvent(entry) {
    // verify entry meets current filter requirements
    for (var i = 0; i < requiredTags.length; ++i) {
        if (entry.keywords.indexOf(requiredTags[i]) === -1) {
            return false;
        }
    }
    for (var i = 0; i < forbidTags.length; ++i) {
        if (entry.keywords.indexOf(forbidTags[i]) !== -1) {
            return false;
        }
    }
    if (eraList.indexOf(entry.era) === -1) return false;
    return true;
}
function doHistoryList() {
    "strict mode";
    saveSettings();

    const showText = document.getElementById("show-text").checked;

    let resultCount = 0;
    const order = document.getElementById("calenderOrder").value;
    let minDate = parseInt(document.getElementById("minDate").value);
    let maxDate = parseInt(document.getElementById("maxDate").value);
    let fromDate = parseInt(document.getElementById("fromDate").value);
    const currentCalendar = document.getElementById("currentCalender").value;
    const timelineText = [];

    if (isNaN(minDate))  minDate  = Number.MIN_SAFE_INTEGER;
    if (isNaN(maxDate))  maxDate  = Number.MAX_SAFE_INTEGER;
    if (isNaN(fromDate)) fromDate = 0;

    if (order === 'asc')    data.sort(historySortAsc);
    else                    data.sort(historySortDec);

    timelineText.push("<thead><tr><th>Date</th><th>Keywords</th><th>Years Since Date</th></tr></thead>");

    const start = performance.now();
    data.forEach(function(entry) {
        if (!includeEvent(entry)) return;

        // convert the timestamp to the selected calendar and check that the
        // date is within the specified range
        const realDate = new calendars[currentCalendar](entry.date);
        if (realDate.year < minDate || realDate.year > maxDate) return;

        // create table entry for timeline entry
        ++resultCount;
        timelineText.push("<tr class='timeline-head'>");
        timelineText.push("<td class='timeline-date' style='background-color:");
        timelineText.push(eras[entry.era][0]);
        timelineText.push("' onclick='setFromDate(" + realDate.year + ")' title='", eras[entry.era][1], "'>");
        timelineText.push(realDate.text);
        timelineText.push("</td><td class='timeline-keywords'>");

        entry.keywords.forEach(function(keyword) {
            timelineText.push("<span class='tag'");
            if (tags.hasOwnProperty(keyword)) {
                timelineText.push(" style='color:", tags[keyword].color, "' title='");
                timelineText.push(tags[keyword].title);
                timelineText.push("'");
            } else {
                timelineText.push(" style='background-color:#F0F;color:#000'");
                console.warn("WARNING: Tag ", keyword, " not defined.");
            }
            timelineText.push(" onclick='toggleTag(\"", keyword, "\")'");
            timelineText.push(">", keyword, "</span> ");
        });
        timelineText.push("</td><td class='timeline-since'>");
        timelineText.push(realDate.year - fromDate);
        timelineText.push("</td></tr>");

        timelineText.push("<tr><td class='timeline-line'>&nbsp;</td>");
        timelineText.push("<td class='timeline-text' colspan='2'><p><b>", entry.initial, "</b> ");
        if (showText) timelineText.push(entry.text);
        timelineText.push("</td></tr>");
    });
    const end = performance.now();
    document.getElementById("timeline").innerHTML = timelineText.join("");

    buildTagList();
    document.getElementById("resultcount").innerHTML = resultCount;
    document.getElementById("runtime").innerHTML = Math.round(end - start) / 1000;
}

function countTags() {
    const tagList = Object.keys(tags);
    tagList.forEach(function (tag) { tags[tag].count = 0; });
    data.forEach(function(entry) {
        if (!includeEvent(entry)) return;
        entry.keywords.forEach(function(word) {
            ++tags[word].count;
        });
    });
}

function buildTagList() {
    const hideEmptyTags = document.getElementById("hide-empty-tags").checked;
    countTags();
    const tagList = Object.keys(tags);

    if (tagSort === "name") {
       tagList.sort();
    } else {
        tagList.sort(function(a, b) {
            if (tags[a].count > tags[b].count) return -1;
            if (tags[a].count < tags[b].count) return 1;
            return 0;
        });
    }

    const tagListText = [];
    tagList.forEach(function(word) {
        const isRequired = requiredTags.indexOf(word) >= 0;
        const isForbidden = forbidTags.indexOf(word) >= 0;
        if (hideEmptyTags && tags[word].count === 0 && !isRequired && !isForbidden) return;
        tagListText.push("<button onclick='toggleTag(\"", word, "\")' style='color:");
        tagListText.push(tags[word].color, "'>");
        if (isRequired) tagListText.push("+ ");
        if (isForbidden) tagListText.push("- ");
        tagListText.push(word, " (", tags[word].count, ")</button><br>");
    });
    document.getElementById("keyword-counts").innerHTML = tagListText.join("");
}


//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
// Page load
document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("totalcount").innerHTML = Object.keys(data).length;

    // update calendars list
    let first = true;
    calendars.forEach(function(calendar, index) {
        var newOpt = document.createElement("option");
        newOpt.value = index;
        ++index;
        newOpt.innerHTML = calendar.name;
        if (first) {
            first = false;
            newOpt.selected = true;
        }
        document.getElementById("currentCalender").appendChild(newOpt);
    });

    // reload settings and build page content
    restoreSettings();
    buildTagList();
});
