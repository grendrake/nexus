"use strict";

//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
// Calendar data
const calendars = [
    class EarthCalendar {
        static get name() { return "Earth Gregorian"; }

        constructor(fracYear) {
            const year = parseInt(fracYear);
            const reminder = fracYear - year;
            const daysPerYear = this.leapYear(year) ? 366 : 365;
            const miliseconds = reminder * daysPerYear * 24 * 60 * 60 * 1000;
            const yearDate = new Date(year, 0, 1);
            this.mDate = new Date(yearDate.getTime() + miliseconds);
            this.mYear = year;
        }
        get text() {
            const monthNames = [
                "January", "February", "March",     "April",   "May",      "June",
                "July",    "August",   "September", "October", "November", "December"
            ];
            return monthNames[this.mDate.getMonth()] + " " + this.mDate.getDate() + ", " + this.mYear;
        }
        get year() {
            return this.mYear;
        }

        leapYear(year) {
            return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
        };

    },
    class ErladiCalendar {
        static get name() { return "Erladi Solar"; }

        constructor(fracYear) {
            const yearLength = 1.89;
            const daysPerYear = 1187;
            fracYear /= yearLength;
            const year = parseInt(fracYear);
            const reminder = fracYear - year;
            this.mDay = Math.abs(Math.trunc(reminder * daysPerYear));
            this.mYear = year;
        }
        get text() {
            return "Year " + this.mYear + ", day " + this.mDay;
        }
        get year() {
            return this.mYear;
        }
    },
    class KzaknriCalendar {
        static get name() { return "Razaz Nudar (Kzaknri)"; }

        constructor(fracYear) {
            const yearLength = 1.524;
            const daysPerYear = 600;
            fracYear -= 1344;
            fracYear /= yearLength;
            const year = parseInt(fracYear);
            const reminder = fracYear - year;

            const baseDays = Math.abs(Math.trunc(reminder * daysPerYear));

            this.mMonth = Math.trunc(baseDays / 30);
            this.mDay = baseDays - this.mMonth * 30;
            this.mYear = year;
        }
        get text() {
            return "Year " + this.mYear + ", Month " + (this.mMonth + 1) + " day " + this.mDay;
        }
        get year() {
            return this.mYear;
        }
    },
    class IngrCalendar {
        static get name() { return "Ingr Solar"; }

        constructor(fracYear) {
            const yearLength = 0.452;
            const daysPerYear = 189;
            fracYear /= yearLength;
            const year = parseInt(fracYear);
            const reminder = fracYear - year;
            this.mDay = Math.abs(Math.trunc(reminder * daysPerYear));
            this.mYear = year;
        }
        get text() {
            return "Year " + this.mYear + ", day " + this.mDay;
        }
        get year() {
            return this.mYear;
        }
    },
    class ZifaelafojunCalendar {
        static get name() { return "Zifaelafojun Solar"; }

        constructor(fracYear) {
            const yearLength = 0.89;
            const daysPerYear = 204;
            fracYear /= yearLength;
            const year = parseInt(fracYear);
            const reminder = fracYear - year;
            this.mDay = Math.abs(Math.trunc(reminder * daysPerYear));
            this.mYear = year;
        }
        get text() {
            return "Year " + this.mYear + ", day " + this.mDay;
        }
        get year() {
            return this.mYear;
        }
    }
];

//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
// General Timeline Data
const eras = {
    "pre-contact":  ["#BB77BB", "Pre-Contact"],
    "warrothserkm": ["#8888CC", "Time of Warrothserkm"],
    "interregnum":  ["#00BBBB", "Interregnum"],
    "modern":       ["#CC8888", "Modern Period"],
    "meta":         ["#AAAAAA", "Meta Events"]
};

const tags = {
    "Earth":                { "color": "rgb(0, 117, 220)",  "title": "Homeworld of the humans.", "count": 0 },

    "Erladi":               { "color": "rgb(153, 63,0)",    "title": "Homeworld of the au.", "count": 0 },
    "Ker Relt":             { "color": "rgb(153, 63,0)",    "title": "", "count": 0 },
    "Gek Relt":             { "color": "rgb(153, 63,0)",    "title": "", "count": 0 },
    "Reka Relt":            { "color": "rgb(153, 63,0)",    "title": "", "count": 0 },

    "Ingr":                 { "color": "rgb(76, 0, 92)",    "title": "Homeworld of the drius.", "count": 0 },
    "Warrothserkm":         { "color": "rgb(76, 0, 92)",    "title": "", "count": 0 },

    "Interworld Nexus":     { "color": "rgb(25, 25, 25)",   "title": "", "count": 0 },
    "Vorsyth":              { "color": "rgb(25, 25, 25)",   "title": "", "count": 0 },

    "Kzaknri":              { "color": "rgb(0, 92, 49)",    "title": "Homeworld of the sakin.", "count": 0 },
    "FASS":                 { "color": "rgb(0, 92, 49)",    "title": "", "count": 0 },
    "Razaz Nudar":          { "color": "rgb(0, 92, 49)",    "title": "", "count": 0 },
    "Zanth Drith":          { "color": "rgb(0, 92, 49)",    "title": "", "count": 0 },
    "Tsukina":              { "color": "rgb(0, 92, 49)",    "title": "", "count": 0 },
    "Rekuzarae":            { "color": "rgb(0, 92, 49)",    "title": "", "count": 0 },
    "Zdajgae":              { "color": "rgb(0, 92, 49)",    "title": "", "count": 0 },

    "Raesan Vie":           { "color": "rgb(143, 124, 0)",  "title": "Homeworld of the asuni.", "count": 0 },

    "Isaguyat":             { "color": "rgb(0,153,143)",    "title": "Homeworld of the sekar.", "count": 0 },
    "Daesra World":         { "color": "rgb(153, 0, 0)",    "title": "Homeworld of the daesra.", "count": 0 },

    "Va-Katr":              { "color": "rgb(194, 0, 136)",  "title": "Homeworld of the garor and rakelm.", "count": 0 },

    "Zaeren Collective":    { "color": "rgb(0, 51, 128)",   "title": "", "count": 0 },

    "Zifaelafojun":         { "color": "rgb(43, 206, 72)",  "title": "Homeworld of the ravon.", "count": 0 },
    "Menota":               { "color": "rgb(43, 206, 72)",  "title": "", "count": 0 },
    "Pasha Za Fojun Fae":   { "color": "rgb(43, 206, 72)",  "title": "", "count": 0 },

    "Contact":              { "color": "rgb(66, 102, 0)",   "title": "The inhabitants of a world make contact with Serkm Hub.", "count": 0 },

    "Meta":                 { "color": "rgb(255, 0, 255)",  "title": "This event serves as a marker rather than an actual event.", "count": 0 },
    "TODO":                 { "color": "rgb(255, 0, 255)",  "title": "This event needs to be updated or completed.", "count": 0 },
    /*
    {255,164,5},{255,168,187},{255,0,16},{94,241,242},{224,255,102},{116,10,255},{255,255,128},{255,255,0},{255,80,5}
    */
};

//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
// Timeline event data
const data = [
    {   "date": -7983.423, "keywords": ["Vorsyth", "Interworld Nexus", "Meta"], "era": "pre-contact",
        "initial": "Estimated time of last known activity of the vorsyth.",
        "text": "This estimate was formed by analysis of the regions the planet-side gates were found in, particular the gates buried beneath the surface." },
    {   "date": 0.0, "keywords": ["Earth", "Meta"], "era": "pre-contact",
        "initial": "Beginning of Earth year 0 in astronomical time.",
        "text": "" },

    {   "date": 520.387, "keywords": ["Menota","Zifaelafojun"], "era": "pre-contact",
        "initial": "Approximate date religious worship in Menota become focused exclusively on Vaere.",
        "text": "All evidence suggests that the other gods from former, more polytheistic times were still acknowledged, but they stopped receiving direct worship." },
    {   "date": 851.825, "keywords": ["Menota","Zifaelafojun"], "era": "pre-contact",
        "initial": "Formation of the Church of Vaere as an organization.",
        "text": "" },

    {   "date": 1202.349, "keywords": ["Ingr", "Warrothserkm"], "era": "pre-contact",
        "initial": "Official formation of Warrothserkm.",
        "text": "The era of Warrothserkm is not considered to begin here as the nation had not yet discovered the Ingr gate or contacted other worlds." },
    {   "date": 1220.684, "keywords": ["Ingr", "Warrothserkm"], "era": "pre-contact",
        "initial": "First Argus System begins operation.",
        "text": "Originally commissioned and installed to monitor and run extra-planetary operations, over the next decade the Argus System slowly accumulated more and more tasks until it was functionally in control of the clan." },
    {   "date": 1268.470, "keywords": ["Ingr", "Warrothserkm"], "era": "pre-contact",
        "initial": "Last Argus System begins operation.",
        "text": "Clan Radageos is the last of the major clans to hand off management of the clan to an Argus System." },
    {   "date": 1344.089, "keywords": ["Kzaknri", "Razaz Nudar"], "era": "pre-contact",
        "initial": "Razaz Nudar is founded when Tkinae begins to conquer and consolidate the nomadic bands of the area.",
        "text": "The priesthood of his own band's god, Teka, God of Fire and Destruction, support his efforts and, in exchange, he persecutes those who publicly follow the gods of other bands. Most of these other gods are no longer worshiped or remembered as more than mythology. One notable exception was (and continues to be) Nratha, God of Trade and Business; the followers of Nratha had long formed the backbone of the area's economy even before the founding of Razaz Nudar." },
    {   "date": 1346.781, "keywords": ["Kzaknri", "Razaz Nudar", "Zanth Drith"], "era": "pre-contact",
        "initial": "The people of Grutha are conquered.",
        "text": "Rather than give up their god, they began to practice their worship in secret. Unlike many others who attempted this, the people of Grutha were successful (meaning they survived), though there were many attempts over the coming centuries to destroy the religion. In response, the followers of Grutha dedicated themselves to the destruction of Razaz Nudar; a feud that continues into modern times." },
    {   "date": 1348.312, "keywords": ["Kzaknri", "Razaz Nudar", "Tsukina"], "era": "pre-contact",
        "initial": "The people of Zakanor, known as the Tsukina, began their exodus from the region.",
        "text": "The Tsukina were one of the more distant bands, but observing Tkinae's actions left them no doubt about what he planned for them. Rather than try and fight the growing nation, the Tsukina fled. Their exodus covered an immense distance and crossed extensive mountain ranges. Many were lost to starvation or environmental conditions. Eventually, they came to the jungles were they settled. While yet more were lost to the jungle, the survivors became the ancestors of the modern-day Tsukina." },
    {   "date": 1358.890, "keywords": ["Kzaknri", "Razaz Nudar"], "era": "pre-contact",
        "initial": "Tkinae, first emperor of Razaz Nudar, dies.",
        "text": "The growing empire survives his death and his successors continue the process of expanding it." },
    {   "date": 1362.839, "keywords": ["Kzaknri", "Razaz Nudar"], "era": "pre-contact",
        "initial": "Razaz Nudar begins to go through several periods of expansion and contraction.",
        "text": "The emperors, though often good at, or at least focused on, conquest and plunder, often proved to be less effective at other aspects of statecraft. Rebellions were common and emperors short-lived. Many were assassinated, a fate considered by the populace as a good way to keep the government alert and attentive to the people. Others died on the battlefield."  },
    {   "date": 1614.937, "keywords": ["Ingr", "Interworld Nexus", "Warrothserkm"], "era": "warrothserkm",
        "initial": "Warrothserkm discovers the gate that leads them to the Interworld Nexus.",
        "text": "Although they had not yet contacted other worlds, this is considered the beginning of the era of Warrothserkm." },
    {   "date": 1631.356, "keywords": ["Contact","Erladi", "Warrothserkm", "Reka Relt"], "era": "warrothserkm",
        "initial": "Warrothserkm discovers a method of opening gates from the Interworld Nexus and, in doing so, discovers the world of Erladi.",
        "text": "Initial contact is made with the medieval-level era nation of Reka Relt and they are welcomed as a part of the Empire." },
    {   "date": 1649.935, "keywords": ["Contact","Warrothserkm", "Zifaelafojun"], "era": "warrothserkm",
        "initial": "Zifaelafojun is discovered when Warrothserkm forces open their gate from the Nexus side.",
        "text": "The local sophant species, the ravon, was experience a continuing decline in population. Warrothserkm brought technology and knowledge to improve the health and living conditions of the locals, though they made not attempt to improve the economy or to develop local industry. As a result, the local population became entirely dependant on Warrothserkm's offerings. Most of this technology was first gifted to the nobility, but it often trickled down to enrich the lives of the common people as well." },
    {   "date": 1650.187, "keywords": ["Warrothserkm", "Zifaelafojun"], "era": "warrothserkm",
        "initial": "The Council of Clans is established.",
        "text": "The thirteen clans of the ravon were established long before Warrothserkm arrived on the planet. When Warrothserkm arrived, they were happy to support the clans, but required that a single overseeing organization oversee relations between the clans. This directive caused the formation of the Council of Clans." },
    {   "date": 1660.234, "keywords": ["Contact", "Warrothserkm", "Daesra World"], "era": "warrothserkm",
        "initial": "Warrothserkm is surprised by the daesra discovering and opening their own gate and establishing contact.",
        "text": "This initial action proves to be the least interesting thing about the daesra and Warrothserkm welcomes the new species even as it desperately tries to research daesra abilities." },
    {   "date": 1671.729, "keywords": ["Contact", "Kzaknri", "Warrothserkm"], "era": "warrothserkm",
        "initial": "Warrothserkm forces the gate of Kzaknri open from the Nexus.",
        "text": "As the gate was in an unpopulated area, the new arrivals initially thought they had discovered an uninhabited world. It does not take long for reconnaissance to reveal the local population, however." },
    {   "date": 1671.181, "keywords": ["Kzaknri", "Warrothserkm", "Razaz Nudar"], "era": "warrothserkm",
        "initial": "Attempts by Warrothserkm to establish a foothold in Razaz Nudar are made, but largely fail.",
        "text": "The harsh climate and minimal seasons make the region inhospitable for the drius. Combined with  the nomadic, decentralized nature of the native population, Warrothserkm agents consistently failed to infiltrate local society. Despite their advanced technology, many did not even survive. Eventually, a series of coastal \"trading posts\" are set up in an attempt to exert a more subtle, economic control over the region, but even this is only partially successful as the local population have little to trade that is of interest to Warrothserkm. That said, there were locals who felt things would be better elsewhere and left for other worlds." },
    {   "date": 1687.423, "keywords": ["Contact", "Warrothserkm", "Isaguyat"], "era": "warrothserkm",
        "initial": "Warrothserkm forces the gate to Isaguyat open.",
        "text": "Not only do the native sekar rebuff the newcomers, Warrothserkm's contact squad is caught off-guard and mostly slaughtered. The low technology level leads to some debate about returning with modern weapons, but ultimately it is decided to close the gate and forbid contact. The death of the contact squad kills popular and political support for the contact program; no further gates are forced open by Warrothserkm." },
    {   "date": 1690.046, "keywords": ["Kzaknri", "Warrothserkm", "Razaz Nudar", "Zanth Drith"], "era": "warrothserkm",
        "initial": "With assistance from Warrothserkm, Razaz Nudar drives the followers of Grutha out.",
        "text": "In exchange for granting Warrothserkm guides and assistance in dealing with the locals as well as a supply of goods and slaves, the emperor obtained assistance in the form of technology intended to allow him to uncover hidden members of the Grutha cult. This was not entirely successful; despite the technology, the emperor managed nothing more than to drive the Grutha worshippers beyond the nation's borders into a swampy region that would become Zanth Drith. The expense and focus on this campaign caused a period of economic decline that the emperor did not survive." },
    {   "date": 1694.529, "keywords": ["Warrothserkm", "Ingr", "Interworld Nexus"], "era": "warrothserkm",
        "initial": "Civil war erupts in Warrothserkm.",
        "text": "This war later becomes known as the \"Days or Fire\" for both its brevity and the sheer destructive power of the weapons used." },
    {   "date": 1694.540, "keywords": ["Kzaknri", "Ingr", "Erladi", "Zifaelafojun", "Warrothserkm", "Interworld Nexus"], "era": "warrothserkm",
        "initial": "The Nexus gate network collapses.",
        "text": "This is caused by an unexpected side-effect of the weapons used during the Days of Fire. All worlds are cut off from the Interworld Nexus, stranding both those on other worlds as well as those currently on the Nexus itself. Due to low numbers and diverse species, most non-native sophants do not form sustainable populations and have little long-term impact." },
    {   "date": 1694.5402, "keywords": ["Zifaelafojun", "Menota"], "era": "interregnum",
        "initial": "Menotan civilization collapses.",
        "text": "Despite number of ravon taken off-planet as slaves, the time of Warrothserkm had been good for Menota. Thanks to imported technology, the population had grown significantly and the quality of life for those on the planet had increased immensely. The fall of Warrothserkm brought and end to this. Younger generations now had neither the technological marvels of the off-worlders nor the experience working the land older generations had. The population survived, but those nobles with access to the remaining technology consolidated their power. The common people were left to pledge themselves as serfs if they wished protection from the wilds or from criminals. The population numbers once again went into a decline.<p>Without a sustainable population or a history of working the land, the remaining non-ravon found themselves worse off, a situation only exacerbated by the ravon nobility blaming the off-worlders for the situation. By the time the gate opened once more, none remained and the locals did not speak of their having ever existed." },
    {   "date": 1694.5401, "keywords": ["Kzaknri", "Razaz Nudar"], "era": "interregnum",
        "initial": "Razaz Nudar loses contact with the Interworld Nexus.",
        "text": "Most civilizations on Kzaknri have only loose ties with Warrothserkm and the non-native population is nearly non-existent. As a result, the loss of contact has minimal effect." },
    {   "date": 1694.552, "keywords": ["Warrothserkm", "Ingr"], "era": "interregnum",
        "initial": "The Days of Fire end.",
        "text": "This is brought about not by negotiation or conquest, but by the collapse of civilization. It is estimated that over 90% of the world population of Ingr died during the war. Despite this, many automated installations and defensive installations remain operational surviving even to the present day." },
    {   "date": 1702.499, "keywords": ["Zifaelafojun", "Menota", "Pasha Za Fojun Fae"], "era": "interregnum",
        "initial": "Initial inhabitation of Pasha Za Fojun Fae.",
        "text": "With the gate off-world closed and no sign of it opening again and with no where for the free-thinkers, rebels, and malcontents to go, the nobility of Menota declared the rocky, undesirable area around the gate a suitable place to exile these undesirables to. This area would later become known for Pasha Za Fojun Fae. It was both littered with the remains of broken technology and the location of the last homes of the off-worlders.<p>Despite the inhospitable land, the exiles were able to build themselves a society that, initially, they shared with the remaining non-ravon. The harshness of the area meant all had to work together to survive, leading to egalitarian attitudes that Menota lacked, and the widespread (if non-functional) technology and literal alien viewpoints lead to the exiles becoming a much more inquisitive and curious society."},
    {   "date": 1844.550, "keywords": ["Erladi", "Gek Relt", "Ker Relt", "Reka Relt"], "era": "interregnum",
        "initial": "The nation of Reka Relt collapses in religious schism.",
        "text": "Gek Relt and Ker Relt split off while the old capital and surrounding land form a new, much smaller, Reka Relt." },
    {   "date": 1853.125, "keywords": ["Erladi", "Gek Relt"], "era": "modern",
        "initial": "Gate in Gek Relt is discovered and opened.",
        "text": "" },
    {   "date": 1853.575, "keywords": ["Kzaknri", "Rekuzarae", "Razaz Nudar"], "era": "interregnum",
        "initial": "Estimated date of hatching of emperor-to-be Rekuzarae.",
        "text": "" },
    {   "date": 1853.700, "keywords": ["Contact", "Erladi", "Zifaelafojun", "Gek Relt", "Menota"], "era": "modern",
        "initial": "Gek Relt forces Zifaelafojun gate from Nexus side.",
        "text": "The initial scouts from Gek Relt made contact with the first locals they found, gathered information about the immediate area, and then left to report home." },
    {   "date": 1853.782, "keywords": ["Zifaelafojun", "Menota", "Pasha Za Fojun Fae"], "era": "modern",
        "initial": "The Hall of the Breakers is formed.",
        "text": "The government of Menota, receiving word of the Gek Relt scouts, decides they don't want the first returning off-worlders experience of Menota to be of disorganized exiles and quickly sets up a formal government, naming the area Pasha Za Fojun Fae, meaning roughly \"The Land of the Broken\" and seat of local government called the \"Hall of the Broken\". They make sure to include plenty of ambassadors ready to communicate with the off-worlders. With this, Menota believes it will be able to benefit from the contact while also having Pasha Za Fojun Fae serve as a buffer between itself and the off-worlders.<p>Although initially governed directly by Menota, the culture differences and the acknowledgement that the new nation would need to be able to quickly respond to events resulted in Pasha Za Fojun Fae becoming more independant than intended." },
    {   "date": 1853.842, "keywords": ["Zifaelafojun", "Menota", "Pasha Za Fojun Fae", "Gek Relt"], "era": "modern",
        "initial": "Zifaelafojun is annexed by Gek Relt.",
        "text": "The extensive preparation by Menota for the return off-worlders quickly becomes moot when forces from Gek Relt return, heavily armed, and take over all government functions while formally disbanding all local government. The name Breakers for the people of Pasha Za Fojun Fae dates to this time; although initially a mistranslation, the people of Pasha Za Fojun Fae collectively decided they'd rather be known as the Breakers than the Broken. Their collective efforts prevented the mistranslation from being discovered for over a decade, at which point they were well established as the Breakers." },
    {   "date": 1869.547, "keywords": ["Kzaknri", "Razaz Nudar", "Rekuzarae"], "era": "modern",
        "initial": "Rekuzarae, a progressive, socially-minded activist, is framed for treason and executed.",
        "text": "The execution is performed according to the old traditions and, unbeknownst to his contemporaries, he survived, albeit not without cost. For the rest of his life he suffered from progressive and crippling health problems. Rather than be re-executed, Rekuzarae went into self-exile in the FASS." },
    {   "date": 1872.514, "keywords": ["Kzaknri", "Interworld Nexus", "FASS", "Rekuzarae"], "era": "modern",
        "initial": "Rekuzarae discovers and activates the Nexus gate.",
        "text": "After living in the FASSfor a couple of years, he found himself considering a return to Razaz Nudar to do what he felt would be best for the country. He knew he was slowly dying, so he had little to lose in such a venture.<p>After arriving in secret, but before meeting anyone, he not only rediscovered the gate, but managed to activate it. A sojourn in the Interworld Nexus not only strengthened his desires, but brought unexpected, but loyal allies. The best known are two ravon slaves descended from those caught on the Nexus when the gates failed. In exchange for their freedom, they agreed to aid him. This he accomplished by virtue of hiding them in a large crate that he subsequently hauled off. It is often suspected that the loyalty of those he brought back came as much from Rekuzarae being their only point of contact as any personal loyalty." },
    {   "date": 1873.420, "keywords": ["Kzaknri", "Razaz Nudar", "Rekuzarae"], "era": "modern",
        "initial": "Rekuzarae ascends the throne of Razaz Nudar.",
        "text": "With the assistance of a couple of ravon for aerial reconnaissance and a small force of dedicated allies, Rekuzarae snuck into the imperial palace, killed the current emperor in his sleep, and proclaimed himself the new emperor. This was not universally accepted, but his allies were able to remove other contenders and any who objected to his rule. In this, they became the first incarnations of the Imperial Guard and secret police." },
    {   "date": 1873.430, "keywords": ["Kzaknri", "Razaz Nudar", "Rekuzarae"], "era": "modern",
        "initial": "Rekuzarae banishes old friends and allies to the FASS and beyond.",
        "text": "For many of those banished, this was the first they were aware of his return. Discovery of his personal journals long after his death revealed his motive: he feared those he cared for would be used against him." },
    {   "date": 1873.051, "keywords": ["Kzaknri", "Razaz Nudar", "FASS"], "era": "modern",
        "initial": "The northern colonies of Razaz Nudar are \"granted\" independence.",
        "text": "This news was not well received in the FASS as they depended on support from Razaz Nudar. Left without it, they banded together to form the modern FASS." },
    {   "date": 1877.624, "keywords": ["Kzaknri", "Razaz Nudar", "Rekuzarae"], "era": "modern",
        "initial": "Emperor Rekuzarae dies; he is succeeded by emperor Nenae.",
        "text": "To the surprise of many, Rekuzarae did not die from assassination or violence, but from his failing health. By the end of his reign, he struggled to walk without assistance." },
    {   "date": 1883.631, "keywords": ["Contact", "Kzaknri", "FASS"], "era": "modern",
        "initial": "The gate is rediscovered in Razaz Nudar.",
        "text": "Although the gate has been rediscovered and used by Rekuzarae, he did not reveal its existence, making this the point of reconnection with the Interworld Nexus." },
    {   "date": 1899.999, "keywords": ["Kzaknri", "Razaz Nudar", "Zdajgae"], "era": "modern",
        "initial": "Estimated year that future emperor Zdajgae hatches.",
        "text": "He was the only one of his clutch to successfully hatch and his mother, siblingless and destitute, died a few years later, not long before he was randomly picked by Thiknrae as the heir." },
    {   "date": 1899.317, "keywords": ["Kzaknri", "Zifaelafojun", "Erladi", "Gek Relt", "Menota", "Razaz Nudar"], "era": "modern",
        "initial": "Razaz Nudar liberates Menota.",
        "text": "This takes the form of an invasion by the forces of Razaz Nudar; all non-ravon in residence are given the choice between death or being sent back to their original homeworld, even if they have no resources or money. Deported individuals are not given recompense for lost property or goods. Menota is set up with an independent government and granted possession of all assets seized from the deported.<p>Although originally believed to be a PR stunt, records discovered later reveal that Emperor Thiknrae had intended the fledgling government to fail, allow him to return, claim the territory, and look like a hero doing it. Unfortunately for his plan, while the government he'd set up was no more effective than he'd intended, the already existing governing bodies were able to pick up the slack so the intended collapse never occurred." },
    {   "date": 1907.208, "keywords": ["Kzaknri", "Razaz Nudar", "Zdajgae"], "era": "modern",
        "initial": "Emperor Thiknrae declares himself immortal.",
        "text": "Thiknrae was largely considered to be insane at this point; when advisors insisted he appoint an heir despite his \"immortality\", the orphan Zdajgae was selected and trained in statecraft by Thiknrae's advisors (or rather, groomed to be a good puppet)." },
    {   "date": 1911.367, "keywords": ["Kzaknri", "Razaz Nudar", "Zdajgae"], "era": "modern",
        "initial": "Emperor Thiknrae the Mad dies and emperor Zdajg ascends the throne.",
        "text": "Thiknrae was assassinated before the time his advisors his planned and they were caught unprepared. The assassin was caught and tortured into a confession that entirely failed to implicate the advisors, but fledgling emperor Zdajgae was already aware of the advisors' plans to use him as a puppet.<p>One of emperor Zdajgae's first actions was to use the assassination as a pretext to have the advisors executed for regicide and was able to reveal enough evidence of their actual plans to make the charge believable. He appointed new advisors from among those he knew and has since proved remarkable resistant to both assassination and manipulation." },
    {   "date": 1986.789, "keywords": ["Kzaknri", "Razaz Nudar", "Rekuzarae"], "era": "modern",
        "initial": "The personal journals of Rekuzarae are discovered in a hidden chamber in the palace.",
        "text": "" },
    {   "date": 2004.680, "keywords": ["Kzaknri", "Razaz Nudar"], "era": "modern",
        "initial": "Djurnae hatches.",
        "text": "" },
    {   "date": 2018.416, "keywords": ["Earth"], "era": "modern",
        "initial": "Earth gate discovered.",
        "text": "" },
    {   "date": 2018.754, "keywords": ["Earth"], "era": "modern",
        "initial": "Earth gate opened.",
        "text": "First contact with the Interworld Nexus is achieved." },
    {   "date": 2023.400, "keywords": ["Earth", "Meta"], "era": "modern",
        "initial": "Current year.",
        "text": "" },
];
