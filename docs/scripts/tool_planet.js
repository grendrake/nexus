
function clearError() {
    const e = document.getElementById("error-message");
    e.style.display = "none";
    e.innerHTML = "";
}
function showError(message) {
    const e = document.getElementById("error-message");
    if (!e) alert(message);
    e.style.display = "block";
    if (e.innerHTML != "") e.innerHTML += "<br>";
    e.innerHTML += message;
}

const starData = [
    {   "mass": 0.10, "type": "M7", "temp":3100, "lmin": 0.0012 },
    {   "mass": 0.15, "type": "M6", "temp":3200, "lmin": 0.0036 },
    {   "mass": 0.20, "type": "M5", "temp":3200, "lmin": 0.0079 },
    {   "mass": 0.25, "type": "M4", "temp":3300, "lmin": 0.015  },
    {   "mass": 0.30, "type": "M4", "temp":3300, "lmin": 0.024  },
    {   "mass": 0.35, "type": "M3", "temp":3400, "lmin": 0.037  },
    {   "mass": 0.40, "type": "M2", "temp":3500, "lmin": 0.054  },
    {   "mass": 0.45, "type": "M1", "temp":3600, "lmin": 0.07,  "lmax": 0.08,   "mspan": 70 },
    {   "mass": 0.50, "type": "M0", "temp":3800, "lmin": 0.09,  "lmax": 0.11,   "mspan": 59 },
    {   "mass": 0.55, "type": "K8", "temp":4000, "lmin": 0.11,  "lmax": 0.15,   "mspan": 50 },
    {   "mass": 0.60, "type": "K6", "temp":4200, "lmin": 0.13,  "lmax": 0.2,    "mspan": 42 },
    {   "mass": 0.65, "type": "K5", "temp":4400, "lmin": 0.15,  "lmax": 0.25,   "mspan": 37 },
    {   "mass": 0.70, "type": "K4", "temp":4600, "lmin": 0.19,  "lmax": 0.35,   "mspan": 30 },
    {   "mass": 0.75, "type": "K2", "temp":4900, "lmin": 0.23,  "lmax": 0.48,   "mspan": 24 },
    {   "mass": 0.80, "type": "K0", "temp":5200, "lmin": 0.28,  "lmax": 0.65,   "mspan": 20 },
    {   "mass": 0.85, "type": "G8", "temp":5400, "lmin": 0.36,  "lmax": 0.84,   "mspan": 17 },
    {   "mass": 0.90, "type": "G6", "temp":5500, "lmin": 0.45,  "lmax": 1,      "mspan": 14 },
    {   "mass": 0.95, "type": "G4", "temp":5700, "lmin": 0.56,  "lmax": 1.3,    "mspan": 12,    "sspan": 1.8, "gspan": 1.1 },
    {   "mass": 1.00, "type": "G2", "temp":5800, "lmin": 0.68,  "lmax": 1.6,    "mspan": 10,    "sspan": 1.6, "gspan": 1   },
    {   "mass": 1.05, "type": "G1", "temp":5900, "lmin": 0.87,  "lmax": 1.9,    "mspan": 8.8,   "sspan": 1.4, "gspan": 0.8 },
    {   "mass": 1.10, "type": "G0", "temp":6000, "lmin": 1.1,   "lmax": 2.2,    "mspan": 7.7,   "sspan": 1.2, "gspan": 0.7 },
    {   "mass": 1.15, "type": "F9", "temp":6100, "lmin": 1.4,   "lmax": 2.6,    "mspan": 6.7,   "sspan": 1,   "gspan": 0.6 },
    {   "mass": 1.20, "type": "F8", "temp":6300, "lmin": 1.7,   "lmax": 3,      "mspan": 5.9,   "sspan": 0.9, "gspan": 0.6 },
    {   "mass": 1.30, "type": "F6", "temp":6500, "lmin": 2.5,   "lmax": 3.9,    "mspan": 4.6,   "sspan": 0.7, "gspan": 0.4 },
    {   "mass": 1.25, "type": "F7", "temp":6400, "lmin": 2.1,   "lmax": 3.5,    "mspan": 5.2,   "sspan": 0.8, "gspan": 0.5 },
    {   "mass": 1.35, "type": "F5", "temp":6600, "lmin": 3.1,   "lmax": 4.5,    "mspan": 4.1,   "sspan": 0.6, "gspan": 0.4 },
    {   "mass": 1.40, "type": "F4", "temp":6700, "lmin": 3.7,   "lmax": 5.1,    "mspan": 3.7,   "sspan": 0.6, "gspan": 0.4 },
    {   "mass": 1.45, "type": "F3", "temp":6900, "lmin": 4.3,   "lmax": 5.7,    "mspan": 3.3,   "sspan": 0.5, "gspan": 0.3 },
    {   "mass": 1.50, "type": "F2", "temp":7000, "lmin": 5.1,   "lmax": 6.5,    "mspan": 3,     "sspan": 0.5, "gspan": 0.3 },
    {   "mass": 1.60, "type": "F0", "temp":7300, "lmin": 6.7,   "lmax": 8.2,    "mspan": 2.5,   "sspan": 0.4, "gspan": 0.2 },
    {   "mass": 1.70, "type": "A9", "temp":7500, "lmin": 8.6,   "lmax": 10,     "mspan": 2.1,   "sspan": 0.3, "gspan": 0.2 },
    {   "mass": 1.80, "type": "A7", "temp":7800, "lmin": 11,    "lmax": 13,     "mspan": 1.8,   "sspan": 0.3, "gspan": 0.2 },
    {   "mass": 1.90, "type": "A6", "temp":8000, "lmin": 13,    "lmax": 16,     "mspan": 1.5,   "sspan": 0.2, "gspan": 0.1 },
    {   "mass": 2.00, "type": "A5", "temp":8200, "lmin": 16,    "lmax": 20,     "mspan": 1.3,   "sspan": 0.2, "gspan": 0.1 },
];

function doStarUpdate() {
    clearError();

    let systemAge = 0, stellarMass = 0;
    try {
        systemAge = new Big(document.getElementById("systemAge").value);
        stellarMass = new Big(document.getElementById("stellarMass").value);
    } catch (err) {
        showError("Star: invalid input value.");
        return;
    }

    var data = undefined;
    for (var i = 0; i < starData.length; ++i) {
        if (stellarMass.eq(starData[i].mass)) {
            data = starData[i];
        }
    }
    if (data === undefined) {
        showError("Unknown stellar mass: " + stellarMass.toFixed(3));
        document.getElementById("starType").innerHTML = "Unknown";
        return;
    }

    var subtype = undefined;
    if (!data.hasOwnProperty("mspan") || systemAge < data.mspan) {
        subtype = "V";
    } else if (systemAge < data.sspan + data.mspan) {
        subtype = "IV";
    } else if (systemAge < data.sspan + data.mspan + data.gspan) {
        subtype = "III";
    } else {
        subtype = "D";
    }

    var lumin = Big(0);
    var temperature = "NA";
    var radius = "NA";

    if (subtype === "V") {
        if (data.hasOwnProperty("mspan")) {
            lumin = systemAge.div(data.mspan).times(data.lmax - data.lmin).plus(data.lmin);
         } else {
            lumin = Big(data.lmin);
         }
        temperature = Big(data.temp);
    } else if (subtype === "D") {
        stellarMass = Big(0);
        temperature = stellarMass;
        lumin = Big(0.001);
    }

    if (subtype !== "D") {
        radius = lumin.sqrt().times(155000).div(temperature.pow(2));
    } else {
        radius = Big(0);
    }

    const innerLimit1 = stellarMass.times(0.1);
    const innerLimit2 = lumin.sqrt().times(0.01);
    var innerLimit = innerLimit1;
    if (innerLimit2.gt(innerLimit1)) {
        innerLimit = innerLimit2;
    }
    const outerLimit = stellarMass.times(40);
    const snowLine = lumin.sqrt().times(4.85);

    document.getElementById("starType").innerHTML = data.type;
    document.getElementById("starSubtype").innerHTML = subtype;
    document.getElementById("stellarLum").innerHTML = lumin.toFixed(3);
    document.getElementById("stellarTemp").innerHTML = temperature;
    document.getElementById("stellarRadius").innerHTML = radius.toFixed(4);
    document.getElementById("innerLimit").innerHTML = innerLimit.toFixed(3);
    document.getElementById("outerLimit").innerHTML = outerLimit.toFixed(3);
    document.getElementById("snowLine").innerHTML = snowLine.toFixed(3);

    doOrbitUpdate(0);
    doPlanetUpdate();
}

function clearOrbits() {
    for (var i = 1; i <= 15; ++i) {
        document.getElementById("orbit"+i).value = "";
    }
}
function doOrbitUpdate(orbitNo) {
    clearError();
    let starLum = 0;
    try {
        starLum = Big(document.getElementById("stellarLum").innerHTML);
    } catch (err) {
        showError("Orbits: invalid stellar luminosity");
        return;
    }

    for (var i = 1; i <= 15; ++i) {
        const fromOrbit = i > 1 && document.getElementById("orbit"+(i-1)).value !== ""
            ? Big(document.getElementById("orbit"+(i-1)).value)
            : "";
        const toOrbit = document.getElementById("orbit"+i).value !== ""
            ? Big(document.getElementById("orbit"+i).value)
            : "";

        if (toOrbit === "" || toOrbit == 0) {
            document.getElementById("orbit"+i+"blackbody").innerHTML = "";
        } else {
            const blackbody = starLum.sqrt().sqrt().times(278).div(toOrbit.sqrt());
            document.getElementById("orbit"+i+"blackbody").innerHTML = blackbody.toFixed(3);
        }

        if (fromOrbit === "" || toOrbit === "") {
            document.getElementById("orbit"+i+"ratio").innerHTML = "";
        } else {
            const ratio = toOrbit.div(fromOrbit);
            const eRatio = document.getElementById("orbit"+i+"ratio");
            eRatio.innerHTML = ratio.toFixed(3);
            if (ratio < 1.4 || ratio > 2.0) eRatio.classList.add("highlight-error");
            else                            eRatio.classList.remove("highlight-error");
        }
    }
}

const typeData = {
    "asteroid belt":        { "absorp": 0.97, "greenhouse": 0,    "pf": "Vacuum", "atmosphere": "Trace" },
    "tiny ice":             { "absorp": 0.86, "greenhouse": 0,    "pf": "Vacuum", "atmosphere": "Trace" },
    "tiny rock":            { "absorp": 0.97, "greenhouse": 0,    "pf": "Vacuum", "atmosphere": "Trace" },
    "tiny sulfur":          { "absorp": 0.77, "greenhouse": 0,    "pf": "Vacuum", "atmosphere": "Trace" },
    "small hadean":         { "absorp": 0.67, "greenhouse": 0,    "pf": "Vacuum", "atmosphere": "Trace" },
    "small ice":            { "absorp": 0.93, "greenhouse": 0.1,  "pf": 10,       "atmosphere": "95% of Suffocating and Mildly Toxic, otherwise Suffocating and Highly Toxic" },
    "small rock":           { "absorp": 0.96, "greenhouse": 0,    "pf": "Trace",  "atmosphere": "Trace" },
    "standard hadean":      { "absorp": 0.67, "greenhouse": 0,    "pf": "Vacuum", "atmosphere": "Trace" },
    "standard ammonia":     { "absorp": 0.84, "greenhouse": 0.2,  "pf": 1,        "atmosphere": "Suffocating, Lethally Toxic, Corrosive" },
    "standard ice":         { "absorp": 0.86, "greenhouse": 0.20, "pf": 1,        "atmosphere": "74% of Suffocating, otherwise Suffocating and Mildly Toxic" },
    "standard ocean":       { "absorp": -1,   "greenhouse": 0.16, "pf": 1,        "atmosphere": "74% of Suffocating, otherwise Suffocating and Mildly Toxic" },
    "standard garden":      { "absorp": -1,   "greenhouse": 0.16, "pf": 1,        "atmosphere": "Breathable; 38% of being Marginal" },
    "standard greenhouse":  { "absorp": 0.77, "greenhouse": 2,    "pf": 100,      "atmosphere": "Suffocating, Lethally Toxic, Corrosive" },
    "standard chthonian":   { "absorp": 0.97, "greenhouse": 0,    "pf": "Trace",  "atmosphere": "Trace" },
    "large ammonia":        { "absorp": 0.84, "greenhouse": 0.2,  "pf": 5,        "atmosphere": "Suffocating, Lethally Toxic, Corrosive" },
    "large ice":            { "absorp": 0.86, "greenhouse": 0.2,  "pf": 5,        "atmosphere": "Suffocating, Highly Toxic" },
    "large ocean":          { "absorp": -1,   "greenhouse": 0.16, "pf": 5,        "atmosphere": "Suffocating, Highly Toxic" },
    "large garden":         { "absorp": -1,   "greenhouse": 0.16, "pf": 5,        "atmosphere": "Breathable; 38% of being Marginal" },
    "large greenhouse":     { "absorp": 0.77, "greenhouse": 2,    "pf": 500,      "atmosphere": "Suffocating, Lethally Toxic, Corrosive" },
    "large chthonian":      { "absorp": 0.97, "greenhouse": 0.00, "pf": "Trace",  "atmosphere": "Trace" }
};
const gardenAbsorp = [
    [ 20, 0.95 ],
    [ 50, 0.92 ],
    [ 90, 0.88 ],
    [ 101, 0.84 ]
];
const sizeConstraints = {
    "large":    { "min": 0.065, "max": 0.091 },
    "standard": { "min": 0.030, "max": 0.065 },
    "small":    { "min": 0.024, "max": 0.030 },
    "tiny":     { "min": 0.004, "max": 0.024 },
};
const pressureNames = [
    { "upto": 0.50, "name": "very thin" },
    { "upto": 0.80, "name": "thin" },
    { "upto": 1.20, "name": "standard" },
    { "upto": 1.50, "name": "dense" },
    { "upto": 9999, "name": "very dense" },
];
function getPressureName(pressure) {
    for (var i = 0; i < pressureNames.length; ++i) {
        if (pressureNames[i].upto > pressure) {
            return pressureNames[i].name
        }
    }
    return pressureNames[pressureNames.length-1].name;
}
const numberWithCommas = (x) => {
  var parts = x.toString().split(".");
  parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return parts.join(".");
}

const earthDiameterKM = 12756;
const earthDiameterMI = 7926;
const earthDensityGCC = 5.52;
const kelvinToCelsiusDifference = 273.15;
const PI4 = 4 * Math.PI;

function getAbsorp(planetData) {
    const hydro = new Big(document.getElementById("hydro").value);
    const data = planetData;
    var absorp = 99999;
    if (data.absorp === -1) {
        for (var i = 0; i < gardenAbsorp.length; ++i) {
            if (hydro <= gardenAbsorp[i][0]) {
                absorp = gardenAbsorp[i][1];
                break;
            }
        }
    } else {
        absorp = data.absorp;
    }
    return absorp;
}

function doPlanetUpdateSurf() {
    clearError();
    const mySize = document.getElementById("planetSize").value;
    const myType = document.getElementById("planetType").value;
    const typeId = myType === "asteroid belt"
        ? myType
        : ( mySize + " " + myType );
    if (!typeData.hasOwnProperty(typeId)) {
        showError("Invalid planet type.");
        return;
    }
    const data = typeData[typeId];
    const absorp = getAbsorp(data);
    const atmoMass = new Big(document.getElementById("atmoMass").value);
    const bbcor = atmoMass.times(data.greenhouse).plus(1).times(absorp);

    const surfTemp = new Big(document.getElementById("surfTemp").value);
    const blackbodyTemp = surfTemp.div(bbcor);
    document.getElementById("blackbodyTemp").value = blackbodyTemp.toFixed(3);
}
function doPlanetUpdateBlackbody() {
    clearError();
    const mySize = document.getElementById("planetSize").value;
    const myType = document.getElementById("planetType").value;
    const typeId = myType === "asteroid belt"
        ? myType
        : ( mySize + " " + myType );
    if (!typeData.hasOwnProperty(typeId)) {
        showError("Invalid planet type.");
        return;
    }
    const data = typeData[typeId];
    const absorp = getAbsorp(data);
    const atmoMass = new Big(document.getElementById("atmoMass").value);
    const bbcor = atmoMass.times(data.greenhouse).plus(1).times(absorp);

    const blackbodyTemp = new Big(document.getElementById("blackbodyTemp").value);
    const surfTemp = blackbodyTemp.times(bbcor);
    document.getElementById("surfTemp").value = surfTemp.toFixed(3);
}

function doPlanetUpdate() {
    clearError();
    const mySize = document.getElementById("planetSize").value;
    const myType = document.getElementById("planetType").value;
    const typeId = myType === "asteroid belt"
        ? myType
        : ( mySize + " " + myType );

    if (!typeData.hasOwnProperty(typeId)) {
        showError("Invalid planet type.");
        return;
    }
    const data = typeData[typeId];

    let starMass = 0, starLum = 0, atmoMass = 0, surfTemp = 0, blackbodyTemp = 0;
    let hydro = 0, density = 0, gravity = 0, rotationPeriod = 0;
    try {
        starMass = new Big(document.getElementById("stellarMass").value);
        starLum = new Big(document.getElementById("stellarLum").textContent);
        atmoMass = new Big(document.getElementById("atmoMass").value);
        surfTemp = new Big(document.getElementById("surfTemp").value);
        blackbodyTemp = new Big(document.getElementById("blackbodyTemp").value);
        hydro = new Big(document.getElementById("hydro").value);
        density = new Big(document.getElementById("density").value);
        gravity = new Big(document.getElementById("gravity").value);
        rotationPeriod = new Big(document.getElementById("rotationPeriod").value);
    } catch (err) {
        showError("Planet: input value error");
        return;
    }

    const hydroPer = hydro.div(100);
    const diameter = gravity.div(density);
    const radiusKM = diameter.times(earthDiameterKM).div(2);
    const radiusMI = diameter.times(earthDiameterMI).div(2);
    const mass = density.times(diameter.pow(3));
    var atmoPressure = "";
    var pressureNum = undefined;
    if (Number.isInteger(data.pf)) {
        pressureNum = atmoMass.times(gravity).times(data.pf).toFixed(3);
        atmoPressure = pressureNum + " atm"
        atmoPressureName = " (" + getPressureName(pressureNum) +")";
    } else {
        atmoPressure = data.pf;
        atmoPressureName = "";
    }
    const orbitalRadius = (Big(77300).div(blackbodyTemp.pow(2))).times(starLum.sqrt());
    const orbitalPeriod = orbitalRadius.pow(3).div(starMass).sqrt();
    const orbitalPeriodEarthDays = orbitalPeriod.times(365.26);
    const escapeVelocity = diameter != 0 ? mass.div(diameter).sqrt().times(11.186) : 0;
    const localDay = rotationPeriod.div(24).times(orbitalPeriodEarthDays).div(
                       orbitalPeriodEarthDays.minus(rotationPeriod.div(24))
                     ).times(24);
    const orbitalPeriodLocalDays = orbitalPeriodEarthDays.times(24).div(localDay);
    const circumferenceKM = diameter.times(earthDiameterKM).div(2).times(Math.PI).times(2);
    const circumferenceMI = diameter.times(earthDiameterMI).div(2).times(Math.PI).times(2);
    const gravMin = blackbodyTemp.times(density).sqrt().times(sizeConstraints[mySize].min);
    const gravMax = blackbodyTemp.times(density).sqrt().times(sizeConstraints[mySize].max);
    const volumeKM = Big(4 / 3).times(Math.PI).times(radiusKM.pow(3));
    const volumeMI = Big(4 / 3).times(Math.PI).times(radiusMI.pow(3));

    document.getElementById("orbitalRadius").innerHTML = orbitalRadius.toFixed(3);
    document.getElementById("orbitalPeriodXEarth").innerHTML = orbitalPeriod.toFixed(3);
    document.getElementById("orbitalPeriodEarthDays").innerHTML = numberWithCommas(orbitalPeriodEarthDays.toFixed(3));
    document.getElementById("orbitalPeriodLocalDays").innerHTML = numberWithCommas(orbitalPeriodLocalDays.toFixed(3));
    document.getElementById("atmosphere").innerHTML = data.atmosphere;
    document.getElementById("surfTempC").innerHTML = surfTemp.minus(kelvinToCelsiusDifference).toFixed(3);
    document.getElementById("densityGCC").innerHTML = density.times(earthDensityGCC).toFixed(3);
    document.getElementById("gravRange").innerHTML = "(" + gravMin.toFixed(3) + " &mdash; " + gravMax.toFixed(3) + ")";
    document.getElementById("diameter").innerHTML = numberWithCommas(diameter.toFixed(3));
    document.getElementById("diameterKM").innerHTML = numberWithCommas(diameter.times(earthDiameterKM).toFixed(3));
    document.getElementById("diameterMI").innerHTML = numberWithCommas(diameter.times(earthDiameterMI).toFixed(3));
    document.getElementById("radiusKM").innerHTML = numberWithCommas(radiusKM.toFixed(3));
    document.getElementById("radiusMI").innerHTML = numberWithCommas(radiusMI.toFixed(3));
    document.getElementById("areaKM").innerHTML = numberWithCommas(radiusKM.times(radiusKM).times(PI4).toFixed(3));
    document.getElementById("areaMI").innerHTML = numberWithCommas(radiusMI.times(radiusMI).times(PI4).toFixed(3));
    document.getElementById("waterAreaKM").innerHTML = numberWithCommas(radiusKM.times(radiusKM).times(PI4).times(hydroPer).toFixed(3));
    document.getElementById("waterAreaMI").innerHTML = numberWithCommas(radiusMI.times(radiusMI).times(PI4).times(hydroPer).toFixed(3));
    document.getElementById("landAreaKM").innerHTML = numberWithCommas(radiusKM.times(radiusKM).times(PI4).times(1 - hydroPer).toFixed(3));
    document.getElementById("landAreaMI").innerHTML = numberWithCommas(radiusMI.times(radiusMI).times(PI4).times(1 - hydroPer).toFixed(3));
    document.getElementById("mass").innerHTML = mass.toFixed(3);
    document.getElementById("atmoPressure").innerHTML = atmoPressure;
    document.getElementById("atmoPressureName").innerHTML = atmoPressureName;
    document.getElementById("localDay").innerHTML = localDay.toFixed(3);
    document.getElementById("escapeVelocity").innerHTML = escapeVelocity.toFixed(3);
    document.getElementById("circumferenceKM").innerHTML = numberWithCommas(circumferenceKM.toFixed(3));
    document.getElementById("circumferenceMI").innerHTML = numberWithCommas(circumferenceMI.toFixed(3));
    document.getElementById("volumeKM").innerHTML = numberWithCommas(volumeKM.toFixed(3));
    document.getElementById("volumeMI").innerHTML = numberWithCommas(volumeMI.toFixed(3));

    if (pressureNum == undefined) {
        document.getElementById('relativePressureTable').innerHTML = "";
    } else {
        var pressureTable = "<div id='pressureTableHead'>Relative Pressure Table</div><table>";
        var lastValue = -1;
        for (var i = 0; i < pressureNames.length; ++i) {
            const row = pressureNames[i];

            var work = "<tr><th>" + row.name + "</th><td>";
            if (lastValue < 0)  work += "Up to";
            else                work += Math.round( (lastValue + 0.01) * pressureNum * 100 ) / 100;
            work += "</td><td>";
            if (row.upto > 9000)    work += "and up";
            else                    work += Math.round( row.upto * pressureNum * 100 ) / 100;
            pressureTable += work + "</td></tr>";
            lastValue = row.upto;
        }
        document.getElementById('relativePressureTable').innerHTML = pressureTable + "</table>";
    }

    doTidesUpdate();
}

function doMoonUpdate() {
    clearError();
    let moonOrbitDiameters = 0, moonMass = 0, planetDiameter = 0, localDay = 0;
    let moonDiameter = 0;
    try {
        moonOrbitDiameters = new Big(document.getElementById("moonOrbitDiameters").value);
        planetMass = new Big(document.getElementById("mass").textContent);
        moonMass = new Big(document.getElementById("moonMass").value);
        moonDiameter = new Big(document.getElementById("moonDiameter").value);
        planetDiameter = new Big(document.getElementById("diameter").textContent.replace(/,/, ""));
        planetDiameterKM = new Big(document.getElementById("diameterKM").textContent.replace(/,/, ""));
        localDay = new Big(document.getElementById("localDay").textContent);
    } catch (err) {
        showError("Moon: input value error");
        return;
    }

    const moonOrbitKM = moonOrbitDiameters.times(planetDiameterKM);
    const moonOrbitEarthDiameters = moonOrbitKM.div(earthDiameterKM);
    const orbitalPeriod = moonOrbitEarthDiameters.pow(3).div(moonMass.plus(planetMass)).sqrt().times(0.166);
    const orbitalPeriodLocalDays = orbitalPeriod.times(24).div(localDay);
    const tidalForceOnPlanet = moonMass.times(planetDiameter).times(2230000).div(moonOrbitEarthDiameters.pow(3));
    const tidalForceFromPlanet = planetMass.times(moonDiameter).times(2230000).div(moonOrbitEarthDiameters.pow(3));

    document.getElementById("moonOrbit").textContent = numberWithCommas(moonOrbitKM.toFixed(3));
    document.getElementById("moonOrbitEarthDiameters").textContent = numberWithCommas(moonOrbitEarthDiameters.toFixed(3));
    document.getElementById("moonOrbitalPeriodEarthDays").textContent = numberWithCommas(orbitalPeriod.toFixed(3));
    document.getElementById("moonOrbitalPeriodLocalDays").textContent = numberWithCommas(orbitalPeriodLocalDays.toFixed(3));
    document.getElementById("tidalForceOnPlanet").textContent = numberWithCommas(tidalForceOnPlanet.toFixed(3));
    document.getElementById("tidalForceFromPlanet").textContent = numberWithCommas(tidalForceFromPlanet.toFixed(3));
}

function doTidesUpdate() {
    clearError();
    let moonOrbitDiameters = 0, moonMass = 0, planetDiameter = 0, localDay = 0;
    let moonDiameter = 0;
    try {
        starMass = new Big(document.getElementById("stellarMass").value);
        starAge = new Big(document.getElementById("systemAge").value);
        planetDiameter = new Big(document.getElementById("diameter").textContent.replace(/,/, ""));
        planetMass = new Big(document.getElementById("mass").textContent.replace(/,/, ""));
        planetOrbitAU = new Big(document.getElementById("orbitalRadius").textContent.replace(/,/, ""));
        moon1tides = new Big(document.getElementById("moon1tides").value);
        moon2tides = new Big(document.getElementById("moon2tides").value);
    } catch (err) {
        showError("Tides: input value error");
        return;
    }

    const solarTide = starMass.times(planetDiameter).times(0.46).div(planetOrbitAU.pow(3));
    const totalTide = moon1tides.plus(moon2tides).plus(solarTide).times(starAge).div(planetMass);

    document.getElementById("solarTide").textContent = numberWithCommas(solarTide.toFixed(3));
    document.getElementById("totalTidal").textContent = numberWithCommas(Math.floor(totalTide));
}

doStarUpdate();
doOrbitUpdate();
doPlanetUpdateSurf();
doPlanetUpdate();
doMoonUpdate();
doTidesUpdate();

