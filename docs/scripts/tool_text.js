const maxLetterSize = 4;
let currentLanguage = undefined;
function selectLanguage() {
    const langSelect = document.getElementById("langSelect");
    const output = document.getElementById("output");
    const demo = document.getElementById("demo");
    const encTable = document.getElementById("transTable");
    const textDir = document.getElementById("textDir");

    if (currentLanguage) {
        demo.classList.remove(languages[currentLanguage].style);
        demo.innerHTML = "";
        output.value = "";
    }
    const option = langSelect.selectedOptions[0];
    currentLanguage = option.id;
    demo.classList.add(languages[currentLanguage].style);
    if (languages[currentLanguage].direction !== "") {
        textDir.innerText = " (reads "
                    + languages[currentLanguage].direction
                    + ")";
    } else {
        textDir.innerText = "";
    }

    while (encTable.childElementCount > 0) {
        encTable.removeChild(encTable.firstChild);
    }
    let cRow = false;
    const encoding = languages[currentLanguage].encoding;
    const encData = languages[currentLanguage].display;
    let index = 0;
    encData.forEach(function (letter) {
        if (!cRow || (index > 0 && index % 8 === 0)) {
            cRow = document.createElement("tr");
            encTable.appendChild(cRow);
        }
        const newHead = document.createElement("th");
        newHead.innerText = letter;
        cRow.appendChild(newHead);


        const newCell = document.createElement("td");
        const newBDO = document.createElement("bdo");
        newBDO.classList.add("lang_demo");
        newBDO.classList.add(languages[currentLanguage].style);
        newBDO.innerText = String.fromCodePoint(0xE000 + encoding[letter]);
        newCell.appendChild(newBDO);
        cRow.appendChild(newCell);
        ++index;
    });
}

function translate() {
    "use strict";
    const text = document.getElementById("input").value;
    const output = document.getElementById("output");
    const demo = document.getElementById("demo");
    const incBDO = document.getElementById("includeBDO").checked;
    const table = languages[currentLanguage].encoding;

    const result = [];
    let demoText = "";
    for (let i = 0; i < text.length;) {
        if (text[i] === "~") {
            ++i;
            continue;
        }
        let letterSize = maxLetterSize;
        while (letterSize > 0) {
            let letter = "";
            if (i + letterSize > text.length) {
                --letterSize;
                continue;
            }

            for (let j = 0; j < letterSize; ++j) {
                letter += text[i + j];
            }

            if (table.hasOwnProperty(letter)) {
                i += letterSize;
                const value = table[letter] + 0xE000;
                result.push("&#x" + value.toString(16) + ";");
                demoText += String.fromCodePoint(value);
                if (letter === " ") {
                    result.push("&#x200B;");
                    demoText += String.fromCodePoint(0x200B);
                }
                break;
            } else {
                --letterSize;
                if (letterSize <= 0) {
                    ++i;
                    const value = 0xFFFD;
                    result.push("&#x" + value.toString(16) + ";");
                    demoText += String.fromCodePoint(value);
                }
            }

        }
        // if (i + 1 < text.length) {
        //     letter = text[i] + text[i + 1];
        // }
        // if (table.hasOwnProperty(letter)) {
        //     ++i;
        //     const value = table[letter] + 0xE000;
        //     result.push("&#x" + value.toString(16) + ";");
        //     demoText += String.fromCodePoint(value);
        //     continue;
        // }

        // letter = text[i];
        // if (table.hasOwnProperty(letter)) {
        //     const value = table[letter] + 0xE000;
        //     result.push("&#x" + value.toString(16) + ";");
        //     demoText += String.fromCodePoint(value);
        // }
    }

    demo.innerHTML = demoText;
    let resultText = undefined;
    if (incBDO) {
        resultText = "<bdo class='nexusFont "
            + languages[currentLanguage].style
            + "'>"
            + result.join("")
            + "</bdo>";

    } else {
        resultText = result.join("");
    }
    output.value = resultText;

}

(function () {
    const langSelect = document.getElementById("langSelect");
    const langList = Object.keys(languages);
    langList.forEach(function (theLang) {
        const newLang = document.createElement("option");
        newLang.id = theLang;
        newLang.innerText = languages[theLang].name;
        langSelect.appendChild(newLang);
    });
    langSelect.selectedIndex = 0;
    selectLanguage();
    const goButton = document.getElementById("goButton");
    goButton.addEventListener("click", translate);
    const input = document.getElementById("input");
    input.addEventListener("keyup", translate);
})();