var language = {
    name: "Ortasa",
    genders: {
        m: "male",
        f: "female",
        n: "neuter"
    },
    partsOfSpeech: {
        adj: "adjective",
        adv: "adverb",
        conj: "conjunction",
        n:   "noun",
        num: "numeral",
        pro: "pronoun",
        v:   "verb",
        art: "article",
        part:"particle",
    },
};

const letters = [ "th", "a", "i", "e", "o", "u", "r", "s", "n", "g", "k", "v", "d", "l", "t", "w", "b", "m" ];

var vocab = [
    {   raw:"ort-asa",          pos:"n",    gender:"n",
        gloss:"ortasa",         defn:"the Ortasa language (this one), used as a common trade language in Serkm Hub, also known as \"low drius\"" },
    {   raw:"ort-ag",           pos:"n",    gender:"n",
        gloss:"ortag",          defn:"the Ortag language spoke by the Kthbra remnants, also known as \"high drius\"" },

    {   raw:"dri-us",           pos:"n",    gender:"n",
        gloss:"drius species",  defn:"the drius species" },
    {   raw:"ak-srith",         pos:"n",    gender:"n",
        gloss:"aksrith",        defn:"species of animal native to ingr; known for their mind influencing defensive measure" },

    {   raw:"engl",             pos:"n",    gender:"n",
        gloss:"engl",           defn:"building material used to construct the hub and gates" },
    {   raw:"nal-wen",          pos:"n",    gender:"n",
        gloss:"nalwen",         defn:"unit of measure equal to 0.8817 metres" },
    {   raw:"vor-sith",         pos:"n",    gender:"n",
        gloss:"vorsyth",        defn:"extinct or otherwise absent aliens that constructed Serkm Hub and the gates" },

    {   raw:"ingr",             pos:"n",    gender:"n",
        gloss:"ingr",           defn:"homeworld of the drius" },
    {   raw:"war-roth-serkm",   pos:"n",    gender:"n",
        gloss:"warrothserkm",   defn:"the world-nation that previously ruled the world of Ingr" },
    {   raw:"kthbra",           pos:"n",    gender:"n",
        gloss:"kthbra clan",    defn:"one of the clans that comprised Warrothserkm" },
    {   raw:"tast",             pos:"n",    gender:"n",
        gloss:"tast clan",      defn:"one of the clans that comprised Warrothserkm" },
    {   raw:"rad-a-ge-os",      pos:"n",    gender:"n",
        gloss:"radageos clan",  defn:"one of the clans that comprised Warrothserkm" },
    {   raw:"ald-tor",          pos:"n",    gender:"n",
        gloss:"aldtor clan",    defn:"one of the clans that comprised Warrothserkm" },
];

/*
rawnt
sivsr
arnnewdatha (arnn-ewd-atha)
rkanas (rka-nas)
atht (atht)
gavsk (gavsk)
*/