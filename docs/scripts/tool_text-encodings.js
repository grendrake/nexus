const languages = {
    "nraezen": {
        "name": "Nraezen",
        "style": "lang_nr",
        "direction": "right-to-left",
        "display": [
            "d",  "t", "g", "k",  "n", "r",
            "th", "z", "j", "l",  "y", "i",
            "a",  "u", "a", "ae", "Nr",
            "0",  "1", "2", "3",  "4", "5",
            ".", "`", "'"
        ],
        "encoding": {
            "d": 1,     "j": 9,
            "t": 2,     "l": 10,
            "g": 3,     "y": 11,
            "k": 4,     "i": 12,
            "n": 5,     "a": 13,
            "r": 6,     "u": 14,
            "th": 7,    "ae": 15,
            "z": 8,     "Nr": 16,

            "0": 65,    "1": 66,
            "2": 67,    "3": 68,
            "4": 69,    "5": 70,

            ".": 71,    ",": 72,
            "`": 73,    "'": 74,
            " ": 0,

            "di": 17,   "da": 18,
            "du": 19,   "dae": 20,
            "ti": 21,   "ta": 22,
            "tu": 23,   "tae": 24,
            "gi": 25,   "ga": 26,
            "gu": 27,   "gae": 28,
            "ki": 29,   "ka": 30,
            "ku": 31,   "kae": 32,
            "ni": 33,   "na": 34,
            "nu": 35,   "nae": 36,
            "ri": 37,   "ra": 38,
            "ru": 39,   "rae": 40,
            "thi": 41,  "tha": 42,
            "thu": 43,  "thae": 44,
            "zi": 45,   "za": 46,
            "zu": 47,   "zae": 48,
            "ji": 49,   "ja": 50,
            "ju": 51,   "jae": 52,
            "li": 53,   "la": 54,
            "lu": 55,   "lae": 56,
            "yi": 57,   "ya": 58,
            "yu": 59,   "yae": 60,
            "Nri": 61,   "Nra": 62,
            "Nru": 63,   "Nrae": 64,
        }
    },


    "jowvon": {
        "name": "Jowvon",
        "style": "lang_jo",
        "direction": "",
        "display": [],
        "encoding": {}
    }
};
