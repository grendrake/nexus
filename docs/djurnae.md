---
category: person
world: kzaknri
parent_name: Kzaknri
parent_addr: kzaknri
notoc: true
---

<div class='infobox'><table>
    <tr><th class='top-header' colspan='2'>Djurnae</th></tr>
    <tr><th>Nraezen</th>        <td><bdo class='nexusFont lang_nr'></bdo></td></tr>
    <tr><th>Species</th>        <td><a href='sakin.html'>Sakin</a></td></tr>
    <tr><th>Position</th>       <td>Heir of <a href='razaznudar.html'>Razaz Nudar</a></td></tr>
    <tr><th>Hatched</th>        <td>2002</td></tr>
    <tr><th>Current Age</th>    <td>17</td></tr>
</table></div>

The current heir apparent of Razaz Nudar is an adolescent youth named Djurnae. He is known to be far more active and involved than [Zdajgae](zdajgae.md) as well as being a prominent duellist and thrill-seeker. Many are concerned about the policy changes that seem likely to come should he ascend to the throne. Others are skeptical that Djurnae’s intentions will matter; Emperor Zdajgae has already outlived two heirs.
