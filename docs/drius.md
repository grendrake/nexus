---
title: Drius
category: sophant
world: ingr
parent_name: Ingr
parent_addr: ingr
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Drius</th></tr>
<tr><th>Homeworld:</th><td><a href='ingr.html'>Ingr</a></td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>6'3" 1.9 m)</td></tr>
<tr><th>Avg. Weight:</th><td>193 lbs (88 kg)</td></tr>
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>1</td></tr>
<tr><th>Gestation:</th><td>10 months</td></tr>
<tr><th>Weaned:</th><td>3 years</td></tr>
<tr><th>Maturity:</th><td>23 years</td></tr>
<tr><th>Lifespan:</th><td>110 years</td></tr>
</table></div>

The drius are an ancient species. Large populations will break into groups of loosely related individuals and these clans will then proceed to try and destroy each other at nearly any cost.

The exact origin of the drius is not known, though it is often believed that they originated elsewhere and that their homeworld was rendered uninhabitable in the aforementioned wars.

Drius live almost exclusively in temperate areas with only a few individuals straying beyond them. They flourish in humid areas with strong, distinct seasons. When in areas that lack this cycle they can become ‘stuck’ in one phase. This can result in uncomfortable feelings and social awkwardness.

Male drius are often far more vain and appearance conscious than female drius; the later tend to be of a more practical mindset.

## Description

<figure class='right' style='width:317px'>
    <img src='/art/species/drius.png' width='317' height='428'>
    <figcaption>
    Male and female drius.<br>
    Art © <a href='https://www.furaffinity.net/user/spelunkersal/'>Spelunker Sal</a>
    </figcaption>
</figure>

The drius have a fur coat that varies with the seasons. During the height of summer it largely disappears, often being visible only on their back and head, while in mid-winter it covers their body in a thick coat, Their hide is typically brown, grey, or tan. Males’ fur is bright orange, red, or yellow (though always a solid colour). In addition, males have a narrow mane that runs from their forehead, ending partway down their tail. Females’ fur is usually the same colour as the underlying hide. Due to these differences, determining the sex of a drius is simple even without checking the more obvious characteristics, such as breasts or genitals, which may not always be visible.

## Diet

During autumn the drius diet consists mostly of meat which they’ll eat in copious amounts, building up a store of insulating fat.

Winter is accompanied by a decrease in physical activity. Their diet will consist of whatever is available, but their appetite is greatly reduced and they primarily survive of the fat they built up in fall.

By the time spring arrives they will be looking quite thin, even starved. As it warms up they will mainly eat fruits and vegetables, supplemented with small amounts of meat and this continues through summer until autumn returns.

Fish and other seafood is consumed year-round where available, but in most cases will not form the core of their diet.

## Reproduction

Females give birth to a single offspring, typically in early spring, and will be infertile for one or two years afterwards. There is a persistent, if slight, trend towards male offspring.

Drius sexuality is strongly seasonal and occurs mostly in spring and early summer; outside of this period they have very little libido and females will not be fertile. This period starts earlier and ends later in males.

As they have induced ovulation, females who have low to moderate amounts of sex will rarely become pregnant. It should be noted that as long as a female has at least one drius partners, the species of the other partners is largely irrelevant.

The drius are unusual as the only sophant species to have a “knot”, causing them to engage in a coital tie.
