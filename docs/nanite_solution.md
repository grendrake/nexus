---
category: general
world: interworld nexus
parent_name: Interworld Nexus
parent_addr: overview
---

The former cross-world empire of [Warrothserkm](warrothserkm.md) developed many medical marvels, but perhaps none so impressive as the nanite solutions.
Unlike most technologies developed by Warrothserkm, nanite solutions were not entirely lost when the empire fell.
The underlying secrets have been lost, preventing new varieties from being created, but ways have been found to "breed" solution to increase pre-existing supplies.
Recovered documentation has provided significant insight into how to use nanite solution, but provides no clues towards unlocking the underlying technologies.

There are several varieties of nanite solution.
The best known allows the species of the subject to be changed.
This may also be used to change the subject's physical sex by "changing" them into their current species.
The second most common variety provides reproductive assistance.
This allows the subject to have children of the target species without changing their species.
The final common variant will make minor, cosmetic changes to the subject.
This is the most complex, and thus expensive, variant while also having the least dramatic effect.

All varieties of solution take the same form: a thick liquid that is light grey when unattuned or pinkish-red when attuned.
Extreme temperatures can cause the solution to spoil.
Regardless of its state, nanite solution is consistently described as tasteless.
If it does, it will turn green; consuming spoiled solution is harmless, but will not have any effect.
A single dose is about 6 ml.
Smaller doses will have no effect while oversized doses will not cause any increased effects.
Nanite solution is typically taken orally but will function regardless of how it's introduced into the body.
It may be safely mixed into food or drink, though the entire dose must be consumed within a short period to take effect.
Due to its inoffensive taste and small dose size, nanite solution is typically taken in its pure form.

In Warrothserkm nanite solution was considered to be sufficiently safe and reliable to be used at home without supervision.
Most modern providers treat its usage as an outpatient procedure performed in their own facilities.
This allows them to monitor the process for any complications or abnormalities.
Most businesses offering nanite solution will also document the patient's change in identity, though similar documentation may be obtained from the [Interworld Governmental Council](serkmhub.md#governmental-council) for a small fee.
Notably, no known form of nanite solution will repair injury or sickness (though changing species will render many diseases non-viable).
Documentation exists for a variant that would serve that purpose, but no samples have been recovered to date.

Except for the reproductive assistance variant, all effects of nanite solution are permanent.
After a dose is consumed, there is a "cool down" period during which additional doses of nanite solution of any variety will have no effect.
The length of this period depends on the specific variety used.

Nanite solution is normally used on sophants, it will also function on animal species.
With a minor exception for the species reassignment variety, the usage and effects of the solution do not change between sophant and animal subjects.

## Attunement

Nanite solution must be attuned to be usable.
Unattuned solution has no effect when consumed.
Multiple doses of solution can be attuned at once and divided into individual doses afterwards.

Attuning a quantity of solution involves exposing it to genetic samples from at least fourteen individuals of the same species.
These individuals can be of either sex, though the sex distribution of the samples may affect the final result.
More than fourteen samples can be used; more variety has been shown to produce better results.
If samples from multiple species are present, the attunement will fail.

The attunement process takes about four hours.
When complete, the solution will turn pinkish-red and be ready for use.
After about eleven days the attunement will fade, but the solution remains viable and can be reattuned.

## Breeding

Unlike most of the lost technologies of Warrothserkm, it is possible to "breed" nanite solution to increase supply.
The minimal quantity required to begin producing nanite solution is a single dose---about 6 ml.
Nanite solution can only be breed while in the unattuned state.

The resources required are not cheap; producing one dose of solution requires a few thousand dollars worth of ingredients.
The starting quantity must be combined with the required resources in complete darkness.
A new dose will be created after about two and a half days.
As long as the requisite resources are present, one dose can be created for each dose in the starting quantity.


## Varieties

### Species Reassignment
Changes the subject's species.
This process takes a couple of hours and has a cool down of about six months---one [Ingr](ingr.md) year.
The subject remains conscious during this process and will experience pleasurable sensations.
Surviving documentation suggests this is a redirection of the actual, far less pleasant sensations caused by radically altering one's body.

The sex of the samples used will determine the sex of the subject's resultant form.
If samples from multiple sexes are used, the resultant sex will be determined randomly with the odds equivalent to the distribution of sexes in the samples.
For instance, if ten female and four male samples are provided, there is a 10 in 14 chance the subject will be female and a 4 in 14 chance they'll be male.
This can be used to change an individual's sex without changing their species by attuning the solution to the subject's current species.
When used this way, the nanites will make only those changes required to performed a full sex change and will be completed in about a third the time.
Subjects using solution attuned to their current species and sex will experience no effect.

The change caused by this variant is total.
The subject will be physically and genetically indistinguishable from a naturally born member of the target species.
Their DNA will be a roughly even mixture of DNA in the attunement samples with the exception that sex-specific chromosomes will be omitted for sexes other than the target sex.
There is one exception to this.
Subjects will gain a non-inheritable genetic "tag" that will link them to their original genetics.
Even if the subject changes species this way multiple times, the tag will always reflect their original identity.
Subjects will be rendered infertile for a period lasting just over a week after which their bodies will follow their new species' normal reproductive cycle.
Their sex drive will not be reduced during this period and many changing into species that are sexually active all year find their sex drive boosted during this period.

The changes do not directly affect the subject's personality.
In particular, sex changes caused by this variant will change neither the subject's sexual identity nor their preferences.
Changes in biological impulses and the user's hormonal balance can result in minor differences.

Sophants transformed into animals retain their mental capacity and mind rather than being reduced to those typical of the animal in question.
Reports exist of individuals changed into animals retaining the ability to speak, but this is not common.
Most subjects become limited to their new species' typical communication abilities.
Experiments involved changing animals into sophant species are inconclusive on whether such "uplifted" individuals gain cognitive capacity.
While animals subjects obtain the physiological ability to speak, they do not gain the required skills do make use of these abilities.


### Reproductive Assistance
This variant of nanite solution is intended to allow individuals of different species to have children in a seemingly natural way.
It affects the gametes of the subject, temporarily replacing them with those of the target species; none of the subject's genetics will be passed on to the resultant offspring.
This will render the subject infertile with their own species for the duration, but this nanite solution is not normally used for that purpose due to the cost.
For species whose sexes are determined genetically, the sex of the resulting gametes is determined the same way as with species reassignment above.
While the child will always be of the target species, the number of offspring will be typical for the mother's species.
If all parents take the solution, it is possible to produce children of a different species than any parent.

This solution takes effect within minutes of being taken.
The effect will last for two to three weeks or, for female subjects, until the end of the subject's reproductive cycle if longer.
Subjects may be under the effect of only one instance of this solution at a time but may take a new dose immediately after it wears off or they give birth if they become pregnant under its effects.

This process is straightforward in males, but females who become pregnant under the solution's effect may experience an additional affect.
The nanite solution will ensure that the mother is capable of bringing the child to term, including enabling them to lay eggs or give live birth as required.
Females belong to species that nurse their offspring will not lactate if the target species does not nurse their young.
Likewise, females that would normally give live birth find themselves ravenous if the target species lays eggs as they need to provide the egg or eggs all the calories required until hatching by the time that it is laid.

The solution has a built-in safety mechanism.
If the offspring would be too large to bring to term or give birth to regardless of any adaptions the solution might make, conception will always fail.
Likewise, the mother will never produce more fetuses than she'd be able to safely bring to term.
The solution has a very little definition of harm, however, and will happily cause mothers to require massively more calories than normal or to have their mobility significantly reduced or eliminated as long as actual physical harm will not occur as a direct result.


### Cosmetic Changes
Often viewed as a form of plastic surgery, this variant allows the subject to gain features not normal for their species.
Examples would be changes changing the subject's horns, ears, tail, or genitals to those of another species.
Perhaps the most common change is changing the subject's skin, giving them fur, scales, etc.
Another common change is creating a satyr-like effect by changing only the user's lower or upper body to another species.
This can create complications if the subject's species and the target species have bodies of significantly different shapes.
Features can be reverted to normal for the subject's species by using solution attuned to their own species.

The changes take between ten and forty minutes depending on the area affected and cause pleasurable sensations in the affected area akin to those caused by species reassignment.
The cooldown for this variant of solution is just over two weeks.

The sex of the samples rarely matters.
If the feature occurs only in one sex of the target species than samples of that sex are required.
Likewise, features that differ between the sexes (common with horns) determine the sex of the feature the same way as with species reassignment.

Changes caused by this solution are purely cosmetic and superficial.
They have are not inheritable and do not affect an individual's reproductive capacity.

This variant is less popular than the other varieties.
A large part of this is that, until the documentation was recovered, it was not known how to control what was changed.
This resulted in random changes and gave the variant a reputation for unreliability that remains despite it being controllable in modern times.
It is also viewed as more of a vanity option than the other variants.
