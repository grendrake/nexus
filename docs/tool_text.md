---
category: tool
---

<p>This tool is intended for use in encoding text into the written form of the various languages used in the Interworld Nexus. As you type, both the visual display and the list of required HTML entities will automatically update. Note that the process is quite primitive and can be very picky about its input. In particular, input is case sensitive. In the event that an input character is not found in the selected language encoding, it will be output using unicode's <a href='https://unicode-table.com/en/FFFD/'>replacement character</a> (&#xFFFD;).

<p>When entering text, the tilde character (~) can be used to prevent adjacent input letters from being combined. For example, with the Nraezen language selected, <b>za</b> becomes the single glyph <bdo class='nexusFont lang_nr'>&#xe02e;</bdo>, but <b>z~a</b> becomes <bdo class='nexusFont lang_nr'>&#xe005;&#xe00d;</bdo>. Also, a <a href='https://en.wikipedia.org/wiki/Zero-width_space'>Zero Width Space</a> will be inserted after every space character to ensure sensible line breaking.

<hr>

<p style='text-indent: 0'><b>Language:</b>
<select id='langSelect' onchange='selectLanguage()'></select>
<label><input type='checkbox' id='includeBDO' checked> Include BDO tag</label>

<p style='text-indent: 0'><b>Output:</b><br>
<textarea id='output' style='width:90%' readonly="true"></textarea><br>
<b>Shows as:</b> <bdo id='demo' class='lang_demo'></bdo> <span id='textDir'></span>

<p style='text-indent: 0'><b>Input:</b><br>
<input type='edit' id='input' style='width:90%'>

<p style='text-indent: 0'><button id='goButton' style='width:90%'>Do it!</button>

<p style='text-indent: 0'><table id='transTable'>
</table><br>

<script src='/scripts/tool_text-encodings.js'></script>
<script src='/scripts/tool_text.js'></script>
