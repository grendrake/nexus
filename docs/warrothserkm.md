---
category: nation
world: ingr
parent_name: Ingr
parent_addr: ingr
---

Warrothserkm was the loose confederation of major [drius](drius.md) clans that acted as a world government on [Ingr](ingr.md).
With the discovery of the gates, the territory of Warrothserkm spread to the Interworld Nexus and even to other worlds.
This ended when the nation collapsed during the civil war known as the *Days of Fire*.
As a result, the clans were left as shattered remnants and the gates to the Interworld Nexus to ceased functioning.

When the nation discovered and opened the Nexus gate, it had no difficulty expanding its reach to other worlds.
The resulting cross-world, multi-species empire is what most think of when they hear "Warrothserkm", but most do not realize the length of the nation's history.
Not only did the nation exist long before the gate was opened, the skill of Warrothserkm bioengineers had already resulted in a significant population of non-drius.
When the gate was first opened, there were already nearly as many non-drius on the planet as drius.
This non-drius population was spread over hundreds, even thousands, of species created by bioengineers.
Most of these species had fewer than a hundred members.

The popular view of Warrothserkm is that of a post-scarcity utopia.
Enough of everything existed that no one is forced to go without or lacked medical care, even the lowliest of citizens had a comfortable life, and everyone was equal under the law.
Few deny the existence of slavery in Warrothserkm, but most believe slaves in Warrothserkm had longer, happier lives than many modern free citizens and that all slaves had willingly entered that state.

The truth of life in Warrothserkm was less saccharine.
While it was nearly post-scarcity, in practice all this meant was the citizens were assured basic shelter, food, and medical care.
Provided housing was small and the food was nutritious but bland, and necessary medical care was provided without question.
Anything beyond these basics needed to be purchased.
Free citizens would receive a basic stipend depending on their status in society.
If they wanted more, they would have to find paying work.

The law in Warrothserkm was mostly equal, but unforgiving.
Each clan's Argus system monitored the clan's territory with systems that covered nearly every inch of a city, including inside personal homes.
When it discovered crimes, it would dispatch enforcers to collect the perpetrators and would itself act as judge and jury.
While minor crimes may have been given fines, major crimes typically resulted in slavery, gladiatorial combat, or direct corporeal punishment.
No sentence was every specifically a death sentence, but the difference was often a matter of technicality.

Slaves in Warrothserkm were granted no rights.
While it was expected that a slave owner would provide shelter, food, and medical care for their slaves, this was a social and practical expectation, not a legal one.
On becoming enslaved, slaves would be sterilized.
As a result, most slaves were those who had sold themselves into slavery, been sentenced to slavery for their crimes, or were prisoners of war.

Most of what has been learned to Warrothserkm has been from recovered media and records or examination of artifacts.
Many artifacts are recovered from the remains of facilities, either on Ingr or other worlds Warrothserkm reached.
These facilities are often protected by devastating, and still functional, automated defences.
Commonly, these defences will avoid targeting drius, making employees of that species popular among those studying Warrothserkm.
While the most promising items are encrypted, even popular media has allowed scholars to learn more about life in Warrothserkm.
Questioning of modern drius has revealed that while nearly all know the general details of Warrothserkm, few know any specifics.
Many drius treat the subject as uncomfortable and avoid the topic.
Scholars looking to hire drius assistants often find they need to over unusually large salaries to overcome this.


## The Days of Fire

While the clans of Warrothserkm lacked neither land nor resources, they commonly engaged each other in violence and warfare.
Most conflicts were fought over insults, ideology, or revenge for past conflicts.
Combat was typically carried out by automated drones.
Not uncommonly, this would be accompanied by raids by drius commandos who would try to capture things or people that would be embarrassing for the target, or that would weaken them.
Prominent figures, entertainers, or leaders were common targets, all of whom would be sold as common slaves after their capture.

This struggles culminated in the civil war known as the Days of Fire.
This name came from both the brevity of the conflict and the awesome power of the weapons involved.
From the time the first shot of fired, the conflict lasted only a few days.
Much of the planet's surface was rendered uninhabitable, most existing facilities were destroyed, and nearly all of the planetary population were killed.
A side-effect of the conflict rendered the Nexus gates inoperable for over a century.

The only population on Ingr known to have survived to the modern day is the [Kthbra Remnants](remnants.md), all of whom descend from the Kthbra clan and all of whom are drius.


## Clans

The federal government of Warrothserkm possessed little power and the clans that it was composed of remained mostly sovereign.
Most clans possessed poor relations with each other and would often engage in open warfare.
Prior to the Days of Fire, this was mostly conducted by automated drones and trained commando units.
Prisoners and captives would be taken as slaves with little hope of returning to their home clan.

Despite this, the variety of clans gave Warrothserkm a wide range of cultures and ideas.
Most clans had one or two areas they specialized in and, despite hostile relations, would work for other clans if the price was right.
As the Nexus gate was located in the territory of clan Kthbra, most Nexus affairs and exploration was handled by Kthbra.
As a result, the modern view of the drius and of Warrothserkm is mostly through the lens of clan Kthbra.

**Aldtor:**
Clan Aldtor was an anomaly amongst Warrothserkm clans.
Many of the seemingly limitless resources of Warrothserkm seem to originate from them and relatively few attacks seem to have been targeted against them.
Perhaps the best explanation is that clan Aldtor looked outwards: they were focused on exploiting space.
It is believed that they were engaged in the automated mining of asteroids and other planets.
Their best known accomplishment was the construction of a space elevator.
During the Days of Fire, the upper anchor of their space elevator was destroyed, causing the space elevator's cable to wrap itself around the planet with great destructive force.
Clan Aldtor is the furthest known clan from the gate and was located on the equator.

<img src='/art/kthbra-logo.svg' width='50' height='70' alt='A partial circle with a vertical line running through the bottom of it.' title="Clan Kthbra Symbol" style='float:right'>
**Kthbra:**
While often thought of as the clan responsible for off-world affairs, this was only true because the Nexus gate happened to be located deep in their territory.
Within Warrothserkm itself, clan Kthbra was known as the master of bioengineering.
The serum used in species reassignment is believed to have been developed by clan Kthbra and surviving records indicate that the design of new species of intelligent life was a standard part of their advanced education.
Most scholars believe that, at the time of the Days of Fire, clan Kthbra was in possession of a wide range of biological weapons.
Trace evidence discovered on [Isaguyat](isaguyat.md) suggests they also had an interest in the sekar.
Much of what is believed to have been clan Kthbra's territory is now underwater.

**Radageos:**
No clan known to modern scholars had nearly as good a reputation as Radageos for their work in the computational fields.
The development of the legendary Argus Systems used by Warrothserkm was entirely the work of clan Radageos.
This development was, in many ways, ironic considered the failures with AI that the clan had experience previously.
A surprising amount of popular entertainment in Warrothserkm, at least in clan Kthbra, suggests that most, if not all, AIs developed by clan Radageos ranged from uncooperative to openly hostile.
Some documentation suggests that a significant portion of attacks suffered by Radageos were by their own rogue AIs and that Radageos had sworn off the development of such systems.
Official documentation for the Argus System indicates that is a "rules-based logic system" rather than an actual AI.

**Tast:**
Warrothserkm was known for its use of energy field technology in general, but it is clan Tast that both pioneered and truly mastered it.
Their energy generation and manipulation in general was far superior to anyone else's short of the [Vorsyth](vorsyth.md) themselves.
Their largest fields were used to protect their clan tower and, while all clans did this to some degree, recovered records suggest that far fewer attacks were able to penetrate these fields than those of other clans.
The believed territory of clan Tast is a single, very large crater; scholars believe that, during the Days of Fire, the quality of clan Tast's defence was countered by the volume of offence.


## Clan Towers and Economy

Each clan was based in a "tower" that was essentially an [arcology](https://en.wikipedia.org/wiki/Arcology).
Around this tower was the clan "village".
The village covered a significant amount of territory but typically had a low population density.
Most residents in the village were low status and non-drius while the drius made up the majority of the population of the tower.
All residents were entitled to a personal home in the village or optionally the tower for drius, at no charge.
These basic residences were minimal, but sufficient for their occupants to be comfortable and had all basic services (e.g. electricity, water, etc.) included.
Larger or more luxurious residences were available at a monthly charge and non-drius could pay to live in the tower itself.

Automated manufacturing located in the lower levels of the tower worked to ensure there would be enough of the basics for all residents of the tower and village; no one in a clan lacked for food or basic care.
Every resident also received a small, regular stipend to allow them to purchase a few luxuries; only those who wanted more in the way of luxuries had any need to look for paying work.

Much of the daily business of life was handled by a supercomputer called an Argus System that operated the services of the tower and village.
This system actively managed the economy, as well as all manufacturing and security.
Residents could actively engage with the system through omnipresent speakers and microphones and the system monitored all activity via CCTV cameras that monitored every space inside the tower and most spaces in the village.
This tracking made identity theft impossible and transactions were typically made by simply telling Argus.
If criminal activity was detected, police squads would be dispatched and, in many areas, energy barriers would cage the perpetrator in place.


## Culture

Life in Warrothserkm revolved around leisure time; paying work was not required to survive.
Few engaged in paying work and those did worked short hours compared to the modern world.
Arts and crafts were common.
Finished items were often given away or sold for a token price.
Other activities included sport, particularly bloodsport like gladiatorial combat, or media consumption such as reading, movies, or interactive virtual reality adventures.

While Warrothserkm culture included non-drius even before contact with other worlds, drius were considered the pinnacle of existence.
Roles in books and movies that did not specifically require being another species were always drius.
Sometimes this resulted in stories set on other worlds that featured no characters native to that world.
While services and manufacturing was made available to all, these were almost exclusively designed with drius in mind and were often not an entirely comfortable fit for other species.

### Gladiatorial Combat

One of the more popular forms of entertainment in Warrothserkm was gladiatorial combat, the bloodier and more violent the better.
Thanks to the presence of advanced medicine, lasting injuries and death were both uncommon.
Events were aimed at a drius audience with drius gladiators always receiving the top billing and equipment, but these were surprisingly popular amongst other species as well.
Participation was the only time that other species were not only allowed, but encouraged to attack the drius and served as an excellent way of releasing resentment and frustration with the drius.
Those who haven't studied Warrothserkm in detail sometimes assume that the gladiators in these combats were slaves or otherwise unwilling, but records suggest that many entered into the arena not only willingly but eagerly.


## Status

Warrothserkm society was very stratified.
Every citizen was assigned a social position.
Service to the clan could raise this position and crimes could reduce it.
At the top were drius officials and military leaders.
At the bottom were off-worlders and slaves.
As Warrothserkm considered the drius to be the ideal, the social status of drius was typically higher than that of non-drius of an otherwise similar station.

### Interspecies Relations

Officially, Warrothserkm welcomed all species into its ranks.
In practice, this welcome was not equal.
If the drius population loved the non-drius, it was a parental love, protective, but limiting.
There was obvious discrimination, such as non-drius being forbidden to own or handle weapons outside the arena, but also less obvious forms as well.
Despite having had a significant non-drius population for centuries, non-drius characters rarely appeared in popular media beyond the occasional token character.

Off-world nations were shown the power and splendour of Warrothserkm, but few possessed the means to acquire any for themselves.
Citizens of off-world nations were officially allowed to buy non-weapon technologies, but only those specifically a citizen of a Warrothserkm clan received a financial stipend, leaving most off-worlders no ability to pay for them.
For many off-worlders, the only way to gain access to the supposedly wonderful society of Warrothserkm was to sell themselves into slavery, an act that would not only get them off world, but provide money for those who remained behind.

Officially, Warrothserkm respected the sovereignty and territory of off-world nations.
In practice, it was not uncommon for highly placed citizens of Warrothserkm to arranged forced buy-outs of land for "necessary" purposes such as government buildings, resource sites (such as mines), or the occasional personal estate.


## Slavery

Despite their technological marvel's, Warrothserkm still practiced slavery.
Slave owners were expected to provide slaves with basic care and necessities, but this was a social and practical expectation, not a legal one.
It was not required that slaves be granted any form of privacy or autonomy.
Individuals could be enslaved for a set period, indefinitely, or permanently.
Any slave without the permanent status could be freed, but this was a rare occurrence.
Most considered it a pointless effort and a waste of time.

Slaves were sterilized on gaining their status.
In the event that they were freed, this process was reversible.
It was strictly forbidden for slaves to have children; in the event that a slave became pregnant, this child would be treated as an orphan, the owner would face criminal charges, and there would be an investigation into the loss of sterility.

Most slaves were those who had sold themselves into slavery, been sentenced to slavery for their crimes, or were prisoners of war.
They typically served as personal servants, test subjects, or for tasks requiring only bulk numbers.
Physical labour was rare; technological progress had rendered most forms of labour obsolete.

Slavery predated off-world contact and, as such, the enslavement of drius was entirely legal.
It was even permitted for non-drius to own drius slaves, though they were typically charged an extra fee for the privilege.

While slavery was explicitly permitted everywhere within Warrothserkm, off-world nations that already had legalized slavery were permitted to maintain their existing systems.
The only exception was that drius were not permitted to be enslaved off-world.
Off-world nations that had outlawed slavery were required to allow it, but without a slave owning culture, slavery remained rare in this nations.
Many would even act to help slaves escape and hide.

Slaves were not the bottom of the social hierarchy.
Those who owned slaves who were not of a recognized sapient species (whether they were born that way or had undergone species reassignment) could apply to have the slave's status changed to "animal".
In theory the slave owner was required to justify the change, but in practice this was a rubber stamp for any slave who was not enslaved for only a set period.
Once approved, the slave's legal status was irreversibly changed to be the same as an animal's, no different from livestock or a pet.
One significant difference between animals and slaves was that breeding animals was permitted; becoming an animal typically involved being unsterilized.
The child of an animal was always an animal, though if someone born as an animal accomplished something sufficiently prestigious they might be granted recognition as a free sophant.


## Technology

At its peak, Warrothserkm achieved technological marvels still unmatched.
Most of this technology was designed to be difficult to reverse engineer and contained booby traps.
Some of the most advanced technology, and especially weapons, is specifically designed to only be usable by drius.
Modern scientists have the additional problem that the infrastructure used to build most Warrothserkm technology no longer exists; manufacturing facilities existed only on Ingr itself and none are known to have survived the Days of Fire.

Artifacts from Warrothserkm consistently maintain a high value.
Those that have ceased functioning often increase in value; it is commonly believed that reverse engineering non-functional artifacts is safer.
Ranged weapons (such as pistols) often have the lowest prices as they are usually secured against being fired by non-drius while also having the best protections against reverse engineering.
Weapons that are not secured often go for the highest prices, but these are quite rare.

The best known technology developed in Warrothserkm is [nanite solution](nanite_solution.md).
This is best known for its ability to change the user's species, but other, related effects are possible as well.
Unlike most technologies, ways have been found to duplicate nanite solution and it is in common use, if expensive.

The best known lost technology was the creation of energy fields.
These served as barriers made of pure energy.
Some records suggest the best could be made selectively permeable.
Unfortunately, no working examples of this technology have been discovered and the underlying principles involved remain entirely unknown.

### Argus Systems

The Argus Systems are supercomputers developed by clan Radageos.
The official description of these translates as "rules-based logic systems" and emphasizes that the systems are not in any way an AI.
Modern scholars almost unanimously agree that these systems were not only AI, but demonstrated behaviour suggestive of free-will and sapience.

Every clan had an Argus System that was responsible for running the day-to-day affairs of the clan.
Everything that occurred was known to the system and it had access to all information and records that classified as top-secret, though some have suggested that even those secrets were given to the Argus System and this just wasn't publicly documented.
In return, the Argus System managed the economy, the judicial system, manufacturing, and all the services required to keep a clan tower running.
While the exact rules given to the systems is unknown and likely varied between clans, recovered documentation suggests most were designed by default to preserve the status quo.
This is supported by the cultural and technological stagnation that took over the nation after clan management had been handed over.
While not commonly known, scholars of Warrothserkm suspect that the Argus System for clan Kthbra is still functioning and that modern drius are capable of interacting with it.

While the Argus Systems are universally described as immense achievements, they are often considered to have been an unfortunate development for Warrothserkm.
Notably, culture and technological development significantly slowed after they have been installed.
The four centuries that followed the implementation of the Argus Systems is often considered to be a golden age for the nation, but none of the technologies associated with Warrothserkm were developed during this time.
Just as significant, once contact with other worlds was established, many of those in positions of power choose to move off-world, despite having to give up access to much of nation's technology.
Living in comparatively primitive conditions appeared to be considered superior to living in the supposedly-utopian clan towers.
