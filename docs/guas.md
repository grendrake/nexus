---
category: sophant
world: ujan
parent_name: Ujan
parent_addr: ujan
notoc: true
---

<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Guas</th></tr>
<tr><th>Homeworld:</th><td>Ujan</td></tr>
<tr><th colspan='2' class='wide-header'>Physical</th></tr>
<tr><th>Avg. Height:</th><td>1.3 m (4’3")</td></tr>
<!-- <tr><th>Avg. Weight:</th><td>96 lbs (43 kg)</td></tr> -->
<tr><th colspan='2' class='wide-header'>Reproductive</th></tr>
<tr><th>Litter size:</th><td>1</td></tr>
<tr><th>Gestation:</th><td>4 months</td></tr>
<tr><th>Incubation:</th><td>4 months</td></tr>
<tr><th>Maturity:</th><td>17 years</td></tr>
<tr><th>Lifespan:</th><td>94 years</td></tr>
</table></div>


## Description

<!-- {% figure /art/species/zaeren.png | 230 | 390 | right | Male zaeren. | | spelunker %} -->


## Diet


## Reproduction
