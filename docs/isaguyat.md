---
category: world
world: isaguyat
notoc: true
---


<div class='infobox'><table>
<tr><th class='top-header' colspan='2'>Isaguyat</th></tr>
<tr><td colspan='2' class='globe'><img src='/art/globe/isaguyat.png'></td></tr>
<tr><th>Stars</th>                  <td>F3V &amp; K6V</td></tr>
<tr><th>Year Length:</th>           <td>129.92 local days<br />700.70 Earth days</td></tr>
<tr><th>Mean Surface Temp.:</th>    <td>288 K (14.85 °C)</td></tr>
<tr><th>Surface Gravity:</th>       <td>0.84 G</td></tr>
<tr><th>Day Length:</th>            <td>129.99 h</td></tr>
<tr><th>Moons</th>                  <td>One</td></tr>
<tr><td colspan='2' class='wide-data'><a href='systems.html#isaguyat'>Isaguyat System</a></td></tr>
</table></div>

Isaguyat is the homeworld of the [sekar](sekar.md). It orbits a close binary pair. The planet itself is tidally locked to its moon and, as a result, experiences very prolonged days and nights. Most of the surface is covered in water, resulting in near-constant cloud cover and frequent, massive storms. The gate is located on the same continent as the sekar; this continent is located in the southern hemisphere.

The planet is often considered something of a death world; the massive oceans breed brutal storms and the local fauna are highly capable, often taking out even armoured drones. Most land area consists of temperate forest teeming animals. Most of the local fauna is highly aggressive and even the flora is notable for its unusual level of activity.

The [Interworld Nexus](overview.md) maintains a small, fortified camp around the gate using military forces and gear supplied by multiple worlds. The area around this camp has been burned clear and much of it has been seeded with landmines to discourage the native fauna from approaching. This force serves to enforce a quarantine on the world; no one is permitted in or out of the rest of the world except small research groups. There is no other permanent off-world presence on the planet.


## Technology

There is no native technological development on Isaguyat; the local population has developed control of fire and stone tools, but nothing more. The export of technology is forbidden and only researchers have been permitted to take equipment outside the camp. Although gear has been occasionally lost, there is no evidence that the local population is aware of it's usage, nor is there sufficient local knowledge or infrastructure to duplicate it.


## Society

The local population lives in groups of one to five adults, plus any associated children. These groups are highly territorial; although individuals and small groups may court each other. Otherwise, or if said courtship fails, they will always engage in semi-ritualistic combat. Injuries, even serious ones, are not uncommon, but combat typically ends with retreat rather than death. It is unthinkable for third parties to join such a conflict or to begin a new conflict immediately after. Any doing so would be despised and outlawed, at least if there where any survivors to complain.

Within a group, all adults are considered to be equals. Children are considered weak and to be protected, but as they become adults they are pushed out of the group and chased off. Children who do not become self-sufficient are considered an embarrassment and tend to be culled by their parent group. Although extremely rare, it is not entirely unheard-of for young adults to remain with their parent group, but this is considered a sign of weakness and an embarrassment to all involved.

Although groups may come together less-violently for special occasions, in regular, day-to-day life groups are fully antagonistic to each other and will not tolerate each other's presence. Communication between groups is often by drums or signal fires, both of which allow communication over large distances.

Although the sekar are omnivores, eating a diet of pure meat is considered a sign of strength and is believed to make one stronger and healthier. Despite this, the sekar are not at the top of the local food chain; a decent amount of the fauna and even some of the flora consider sekar to be edible and predation is not an uncommon cause of death.

### Festivals

There are multiple festival periods throughout the year during which individual groups come together in a more-or-less peaceful fashion. Each typically lasts for one or two days, but due to the length of the local day, these are effectively closer in length to a week long festival.

Because festivals deplete the resources of the area where they are held, hosting one is viewed as a sign of strength. Attacking the group that last held a festival in the general area is considered a sign of weakness is tends to lead to the attacker being attacked in turn.

Festivals are held under a sacred truce that requires all participants to act peacefully towards one another. Groups that cannot abide by these terms are expected to avoid the festival. Individuals caught fighting outside of special festival events, whether attacker or defender, are summarily executed.
